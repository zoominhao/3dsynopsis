#pragma once

#ifndef COMBO_BOX_H
#define COMBO_BOX_H

#include <QComboBox>

class ComboBox : public QComboBox
{
	Q_OBJECT

public:
	ComboBox( const std::vector<QString>& labels, 
		QWidget * parent = 0);

	virtual ~ComboBox();

private:
};

#endif