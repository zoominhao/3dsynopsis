#pragma once
#ifndef RESIZE_HANDLER_H
#define RESIZE_HANDLER_H

#include <osgViewer/ViewerEventHandlers>

class ResizeHandler : public osgGA::GUIEventHandler
{
public:
  ResizeHandler(void);
  ~ResizeHandler(void);

  bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& aa);
};

#endif // ShowAxesHandlers_H