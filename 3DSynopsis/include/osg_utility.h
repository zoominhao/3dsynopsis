#pragma once
#ifndef OSG_UTILITY_H
#define OSG_UTILITY_H

#include <osg/Node>
#include <osg/Array>
#include <osg/ref_ptr>
#include <osgViewer/View>
#include <osg/BoundingBox>
#include <osgViewer/ViewerEventHandlers>

#include "eigen_types.h"


namespace osg{
  class Geode;
  class LineSegment;
}

namespace OSGUtility
{
  osg::Geode* drawCylinder(const osg::LineSegment& axis, double thickness, const osg::Vec4& color);

  osg::Geode* drawCylinder(const osg::Vec3& top, const osg::Vec3& base, double thickness, const osg::Vec4& color);

  osg::Geode* drawCone(const osg::LineSegment& axis, double radius, const osg::Vec4& color);

  osg::Geode* drawCone(const osg::Vec3& top, const osg::Vec3& base, double radius, const osg::Vec4& color);

  osg::Geode* drawTriangle(const osg::Vec3& src, const osg::Vec3& dest, double radius, const osg::Vec4& color);

  osg::Geode* drawVertex(const osg::Vec3& center, double thickness, const osg::Vec4& color);

  osg::Geode* drawSphere(const osg::Vec3& center, double radius, const osg::Vec4& color);

  osg::Geode* drawBox(const osg::Vec3& center, double width, const osg::Vec4& color);

  osg::Geode* drawTetrahedron(const osg::Vec3& center, double radius, const osg::Vec4& color);

  osg::Geode* drawPolygon(osg::Vec3Array* polygon, const osg::Vec4& color);

  osg::Geode* drawBBox(osg::BoundingBox* bbox, const osg::Vec4& color);

  osg::Geode* drawDisk(const osg::Vec2& center, double raduis, const osg::Vec4& color );

  osg::Geode* drawLines(const Eigen::MatrixXd& V, const osg::Vec4& color , double thick);

  osg::Geode* drawLines(const Eigen::MatrixXd& V, const osg::ref_ptr<osg::Vec4Array> colors , double thick);


  osg::UIntArray* generateIndices(size_t partition_size, size_t partition_index, size_t item_num, size_t item_size=1);

  void computeNodePathToRoot(osg::Node& node, osg::NodePath& np);

  template <class T>
  T* computeIntersection(osgViewer::View* view, const osgGA::GUIEventAdapter& ea,
    osgUtil::LineSegmentIntersector::Intersection& intersection, osg::NodePath& nodePath);


  osg::Quat computeQuat( const osg::Vec3& source_dir, const osg::Vec3 target_dir );


  //////////////////////////////////////////////////////////////////////////
  // Eigen �����Ⱦ�ӿ�
  //////////////////////////////////////////////////////////////////////////
  osg::Geode* drawPoints( const EMatXd& V);
  osg::Geode* drawPoints( const EMatXd& V, const osg::Vec4& color);
  osg::Geode* drawPoints( const EMatXd& V, const EMatXd& colors);
  osg::Geode* drawNormals( const EMatXd& V, const EMatXd& N, double len, const osg::Vec4& color );
  osg::Geode* drawGraph( const EMatXd& V, const EMatXi& E, const osg::Vec4& color);
  osg::Geode* drawFaces( const EMatXd& V, const EMatXi& F, double alpha , const EMatXd& color);
  osg::Geode* drawFaces( const EMatXd& V, const EMatXi& F, double alpha , const osg::Vec4 color);
  osg::Geode* drawBBox( const EVecXd v_min, const EVecXd v_max, const osg::Vec4& color);
 
  osg::Vec3Array* convertToOSGArray( const EMatXd& V);

  void setupProperties(osg::ref_ptr<osg::Camera> camera, osg::ref_ptr<osgText::Text> textObject,osgText::Font* font,float size,const osg::Vec3& pos,const osg::Vec4& color );
  void createContent(osg::ref_ptr<osgText::Text> textObject,const char* string);


};

#include "impl/osg_utility.hpp"

#endif // OSG_UTILITY_H
