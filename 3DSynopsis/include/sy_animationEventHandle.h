#pragma once

#include <osgGA/GUIEventHandler>
#include <osg/ref_ptr>


class AnimationEventHandle:public osgGA::GUIEventHandler
{
public:
	AnimationEventHandle(osg::ref_ptr<osg::Node> sceneNode);
	virtual bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& aa);

	//osg::ref_ptr<osg::Node> m_sceneNode;
	osg::Node* m_sceneNode;
};