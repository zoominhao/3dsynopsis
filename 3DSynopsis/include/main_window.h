#pragma once
#ifndef MainWindow_H
#define MainWindow_H


#include <cassert>
#include <QStringList>
#include <QMainWindow>
#include <QTimer>

#include "sy_drive.h"
#include "ui_main_window.h"

class QGroupBox;
class DataSet;
class CheckBox;
class ComboBox;
class ParameterManager;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow();
  void init(void);
  virtual ~MainWindow();
  static MainWindow* getInstance();


signals:
  void checkBoxRenderStateChanged(const std::vector<bool>& states);

public slots:
	// file
  void updateCurrentFile(const QString& filename);
  bool setWorkspace(void);
  std::string getWorkspaceSafe(void) {return workspace_.toStdString();}
  const QString& getWorkspace(void) const {return workspace_;}


  //��������
  void toggleParameterMaster( int mode );
  void loadParameters();
  void saveParameters();
  bool changeParaState();
  void openRecentWorkspace();

  //��Ⱦ
  void sendCheckBoxRenderState(void);
  void updateCheckBoxRenderState(const std::vector<bool>& state);

  //drive simulator
  void SYReset(void);
  void SYStart(void);
  void SYPause(void);
  void SYSpeedUp(void);
  void SYSlowDown(void);
  void SYVideo(void);

protected:
    void dragEnterEvent(QDragEnterEvent *event); 
    void dropEvent(QDropEvent *event); 

protected:
  virtual void closeEvent(QCloseEvent *event);

private:
  void setCheckBox();
  void setParameterBoxColor();
  void setWorkspace(const QString& workspace);
  void createActionRecentWorkspaces();
  void updateRecentWorkspaces();
  void loadSettings();
  void saveSettings();

  void initDataSet();

  

private:
  Ui::MainWindowClass              ui_;
  QString                          workspace_;

  static const int                 MaxRecentWorkspaces = 10;
  QStringList                      recent_workspaces_;
  QAction*                         action_recent_workspaces_[MaxRecentWorkspaces];

private:
  DataSet*                         data_set_;
  std::vector<CheckBox*>           render_states_;

  ComboBox*                        parameter_combo_box_;

  QDockWidget*                     current_parameter_;
  QTimer*                          sysTimer_;

  SyDrive                          m_driver;

};


class MainWindowInstancer
{
public:
	static MainWindowInstancer& getInstance() {
		static MainWindowInstancer theSingleton;
		return theSingleton;
	}

private:
	MainWindowInstancer():main_window_(NULL){}
	MainWindowInstancer(const MainWindowInstancer &) {}            // copy ctor hidden
	MainWindowInstancer& operator=(const MainWindowInstancer &) {return (*this);}   // assign op. hidden
	virtual ~MainWindowInstancer(){}

	friend class MainWindow;
	MainWindow*   main_window_;
};



#endif // MainWindow_H
