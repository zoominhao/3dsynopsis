#pragma once
#ifndef SNAPSHOT_HANDLER_H
#define SNAPSHOT_HANDLER_H

#include <QString>
#include <osgViewer/View>
#include <osgViewer/ViewerEventHandlers>

class WriteToFile : public osgViewer::ScreenCaptureHandler::WriteToFile
{
public:

  WriteToFile(QString& workspace, osgViewer::ScreenCaptureHandler::WriteToFile::SavePolicy savePolicy = SEQUENTIAL_NUMBER);
  virtual ~WriteToFile(void);

  virtual void operator() (const osg::Image& image, const unsigned int context_id);

private:
  QString&    workspace_;
};

class RecordCameraPathHandler : public osgViewer::RecordCameraPathHandler
{
public:
  RecordCameraPathHandler(QString& workspace);
  virtual ~RecordCameraPathHandler(void);

  bool handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa);

private:
  QString&    workspace_;
};

#endif // SNAPSHOT_HANDLER_H