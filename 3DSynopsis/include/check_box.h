#pragma once
#ifndef CHECK_BOX_H
#define CHECK_BOX_H

#include <QCheckBox>

class CheckBox : public QCheckBox
{
  Q_OBJECT

public:
  CheckBox(const QString & text, QWidget * parent = 0);
  virtual ~CheckBox();

protected:
  virtual void mouseReleaseEvent(QMouseEvent * event);

private:
};

#endif // CHECK_BOX_H