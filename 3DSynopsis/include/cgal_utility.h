#pragma once
#ifndef CGAL_UTILITY_H_
#define CGAL_UTILITY_H_

#include "cgal_types.h"


namespace CGALUtility
{
	bool check_inside(CgalPoint2 pt, CgalPoint2 *pgn_begin, CgalPoint2 *pgn_end, Kernel traits);
	bool check_inside(CgalPoint pt, CgalPoint* pgn_begin, CgalPoint* png_end, Kernel traits);
	
};

#endif // CGAL_UTILITY_H_
