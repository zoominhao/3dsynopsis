#pragma once
#ifndef OSG_UTILITY_IMPL_HPP
#define OSG_UTILITY_IMPL_HPP

namespace osg{
  class Geode;
  class LineSegment;
}

namespace OSGUtility
{
  template <class T>
  T* computeIntersection(osgViewer::View* view, const osgGA::GUIEventAdapter& ea,
    osgUtil::LineSegmentIntersector::Intersection& intersection, osg::NodePath& nodePath)
  {
    T* intersectionObject = NULL;

    osgUtil::LineSegmentIntersector::Intersections intersections;

    float x = ea.getX();
    float y = ea.getY();

    if (!view->computeIntersections(x,y,intersections)) {
      return intersectionObject;
    }

    if (intersections.size() == 0) {
      return intersectionObject;
    }

    intersection = *intersections.begin();
    nodePath = intersection.nodePath;

    while (!nodePath.empty()) {
      intersectionObject = dynamic_cast<T*>(nodePath.back());
      if (intersectionObject != NULL) {
        break;
      }
      nodePath.pop_back();
    }

    return intersectionObject;
  }
};

#endif // OSG_UTILITY_IMPL_HPP
