#pragma once

#include <time.h>
#include <string>
#include <iostream>

class SyTimer
{
public:

	void start(const std::string& str)
	{
		std::cout << std::endl;
		starttime = clock();
		mid_start = clock();
		std::cout << "Time Count Start For: " << str << std::endl;

		m_str = str;
	}

	void insert(const std::string& str)
	{
		mid_end = clock();
		timeused = mid_end - mid_start;
		std::cout << "##" << str << "  time used:  " << timeused / double(CLOCKS_PER_SEC) << " seconds." << std::endl;
		mid_start = clock();
	}

	void end()
	{
		stoptime = clock();
		timeused = stoptime - starttime;
		std::cout <<"Finish	" << m_str << "  time used:  " << timeused / double(CLOCKS_PER_SEC) << " seconds." << std::endl;
		std::cout << std::endl;
	}

private:
	int starttime, mid_start, mid_end, stoptime, timeused;
	std::string m_str;
};
