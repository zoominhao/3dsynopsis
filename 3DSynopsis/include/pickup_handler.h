#pragma once
#include "basic_types.h"
#include "sy_manipulator.h"
#include "sy_scene.h"

#include <osgGA/GUIEventHandler>
#include <osgViewer/ViewerEventHandlers>
#include <QPoint>


class BasicSelectModel
{
public:
	BasicSelectModel(osg::Group* root)
	{
		pickup_root_ = new osg::Group;
		root->addChild( pickup_root_ );
		selectedColor_ = osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	void setPointAttributes( osg::Geometry* selector );
	void setLineAttributes( osg::Geometry* selector );
	bool setPointPosition( osg::Geometry* selector, osg::Vec3 pos);
	bool setLinePosition( osg::Geometry* selector, osg::Vec3 start, osg::Vec3 end );
	void setLineAttributes2( osg::Geometry* selector );
	//void addAGeometry( osg::Geometry* selector );

	virtual void createSelector() = 0;

	void addANode( osg::Node* node );

	void setSelectedColor(osg::Vec4 selectedColor) {selectedColor_ = selectedColor;}

protected:
	osg::ref_ptr<osg::Group> pickup_root_;

private:
	osg::Vec4 selectedColor_;

};

class SelectScenePosHandler :  public osgGA::GUIEventHandler, public BasicSelectModel
{
public:
	SelectScenePosHandler( osg::Group* root, osg::Vec3* cur_origin );
	virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
	void createSelector();

protected:
	osg::ref_ptr<osg::Geometry> _origin_geometry;
	osg::Vec3* _origin;

};

class SelectPointHandler :  public osgGA::GUIEventHandler, public BasicSelectModel
{
public:
	SelectPointHandler( osg::Group* root, osg::ref_ptr<SyScene> scene);
	virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa );
	void createSelector();
protected:
	osg::ref_ptr<osg::Geometry> _point_selector;
	osg::ref_ptr<SyScene>       _scene;
	int							_point_id;
	bool                        _visible;
};

class SelectGeometryHandler : public osgGA::GUIEventHandler
{
public:
	SelectGeometryHandler() {}
	virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa );
};

class SyKeyHandler :  public osgGA::GUIEventHandler
{
public:
	SyKeyHandler( osg::Group* root, SynopsisManipulator* pSynopsisManipulator );
	virtual bool handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
private:
	SynopsisManipulator* m_pSynopsisManipulator;
};


