#include <osg/Geometry>
/* -*-c++-*- OpenSceneGraph Cookbook
 * Chapter 10 Recipe 7
 * Author: Wang Rui <wangray84 at gmail dot com>
*/

#ifndef H_COOKBOOK_CH10_PLANEINTERSECTOR
#define H_COOKBOOK_CH10_PLANEINTERSECTOR

#include <osgUtil/LineSegmentIntersector>

class PlaneIntersector : public osgUtil::LineSegmentIntersector
{
public:
    PlaneIntersector();
    PlaneIntersector( const osg::Vec3& start, const osg::Vec3& end );
    PlaneIntersector( CoordinateFrame cf, double x, double y );
    
    void setPickBias( float bias ) { _pickBias = bias; }
    float getPickBias() const { return _pickBias; }
    
    virtual Intersector* clone( osgUtil::IntersectionVisitor& iv );
    virtual void intersect( osgUtil::IntersectionVisitor& iv, osg::Drawable* drawable );
    
protected:
    virtual ~PlaneIntersector() {}
    float _pickBias;
};



////////////////////////////////////////////////////////////////////
class PlaneIntersector2 : public osgUtil::LineSegmentIntersector
{
public:
	PlaneIntersector2();
	PlaneIntersector2( const osg::Vec3& start, const osg::Vec3& end );
	PlaneIntersector2( CoordinateFrame cf, double x, double y );

	void setPickBias( float bias ) { _pickBias = bias; }
	float getPickBias() const { return _pickBias; }

	virtual Intersector* clone( osgUtil::IntersectionVisitor& iv );
	virtual void intersect( osgUtil::IntersectionVisitor& iv, osg::Drawable* drawable );

protected:
	virtual ~PlaneIntersector2() {}
	float _pickBias;
};

#endif
