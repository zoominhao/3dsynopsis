#pragma  once

#include <osg/Vec4>
#include <string>
#include <osgText/Text>


class GConstant
{

public:
	//model
	static std::string C_SCENE_MODEL;                      //场景模型    
	static std::string C_COLOR_MODEL;                      //场景颜色模型
	static std::string C_SCENE_MODEL_EXTENSION;                     //场景模型后缀
	static std::string C_SCENE_SKY;
	static std::string C_COLOR_SKY;
	static std::string C_PORSCHE; 
	static std::string C_CAMERA;     
	static std::string C_VIDEO_WALL;
	///////////water///////////
	static std::string C_SCENE_WATER;
	static std::string C_SCENE_WATER_SZ;
	static std::string C_SCENE_WATER_VERT;
	static std::string C_SCENE_WATER_FRAG;
	static std::string C_SCENE_WATER_IMAGE_POS_X;
	static std::string C_SCENE_WATER_IMAGE_NEG_X;
	static std::string C_SCENE_WATER_IMAGE_POS_Y;
	static std::string C_SCENE_WATER_IMAGE_NEG_Y;
	static std::string C_SCENE_WATER_IMAGE_POS_Z;
	static std::string C_SCENE_WATER_IMAGE_NEG_Z;

	//data
	static std::string C_CAMERA_OP_SVIEW;
	static std::string C_CAMERA_PRE;
	static std::string C_CAMERA_GOOGLE_VIEW;
	static std::string C_CAMERA_GOOGLE_PRE;
	static std::string C_CAMERA_JUMP_VIEW;
	static std::string C_CAMERA_JUMP_PRE;
	static std::string C_OP_SPEED;  
	static std::string C_GOOGLE_SPEED;  
	static std::string C_JUMP_SPEED;  
	static std::string C_IMPORTANCE; 
	static std::string C_LIMPORTANCE;
	static std::string C_OPTICALFLOW;
	static std::string C_OPTICALFLOWACC;

	//images
	static std::string C_SAMPLEVIEW_IMAGES;              //采样点的采样图片
	static std::string C_VIDEO_DIFF_PATH;
	static std::string C_VIDEO_PATH;
	static std::string C_IMAGE_EXTENSION;

	//root config
	static std::string C_SCENE_CAMERA;                   //场景相机视角
	static std::string C_SAMPLE_PATH; 
	static std::string C_SSAMPLE_PATH; 
	static std::string C_SCSAMPLE_PATH; 
	static std::string C_SJSAMPLE_PATH; 
	static std::string C_CONFIG;


	//font
	static osgText::Font* C_FONT_HEI;                           //黑体
	static osgText::Font* C_FONT_KAI;                            //楷体
	static osgText::Font* C_FONT_TIMES;         
};
