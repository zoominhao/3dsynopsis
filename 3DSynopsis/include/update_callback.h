#pragma once
#ifndef UpdateRenderGeometryCallback_H
#define UpdateRenderGeometryCallback_H

#include <osg/NodeCallback>


//////////////////////////////////////////////////////////////////////////
namespace osg {
    class Node;
    class NodeVisitor;
}

class Renderable;

class RenderableUpdateCallback : public osg::NodeCallback
{
public:
    RenderableUpdateCallback(Renderable* renderable);
    virtual ~RenderableUpdateCallback(void);

    virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);

private:
  Renderable*   renderable_;
};   //  end RenderableUpdateCallback






#endif // UpdateRenderGeometryCallback_H