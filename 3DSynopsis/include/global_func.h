#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

#include <Eigen/Core>
#include <Vector>
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Matrix>
#include <string>

#include "cgal_types.h"
#include "eigen_types.h"

namespace GLOBAL
{

	void calPCA( const std::vector<osg::Vec3>& points_3d, std::vector<osg::Vec2>& points_2d);

	double EurlDisPL(CgalPoint sPoint, CgalSegment tSeg);
	double EurlDisPL(CgalPoint2 sPoint, CgalSegment2 tSeg);

	bool delVecDupElem(std::vector<int>& vec);
	bool PtInPolygon(CgalPoint2 pt, CgalPolygon2& poly);

	double calTheta(CgalPoint2 pt1,CgalPoint2 pt2);
	double calTheta(CgalVector2 dir1,CgalVector2 dir2);
	double calTheta(EV2 &dir1,EV2 &dir2);
	double calReTheta(EV2 &dir1,EV2 &dir2);
	
	//��н�
	double calAngle(EV2 &dir1,EV2 &dir2);
	double calAngle(EV3 &dir1,EV3 &dir2);


	double gaussmf( double x, double sig, double c);

	EV4  scalar2color( double scalar );
	void normalize(std::vector<double>& tonorm);
	std::string double2string(double input,int precision);
	std::string int2string(int input);
	bool  strreplace(std::string& input, std::string from, std::string to);
	void laplaceSmooth(std::vector<double> input, std::vector<double>& output, int iterationnum);
	void laplaceSmooth(std::vector<EV3>& input, std::vector<EV3>& output, int iterationnum);
	void laplaceSmooth(std::vector<EV4>& input, std::vector<EV4>& output, int iterationnum);

	//-180~180
	double fixAngle(double deg);
	//0~360
	double fixAngle360(double deg);
};



#endif // GLOBAL_H