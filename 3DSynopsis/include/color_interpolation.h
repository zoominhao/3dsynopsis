#include <QColor>
#include <osg/Vec4>


//////////////////////////////////////////////////////////////////////////
// reference: http://en.wikipedia.org/wiki/HSL_and_HSV
//////////////////////////////////////////////////////////////////////////

enum ColorInterpolationType
{
	HDir = 0,   //hue
	SDIR = 1,   //saturation
	VDIR = 2,   //value
	SVDIR = 3,  //
};

class ColorInterpolation
{

public:
	ColorInterpolation( double v_min, double v_max, ColorInterpolationType type = HDir)
		:v_min_(v_min)
		,v_max_(v_max)
		,type_(type)
	{
	}

	osg::Vec4 getColor( double v) const
	{
		double h = (v-v_min_)/(double)(v_max_-v_min_);
		if( h< 0)
			h=0;
		if( h>1)
			h=1;
		QColor color;
		switch (type_)
		{
		case HDir:
			color.setHsvF( 0.5*h, 1, 1, 0);
			break;
		case SDIR:
			color.setHsvF(1, h, 1, 0);
			break;
		case VDIR:
			color.setHsvF(1,1, h, 0);
			break;
		case SVDIR:
			//color.setHsvF(0.12,1-h, 0.8*h, 0);
			//color.setHsvF(0.0,1-0.8*h, 0.6*h, 0);
			color.setHsvF(0.33*h+0.667,1,0.5, 0);
			break;
		default:
			break;
		}
		return osg::Vec4(color.redF(), color.greenF(), color.blueF(), color.alphaF() );
	}
private:
	double v_min_;
	double v_max_;
	ColorInterpolationType type_;


};