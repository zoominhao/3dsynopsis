#include <osg/Geometry>


#ifndef H_TRANSFORMATIONINTERSECTOR
#define H_TRANSFORMATIONINTERSECTOR

#include <osgUtil/LineSegmentIntersector>

class TransformationIntersector : public osgUtil::LineSegmentIntersector
{
public:
    TransformationIntersector();
    TransformationIntersector( const osg::Vec3& start, const osg::Vec3& end );
    TransformationIntersector( CoordinateFrame cf, double x, double y );
    
    void setPickBias( float bias ) { _pickBias = bias; }
    float getPickBias() const { return _pickBias; }
    
    virtual Intersector* clone( osgUtil::IntersectionVisitor& iv );
    virtual void intersect( osgUtil::IntersectionVisitor& iv, osg::Drawable* drawable );
    
protected:
    virtual ~TransformationIntersector() {}
    float _pickBias;
};

#endif
