#pragma once

#include <osgGA/TrackballManipulator>
#include <osg/Vec3d>

class SynopsisManipulator : public osgGA::TrackballManipulator
{

public:

	SynopsisManipulator();

	virtual void setTargetPosition(const osg::Vec3d target);

	virtual void setCenterPosition(const osg::Vec3d target);

	virtual void setTargetDeta(const float detaHeading, const float detaTilt);
	virtual void setTargetPosition(const float targetHeading, const float targetTilt);
	virtual void setAltitude(const double naltitude);
	virtual double getAltitude();
	virtual void setPosition(const float x, const float y, const float z=0);
	virtual void setPosition(const osg::Vec3& npos);
	void QuatToHPR(const osg::Quat& q, double& heading, double& pitch, double& roll);
	double getheading();

private:
	float m_heading, m_tilt;
};

