#pragma once

#include <osg/Camera>
#include <qt/QtGui>

 //#define  DEPTH
 //#define  OPTICAL_FLOW

class SyScreenCapture : public osg::Camera::DrawCallback
{
public:
	SyScreenCapture( const std::string& rootName=std::string( "" ), const std::string& ext=std::string( ".png" ),  double znear=1.0, double zfar=1400.0,  bool useFrameNum=true );
	~SyScreenCapture();

	virtual void operator()( osg::RenderInfo& ri ) const;

	void setCapture( bool enable ) { captureOn_ = enable; }
    bool getCaptureEnabled() const;

    /** Sets to a positive number of frames to capture a specific frame count.
    If set to 0 (the default), capture continues until it explicitly
    disables with setCapture(false). */
	void setNumFramesToCapture( unsigned int numFrames ) { numFrames_ = numFrames; }

	void setRootName( const std::string& name ) { rootName_ = name; }
	void setExtension( const std::string& extension ) { ext_ = extension; }
	void setUseFrameNumber( bool useFrameNum ) { useFrameNum_ = useFrameNum; }

	/** The default is full screen. Passes non-NULL to specify a viewport other than full screen. */
	void setViewport( osg::Viewport* vp ) { vp_ = vp; }
	void setSnapNo(int snapNo){ snapNo_ = snapNo; }
	void setNearFar(double znear, double zfar){ znear_ = znear; zfar_ = zfar; }

	float calRealDepth(float val) const;
	//by hisong
	void fromOsgMatrix2QtMatrix( const osg::Matrix &srcMatrix, QMatrix4x4 &destMatrix ) const;
protected:
	std::string rootName_;
	std::string ext_;
	double znear_;
	double zfar_;
	bool useFrameNum_;
	int  snapNo_;
	osg::ref_ptr< osg::Viewport > vp_;

	mutable bool captureOn_;
	mutable unsigned int numFrames_;

	
	std::string getFileName( osg::FrameStamp* fs ) const;
};