#pragma once
#ifndef PARAMETER_VISUAL_H_
#define PARAMETER_VISUAL_H_

#include <QString>
#include "basic_types.h"
#include "osg_utility.h"
#include "renderable.h"


class ParameterVisual :public Renderable
{
public:
	ParameterVisual();
	bool setSphere(const osg::Vec3& center, double radius);
	
protected:
	virtual void updateImpl();
	bool renderSphere();

private:
	virtual ~ParameterVisual();

protected:
	osg::Vec3 center_;
	double radius_;
	osg::Vec4 color_;
	
};

#endif // PARAMETER_MANAGER_H_
