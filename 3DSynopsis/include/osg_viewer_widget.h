#pragma once
#ifndef OSG_VIEWER_WIDGET_H
#define OSG_VIEWER_WIDGET_H

#include <QMutex>
#include <QThread>
#include <osgViewer/Viewer>
#include <osg/BoundingSphere>

#include "threaded_painter.h"
#include "adapter_widget.h"

#include "sy_manipulator.h"

class LightSource;
class QResizeEvent;

namespace osg
{
  class Node;
}

class OSGViewerWidget : public AdapterWidget, public osgViewer::Viewer
{
  Q_OBJECT
public:
  OSGViewerWidget(QWidget * parent = 0, const QGLWidget * shareWidget = 0, Qt::WindowFlags f = 0);
  virtual ~OSGViewerWidget();

  void startRendering();
  void stopRendering();

  void addChild(osg::Node *child, bool in_scene=true);
  void removeChild(osg::Node *child, bool in_scene=true);
  void removeChildren(bool in_scene=true);
  osg::ref_ptr<SynopsisManipulator> psynopsisManipulator(){return m_pSynopsisManipulator;}


signals:
  void frameend();

public slots:
  void increasePointSize(void);
  void decreasePointSize(void);
  void increaseLineWidth(void);
  void decreaseLineWidth(void);
  void centerScene(void);
  void writeCameraParameters( const QString& filename );
  void readCameraParameters( const QString& filename );


protected:
  virtual void paintGL(void);
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  virtual void closeEvent(QCloseEvent *event);
  //virtual void keyPressEvent( QKeyEvent* event );

  osg::ref_ptr<osg::Group>                scene_root_;
  osg::BoundingSphere                     scene_bounding_sphere_;
  osg::ref_ptr<osg::Group>                other_root_;
  QThread                                 gl_thread_;
  ThreadedPainter                         threaded_painter_;
  std::vector<osg::ref_ptr<LightSource> > light_sources_;
  QMutex                                  mutex_;
  osg::ref_ptr<osg::Group>                pickup_root_;
  osg::ref_ptr<osg::Group>                para_root_;
  osg::ref_ptr<osg::Group>                dragger_root_;


protected:
  //add by haisong 2014.5.10
  osg::ref_ptr<SynopsisManipulator>       m_pSynopsisManipulator;
  osg::ref_ptr<osg::Group>                m_root;

  //修改帧率
  osg::Timer*                                  m_osgtimer;
  osg::Timer_t                                 start_frame_time;
  osg::Timer_t                                 end_frame_time;
  //控制帧率使用的睡眠时间
  float                                        sleep_time;
  float                                        last_sleep_time;
  //每帧的实际使用时间
  float                                        current_time;
  //计算帧率
  int                                           counts;
};


#endif // OSG_VIEWER_WIDGET_H