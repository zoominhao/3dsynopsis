#pragma once

#include <QDockWidget>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QTableView>


class Parameter;

class ParameterModel: public QStandardItemModel
{
public: 
  ParameterModel(QObject * parent = 0) : QStandardItemModel(parent){}
  ParameterModel(int rows, int columns, QObject * parent = 0) : QStandardItemModel(rows, columns, parent){}
  virtual ~ParameterModel() {}

  Qt::ItemFlags
    flags ( const QModelIndex & index ) const
  {
    return (index.column() == 0)?(Qt::ItemIsEnabled | Qt::ItemIsSelectable):QStandardItemModel::flags(index);
  }

};


class ParameterDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  ParameterDelegate(std::map<std::string, Parameter*>& parameterMap, QObject *parent = 0);

  QWidget *
    createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

  void
    setEditorData(QWidget *editor, const QModelIndex &index) const;

  void
    setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

  void
    updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
  void
    initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;


private:
  Parameter*
    getCurrentParameter(const QModelIndex &index) const;

  std::map<std::string, Parameter*>&      parameter_map_;
};


class ParameterDockWidget : public QDockWidget
{
	Q_OBJECT
public:
	ParameterDockWidget(const std::string& title, QWidget* parent=0);
	~ParameterDockWidget(void){}

	void
		addParameter(Parameter* parameter);
	void
		removeParameter(Parameter* parameter);

	void generateModel();

public slots:
	void updateModelData(void);

signals:
	void ModelDataChanged();

protected:
	std::map<std::string, Parameter*>       name_parameter_map_;
	ParameterModel*                         parameter_model_;
};

