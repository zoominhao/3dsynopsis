#pragma once

#include <osg/NodeCallback>


//////////////////////////////////////////////////////////////////////////
namespace osg {
	class Node;
	class NodeVisitor;
}


class AxisCameraUpdateCallback : public osg::NodeCallback
{
public:
	virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
};