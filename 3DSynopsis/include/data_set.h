#pragma once
#include "osg_viewer_widget.h"
#include "sy_scene.h"
#include "sy_car.h"
#include "sy_cam.h"
#include "sy_camera.h"
#include "sy_water.h"
#include "sy_cars.h"
#include "sy_interest.h"
#include "sy_timer.h"
#include "basic_types.h"
#include "osg_utility.h"

class DataSet : public OSGViewerWidget
{
	Q_OBJECT 
public:
	DataSet(QWidget * parent = 0);
	virtual ~DataSet(void);

	void reinit();
	//从外部文件保存和加载数据
	bool load(const QString& folder);
	//io
	QString floder() {return m_folder;}
	osg::ref_ptr<SyCar>  myCar() {return m_car;}
	osg::ref_ptr<SyCam> myCam() {return m_DCcam;}
	osg::ref_ptr<SyScene> myScene() {return m_scene;}
	void setMeterCamera(osg::ref_ptr<osg::Camera> ncamera) { m_meterCamera = ncamera;}

signals:
	void zoomin(QWidget* data_set);
	void renderStateChanged( const std::vector<bool>& state );

public slots:
		void toggleRender(const std::vector<bool>& state);

		void ZoominPoints();
		void ZoomoutPoints();

		void saveCameraParameters(void);
		void loadCameraParameters(void);

		//drive里面控制天空旋转
		void skyAnimation(void);


		//截取一张图片
		void snap(void);
		//precompute
		void SYSample(void);
		void SYConstant(void);
		void SYSnaps(void);
		void SYImgPro(void);
		void SYSpeed(void);
		void SYSmooth(void);
		void SYView(void);
		void SYCmp(void);
		//process
		void SYOptFlow(void);
		void SYOptAcc(void);
		void SYGenOptVideo(void);
		void SYGenMap(void);
		void SYViewCmp(void);
		//testtool
		void SYTest(void);

protected:
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
	virtual void wheelEvent(QWheelEvent *event);
	virtual void contextMenuEvent(QContextMenuEvent *event);


private:
	//加载一些配置文件
	void createAxiaCamera();

private:
	//目录路径
	QString                                      m_folder;
	//坐标轴
	osg::ref_ptr<osg::Camera>                    m_axisCamera;
	//仪表盘附着的相机
	osg::ref_ptr<osg::Camera>                    m_meterCamera;

	//render models
	osg::ref_ptr<SyScene>                        m_scene;
	osg::ref_ptr<SyCar>                          m_car;
	osg::ref_ptr<SyCam>                          m_DCcam;
	osg::ref_ptr<SyCamera>                       m_sceneCamera;
	SyInterest                                   m_interest;
	osg::ref_ptr<SyWater>                        m_water;    //water
	osg::ref_ptr<SyCars>                         m_cars;    //cars

	//统计时间
	SyTimer                                      m_timer;

	

};