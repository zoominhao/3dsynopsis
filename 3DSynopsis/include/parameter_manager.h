#pragma once
#ifndef PARAMETER_MANAGER_H_
#define PARAMETER_MANAGER_H_

#include <QString>
#include <QDockWidget>
#include <QDomDocument>

#include "parameter_widget.h"

class Parameter;
class IntParameter;
class DoubleParameter;
class BoolParameter;
class ParameterDockWidget;


class ParameterManager
{
public:
  static ParameterManager& getInstance() {
    static ParameterManager theSingleton;
    return theSingleton;
  }

  void loadParameters(const QString& filename);
  void saveParameters(const QString& filename);

  bool   getTrackMode(void)  const;
  bool   getVideoMode(void)  const;
  int    getRenderDensity(void) const;
 
  //driving
  int    getIterationNum(void) const;
  int    getDrivingMode(void)  const;
  double getTotalTime(void)  const;
  double getDeta(void) const;


   // visualization
  QDockWidget* getParameterWidget( int mode );
  void generateAllParaWidget();
  void generateRenderWidget();
  void generateDrivingWidget();

private:
	void readAParameter( Parameter* paramter, QDomElement& root );
	void writeAParameter( Parameter* paramter, QDomElement& root, QDomDocument& doc  );

private:
  ParameterManager(void);
  ParameterManager(const ParameterManager &) {}            // copy ctor hidden
  ParameterManager& operator=(const ParameterManager &) {return (*this);}   // assign op. hidden
  virtual ~ParameterManager();

  

  ParameterDockWidget*                                 all_parameters_widget_;
  ParameterDockWidget*                                 render_parameters_widget_;
  ParameterDockWidget*                                 driving_parameters_widget_;

  //render
  BoolParameter*                                       m_trackMode;
  BoolParameter*                                       m_videoMode;
  IntParameter*                                        m_render_density;

  //driving
  IntParameter*                                        m_iteration_num;
  IntParameter*                                        m_driving_mode;
  DoubleParameter*                                     m_totaltime;
  DoubleParameter*                                     m_deta;
};

#endif // PARAMETER_MANAGER_H_
