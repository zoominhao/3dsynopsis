#pragma once
#ifndef CGAL_TYPES_H
#define CGAL_TYPES_H

#include <CGAL/Cartesian.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Tetrahedron_3.h>



typedef CGAL::Cartesian<double>               Kernel;
typedef Kernel::Line_3                        CgalLine;
typedef Kernel::Line_2                        CgalLine2;
typedef Kernel::Ray_3                         CgalRay3;
typedef Kernel::Ray_2                         CgalRay2;
typedef Kernel::Plane_3                       CgalPlane;
typedef Kernel::Point_3                       CgalPoint;
typedef Kernel::Point_2                       CgalPoint2;
typedef Kernel::Vector_3                      CgalVector;
typedef Kernel::Vector_2                      CgalVector2;
typedef Kernel::Segment_3                     CgalSegment;
typedef Kernel::Segment_2                     CgalSegment2;
typedef Kernel::Direction_3                   CgalDirection;
typedef Kernel::Aff_transformation_2          CgalTransformation2;
typedef Kernel::Aff_transformation_3          CgalTransformation;
typedef Kernel::Iso_cuboid_3                  CgalCuboid;
typedef CGAL::Polygon_2<Kernel>               CgalPolygon2;
typedef CGAL::Polyhedron_3<Kernel>            CgalPolygon3;
typedef CGAL::Tetrahedron_3<Kernel>           CgalPyramid3;




template <class Vector3_from, class Vector3_to>
class Caster {
public:
  Caster(const Vector3_from& v):x_(v.x()), y_(v.y()),z_(v.z()){}
  operator Vector3_to() const { return Vector3_to(x_, y_, z_);}
private:
  double x_, y_, z_;
};


template <class Vector3_from, class Vector2_to>
class  DimReducer {
public: 
	DimReducer(const Vector3_from& v, int d)
		:x_(v.x())
		,y_(v.y())
		,z_(v.z())
		,d_(d%3)
	{

	}
	operator Vector2_to() const
	{
		if( d_ == 0)
			return Vector2_to(y_,z_);
		else if( d_== 1)
			return Vector2_to(x_,z_);
		else 
			return Vector2_to(x_,y_);
	} 
private:
	double x_, y_, z_;
	int d_;
};

template <class Vector2_from, class Vector3_to>
class DimRaiser {
public:
	DimRaiser( const Vector2_from& v)
		:x_(v.x())
		,y_(v.y())
	{

	}
	operator Vector3_to() const
	{
		return Vector3_to(x_,y_, 0);
	}
private:
	double x_, y_;
};



#endif // CGAL_TYPES_H