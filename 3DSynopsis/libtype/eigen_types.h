#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

typedef Eigen::Matrix4d EMat4d;
typedef Eigen::Matrix<double, 3,4> EMat34d; 
typedef Eigen::Vector4d EV4;
typedef Eigen::Vector3d EV3;
typedef Eigen::Vector2d EV2;
typedef Eigen::MatrixXd EMatXd;
typedef Eigen::MatrixXi EMatXi;
typedef Eigen::VectorXd EVecXd;
typedef Eigen::VectorXi EVecXi;
typedef Eigen::Transform<double,3,Eigen::Affine> ETform3;
typedef Eigen::Transform<double,2,Eigen::Affine> ETform2;
typedef Eigen::Transform<double,3,Eigen::Projective> ETProj3;
typedef Eigen::Quaterniond EQuat;

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#ifndef R_PI
#define R_PI 180.0f
#endif