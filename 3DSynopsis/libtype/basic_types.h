#pragma once

#include <string>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <vector>


typedef std::vector<std::pair<int,int>> Int2Vector;
typedef std::vector<std::pair<double,double>> Double2Vector;
typedef std::vector<std::pair<int,double>>  IntDoubleVector;


class IndexStringConvertor
{
public:
	static std::string toString( int id)
	{
		return boost::lexical_cast<std::string>( id);
	}
	static int toIndex(const std::string name)
	{
		int id = -1;
		try
		{
			id = boost::lexical_cast<int>( name);
		}
		catch(boost::bad_lexical_cast &)
		{
			std::cout << "Can't convert this string to index: " << name << std::endl; 
			id = -1;
		}
		return id;
	}
};


enum MapMode
{
	MAP_UNDEFINED = -1,
	MAP_ALL = 0,
	MAP_SCORE = 2,
};


enum ControlMode
{
	CONTROL_RESET = 0,
	CONTROL_START = 1,
	CONTROL_PAUSE = 2,
	CONTROL_SPEEDUP = 3,
	CONTROL_SLOWDOWN = 4,
};

