#include "sy_cars.h"
#include "file_system.h"
#include "global_constant.h"
#include "osg_viewer_widget.h"
#include <osgDB/ReadFile>
#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowMap>
#include <osg/PositionAttitudeTransform>
#include <osg/AnimationPath>
#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>

SyCars::SyCars()
	:m_folder(""),
	m_showCars(true)
{
}

SyCars::~SyCars()
{

}

bool SyCars::setFromFile( const std::string& folder )
{
	m_folder = folder;
	std::string carsModelDir = folder + "\\model\\cars";
	QDir carsRootDir(carsModelDir.c_str());
	scanAllFile(carsRootDir);
	bool isAddChild = false;
	for (int i = 0; i < m_FilePathList.size(); i++)
	{
		QString tmpCarPath = m_FilePathList.at(i);
		std::string carModelPath = tmpCarPath.toStdString();
		
		QString tmpAnimationPath = tmpCarPath.replace(tmpCarPath.size() - 3, 3, "path");
		std::string carAnimationPath = tmpAnimationPath.toStdString();
		QFileInfo carfile(carModelPath.c_str());
		QFileInfo carPathFile(carAnimationPath.c_str());
		if(carfile.exists() && carPathFile.exists())
		{
			osg::ref_ptr<osg::PositionAttitudeTransform> node = getCarAnimationTransform(carModelPath, carAnimationPath);
			addChild(node, true);
			isAddChild = true;
		}else{
			continue;
		}
		//renderCars();
	}
	return isAddChild;
}


bool SyCars::toggleRenderCars(bool toggle)
{
	//if (m_showCars == toggle)
	//	return false;

	//m_showCars = toggle;

	//renderCars();
	if (toggle)
	{
		return setAllChildrenOn();
	}else{
		return setAllChildrenOff();
	}
	//return true;
}


void SyCars::renderCars()
{
	if (m_showCars)
	{
		if (m_bus != NULL)
			addChild(m_bus);
		if (m_taxi!=NULL)
			addChild(m_taxi);
	}
	else
	{
		if (m_bus != NULL)
			removeChild(m_bus);
		if (m_taxi!=NULL)
			removeChild(m_taxi);
	}
}

osg::ref_ptr<osg::PositionAttitudeTransform> SyCars::getCarAnimationTransform(const std::string carModelPath, const std::string carAnimationPath)
{
	//std::string carModel = m_folder + "\\model\\cars\\bus.ive";
	//std::string carPath = m_folder + "\\model\\cars\\taxi.path";
	QFileInfo carfile(carModelPath.c_str());
	QFileInfo carPathFile(carAnimationPath.c_str());
	if(carfile.exists() && carPathFile.exists())
	{

		osg::ref_ptr<osg::Node> busNode = osgDB::readNodeFile( carModelPath );
		osg::Vec3 carCenter = busNode->getBound().center();
		osg::ref_ptr<osg::MatrixTransform> trans = new osg::MatrixTransform;
		//trans->setMatrix(osg::Matrix::translate(-busCenter) * osg::Matrix::scale(0.01, 0.01, 0.01) /** osg::Matrix::rotate(osg::Quat(osg::DegreesToRadians(90.0), osg::Vec3(0, 0, 1.0)))*/);
		trans->setMatrix(osg::Matrix::translate(-carCenter) * osg::Matrix::scale(0.01, 0.01, 0.01) /** osg::Matrix::rotate(osg::Quat(osg::DegreesToRadians(90.0), osg::Vec3(0, 0, 1.0)))*/);
		trans->addChild(busNode);

		osgDB::ifstream carPathIn(carAnimationPath.c_str());
		osg::ref_ptr<osg::AnimationPath> carAnimation = new osg::AnimationPath;
		carAnimation->read(carPathIn);
		m_taxiTrans = new osg::PositionAttitudeTransform;
		m_taxiTrans->addChild(trans);
		m_taxiTrans->setUpdateCallback(new osg::AnimationPathCallback(carAnimation));
		return m_taxiTrans;
	}
	return NULL;
}

osg::ref_ptr<osg::Node> SyCars::getTestPath()
{
	osg::ref_ptr<osg::Node> lineObject = osgDB::readNodeFile("F:\\VSProject\\3DSynopsis\\scenedata\\scene\\sz_hw_sh\\model\\cars\\path.obj");
	return lineObject;
}

void SyCars::scanAllFile(QDir rootDir)
{
	QStringList files = rootDir.entryList(QStringList() << "*.ive", QDir::Files, QDir::Name);
	for (int i = 0; i < files.count(); ++i)
	{
		//QString temp=QString::number(index);
		m_FilePathList<<rootDir.absolutePath() + QDir::separator() + files.value(i);
	}
	QStringList directories = rootDir.entryList(QStringList(), QDir::AllDirs | QDir::NoDotAndDotDot, QDir::Name);
	for (int i = 0; i < directories.count(); ++i)
	{
		scanAllFile(rootDir.absolutePath() + QDir::separator() + directories.at(i));
	}
}
