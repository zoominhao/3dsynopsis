#include "sy_water.h"
#include "sy_scene.h"
#include "global_constant.h"
#include "file_system.h"
#include "sy_config.h"

#include <QFileInfo>
#include <osgDB/ReadFile>


SyWater::SyWater()
	:m_folder(""),
	m_show_water(true)
{
	
}


SyWater::~SyWater(void)
{
}

bool SyWater::setFromFile( const std::string& folder )
{
	m_folder = folder;
	std::string waterm = m_folder + "\\model";
	QFileInfo waterfile(waterm.c_str());
	bool flag = false;
	if(waterfile.exists())
	{
		flag = true;
		if (SyConfig::getInstance().getWhichCity()==1)
		{
			m_sh_water = createWaterNode(2);
			m_highway_water = createWaterNode(4);
		}
		else if (SyConfig::getInstance().getWhichCity()==2)
		{
			m_sh_water = createWaterNode(2);
			m_sz_water = createWaterNode(3);
			m_highway_water = createWaterNode(4);
			m_highway_water2 = createWaterNode(5);
			m_highway_water3 = createWaterNode(6);
		}
		renderWater();
	}

	return flag;
}

osg::ref_ptr<osg::Node> SyWater::createWaterNode(int sceneMode)
{
	osg::ref_ptr<osg::Node> waternode;
	std::string scenename;
	if (sceneMode==2)
	  scenename = "\\model\\water\\shanghai\\";
	else if (sceneMode==3)
      scenename = "\\model\\water\\suzhou\\";
	else if(sceneMode==4)
      scenename = "\\model\\water\\highway\\";
	else if(sceneMode==5)
	  scenename = "\\model\\water\\highway2\\";
	else if(sceneMode==6)
	  scenename = "\\model\\water\\highway3\\";
	if(sceneMode==3)
		waternode = osgDB::readNodeFile(m_folder + scenename+ GConstant::C_SCENE_WATER_SZ);  //苏州的格式不一致
	else
	{
		waternode = osgDB::readNodeFile(m_folder + scenename + GConstant::C_SCENE_WATER);
		osg::StateSet* stateset = waternode->getOrCreateStateSet();
		osg::Shader* vertextShader = new osg::Shader(osg::Shader::VERTEX);
		osg::Shader* fragmentShaser = new osg::Shader(osg::Shader::FRAGMENT);
		vertextShader->loadShaderSourceFromFile(m_folder + scenename +  GConstant::C_SCENE_WATER_VERT);
		fragmentShaser->loadShaderSourceFromFile(m_folder + scenename +  GConstant::C_SCENE_WATER_FRAG);

		if (vertextShader && fragmentShaser)
		{
			osg::Program* program = new osg::Program;
			program->addShader(vertextShader);
			program->addShader(fragmentShaser);
			stateset->setAttributeAndModes(program, osg::StateAttribute::ON);

			osg::Uniform* param1 = new osg::Uniform("LightPos", osg::Vec3(1.0, 1.0, 0.0));
			stateset->addUniform(param1);
			osg::Uniform* param2 = new osg::Uniform("time", 0.0f);
			param2->setUpdateCallback(new UpdateShader);
			stateset->addUniform(param2);
			osg::Uniform* param3 = new osg::Uniform( "normalMap", 0 );
			stateset->addUniform( param3 );
			osg::Uniform* param4 = new osg::Uniform( "cubeMap", 1 );
			stateset->addUniform( param4 );

			stateset->setTextureAttributeAndModes( 1, readCubeMap(), osg::StateAttribute::ON );
		}
	}
	
	return waternode;
}

osg::TextureCubeMap* SyWater::readCubeMap()
{
	osg::TextureCubeMap* cubemap= new osg::TextureCubeMap;

	osg::Image* imagePosX = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_POS_X );
	osg::Image* imageNegX = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_NEG_X  );
	osg::Image* imagePosY = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_POS_Y  );
	osg::Image* imageNegY = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_NEG_Y  );
	osg::Image* imagePosZ = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_POS_Z  );
	osg::Image* imageNegZ = osgDB::readImageFile( m_folder + GConstant::C_SCENE_WATER_IMAGE_NEG_Z  );

	if (imagePosX && imageNegX && imagePosY)
	{
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_X, imagePosX);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_X, imageNegX);
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_Y, imagePosY);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_Y, imageNegY);
		cubemap->setImage(osg::TextureCubeMap::POSITIVE_Z, imagePosZ);
		cubemap->setImage(osg::TextureCubeMap::NEGATIVE_Z, imageNegZ);

		cubemap->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
		cubemap->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
		cubemap->setWrap(osg::Texture::WRAP_R, osg::Texture::CLAMP_TO_EDGE);

		cubemap->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR_MIPMAP_LINEAR);
		cubemap->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
	}

	return cubemap;
}

void SyWater::renderWater()
{
	if (m_show_water)
	{
		if (m_sz_water!=NULL)
			addChild(m_sz_water);
		if (m_sh_water!=NULL)
			addChild(m_sh_water);
		if (m_highway_water!=NULL)
			addChild(m_highway_water);
		if (m_highway_water2!=NULL)
			addChild(m_highway_water2);
		if (m_highway_water3!=NULL)
			addChild(m_highway_water3);
	}
	else
	{
		if (m_sz_water!=NULL)
			removeChild(m_sz_water);
		if (m_sh_water!=NULL)
			removeChild(m_sh_water);
		if (m_highway_water!=NULL)
			removeChild(m_highway_water);
		if (m_highway_water2!=NULL)
			removeChild(m_highway_water2);
		if (m_highway_water3!=NULL)
			removeChild(m_highway_water3);
	}
}

bool SyWater::toggleRenderWater( bool toggle )
{
	if (m_show_water == toggle)
		return false;

	m_show_water = toggle;

	renderWater();
	return true;
}
