#pragma once
#ifndef SY_CARS_H
#define SY_CARS_H

#include <vector>
#include <osg/PositionAttitudeTransform>
#include <QtCore/QDir>
#include <osg/Switch>
#include "renderable.h"

class SyCars : public osg::Switch
{
public:
	SyCars();
	~SyCars();

	//io����
	bool setFromFile( const std::string& folder );
	bool toggleRenderCars(bool toggle);

	osg::ref_ptr<osg::PositionAttitudeTransform> getCarAnimationTransform(const std::string carModelPath, const std::string carAnimationPath);
	osg::ref_ptr<osg::Node> getTestPath();
	void scanAllFile(QDir rootDir);

protected:
	void renderCars();
private:


private:
	std::string                           m_folder;
	bool                                  m_showCars;
	osg::ref_ptr<osg::Node>               m_porsche;
	osg::ref_ptr<osg::Node>               m_taxi;
	osg::ref_ptr<osg::Node>               m_bus;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_taxiTrans;
	osg::ref_ptr<osg::PositionAttitudeTransform> m_carTrans;
	QStringList                           m_FilePathList;
};
#endif