#pragma once
#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <QMutex>
#include <osg/MatrixTransform>


class Renderable : public osg::MatrixTransform
{
public:
  Renderable(void);
  virtual ~Renderable(void);

  virtual const char* className() const {return "Renderable";}

  inline bool isHidden(void) const {return hidden_;}
  virtual void toggleHidden(void);
  bool toggleHidden(bool hidden);
  
  
  inline bool isExpire(void) const {return expired_; } 
  void expire();
  void update();

protected:
  virtual void clear();
  virtual void updateImpl() = 0;

protected:
  QMutex  mutex_;
  bool    hidden_;

private:
  bool    expired_;
};

#endif // RENDERABLE_H