#include "sy_path.h"

SyPath::SyPath( osg::Vec3 spos, osg::Vec3 tpos, bool crossFlag, double duration, double distance )
	:m_spos(spos),
	m_tpos(tpos),
	m_crossFlag(crossFlag),
	m_duration(duration),
	m_distance(distance)
{
	
}

SyPath::~SyPath()
{

}
