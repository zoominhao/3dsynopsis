#include "sy_camera.h"
#include "global_constant.h"
#include "global_func.h"
#include "sy_config.h"
#include "parameter_manager.h"
#include "file_system.h"
#include "osg_utility.h"

#include <QFileInfo>
#include <osgDB/ReadFile>


SyCamera::SyCamera()
	:m_folder(""),
	m_showCameraPath(false),
	m_showCamera(false),
	m_showCorLines(false),
	m_showVisualPorsche(false),
	m_scale(0.01,0.01,0.01),
	m_car_scale(0.02,0.02,0.02)
{

}

SyCamera::~SyCamera()
{

}

bool SyCamera::setFromFile( const std::string& folder )
{
	std::string cameraModel = folder +  GConstant::C_CAMERA;
	std::string porscheModel = folder + GConstant::C_PORSCHE;
	std::string samplepath = folder + GConstant::C_SAMPLE_PATH;
	std::string extension = FileSystem::extension( cameraModel );
	m_folder = folder;
	bool flag = false;
	if( extension == GConstant::C_SCENE_MODEL_EXTENSION)
	{ 
		QFileInfo camerafile(cameraModel.c_str());
		QFileInfo porschefile(porscheModel.c_str());
		if(camerafile.exists()&&porschefile.exists())
		{
			flag = true;
			m_camera = osgDB::readNodeFile( cameraModel );
			m_porsche = osgDB::readNodeFile( porscheModel );
			m_IOManager.loadSamplePath(samplepath.c_str(),m_samplePath);
			expire();
		}
	}
	return flag;
}

bool SyCamera::toggleRenderCamera( bool toggle )
{
	if (m_showCamera == toggle)
		return false;

	m_showCamera = toggle;

	expire();
	return true;
}

bool SyCamera::toggleRenderCameraPath( bool toggle )
{
	if (m_showCameraPath == toggle)
		return false;

	m_showCameraPath = toggle;

	expire();
	return true;
}

bool SyCamera::toggleRenderCorLines( bool toggle )
{
	if (m_showCorLines == toggle)
		return false;

	m_showCorLines = toggle;

	expire();
	return true;
}

bool SyCamera::toggleRenderVisualPorsche( bool toggle )
{
	if (m_showVisualPorsche == toggle)
		return false;

	m_showVisualPorsche = toggle;

	expire();
	return true;
}

void SyCamera::updateImpl()
{
	if (m_showCamera)
	{
		renderCamera();
	}
	if (m_showCorLines)
	{
		renderCorLines();
	}
	if (m_showVisualPorsche)
	{
		renderVisulPorches();
	}
	if (m_showCameraPath)
	{
		renderCameraPath();
	}
}

/*
void SyCamera::renderCamera()
{
	//camera 的位置
	readViewV();
	readSpeedV();

	if (m_samplePath.size()!=0)
	{
		for (int i = 0; i < m_samplePath.size(); i += ParameterManager::getInstance().getRenderDensity())
		{
			osg::Matrix mscale,mtrans;
			//设置参数
			mscale.makeScale(m_scale);
			mtrans.setTrans(m_samplePath[i].spos()[0]+2,m_samplePath[i].spos()[1]+30,m_samplePath[i].spos()[2]+m_views[i].first.m_height-15);
			osg::MatrixTransform *nmat = new osg::MatrixTransform;
			nmat->setMatrix(mscale*mtrans);
			nmat->addChild(m_camera);
			//heading
			osg::Vec3 cam_cen = nmat->getBound().center();
			osg::Matrixd headingMat,tiltMat,toriginMat,tocenMat;
			toriginMat.makeTranslate(-(cam_cen));
			headingMat.makeRotate(osg::DegreesToRadians(m_views[i].first.m_heading), osg::Vec3f(0,0,1));
			if (m_views[i].first.m_heading<180)
				tiltMat.makeRotate(osg::DegreesToRadians(-m_views[i].first.m_tilt), osg::Vec3f(0,1,0));
			else
				tiltMat.makeRotate(osg::DegreesToRadians(m_views[i].first.m_tilt), osg::Vec3f(0,1,0));
			tocenMat.makeTranslate(cam_cen);
			nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tiltMat*tocenMat);
			addChild(nmat);	
	}
	osg::Matrix mscale,mtrans;
	//设置参数
	mscale.makeScale(m_scale);
	mtrans.setTrans(m_samplePath[m_samplePath.size()-1].tpos()[0]+2,m_samplePath[m_samplePath.size()-1].tpos()[1]+30,m_samplePath[m_samplePath.size()-1].tpos()[2]+m_views[m_samplePath.size()-1].second.m_height-15);
	osg::MatrixTransform *nmat = new osg::MatrixTransform;
	nmat->setMatrix(mscale*mtrans);
	nmat->addChild(m_camera);
	osg::Vec3 cam_cen = nmat->getBound().center();
	osg::Matrixd headingMat,tiltMat,toriginMat,tocenMat;
	toriginMat.makeTranslate(-(cam_cen));
	headingMat.makeRotate(osg::DegreesToRadians(m_views[m_samplePath.size()-1].second.m_heading), osg::Vec3f(0,0,1));
	if (m_views[m_samplePath.size()-1].second.m_heading<180)
		tiltMat.makeRotate(osg::DegreesToRadians(-m_views[m_samplePath.size()-1].second.m_tilt), osg::Vec3f(0,1,0));
	else
		tiltMat.makeRotate(osg::DegreesToRadians(m_views[m_samplePath.size()-1].second.m_tilt), osg::Vec3f(0,1,0));
	tocenMat.makeTranslate(cam_cen);
	nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tiltMat*tocenMat);
	addChild(nmat);	
	}
	//Render path
	if (m_samplePath.size()!=0)
	{
		EMatXd nodesMat(m_samplePath.size()+1,3);
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			nodesMat(i,0) = m_samplePath[i].spos()[0];
			nodesMat(i,1) = m_samplePath[i].spos()[1];
			nodesMat(i,2) = m_samplePath[i].spos()[2]+m_views[i].first.m_height;
		}
		nodesMat(m_samplePath.size(),0) = m_samplePath[m_samplePath.size()-1].tpos()[0];
		nodesMat(m_samplePath.size(),1) = m_samplePath[m_samplePath.size()-1].tpos()[1];
		nodesMat(m_samplePath.size(),2) = m_samplePath[m_samplePath.size()-1].tpos()[2]+m_views[m_samplePath.size()-1].second.m_height;

		//根据速度赋予颜色
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
		std::vector<double> m_tmpSpeedV = m_speedV;
		GLOBAL::normalize(m_tmpSpeedV);
		/ *for (int i = 0 ; i < m_tmpSpeedV.size()-1; ++i)
		{
			EV4 eColor = GLOBAL::scalar2color(m_tmpSpeedV[i]);
			osg::Vec4 oColor(eColor[0],eColor[1],eColor[2],eColor[3]);
		    colors->push_back(oColor);
		}
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, colors, 15);
		addChild(sceneroute);* /

		//draw segments
		for (int i = 0 ; i < m_tmpSpeedV.size(); ++i)
		{
			EV4 eColor = GLOBAL::scalar2color(m_tmpSpeedV[i]);
			osg::Vec4 oColor(eColor[0],eColor[1],eColor[2],eColor[3]);
			osg::Vec3 top(nodesMat(i,0),nodesMat(i,1),nodesMat(i,2)); 
			osg::Vec3 base(nodesMat(i+1,0),nodesMat(i+1,1),nodesMat(i+1,2)); 
			osg::Geode* sceneroute = OSGUtility::drawCylinder(top, base, 1, oColor);
			addChild(sceneroute);
		}
		//draw nodes
		osg::Geode* sceneNodes = OSGUtility::drawPoints(nodesMat, osg::Vec4(1.0,1.0,1.0,0.0));
		addChild(sceneNodes);
	}
}*/

void SyCamera::renderCameraPath()
{
	//Render path
	readViewV();
	readSpeedV();
	readCamPreV();
	loadSamplePath();

	if (m_ssamplePath.size()!=0)
	{
		EMatXd nodesMat(m_ssamplePath.size()+1,3);
		for (int i = 0; i < m_ssamplePath.size(); ++i)
		{
			if (ParameterManager::getInstance().getTrackMode())
			{
				nodesMat(i,0) = m_ssamplePath[i].spos()[0]-0.8 * m_camPreCordsV[i].x();
				nodesMat(i,1) = m_ssamplePath[i].spos()[1]-0.8 * m_camPreCordsV[i].y();
			}
			else
			{
				nodesMat(i,0) = m_ssamplePath[i].spos()[0]-m_camPreCordsV[i].x();
				nodesMat(i,1) = m_ssamplePath[i].spos()[1]-m_camPreCordsV[i].y();
			}
			
			nodesMat(i,2) = m_ssamplePath[i].spos()[2] + m_views[i].first.m_height;
		}
		if (ParameterManager::getInstance().getTrackMode())
		{
			nodesMat(m_ssamplePath.size(),0) = m_ssamplePath[m_ssamplePath.size()-1].tpos()[0]-0.8 * m_camPreCordsV[m_ssamplePath.size()].x();
			nodesMat(m_ssamplePath.size(),1) = m_ssamplePath[m_ssamplePath.size()-1].tpos()[1]-0.8 * m_camPreCordsV[m_ssamplePath.size()].y();
		}
		else
		{
			nodesMat(m_ssamplePath.size(),0) = m_ssamplePath[m_ssamplePath.size()-1].tpos()[0]-m_camPreCordsV[m_ssamplePath.size()].x();
			nodesMat(m_ssamplePath.size(),1) = m_ssamplePath[m_ssamplePath.size()-1].tpos()[1]-m_camPreCordsV[m_ssamplePath.size()].y();
		}
		nodesMat(m_ssamplePath.size(),2) = m_ssamplePath[m_ssamplePath.size()-1].tpos()[2]+m_views[m_ssamplePath.size()-1].second.m_height;

		//根据速度赋予颜色
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
		std::vector<double> m_tmpSpeedV = m_speedV;
		GLOBAL::normalize(m_tmpSpeedV);

		
		//draw segments
		/*for (int i = 0 ; i < m_tmpSpeedV.size(); ++i)
		{
			EV4 eColor = GLOBAL::scalar2color(m_tmpSpeedV[i]);
			osg::Vec4 oColor(eColor[0],eColor[1],eColor[2],eColor[3]);
			osg::Vec3 top(nodesMat(i,0),nodesMat(i,1),nodesMat(i,2)); 
			osg::Vec3 base(nodesMat(i+1,0),nodesMat(i+1,1),nodesMat(i+1,2)); 
			osg::Geode* sceneroute = OSGUtility::drawCylinder(top, base, 1, oColor);
			addChild(sceneroute);
		}*/

	
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, osg::Vec4(0.6,0.0,0.0,1.0), 5);
		addChild(sceneroute);
		//draw nodes
		/*osg::Geode* sceneNodes = OSGUtility::drawPoints(nodesMat, osg::Vec4(1.0,1.0,1.0,0.0));
		addChild(sceneNodes);*/
	}
}

void SyCamera::renderCamera()
{
	//camera 的位置
	readViewV();
	readSpeedV();
	readCamPreV();
	loadSamplePath();

	if (m_ssamplePath.size()!=0)
	{
		for (int i = 0; i < m_ssamplePath.size(); i += ParameterManager::getInstance().getRenderDensity())
		{
			osg::Matrix mscale,mtrans;
			//设置参数
			mscale.makeScale(m_scale);
			mtrans.setTrans(m_ssamplePath[i].spos()[0]+2-m_camPreCordsV[i].x(),m_ssamplePath[i].spos()[1]+30-m_camPreCordsV[i].y(),m_ssamplePath[i].spos()[2]+m_views[i].first.m_height-15);
			osg::MatrixTransform *nmat = new osg::MatrixTransform;
			nmat->setMatrix(mscale*mtrans);
			nmat->addChild(m_camera);
			//heading
			osg::Vec3 cam_cen = nmat->getBound().center();
			osg::Matrixd headingMat,tiltMat,toriginMat,tocenMat;
			toriginMat.makeTranslate(-(cam_cen));
			headingMat.makeRotate(osg::DegreesToRadians(m_views[i].first.m_heading), osg::Vec3f(0,0,1));
			if (m_views[i].first.m_heading<180)
				tiltMat.makeRotate(osg::DegreesToRadians(-m_views[i].first.m_tilt), osg::Vec3f(0,1,0));
			else
				tiltMat.makeRotate(osg::DegreesToRadians(m_views[i].first.m_tilt), osg::Vec3f(0,1,0));
			tocenMat.makeTranslate(cam_cen);
			nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tiltMat*tocenMat);
			addChild(nmat);	
		}
		osg::Matrix mscale,mtrans;
		//设置参数
		mscale.makeScale(m_scale);
		mtrans.setTrans(m_ssamplePath[m_ssamplePath.size()-1].tpos()[0]+2-m_camPreCordsV[m_ssamplePath.size()].x(),m_ssamplePath[m_ssamplePath.size()-1].tpos()[1]+30-m_camPreCordsV[m_ssamplePath.size()].y(),m_ssamplePath[m_ssamplePath.size()-1].tpos()[2]+m_views[m_ssamplePath.size()-1].second.m_height-15);
		osg::MatrixTransform *nmat = new osg::MatrixTransform;
		nmat->setMatrix(mscale*mtrans);
		nmat->addChild(m_camera);
		osg::Vec3 cam_cen = nmat->getBound().center();
		osg::Matrixd headingMat,tiltMat,toriginMat,tocenMat;
		toriginMat.makeTranslate(-(cam_cen));
		headingMat.makeRotate(osg::DegreesToRadians(m_views[m_ssamplePath.size()-1].second.m_heading), osg::Vec3f(0,0,1));
		if (m_views[m_ssamplePath.size()-1].second.m_heading<180)
			tiltMat.makeRotate(osg::DegreesToRadians(-m_views[m_ssamplePath.size()-1].second.m_tilt), osg::Vec3f(0,1,0));
		else
			tiltMat.makeRotate(osg::DegreesToRadians(m_views[m_ssamplePath.size()-1].second.m_tilt), osg::Vec3f(0,1,0));

		tocenMat.makeTranslate(cam_cen);
		nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tiltMat*tocenMat);
		addChild(nmat);	
	}
}

void SyCamera::renderCorLines()
{
	//camera 的位置
	readCamPreV();
	readViewV();
	loadSamplePath();

	if (m_ssamplePath.size() != 0 && m_samplePath.size()!=0)
	{
		osg::Vec4 oColor(1.0,0.0,0.0,0.0);   //red
		for (int i = 0; i < m_ssamplePath.size(); i += ParameterManager::getInstance().getRenderDensity())
		{
			osg::Vec3 top(m_samplePath[i].spos()[0],m_samplePath[i].spos()[1],m_samplePath[i].spos()[2]); 
			osg::Vec3 base(m_ssamplePath[i].spos()[0]-m_camPreCordsV[i].x(),m_ssamplePath[i].spos()[1]-m_camPreCordsV[i].y(),m_ssamplePath[i].spos()[2]+m_views[i].first.m_height); 
			osg::Geode* sceneCor = OSGUtility::drawCylinder(top, base, 0.5, oColor);
			addChild(sceneCor);
		}
		osg::Vec3 top(m_samplePath[m_ssamplePath.size()-1].tpos()[0],m_samplePath[m_ssamplePath.size()-1].tpos()[1],m_samplePath[m_ssamplePath.size()-1].tpos()[2]); 
		osg::Vec3 base(m_ssamplePath[m_ssamplePath.size()-1].tpos()[0]-m_camPreCordsV[m_ssamplePath.size()].x(),m_ssamplePath[m_ssamplePath.size()-1].tpos()[1]-m_camPreCordsV[m_ssamplePath.size()].y(),m_ssamplePath[m_ssamplePath.size()-1].tpos()[2]+m_views[m_ssamplePath.size()-1].second.m_height); 
		osg::Geode* sceneCor = OSGUtility::drawCylinder(top, base, 0.5, oColor);
		addChild(sceneCor);
	}
}

void SyCamera::renderVisulPorches()
{
    //camera 的位置
	readViewV();
	readSpeedV();

	if (m_samplePath.size()!=0)
	{
	for (int i = 0; i < m_samplePath.size(); i += ParameterManager::getInstance().getRenderDensity())
	{
		//先设置scale
		osg::Matrix mscale;
		mscale.makeScale(m_car_scale);
		osg::MatrixTransform *nmat = new osg::MatrixTransform;
		nmat->setMatrix(mscale);
		nmat->addChild(m_porsche);
		//设置位置和heading
		osg::Matrixd headingMat,toriginMat,tocenMat;
		osg::Vec3 cam_cen = nmat->getBound().center();
		toriginMat.makeTranslate(-(cam_cen));
		headingMat.makeRotate(-nmat->getMatrix().getRotate());
		headingMat.makeRotate(osg::DegreesToRadians(m_views[i].first.m_heading+180), osg::Vec3f(0,0,1));
		tocenMat.makeTranslate(m_samplePath[i].spos()+osg::Vec3(0.0,0.0,1.0));
		nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tocenMat);
		addChild(nmat);	
	}
	//先设置scale
	osg::Matrix mscale;
	mscale.makeScale(m_car_scale);
	osg::MatrixTransform *nmat = new osg::MatrixTransform;
	nmat->setMatrix(mscale);
	nmat->addChild(m_porsche);
	//设置位置和heading
	osg::Matrixd headingMat,toriginMat,tocenMat;
	osg::Vec3 cam_cen = nmat->getBound().center();
	toriginMat.makeTranslate(-(cam_cen));
	headingMat.makeRotate(-nmat->getMatrix().getRotate());
	headingMat.makeRotate(osg::DegreesToRadians(m_views[m_samplePath.size()-1].second.m_heading+180), osg::Vec3f(0,0,1));
	tocenMat.makeTranslate(m_samplePath[m_samplePath.size()-1].tpos()+osg::Vec3(0.0,0.0,1.0));
	nmat->setMatrix(nmat->getMatrix()*toriginMat*headingMat*tocenMat);
	addChild(nmat);	
	}
	//Render path
	if (m_samplePath.size()!=0)
	{
		EMatXd nodesMat(m_samplePath.size()+1,3);
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			nodesMat(i,0) = m_samplePath[i].spos()[0];
			nodesMat(i,1) = m_samplePath[i].spos()[1];
			nodesMat(i,2) = m_samplePath[i].spos()[2];
		}
		nodesMat(m_samplePath.size(),0) = m_samplePath[m_samplePath.size()-1].tpos()[0];
		nodesMat(m_samplePath.size(),1) = m_samplePath[m_samplePath.size()-1].tpos()[1];
		nodesMat(m_samplePath.size(),2) = m_samplePath[m_samplePath.size()-1].tpos()[2];

		//根据速度赋予颜色
		osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
		std::vector<double> m_tmpSpeedV = m_speedV;
		GLOBAL::normalize(m_tmpSpeedV);
		/*for (int i = 0 ; i < m_tmpSpeedV.size()-1; ++i)
		{
			EV4 eColor = GLOBAL::scalar2color(m_tmpSpeedV[i]);
			osg::Vec4 oColor(eColor[0],eColor[1],eColor[2],eColor[3]);
		    colors->push_back(oColor);
		}
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, colors, 15);
		addChild(sceneroute);*/

		//draw segments
		for (int i = 0 ; i < m_tmpSpeedV.size(); ++i)
		{
			EV4 eColor = GLOBAL::scalar2color(m_tmpSpeedV[i]);
			osg::Vec4 oColor(eColor[0],eColor[1],eColor[2],eColor[3]);
			osg::Vec3 top(nodesMat(i,0),nodesMat(i,1),nodesMat(i,2)); 
			osg::Vec3 base(nodesMat(i+1,0),nodesMat(i+1,1),nodesMat(i+1,2)); 
			osg::Geode* sceneroute = OSGUtility::drawCylinder(top, base, 1, oColor);
			addChild(sceneroute);
		}
		//draw nodes
		/*osg::Geode* sceneNodes = OSGUtility::drawPoints(nodesMat, osg::Vec4(1.0,1.0,1.0,0.0));
		addChild(sceneNodes);*/
	}
}

void SyCamera::clearData()
{
	//todo
}

bool SyCamera::loadSamplePath()
{
	m_ssamplePath.clear();
	std::string spathFile;
	if(ParameterManager::getInstance().getDrivingMode()==1)
		spathFile = m_folder+GConstant::C_SCSAMPLE_PATH;
	else if (ParameterManager::getInstance().getDrivingMode()==5)   //设成5，确保不与其他情况冲突
		spathFile = m_folder+GConstant::C_SJSAMPLE_PATH; 
	else
		spathFile = m_folder+GConstant::C_SSAMPLE_PATH;

	return m_IOManager.loadSamplePath(spathFile.c_str(),m_ssamplePath);
}

bool SyCamera::readViewV()
{
	m_views.clear();
	std::string viewVfile;
	if(ParameterManager::getInstance().getDrivingMode()==1)
		viewVfile = m_folder + GConstant::C_CAMERA_GOOGLE_VIEW;
	else if (ParameterManager::getInstance().getDrivingMode()==5)   //设成5，确保不与其他情况冲突
	    viewVfile = m_folder + GConstant::C_CAMERA_JUMP_VIEW;
	else
		viewVfile = m_folder + GConstant::C_CAMERA_OP_SVIEW;

	return m_IOManager.readView(viewVfile.c_str(),m_views);
}

bool SyCamera::readSpeedV()
{
	m_speedV.clear();
	std::string speedVfile;
	if(ParameterManager::getInstance().getDrivingMode()==1)
		speedVfile = m_folder + GConstant::C_GOOGLE_SPEED;
	else if (ParameterManager::getInstance().getDrivingMode()==5)   //设成5，确保不与其他情况冲突
		speedVfile = m_folder + GConstant::C_JUMP_SPEED;
	else
		speedVfile = m_folder + GConstant::C_OP_SPEED;

	return m_IOManager.readSpeed(speedVfile.c_str(),m_speedV);
}

bool SyCamera::readCamPreV()
{
	m_camPreCordsV.clear();
	std::string campreVfile;
	if(ParameterManager::getInstance().getDrivingMode()==1)
		campreVfile = m_folder + GConstant::C_CAMERA_GOOGLE_PRE;
	else if (ParameterManager::getInstance().getDrivingMode()==5)   //设成5，确保不与其他情况冲突
		campreVfile = m_folder + GConstant::C_CAMERA_JUMP_PRE;
	else
		campreVfile = m_folder + GConstant::C_CAMERA_PRE;

	return m_IOManager.readCamDis(campreVfile.c_str(),m_camPreCordsV);
}

void SyCamera::saveCameraRoute()
{
	/*std::string lineobjfile = "line.obj";
	std::ofstream fout(lineobjfile,std::ios::binary);
	if (m_SmoothViewV.size()==0)
	{
		std::cout<<"m_SmoothViewV no data!\n";
		return ;
	}
	fout<<"# 3ds by zoomin"<<std::endl;
	fout<<"#"<<std::endl;
	fout<<"# shape camLine"<<std::endl;
	fout<<"#"<<std::endl<<std::endl;

	for (int i = 0; i < m_SmoothViewV.size(); ++i)
	{
		if (i==m_SmoothViewV.size()-1)
          fout<<"v  "<<m_SmoothPathV[i-1].tpos().x()<<" "<<m_SmoothPathV[i-1].tpos().y()<<" "<<m_SmoothViewV[i].m_height<<std::endl;
		else
		  fout<<"v  "<<m_SmoothPathV[i].spos().x()<<" "<<m_SmoothPathV[i].spos().y()<<" "<<m_SmoothViewV[i].m_height<<std::endl;
	}
	fout<<"# "<<m_SmoothViewV.size()<<" vertices"<<std::endl<<std::endl;
	fout<<"g camLine"<<std::endl;
	fout<<"l ";
	for (int i = 1; i <= m_SmoothViewV.size(); ++i)
	{
         fout<<i<<" ";
	}
	fout<<std::endl;
	fout.close();*/
}








