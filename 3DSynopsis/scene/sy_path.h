#pragma once
#ifndef SY_PATH_H
#define SY_PATH_H

#include <vector>
#include <osg/vec3>

class SyPath 
{
public:
	SyPath(osg::Vec3 spos, osg::Vec3 tpos, bool crossFlag, double duration, double distance);
	~SyPath();

	//成员变量get操作
	osg::Vec3    spos() {return m_spos;}
	osg::Vec3    tpos() {return m_tpos;}
	bool         crossFlag() {return m_crossFlag;}                           
	double       duration() {return m_duration;}                         
	double       distance() {return m_distance;}                               
	double       altitude() {return m_altitude;}                                
	double       heading() {return m_heading;}                            
	double       tilt() {return m_tilt;}                             
	//成员变量set操作
	void        setSpos(osg::Vec3 nspos)          {m_spos = nspos;}
	void        setTpos(osg::Vec3 ntpos)          {m_tpos = ntpos;}
	void        setCrossFlag(bool ncrossFlag)     {m_crossFlag = ncrossFlag;}
	void        setDuration(double nduration)     {m_duration = nduration;}
	void        setDistance(double ndistance)     {m_distance = ndistance;}
	void        setAltitude(double naltitude)     {m_altitude = naltitude;}
	void        setHeading(double nheading)       {m_heading = nheading;}
	void        setTilt(double ntilt)             {m_tilt = ntilt;}

	void        setCam(double naltitude,double nheading,double ntilt) {m_altitude = naltitude;m_heading = nheading;m_tilt = ntilt;}

private:
	osg::Vec3                              m_spos;
	osg::Vec3                              m_tpos;
	bool                                   m_crossFlag;
	double                                 m_duration;
	double                                 m_distance;
	double                                 m_altitude;
	double                                 m_heading;
	double                                 m_tilt;
};
#endif //SY_SCENE_H