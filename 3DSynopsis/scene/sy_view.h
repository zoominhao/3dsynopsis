#pragma once
#ifndef SY_VIEW_H
#define SY_VIEW_H

class SyView
{
public:
	SyView() {}
	~SyView() {}
	double m_height;
	double m_tilt;
	double m_heading;
};
#endif