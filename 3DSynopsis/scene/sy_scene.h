#pragma once

#include "renderable.h"
#include "sy_ioManager.h"

class SyScene : public Renderable
{
public:
	SyScene();
	~SyScene();

	//initial & io
	bool setFromFile( const std::string& folder );
	std::vector<SyPath>      SamplePath(){return m_samplePath;}

	//渲染触发器
	bool toggleRenderScene(bool toggle);
	bool toggleRenderSky(bool toggle);
	bool toggleRenderColor(bool toggle);
	bool toggleRenderColorSky(bool toggle);
	bool toggleRenderPath(bool toggle);
	bool toggleRenderCoolPath(bool toggle);
	bool toggleRenderSmoothPath(bool toggle);
	bool toggleRenderCorLines(bool toggle);

	//场景sky旋转和视频wall
	void SkyAnimation();
	osg::ref_ptr<osg::Node> createVideoQuard(int imgNo);

protected:
	virtual void updateImpl();             //重写了父类更新的方法
	void renderScene();
	void renderVieoWall();
	void renderSky();
	void renderColor();
	void renderColorSky();
	void renderPath();
	void renderCoolPath();
	void renderSmoothPath();
	void renderCorLines();

	void renderPanoEye();

	virtual void clearData();              //清除点和曲线	

private:
	bool loadModels(const QString& dirname, int mode);

private:
	std::string                           m_folder;
	//渲染控制
	bool                                  m_showScene;
	bool                                  m_showSky;
	bool                                  m_showColor;
	bool                                  m_showColorSky; 
	bool                                  m_showPath;
	bool                                  m_showCoolPath;
	bool                                  m_showSmoothPath;
	bool                                  m_showCorLines;

	//模型节点
	std::vector<osg::ref_ptr<osg::Node>>  m_scenenode;
	std::vector<osg::ref_ptr<osg::Node>>  m_colornode;
	osg::ref_ptr<osg::Node>               m_skynode;
	osg::ref_ptr<osg::Node>               m_colorskynode;
	osg::ref_ptr<osg::MatrixTransform>    m_skytransform;
	osg::ref_ptr<osg::Node>               m_video_node;

	std::vector<SyPath>                   m_samplePath;

	SyIoManager                           m_IOManager;
};