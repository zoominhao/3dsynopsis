#pragma once

#include "renderable.h"
#include "sy_ioManager.h"
#include "eigen_types.h"

class SyCamera : public Renderable
{
public:
	SyCamera();
	~SyCamera();


	//io操作
	bool setFromFile( const std::string& folder );
	//打开渲染
	bool toggleRenderCamera(bool toggle);
	bool toggleRenderCameraPath(bool toggle);
	bool toggleRenderCorLines(bool toggle);
	bool toggleRenderVisualPorsche(bool toggle);

	void setCenter(osg::Vec3 cen){m_cen = cen;};
	osg::Vec3 center() { return m_cen;}
protected:
	virtual void updateImpl();             //重写了父类更新的方法
	//void renderCamera();
	void renderCameraPath();
	void renderCamera();
	void renderCorLines();
	void renderVisulPorches();

	virtual void clearData();              //清除点和曲线	

private:
	bool loadSamplePath();
	bool readViewV();
	bool readSpeedV();
	bool readCamPreV();

	//存储相机路径
	void saveCameraRoute();   //保存成obj

private:
	std::string                           m_folder;
	bool                                  m_showCamera;
	bool                                  m_showCameraPath;
	bool                                  m_showCorLines;
	bool                                  m_showVisualPorsche;
	//scale
	osg::Vec3f                            m_scale;
	osg::Vec3f                            m_car_scale;
	//相机中心
	osg::Vec3                             m_cen;    
	//模型节点
	osg::ref_ptr<osg::Node>               m_camera;
	osg::ref_ptr<osg::Node >              m_porsche;
	//IO 控制
	SyIoManager                           m_IOManager;
	//数据存储
	std::vector<std::pair<SyView,SyView>> m_views;       
	std::vector<double>                   m_speedV; 
	std::vector<SyPath>                   m_samplePath;
	std::vector<SyPath>                   m_ssamplePath;
	std::vector<EV2>                      m_camPreCordsV;
};