#pragma once
#ifndef SY_IOMANAGER_H
#define SY_IOMANAGER_H


#include "sy_path.h"
#include "sy_view.h"
#include "eigen_types.h"

#include <QString>

class SyIoManager
{
public:
	SyIoManager();
	~SyIoManager();

	//读路径文件
	bool loadSamplePath( const QString& filename, std::vector<SyPath>& pathV);
	bool saveSamplePath( const QString& filename, std::vector<SyPath>& pathV);  //用不到这个方法，备用

	//外部光滑路径
	bool writeSPath( const QString& filename, std::vector<EV2>& pathV);
	bool readSPath( const QString& filename, std::vector<EV2>& pathV);

	//转化xml
	bool writeSPath( const QString& filename, std::vector<EV3>& pathV);

	//读写相机视角文件
	bool readView( const QString& filename, std::vector<SyView>& viewV);
	bool writeView( const QString& filename, const std::vector<SyView>& viewV );

	bool readView( const QString& filename, std::vector<std::pair<SyView,SyView>>& viewV);
	bool writeView( const QString& filename, const std::vector<std::pair<SyView,SyView>>& viewV );
	//读取相机后退向量文件
	bool readCamDis( const QString& filename, std::vector<EV2>& camDisV );
	bool writeCamDis( const QString& filename, const std::vector<EV2>& camDisV );
	//读写速度文件
	bool readSpeed( const QString& filename, std::vector<double>& speedV);
	bool writSpeed( const QString& filename, const std::vector<double>& speedV);
	//读写重要性文件
	bool readImportance( const QString& filename, std::vector<double>& importanceV);
	bool writeImportance( const QString& filename, const std::vector<double>& importanceV );
	//存取cross文件
	bool writeCross( const QString& filename, const std::vector<int>& crossV );
	//存取分段文件
	bool writeSection( const QString& filename, const std::vector<EV2>& sectionV );
};

#endif  //SY_IOMANAGER_H