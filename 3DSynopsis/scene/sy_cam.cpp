#include "sy_cam.h"
#include "file_system.h"
#include "global_constant.h"
#include "osg_viewer_widget.h"
#include "parameter_manager.h"
#include "osg_utility.h"
#include "global_func.h"

#include <osgDB/ReadFile>
#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowMap>
#include <QFileInfo>
#include <QTextStream>

SyCam::SyCam()
	:m_folder(""),
	m_showDCCam(true),
	m_scale(0.005,0.005,0.005)
{

}

SyCam::~SyCam()
{

}

bool SyCam::setFromFile( const std::string& folder )
{
	std::string dcCamModel = folder + GConstant::C_CAMERA;
	std::string extension = FileSystem::extension( dcCamModel );
	m_folder = folder;
	bool flag = false;
	if( extension == GConstant::C_SCENE_MODEL_EXTENSION)
	{ 
		QFileInfo dcCamfile(dcCamModel.c_str());
		if(dcCamfile.exists())
		{
			flag = true;
			m_dcCam = osgDB::readNodeFile( dcCamModel );
			expire();
		}
	}
	return flag;
}


bool SyCam::toggleRenderDCCam(bool toggle)
{
	if (m_showDCCam == toggle)
		return false;

	m_showDCCam = toggle;

	expire();
	return true;
}

void SyCam::updateImpl()
{
	if (m_showDCCam)
	{
		renderDCCam();
	}
}

void SyCam::renderDCCam()
{
	if (m_dcCam!=NULL)
	{
		osg::Matrix mscale;
		mscale.makeScale(m_scale);
		osg::MatrixTransform *nmat = new osg::MatrixTransform;
		nmat->setMatrix(mscale);
		nmat->addChild(m_dcCam);
		addChild(nmat);
	}

}

void SyCam::clearData()
{
	//todo
}
