#pragma once
#ifndef SY_CAR_H
#define SY_CAR_H

#include <vector>

#include "renderable.h"

class SyCar : public Renderable
{
public:
	SyCar();
	~SyCar();

	//io操作
	bool setFromFile( const std::string& folder );
	bool toggleRenderPorsche(bool toggle);
	void setCenter(osg::Vec3 cen){m_cen = cen;};
	osg::Vec3 center() { return m_cen;}
	osg::Vec3f scaleV() {return m_scale;}

protected:
	virtual void updateImpl();             //重写了父类更新的方法
	void renderPorsche();
	virtual void clearData();              //清除点和曲线	
private:

 
private:
	std::string                           m_folder;
	bool                                  m_showPorsche;
	osg::ref_ptr<osg::Node >              m_porsche;
	osg::Vec3                             m_cen;    //汽车中心
	osg::Vec3f                            m_scale;

};
#endif //SY_CAR_H