#pragma once

#include <osg/Node>
#include <osg/TextureCubeMap>
#include <osg/MatrixTransform>

class UpdateShader:public osg::Uniform::Callback
{
public:
	UpdateShader(void) : time(0.0), dir(true){};
	virtual void operator()(osg::Uniform* uniform, osg::NodeVisitor* nv)
	{
		time += 0.03;
		uniform->set( time );
	}
	~UpdateShader(void){}
	bool dir;
	float time;
};

class SyWater: public osg::MatrixTransform
{
public:
	SyWater();
	~SyWater(void);
	bool setFromFile( const std::string& folder );
	bool toggleRenderWater(bool toggle);
private:
	void renderWater();
	osg::ref_ptr<osg::Node> createWaterNode(int sceneMode);
	osg::TextureCubeMap* readCubeMap();
private:
    std::string							 m_folder;
	bool								 m_show_water;

	osg::ref_ptr<osg::Node>              m_sz_water;
	osg::ref_ptr<osg::Node>              m_sh_water;
	osg::ref_ptr<osg::Node>              m_highway_water;
	osg::ref_ptr<osg::Node>              m_highway_water2;
	osg::ref_ptr<osg::Node>              m_highway_water3;
};

