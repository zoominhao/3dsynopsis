#include "sy_scene.h"

#include "global_constant.h"
#include "global_func.h"
#include "parameter_manager.h"
#include "sy_config.h"
#include "osg_utility.h"

#include <QFileInfo>
#include <QDir>
#include <osgDB/ReadFile>
#include <osg/ImageSequence>

SyScene::SyScene()
	:m_folder(""),
	m_showScene(true),
	m_showSky(true),
	m_showColor(false),
	m_showPath(true),
	m_showCoolPath(false),
	m_showSmoothPath(false),
	m_showCorLines(false),
	m_showColorSky(false)
{

}

SyScene::~SyScene()
{

}

bool SyScene::setFromFile( const std::string& folder )
{
	std::string scenem = folder + GConstant::C_SCENE_MODEL;
	std::string colorm = folder + GConstant::C_COLOR_MODEL;
	std::string skym = folder + GConstant::C_SCENE_SKY;
	std::string colorskym = folder + GConstant::C_COLOR_SKY;

	std::string samplepath = folder + GConstant::C_SAMPLE_PATH;


	m_folder = folder;
	bool flag = false;

	QFileInfo skyfile(skym.c_str());
	QFileInfo colorskyfile(colorskym.c_str());
	QFileInfo samplepathfile(samplepath.c_str());
	

	if( skyfile.exists()  && colorskyfile.exists() && samplepathfile.exists() )
	{
		m_skynode = osgDB::readNodeFile( skym );
		m_colorskynode = osgDB::readNodeFile(colorskym);
		m_skytransform = new osg::MatrixTransform();
		m_skytransform->addChild(m_skynode);
		m_IOManager.loadSamplePath(samplepath.c_str(), m_samplePath);
	}
	//取出纹理模型
	flag = loadModels(scenem.c_str(),1);
	//取出颜色模型
	flag &= loadModels(colorm.c_str(),2);

	expire();

	return flag;
}

bool SyScene::loadModels( const QString& dirname, int mode )
{
	QDir scenedir(dirname);
	if (!scenedir.exists())
		return false;
	scenedir.setFilter(QDir::Dirs|QDir::Files);
	scenedir.setSorting(QDir::DirsFirst);
	QFileInfoList list = scenedir.entryInfoList();
	bool flag = false;
	int i = 0;
	do{
		QFileInfo fileInfo = list.at(i);
		i++;
		if(fileInfo.fileName()=="."|fileInfo.fileName()=="..")
			continue;
		if (fileInfo.isFile())
		{
			std::string extension =fileInfo.suffix().toStdString();
			if(extension == GConstant::C_SCENE_MODEL_EXTENSION)
			{
				osg::ref_ptr<osg::Node> nScenenode = osgDB::readNodeFile( fileInfo.filePath().toStdString()); 
				if (mode==1)
					m_scenenode.push_back(nScenenode);
				else if (mode==2)
					m_colornode.push_back(nScenenode);
				flag = true;
			}
		}
	}
	while(i<list.size());
	return flag;
}

bool SyScene::toggleRenderScene( bool toggle )
{
	if (m_showScene == toggle)
		return false;

	m_showScene = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderSky( bool toggle )
{
	if (m_showSky == toggle)
		return false;

	m_showSky = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderColor( bool toggle )
{
	if (m_showColor == toggle)
		return false;

	m_showColor = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderColorSky( bool toggle )
{
	if (m_showColorSky == toggle)
		return false;

	m_showColorSky = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderPath( bool toggle )
{
	if (m_showPath == toggle)
		return false;

	m_showPath = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderCoolPath( bool toggle )
{
	if (m_showCoolPath == toggle)
		return false;

	m_showCoolPath = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderSmoothPath( bool toggle )
{
	if (m_showSmoothPath == toggle)
		return false;

	m_showSmoothPath = toggle;

	expire();
	return true;
}

bool SyScene::toggleRenderCorLines( bool toggle )
{
	if (m_showCorLines == toggle)
		return false;

	m_showCorLines = toggle;

	expire();
	return true;
}

void SyScene::updateImpl()
{
	if( m_showScene )
	{
		renderScene();
		//video wall
		renderVieoWall();
	}
	if (m_showSky)
	{
		renderSky();
	}
	if (m_showColor)
	{
		renderColor();
	}
	if (m_showColorSky)
	{
		renderColorSky();
	}
	if (m_showPath)
	{
		renderPath();
	}
	if(m_showCoolPath)
	{
		renderCoolPath();
	}
	if (m_showSmoothPath)
	{
		renderSmoothPath();
	}
	if (m_showCorLines)
	{
		renderCorLines();
	}
}

void SyScene::renderScene()
{
	for (int i = 0; i < m_scenenode.size(); ++i)
	{
		addChild(m_scenenode[i]);
	}
}

void SyScene::renderVieoWall()
{
	if (ParameterManager::getInstance().getVideoMode())
	{
		int static imgcount = 0;
		int endtime = clock();
		if(endtime % (int(SyConfig::getInstance().getVideoDeta()/2)) == 0)
		{
			addChild(createVideoQuard((imgcount)%150));
			imgcount++;
		}
		else
		{
			addChild(createVideoQuard((imgcount)%150));
		}
	}
	else
	{
		static int imgcount = 0;
		addChild(createVideoQuard((imgcount)%150));
		imgcount++;
	}
}

osg::ref_ptr<osg::Node> SyScene::createVideoQuard( int imgNo )
{
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;

	if(SyConfig::getInstance().getWhichCity()==1)
	{
		//深圳
		v->push_back(osg::Vec3(23521.9727,5.5044,-10618.1641));
		v->push_back(osg::Vec3(23512.0117,5.5044,-10612.9941));
		v->push_back(osg::Vec3(23512.0117,16.4144,-10612.9941));
		v->push_back(osg::Vec3(23521.9727,16.4144,-10618.1641));
	}
	else if(SyConfig::getInstance().getWhichCity()==2)
	{
		//上海
		v->push_back(osg::Vec3(-221.5056,-22.8902,8.2308));
		v->push_back(osg::Vec3(-232.5284,-15.7749,8.2308));
		v->push_back(osg::Vec3(-232.5793,-15.8538,19.7770));
		v->push_back(osg::Vec3(-221.5565,-22.9691,19.7770));
	}

	geom->setVertexArray(v.get());


	osg::ref_ptr<osg::Vec2Array> vt = new osg::Vec2Array;
	vt->push_back(osg::Vec2(0.0f, 0.0f));
	vt->push_back(osg::Vec2(1.0f, 0.0f));
	vt->push_back(osg::Vec2(1.0f, 1.0f));
	vt->push_back(osg::Vec2(0.0f, 1.0f));
	geom->setTexCoordArray(0, vt.get());

	osg::ref_ptr<osg::Vec3Array> n = new osg::Vec3Array;
	geom->setNormalArray(n.get());
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);
	n->push_back(osg::Vec3(0.f, -1.f, 0.f));

	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));
	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	geode->addDrawable(geom.get());

	osg::StateSet* stateset = new osg::StateSet;

	std::string videoFramePath = m_folder + "\\model\\video\\"+ GLOBAL::int2string(imgNo) + ".jpg";
	osg::Image* videoFrame = osgDB::readImageFile(videoFramePath);

	osg::ref_ptr<osg::Texture2D> texture = new osg::Texture2D();
	texture->setImage(videoFrame);

	stateset->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	geode->setStateSet(stateset);
	osg::ref_ptr<osg::MatrixTransform> rotate = new osg::MatrixTransform;
	osg::Quat quat;
	quat.makeRotate(osg::DegreesToRadians(90.0), osg::Vec3d(1.0, 0.0, 0.0));//3dmax 的坐标轴和osg的坐标轴不一样，故需要对节点进行旋转
	rotate->setMatrix(osg::Matrix::rotate(quat));
	rotate->addChild(geode.get());

	if(SyConfig::getInstance().getWhichCity()==1)
		return rotate.get();
	else if(SyConfig::getInstance().getWhichCity()==2)
		return geode.get();
	else
		return geode.get();
}

void SyScene::renderSky()
{
	if (m_skynode!=NULL)
		addChild(m_skytransform);
}

void SyScene::renderColor()
{
	for (int i = 0; i < m_colornode.size(); ++i)
	{
		addChild(m_colornode[i]);
	}
}

void SyScene::renderColorSky()
{
	if (m_colorskynode!=NULL)
		addChild(m_colorskynode);
}

void SyScene::renderPath()
{
	if (m_samplePath.size()!=0)
	{
		int nodecount = 0;
		EMatXd nodesMat(m_samplePath.size()+1,3);
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			nodesMat(i,0) = m_samplePath[i].spos()[0];
			nodesMat(i,1) = m_samplePath[i].spos()[1];
			nodesMat(i,2) = m_samplePath[i].spos()[2]+0.5;
			if (m_samplePath[i].crossFlag())
				nodecount++;
		}
		nodesMat(m_samplePath.size(),0) = m_samplePath[m_samplePath.size()-1].tpos()[0];
		nodesMat(m_samplePath.size(),1) = m_samplePath[m_samplePath.size()-1].tpos()[1];
		nodesMat(m_samplePath.size(),2) = m_samplePath[m_samplePath.size()-1].tpos()[2]+0.5;
		//draw segments
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, osg::Vec4(0.0,0.0,1.0,1.0), 5);
		addChild(sceneroute);
		//draw nodes
		/*osg::Geode* sceneNodes = OSGUtility::drawPoints(nodesMat, osg::Vec4(1.0,0.0,0.0,0.0));
		addChild(sceneNodes);*/

		EMatXd gnodesMat(nodecount,3);
		EMatXd rnodesMat(m_samplePath.size()-nodecount,3);   //最后一个点不绘制
		int gindex = 0;
		int rindex = 0;
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			if (m_samplePath[i].crossFlag())
			{
				gnodesMat(gindex,0) = m_samplePath[i].spos()[0];
				gnodesMat(gindex,1) = m_samplePath[i].spos()[1];
				gnodesMat(gindex,2) = m_samplePath[i].spos()[2]+0.5;
				gindex++;
			}
			else
			{
				rnodesMat(rindex,0) = m_samplePath[i].spos()[0];
				rnodesMat(rindex,1) = m_samplePath[i].spos()[1];
				rnodesMat(rindex,2) = m_samplePath[i].spos()[2]+0.5;
				rindex++;
			}
		}
		osg::Geode* scenegNodes = OSGUtility::drawPoints(gnodesMat, osg::Vec4(0.0,1.0,0.0,0.0));
		addChild(scenegNodes);
		osg::Geode* scenerNodes = OSGUtility::drawPoints(rnodesMat, osg::Vec4(1.0,0.0,0.0,0.0));
		addChild(scenerNodes);
	}
}

void SyScene::renderCoolPath()
{
	if (m_samplePath.size()!=0)
	{
		EMatXd nodesMat(m_samplePath.size()+1,3);
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			nodesMat(i,0) = m_samplePath[i].spos()[0];
			nodesMat(i,1) = m_samplePath[i].spos()[1];
			nodesMat(i,2) = m_samplePath[i].spos()[2]+0.5;
		}
		nodesMat(m_samplePath.size(),0) = m_samplePath[m_samplePath.size()-1].tpos()[0];
		nodesMat(m_samplePath.size(),1) = m_samplePath[m_samplePath.size()-1].tpos()[1];
		nodesMat(m_samplePath.size(),2) = m_samplePath[m_samplePath.size()-1].tpos()[2]+0.5;
		//draw segments
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, osg::Vec4(0.0,0.0,1.0,0.5), 10);
		addChild(sceneroute);

		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			if (i%5 == 0)
			{
				osg::Vec3 sp(m_samplePath[i].spos()[0], m_samplePath[i].spos()[1],m_samplePath[i].spos()[2]);
				osg::Vec3 tp(m_samplePath[i].tpos()[0], m_samplePath[i].tpos()[1],m_samplePath[i].tpos()[2]);
				osg::Geode* scenerArrows = OSGUtility::drawTriangle(sp, tp, 3, osg::Vec4(0.2,0.8,0.2,0.5));
				addChild(scenerArrows);
			}
		}
	}
}

void SyScene::renderSmoothPath()
{
	std::vector<SyPath>   smoothPath;
	if (ParameterManager::getInstance().getDrivingMode() == 1)
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SCSAMPLE_PATH).c_str(), smoothPath);
	if (ParameterManager::getInstance().getDrivingMode() == 5)
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SJSAMPLE_PATH).c_str(), smoothPath);
	else
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SSAMPLE_PATH).c_str(), smoothPath);
	 

	if (smoothPath.size()!=0)
	{
		int nodecount = 0;
		EMatXd nodesMat(smoothPath.size()+1,3);
		for (int i = 0; i < smoothPath.size(); ++i)
		{
			nodesMat(i,0) = smoothPath[i].spos()[0];
			nodesMat(i,1) = smoothPath[i].spos()[1];
			nodesMat(i,2) = smoothPath[i].spos()[2]+0.5;
			if (smoothPath[i].crossFlag())
				nodecount++;
		}
		nodesMat(smoothPath.size(),0) = smoothPath[smoothPath.size()-1].tpos()[0];
		nodesMat(smoothPath.size(),1) = smoothPath[smoothPath.size()-1].tpos()[1];
		nodesMat(smoothPath.size(),2) = smoothPath[smoothPath.size()-1].tpos()[2]+0.5;
		//draw segments
		osg::Geode* sceneroute = OSGUtility::drawLines(nodesMat, osg::Vec4(0.0,0.0,1.0,1.0), 5);
		addChild(sceneroute);
		//draw nodes
		/*osg::Geode* sceneNodes = OSGUtility::drawPoints(nodesMat, osg::Vec4(1.0,0.0,0.0,0.0));
		addChild(sceneNodes);*/

		EMatXd gnodesMat(nodecount,3);
		EMatXd rnodesMat(smoothPath.size()-nodecount,3);   //最后一个点不绘制
		int gindex = 0;
		int rindex = 0;
		for (int i = 0; i < smoothPath.size(); ++i)
		{
			if (smoothPath[i].crossFlag())
			{
				gnodesMat(gindex,0) = smoothPath[i].spos()[0];
				gnodesMat(gindex,1) = smoothPath[i].spos()[1];
				gnodesMat(gindex,2) = smoothPath[i].spos()[2]+0.5;
				gindex++;
			}
			else
			{
				rnodesMat(rindex,0) = smoothPath[i].spos()[0];
				rnodesMat(rindex,1) = smoothPath[i].spos()[1];
				rnodesMat(rindex,2) = smoothPath[i].spos()[2]+0.5;
				rindex++;
			}
		}
		osg::Geode* scenegNodes = OSGUtility::drawPoints(gnodesMat, osg::Vec4(0.0,1.0,0.0,0.0));
		addChild(scenegNodes);
		osg::Geode* scenerNodes = OSGUtility::drawPoints(rnodesMat, osg::Vec4(1.0,0.0,0.0,0.0));
		addChild(scenerNodes);
	}
}

void SyScene::renderCorLines()
{
	std::vector<SyPath>   smoothPath;
	if (ParameterManager::getInstance().getDrivingMode() == 1)
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SCSAMPLE_PATH).c_str(), smoothPath);
	if (ParameterManager::getInstance().getDrivingMode() == 5)
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SJSAMPLE_PATH).c_str(), smoothPath);
	else
		m_IOManager.loadSamplePath((m_folder + GConstant::C_SSAMPLE_PATH).c_str(), smoothPath);

	if (smoothPath.size()!=0&&m_samplePath.size()!=0)
	{
		osg::Vec4 oColor(1.0,1.0,0.0,0.0);   //yellow
		for (int i = 0; i < smoothPath.size(); i+=ParameterManager::getInstance().getRenderDensity())
		{
			osg::Vec3 top(m_samplePath[i].spos()[0],m_samplePath[i].spos()[1],m_samplePath[i].spos()[2]); 
			osg::Vec3 base(smoothPath[i].spos()[0],smoothPath[i].spos()[1],smoothPath[i].spos()[2]); 
			osg::Geode* sceneCor = OSGUtility::drawCylinder(top, base, 0.5, oColor);
			addChild(sceneCor);
		}
		osg::Vec3 top(m_samplePath[m_samplePath.size()-1].tpos()[0],m_samplePath[m_samplePath.size()-1].tpos()[1],m_samplePath[m_samplePath.size()-1].tpos()[2]); 
		osg::Vec3 base(smoothPath[smoothPath.size()-1].tpos()[0],smoothPath[smoothPath.size()-1].tpos()[1],smoothPath[smoothPath.size()-1].tpos()[2]); 
		osg::Geode* sceneCor = OSGUtility::drawCylinder(top, base, 0.5, oColor);
		addChild(sceneCor);
	}
}

void SyScene::renderPanoEye()
{
	osg::Geode* panoEyeNodes = OSGUtility::drawSphere(osg::Vec3(-90.8077,-13.0295,34.8157), 5, osg::Vec4(1.0,0.0,0.0,0.0));
	addChild(panoEyeNodes);
}

void SyScene::clearData()
{
	//todo
}

void SyScene::SkyAnimation()
{
	double detaAngle = 0.1;
	//if (ParameterManager::getInstance().getVideoMode())
	// detaAngle = 0.005;

	osg::Matrix originmat = m_skytransform->getMatrix();
	//旋转sky操作,沿着垂直于地面的轴线，Z轴
	//创建变换实例
	osg::Matrixd rotateMat,toriginMat,tocenMat;
	//移动到原点
	osg::Vec3 sky_cen = m_skytransform->getBound().center();
	toriginMat.makeTranslate(-sky_cen);
	//旋转
	rotateMat.makeRotate(osg::PI*(detaAngle/180.0), osg::Vec3f(0,0,1));
	//移动回中心
	tocenMat.makeTranslate(sky_cen);
	m_skytransform->setMatrix(originmat*toriginMat*rotateMat*tocenMat);
	expire();
}
