#include "sy_config.h"
#include <QFile>
#include <QDomDocument>

SyConfig::SyConfig()
{

}

SyConfig::~SyConfig()
{

}

bool SyConfig::loadConfig( const QString& filename )
{
	clear();
	std::string str = filename.toStdString();

	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		return false;
	}

	QDomDocument doc("configParas");
	if (!doc.setContent(&file)) {
		file.close();
		return false;
	}
	QDomElement root = doc.documentElement();

	m_which_city = root.firstChildElement("WhichCity").attribute("value", "").toInt();
	m_detaTime = root.firstChildElement("Deta_Time").attribute("value", "").toDouble();
	m_sampleInterval = root.firstChildElement("Sample_Interval").attribute("value", "").toDouble();
	m_depthThrehold = root.firstChildElement("Depth_Threhold").attribute("value", "").toFloat();
	m_videoDeta = root.firstChildElement("Video_deta").attribute("value", "").toFloat();
	m_initHeight = root.firstChildElement("Init_Height").attribute("value", "").toFloat();

	QDomNodeList citylist = root.firstChildElement("City").childNodes();
	for(int i = 0;i < citylist.count();i++) {  
		QDomNode node = citylist.at(i);  
		int sindex = node.toElement().attribute("sindex").toInt(); 
		int tindex = node.toElement().attribute("tindex").toInt(); 
		m_cityRange.push_back(std::make_pair(sindex,tindex));
	}
	
	QDomNodeList ramplist = root.firstChildElement("Ramp").childNodes();
	for(int i = 0;i < ramplist.count();i++) {  
		QDomNode node = ramplist.at(i);  
		int sindex = node.toElement().attribute("sindex").toInt(); 
		int tindex = node.toElement().attribute("tindex").toInt(); 
		m_rampRange.push_back(std::make_pair(sindex,tindex));
	}

	QDomNodeList hwlist = root.firstChildElement("HighWay").childNodes();
	for(int i = 0;i < hwlist.count();i++) {  
		QDomNode node = hwlist.at(i);  
		int sindex = node.toElement().attribute("sindex").toInt(); 
		int tindex = node.toElement().attribute("tindex").toInt(); 
		m_highwayRange.push_back(std::make_pair(sindex,tindex));
	}

	m_hw_weight = root.firstChildElement("Hw_Weight").attribute("value", "").toFloat();
	m_hw_cross_weight = root.firstChildElement("Hw_CROSS_Weight").attribute("value", "").toFloat();
	m_ramp_weight = root.firstChildElement("Ramp_Weight").attribute("value", "").toFloat();
	m_ramp_cross_weight = root.firstChildElement("Ramp_CROSS_Weight").attribute("value", "").toFloat();
	m_city_weight = root.firstChildElement("City_Weight").attribute("value", "").toFloat();
	m_city_cross_weight = root.firstChildElement("City_Cross_Weight").attribute("value", "").toFloat();

	return true;
}

void  SyConfig::setWhichCity(const int whichCity)
{
	m_which_city = whichCity;
}

void SyConfig::setDetaTime( const double detaTime )
{
	m_detaTime = detaTime;
}

void SyConfig::setSampleInterval( const double sampleInterval )
{
	m_sampleInterval = sampleInterval;
}


void SyConfig::setDepthThrehold( const float depthThrehold )
{
	m_depthThrehold = depthThrehold;
}

void SyConfig::setVideoDeta(const float videoDeta)
{
	m_videoDeta = videoDeta;
}

void SyConfig::setInitHeight(const float initHeight)
{
	m_initHeight = initHeight;
}

void SyConfig::setHw_weight( const float hw_weight )
{
	m_hw_weight = hw_weight;
}

void SyConfig::setHw_cross_weight( const float hw_cross_weight )
{
	m_hw_cross_weight = hw_cross_weight;
}

void SyConfig::setRamp_weight( const float ramp_weight )
{
	m_ramp_weight = ramp_weight;
}

void SyConfig::setRamp_cross_weight( const float ramp_cross_weight )
{
	m_ramp_cross_weight = ramp_cross_weight;
}

void SyConfig::setCity_weight( const float city_weight )
{
	m_city_weight = city_weight;
}

void SyConfig::setCity_cross_weight( const float city_cross_weight )
{
	m_city_cross_weight = city_cross_weight;
}

void SyConfig::setCityRange( const std::vector<std::pair<int,int>> cityRange )
{
	m_cityRange = cityRange;
}

void SyConfig::setRampRange( const std::vector<std::pair<int,int>> rampRange )
{
	m_rampRange = rampRange;
}

void SyConfig::setHighwayRange( const std::vector<std::pair<int,int>> highwayRange )
{
	m_highwayRange = highwayRange;
}

int SyConfig::getWhichCity() const
{
	return m_which_city;
}

double SyConfig::getDetaTime() const
{
	return m_detaTime;
}

double SyConfig::getSampleInterval() const
{
	return m_sampleInterval;
}

float SyConfig::getDepthThrehold() const
{
	return m_depthThrehold; 
}

float SyConfig::getVideoDeta() const
{
	return m_videoDeta; 
}

float SyConfig::getInitHeight() const
{
	return m_initHeight;
}

float SyConfig::getHw_weight() const
{
	return m_hw_weight;
}

float SyConfig::getHw_cross_weight() const
{
	return m_hw_cross_weight;
}

float SyConfig::getRamp_weight() const
{
	return m_ramp_weight;
}

float SyConfig::getRamp_cross_weight() const
{
	return m_ramp_cross_weight;
}

float SyConfig::getCity_weight() const
{
	return m_city_weight;
}

float SyConfig::getCity_cross_weight() const
{
	return m_city_cross_weight;
}

std::vector<std::pair<int,int>> SyConfig::getCityRange() const
{
	return m_cityRange;
}

std::vector<std::pair<int,int>> SyConfig::getRampRange() const
{
	return m_rampRange;
}

std::vector<std::pair<int,int>> SyConfig::getHighwayRange() const
{
	return m_highwayRange;
}

void SyConfig::clear()
{
	m_highwayRange.clear();
	m_rampRange.clear();
	m_cityRange.clear();
}

