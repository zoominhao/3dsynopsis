#include "sy_ioManager.h"

#include <QDir>
#include <QDomElement>
#include <QDomDocument>
#include <QTextStream>
#include <QFileInfo>
#include <QFile>
#include <fstream>
#include <iostream>

SyIoManager::SyIoManager()
{

}


SyIoManager::~SyIoManager()
{

}


bool SyIoManager::loadSamplePath( const QString& filename, std::vector<SyPath>& pathV )
{
	pathV.clear();
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		return false;
	}

	QDomDocument doc("paths");
	if (!doc.setContent(&file)) {
		file.close();
		return false;
	}
	QDomElement root = doc.documentElement();
	QDomNodeList seglist = root.childNodes();
	for(int i = 0;i < seglist.count();i++) {  
		QDomNode node = seglist.at(i);  
		double sx = node.toElement().attribute("sx").toDouble(); 
		double sy = node.toElement().attribute("sy").toDouble(); 
		double sz = node.toElement().attribute("sz").toDouble(); 
		osg::Vec3 spos(sx,sy,sz);
		double tx = node.toElement().attribute("tx").toDouble(); 
		double ty = node.toElement().attribute("ty").toDouble(); 
		double tz = node.toElement().attribute("tz").toDouble(); 
		osg::Vec3 tpos(tx,ty,tz);
		int crossFlag = node.toElement().attribute("CrossFlag").toInt();
		bool iscross = crossFlag==1?true:false;
		double duration = node.toElement().attribute("Duration").toDouble();
		double distance = node.toElement().attribute("Distance").toDouble();
		SyPath samplePath(spos,tpos,iscross,duration,distance);
		pathV.push_back(samplePath);
	}  
	return true;
}

bool SyIoManager::saveSamplePath( const QString& filename, std::vector<SyPath>& pathV)
{	
	QFile file(filename);
	if (!file.open(QIODevice::WriteOnly)) {
		return false;
	}

	QDomDocument doc("paths");
	QDomProcessingInstruction xml_declaration = doc.createProcessingInstruction("xml", "version=\"1.0\"");
	doc.appendChild(xml_declaration);

	QDomElement root = doc.createElement(QString("paths"));
	doc.appendChild(root);

	for (int i = 0; i < pathV.size(); ++i)
	{
		QDomElement nseg = doc.createElement(QString("seg"));
		nseg.setAttribute("sx", (double)(pathV[i].spos().x()));
		nseg.setAttribute("sy", (double)(pathV[i].spos().y()));
		nseg.setAttribute("sz", (double)(pathV[i].spos().z()));
		nseg.setAttribute("tx", (double)(pathV[i].tpos().x()));
		nseg.setAttribute("ty", (double)(pathV[i].tpos().y()));
		nseg.setAttribute("tz", (double)(pathV[i].tpos().z()));
		nseg.setAttribute("CrossFlag", (int)(pathV[i].crossFlag()));
		nseg.setAttribute("Duration", (double)(pathV[i].duration()));
		nseg.setAttribute("Distance", (double)(pathV[i].distance()));
		root.appendChild(nseg);
	}


	QTextStream text_stream(&file);
	text_stream << doc.toString();
	file.close();
	return true;
}

bool SyIoManager::writeSPath( const QString& filename, std::vector<EV2>& pathV)
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (pathV.size()==0)
	{
		std::cout<<"smooth pathV no data!\n";
		return false;
	}
	for (int i = 0; i < pathV.size()-1; ++i)
	{
		fout<<pathV[i].x()<<" "<<pathV[i].y()<<std::endl;  
	}
	fout<<pathV[pathV.size()-1].x()<<" "<<pathV[pathV.size()-1].y(); //防止多出一行空白
	fout.close();
	return true;
}

bool SyIoManager::readSPath(const QString& filename, std::vector<EV2>& pathV)
{
	pathV.clear();
	std::ifstream fin(filename.toStdString(),std::ios::binary);
	if (!fin)
	{
		std::cout<<"smooth route doesn't exist!\n";
		return false;
	}
	while(!fin.eof())
	{
		double xpos,ypos;
		fin>>xpos>>ypos;
		pathV.push_back(EV2(xpos,ypos));
	}
	fin.close();
	return true;
}

bool SyIoManager::writeSPath( const QString& filename, std::vector<EV3>& pathV)
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (pathV.size()==0)
	{
		std::cout<<"smooth pathV no data!\n";
		return false;
	}
	for (int i = 0; i < pathV.size()-1; ++i)
	{
		fout<<pathV[i].x()<<" "<<pathV[i].y()<<" "<<pathV[i].z()<<std::endl;  
	}
	fout<<pathV[pathV.size()-1].x()<<" "<<pathV[pathV.size()-1].y()<<" "<<pathV[pathV.size()-1].z(); //防止多出一行空白
	fout.close();
	return true;
}

bool SyIoManager::readView( const QString& filename, std::vector<SyView>& viewV )
{
	viewV.clear();
	std::ifstream fin(filename.toStdString(),std::ios::binary);
	if(!fin){
		std::cout<<"view file doesn't exist!\n";
		return false;
	}

	while(!fin.eof())
	{
		double heading,tilt,height;
		SyView nview;
		fin>>heading>>tilt>>height;

		nview.m_heading = heading;
		nview.m_tilt = tilt;
		nview.m_height = height;
		viewV.push_back(nview);
	}
	fin.close();
	return true;
}

bool SyIoManager::writeView( const QString& filename, const std::vector<SyView>& viewV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (viewV.size()==0)
	{
		std::cout<<"viewV no data!\n";
		return false;
	}
	for (int i = 0; i < viewV.size()-1; ++i)
	{
		fout<<viewV[i].m_heading<<" "<<viewV[i].m_tilt<<" "<<viewV[i].m_height<<std::endl;
	}
	fout<<viewV[viewV.size()-1].m_heading<<" "<<viewV[viewV.size()-1].m_tilt<<" "<<viewV[viewV.size()-1].m_height; //防止多出一行空白
	fout.close();
	return true;
}

bool SyIoManager::readView( const QString& filename, std::vector<std::pair<SyView,SyView>> & viewV)
{
	viewV.clear();
	std::vector<SyView> tmpViewV;
	if (readView( filename , tmpViewV ))
	{
		for (int i = 0; i < tmpViewV.size()-1; ++i)
		{
			viewV.push_back(std::make_pair(tmpViewV[i],tmpViewV[i+1]));
		}
		return true;
	}
	else
		return false;
}

bool SyIoManager::writeView( const QString& filename, const std::vector<std::pair<SyView,SyView>>& viewV )
{
	if (viewV.size()==0)
	{
		std::cout<<"viewV no data!\n";
		return false;
	}
	std::vector<SyView> tmpViewV;
	for (int i = 0; i < viewV.size(); ++i)
	{
		tmpViewV.push_back(viewV[i].first);
	}
	tmpViewV.push_back(viewV[viewV.size()-1].second);
	return writeView( filename, viewV );
}

bool SyIoManager::readCamDis( const QString& filename, std::vector<EV2>& camDisV )
{
	camDisV.clear();
	std::ifstream fin(filename.toStdString(),std::ios::binary);
	if(!fin){
		std::cout<<"camdis file doesn't exist!\n";
		return false;
	}

	while(!fin.eof())
	{
		double prex,prey;
		fin>>prex>>prey;
		EV2 npre(prex,prey);
		camDisV.push_back(npre);
	}

	fin.close();
	return true;
}

bool SyIoManager::writeCamDis( const QString& filename, const std::vector<EV2>& camDisV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (camDisV.size()==0)
	{
		std::cout<<"m_camPreCordsV no data!\n";
		return false;
	}

	for (int i = 0; i < camDisV.size(); ++i)
	{
		if (i==camDisV.size()-1)
			fout<<camDisV[i].x()<<" "<<camDisV[i].y();
		else
			fout<<camDisV[i].x()<<" "<<camDisV[i].y()<<std::endl;
	}
	fout.close();
	return true;
}

bool SyIoManager::readSpeed( const QString& filename, std::vector<double>& speedV )
{
	speedV.clear();
	std::ifstream fin(filename.toStdString(),std::ios::binary);
	if(!fin){
		std::cout<<"speed file doesn't exist!\n";
		return false;
	}

	while(!fin.eof())
	{
		double speedval;
		fin>>speedval;
		speedV.push_back(speedval);
	}

	fin.close();
	return true;
}

bool SyIoManager::writSpeed( const QString& filename, const std::vector<double>& speedV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (speedV.size()==0)
	{
		std::cout<<"m_speedV no data!\n";
		return false;
	}
	for (int i = 0; i < speedV.size()-1; ++i)
	{
		fout<<speedV[i]<<std::endl;
	}
	fout<<speedV[speedV.size()-1];  //防止最后一行空白

	fout.close();
	return true;
}

bool SyIoManager::readImportance( const QString& filename, std::vector<double>& importanceV )
{

	importanceV.clear();
	std::ifstream fin(filename.toStdString(),std::ios::binary);
	if(!fin){
		std::cout<<"importance file doesn't exist!\n";
		return false;
	}

	while(!fin.eof())
	{
		double importanceval;
		fin>>importanceval;
		importanceV.push_back(importanceval);
	}
	
	fin.close();
	if (importanceV.size()==1&&importanceV[0]<0.000000001)
		return false;
	else
		return true;
}

bool SyIoManager::writeImportance( const QString& filename, const std::vector<double>& importanceV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (importanceV.size()==0)
	{
		std::cout<<"importanceV no data!\n";
		return false;
	}
	for (int i = 0; i < importanceV.size()-1; ++i)
	{
		fout<<importanceV[i]<<std::endl;
	}
	fout<<importanceV[importanceV.size()-1];  //防止最后一行空白

	fout.close();
	return true;
}

bool SyIoManager::writeCross( const QString& filename, const std::vector<int>& crossV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (crossV.size()==0)
	{
		std::cout<<"crossV no data!\n";
		return false;
	}
	for (int i = 0; i < crossV.size()-1; ++i)
	{
		fout<<crossV[i]<<std::endl;
	}
	fout<<crossV[crossV.size()-1];  //防止最后一行空白

	fout.close();
	return true;
}

bool SyIoManager::writeSection( const QString& filename, const std::vector<EV2>& sectionV )
{
	std::ofstream fout(filename.toStdString(),std::ios::binary);
	if (sectionV.size()==0)
	{
		std::cout<<"sectionV no data!\n";
		return false;
	}
	for (int i = 0; i < sectionV.size()-1; ++i)
	{
		fout<<sectionV[i].x()<<" "<<sectionV[i].y()<<std::endl;
	}
	fout<<sectionV[sectionV.size()-1].x()<<" "<<sectionV[sectionV.size()-1].y();  //防止最后一行空白

	fout.close();
	return true;
}




