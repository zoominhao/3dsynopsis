#pragma once
#ifndef SY_CONFIG_H 
#define SY_CONFIG_H

#include <vector>
#include <QString>

class SyConfig
{
public:
	SyConfig();
	~SyConfig();
	static SyConfig& getInstance() {
		static SyConfig theSingleton;
		return theSingleton;
	}

	//读取config文件
	bool  loadConfig( const QString& filename);

	//set
	void  setWhichCity(const int whichCity);
	void  setDetaTime(const double detaTime);
	void  setSampleInterval(const double sampleInterval);
	void  setDepthThrehold(const float depthThrehold);
	void  setVideoDeta(const float videoDeta);
	void  setInitHeight(const float initHeight);

	void  setHw_weight(const float hw_weight);
	void  setHw_cross_weight(const float hw_cross_weight);
	void  setRamp_weight(const float ramp_weight);
	void  setRamp_cross_weight(const float ramp_cross_weight);
	void  setCity_weight(const float city_weight);
	void  setCity_cross_weight(const float city_cross_weight);
	void  setCityRange(const std::vector<std::pair<int,int>> cityRange);
	void  setRampRange(const std::vector<std::pair<int,int>> rampRange);
	void  setHighwayRange(const std::vector<std::pair<int,int>> highwayRange);
	
	//get
	int     getWhichCity() const;
	double  getDetaTime() const;
	double  getSampleInterval() const;
	float   getDepthThrehold() const; 
	float   getVideoDeta() const;
	float   getInitHeight() const;

	float   getHw_weight() const;
	float   getHw_cross_weight() const;
	float   getRamp_weight() const;
	float   getRamp_cross_weight() const;
	float   getCity_weight() const;
	float   getCity_cross_weight() const;
	std::vector<std::pair<int,int>> getCityRange() const;
	std::vector<std::pair<int,int>> getRampRange() const;
	std::vector<std::pair<int,int>> getHighwayRange() const;


private:
	void clear();
private:
	//City
	int                  m_which_city;    //1 shenzhen   2 shanghai  3 suzhou  4 highway shanghai&suzhou
	//求解heading用
	double               m_detaTime;
	double               m_sampleInterval;
	float                m_depthThrehold;
	float                m_videoDeta;
	float                m_initHeight;
	//weight
	float                m_hw_weight;
	float                m_hw_cross_weight;
	float                m_ramp_weight;
	float                m_ramp_cross_weight;
	float                m_city_weight;
	float                m_city_cross_weight;
	//range
	std::vector<std::pair<int,int>> m_cityRange;
	std::vector<std::pair<int,int>> m_rampRange;
	std::vector<std::pair<int,int>> m_highwayRange;
};
#endif
