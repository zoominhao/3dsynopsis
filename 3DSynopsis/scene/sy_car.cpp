#include "sy_car.h"
#include "file_system.h"
#include "global_constant.h"
#include "osg_viewer_widget.h"
#include "parameter_manager.h"
#include "osg_utility.h"
#include "global_func.h"

#include <osgDB/ReadFile>
#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowMap>
#include <QFileInfo>
#include <QTextStream>

SyCar::SyCar()
	:m_folder(""),
	m_showPorsche(true),
	m_scale(0.02,0.02,0.02)
{

}

SyCar::~SyCar()
{

}

bool SyCar::setFromFile( const std::string& folder )
{
	std::string porscheModel = folder + GConstant::C_PORSCHE;
	std::string extension = FileSystem::extension( porscheModel );
	m_folder = folder;
	bool flag = false;
	if( extension == GConstant::C_SCENE_MODEL_EXTENSION)
	{ 
		QFileInfo porschefile(porscheModel.c_str());
		if(porschefile.exists())
		{
			flag = true;
			m_porsche = osgDB::readNodeFile( porscheModel );
			expire();
		}
	}
	return flag;
}


bool SyCar::toggleRenderPorsche(bool toggle)
{
	if (m_showPorsche == toggle)
		return false;

	m_showPorsche = toggle;

	expire();
	return true;
}

void SyCar::updateImpl()
{
	if (m_showPorsche)
	{
		renderPorsche();
	}
}

void SyCar::renderPorsche()
{
	if (m_porsche!=NULL)
	{
		osg::Matrix mscale;
		mscale.makeScale(m_scale);
		osg::MatrixTransform *nmat = new osg::MatrixTransform;
		nmat->setMatrix(mscale);
		nmat->addChild(m_porsche);
		addChild(nmat);
	}

}

void SyCar::clearData()
{
	//todo
}
