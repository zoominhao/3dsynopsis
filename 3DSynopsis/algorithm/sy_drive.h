#pragma once
#include "data_set.h"

#include <QObject>

#define TICK_SIM_MS 83.33     //相机帧率 1000/66 = 15 rate为1     shanghai->shenzen 83.33

#define CARSINDEX 0        //车提前的位置

//camera trajectory
#define PRERATE 1.8
#define HEIGHTRATE 3
#define TILTDETA 15
#define CAMPRRATE 0.8

class SyDrive: public QObject
{
	Q_OBJECT 

		private slots:
			void frameTick();
			void videoTick();
public:
	SyDrive(QObject * parent = 0 );
	virtual ~SyDrive();

	//初始化
	void init(DataSet* datasetIns);

	//控制模块
	void SYControl(ControlMode cmode);

	//创造仪表盘
	osg::ref_ptr<osg::Camera> createMeter();
	bool toggleRenderMeter(bool toggle);
	//录屏
	void snapVideos();

private:
	//重置初始化
	void reinit();
	void initCarPos();
	void initDCCamPos();

	//模拟
	void simulate();
	//只有drive模式，都读取配置文件
	void drive(osg::Vec3 tcarpoint, osg::Vec3 tpoint, osg::Vec3 detaDis, SyView& tview, SyView& tcarview);
	//车
	void moveCar(osg::Vec3 tcarpoint, SyView& tcarview);
	//相机
	void moveCamera(osg::Vec3 tpoint, osg::Vec3 detaDis, SyView& tview);
	//DC相机，相机模型
	void moveDCCamera(osg::Vec3 tpoint, osg::Vec3 detaDis, double dcheading, double dctilt, double dcheight);  

	//用于插值
	osg::Vec3 interpolateLoc(osg::Vec3 p1, osg::Vec3 p2, double f);
	SyView interpolateView(SyView& v1, SyView& v2, double f);

	double getHeadingMove(double heading1, double heading2);
	double getCarHeadingMove(double heading1, double heading2);
	double getTiltMove(double tilt1, double tilt2);
	double getHeightMove(double height1, double height2);
	SyView getViewMove(SyView view1, SyView view2);

	//用于仪表显示
	std::string formatTime(double seconds);
	void updateSpeedIndicator();

	//统计某一文件夹中某一格式文件的数量
	int GetRSSCount(std::string folder, int count);

	bool readSpeedV();    
	bool readViewV();      
	bool readCamDis();
	bool loadSmoothPath();
private:
	//路径
	std::string                                  m_folder;
	//模式
	int                                          m_DrivingMode;
	//相机
	osg::ref_ptr<SynopsisManipulator>            m_pSynopsisManipulator;
	

	//计数操作
	bool                                         m_doTick;
	int                                          m_pathIndex;
	double                                       m_segmentTime;
	double                                       m_segmentDistance;
	double                                       m_beforeSegmentDistance;
	double                                       m_TotalTime;
	//用于仪表显示
	//仪表盘
	osg::ref_ptr<osgText::Text>                  m_updateMeter;
	bool                                         m_showMeter;
	double                                       m_curTotalTime;
	double                                       m_curTotalDistance;
	double                                       m_curSpeed;
	//倍率
	double                                       m_rate;          //控制速度
	double                                       m_rate_factor;
	//为车路的行走设计计数
	osg::Vec3                                   m_lastCarPos; 
	double                                      m_carsegmentTime;
	int                                         m_carPathIndex;
	//状态
	//heading 
	double                                       m_lastheading;
	double                                       m_lastcarheading;
	//tilt
	double                                       m_lasttilt;

	//height
	double                                       m_lastheight;

	//视角
	SyView                                       m_lastView;
	SyView                                       m_lastCarView;
	//pos
	osg::Vec3                                    m_curPos;
	osg::Vec3                                    m_lastPos;
	osg::Vec3                                    m_lastDetaDis;

	//录屏
	osg::ref_ptr<SyScreenCapture>                m_sc;
	int                                          m_imageNo;
	int                                          m_startNo;
	int                                          m_semaphore;

	//dataset 对象
	DataSet*                                     m_dataset;
	//操作对象
	osg::ref_ptr<SyCar>                          m_car;
	osg::ref_ptr<SyCam>                          m_DCCam;

	//数据保存的文件
	SyIoManager                                  m_IOManager;
	std::vector<SyPath>                          m_samplePath;
	std::vector<SyPath>                          m_smoothPath;
	std::vector<double>                          m_speedV;    
	std::vector<EV2>                             m_camPreCordsV;  	//提前坐标
	std::vector<std::pair<SyView,SyView>>        m_views;     //渲染出来的车的姿势
};