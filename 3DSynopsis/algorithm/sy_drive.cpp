#include "sy_drive.h"
#include "parameter_manager.h"



SyDrive::SyDrive( QObject * parent /*= 0 */ )
	:QObject(parent)
	,m_doTick(false)
	,m_showMeter(false)
{

}

SyDrive::~SyDrive()
{

}

void SyDrive::init( DataSet* datasetIns )
{
	//成员变量赋值
	m_dataset = datasetIns;
	m_folder = datasetIns->floder().toStdString();
	m_car = datasetIns->myCar();
	m_DCCam = datasetIns->myCam();
	m_samplePath = datasetIns->myScene()->SamplePath();
	m_pSynopsisManipulator = datasetIns->psynopsisManipulator();

	reinit();

	//frameend 
	connect(m_dataset,SIGNAL(frameend()),this,SLOT(videoTick()));
	connect(m_dataset,SIGNAL(frameend()),this,SLOT(frameTick()));
}

void SyDrive::reinit()
{
	//初始化
	//相机操作
	m_pathIndex = 0;
	m_curTotalTime = 0;
	m_curTotalDistance = 0;
	m_curSpeed = 0;
	m_segmentTime = 0;
	m_segmentDistance = 0;
	m_rate = 1;
	m_beforeSegmentDistance = 0;

	m_DrivingMode = ParameterManager::getInstance().getDrivingMode();
	m_TotalTime = ParameterManager::getInstance().getTotalTime();
	m_TotalTime /= 1.5;                //根据比例关系调整
	loadSmoothPath();
	readViewV();

	//车的操作
	m_carsegmentTime = 0;
	m_carPathIndex = CARSINDEX;


	m_lastPos = m_smoothPath[0].spos();
	m_lastCarPos = m_samplePath[m_carPathIndex].spos();

	m_curPos = m_lastPos;

	double defaultTotalTime = 0;
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		defaultTotalTime += m_samplePath[i].duration();
	}
	m_rate_factor = defaultTotalTime/m_TotalTime;         //意义！！！
	m_rate_factor*=8; //设置

	readSpeedV();
	readCamDis();    //不管密度怎样,view,speed,path长度要保持一致,外部处理数据的时候一定要注意
	m_lastDetaDis = osg::Vec3(m_camPreCordsV[0].x(), m_camPreCordsV[0].y(),0);

	if (m_views.size()!=0 && m_speedV.size()!=0 && m_views.size()!=0)
	{
		for (int i = 0; i < m_samplePath.size(); ++i)
		{
			m_samplePath[i].setDuration(m_samplePath[i].distance()/m_speedV[i]);
			m_smoothPath[i].setDuration(m_smoothPath[i].distance()/m_speedV[i]);
		}

		m_lastCarView = m_views[m_carPathIndex].first;
		m_lastView = m_views[0].first;
		m_lastheading = m_views[0].first.m_heading;
		m_lasttilt = m_views[0].first.m_tilt;
		m_lastheight = m_views[0].first.m_height;
		m_lastcarheading = m_views[m_carPathIndex].first.m_heading;
	}

	initCarPos();
	initDCCamPos();
	//车往前一些位置,保证在视线内
	osg::Matrix tmat = m_car->getMatrix();
	osg::Matrix transm;
	transm.makeTranslate(m_samplePath[m_carPathIndex].spos()-m_samplePath[0].spos());       
	m_car->setMatrix(tmat*transm); //如果不是直线移动的话，此处还要修改，包括车的朝向和位置关系，为简便取默认朝向，车在直线上做移动,所以采样的时候要注意
}

void SyDrive::initCarPos()
{
	//car 的位置初始化
	/*osg::Matrix scaleM;
	//scale
	scaleM.makeScale(m_car->scaleV());
	m_car->setMatrix(scaleM);*/
	m_car->setMatrix(osg::Matrix::identity());

	osg::Matrix originMat,toriginMat,tocenMat,rotMat;
	//rotation
	osg::Vec3 iniDir = m_samplePath[0].tpos()-m_samplePath[0].spos();
	EV2 xaxis(1, 0);
	EV2 EiniDir(iniDir.x(),iniDir.y());
	double iniangle = GLOBAL::calTheta(EiniDir,xaxis);
	//rotMat.makeRotate(-m_car->getMatrix().getRotate());
	rotMat.makeRotate(osg::DegreesToRadians(iniangle+90), osg::Vec3f(0,0,1));
	//translation
	originMat = m_car->getMatrix();
	toriginMat.makeTranslate(-m_car->getBound().center());
	tocenMat.makeTranslate(m_samplePath[0].spos()+osg::Vec3(0.0,0.0,2.0));
	m_car->setMatrix(originMat*toriginMat*rotMat*tocenMat);
}

void SyDrive::initDCCamPos()
{
	if (m_views.size() == 0)
	{
		return;
	}
	m_DCCam->setMatrix(osg::Matrix::identity());

	osg::Matrix originMat,toriginMat,tocenMat,headingMat,tiltMat;
	//rotation
	//rotMat.makeRotate(-m_DCCam->getMatrix().getRotate());
	headingMat.makeRotate(osg::DegreesToRadians(m_views[0].first.m_heading), osg::Vec3f(0,0,1));
	if (m_views[0].first.m_heading < 180)
		tiltMat.makeRotate(osg::DegreesToRadians(-m_views[0].first.m_tilt), osg::Vec3f(0,1,0));
	else
		tiltMat.makeRotate(osg::DegreesToRadians(m_views[0].first.m_tilt), osg::Vec3f(0,1,0));
	//translation
	originMat = m_DCCam->getMatrix();
	toriginMat.makeTranslate(-m_DCCam->getBound().center());
	tocenMat.makeTranslate(m_smoothPath[0].spos()[0]-CAMPRRATE*m_camPreCordsV[0].x(),m_smoothPath[0].spos()[1]-CAMPRRATE*m_camPreCordsV[0].y(),m_smoothPath[0].spos()[2]+m_views[0].first.m_height);
	m_DCCam->setMatrix(originMat*toriginMat*headingMat*tiltMat*tocenMat);
}

void SyDrive::frameTick()
{
	if (ParameterManager::getInstance().getVideoMode())
	{
		return;
	}
	m_dataset->skyAnimation();
	if (m_doTick)
	{
		simulate();
	}
}

void SyDrive::SYControl( ControlMode cmode )
{
	switch(cmode)
	{
	case CONTROL_RESET:
		reinit();
		if (ParameterManager::getInstance().getTrackMode())
		{
			m_pSynopsisManipulator->setPosition(m_lastPos+osg::Vec3(0.0,0.0,m_lastheight*HEIGHTRATE)-osg::Vec3(PRERATE*m_camPreCordsV[m_pathIndex].x(),PRERATE*m_camPreCordsV[m_pathIndex].y(),0));
			m_pSynopsisManipulator->setTargetPosition(m_lastheading,m_lasttilt-TILTDETA);
		}
		else
		{
			m_pSynopsisManipulator->setPosition(m_lastPos+osg::Vec3(0.0,0.0,m_lastheight)-osg::Vec3(m_camPreCordsV[m_pathIndex].x(),m_camPreCordsV[m_pathIndex].y(),0));
			m_pSynopsisManipulator->setTargetPosition(m_lastheading,m_lasttilt);
		}
		
		m_doTick = false;
	
		break;
	case CONTROL_START:
		m_doTick = true;
		break;
	case CONTROL_PAUSE:
		m_doTick = false;
		break;
	case CONTROL_SPEEDUP:
		if (m_rate < 16.0) {
			m_rate *= 2.0;
			if (m_showMeter)
			{
				updateSpeedIndicator();
			}
		}
		break;
	case CONTROL_SLOWDOWN:
		if (m_rate > 0.125) {
			m_rate /= 2.0;
			if (m_showMeter)
			{
				updateSpeedIndicator();
			}
		}
		break;
	default:
		break;
	}
}

osg::ref_ptr<osg::Camera> SyDrive::createMeter()
{
	setlocale(LC_ALL,".936");//　配置地域化信息
	const char* titleString="3D SYNPOSIS";

	m_updateMeter = new osgText::Text;
	osg::ref_ptr<osg::Camera> camera = new osg::Camera;
	OSGUtility::setupProperties(camera,m_updateMeter,GConstant::C_FONT_HEI,20.0f,osg::Vec3(20.0f,80.0f,0.0f),osg::Vec4(0.0,0.5,0.7,1.0));
	OSGUtility::createContent(m_updateMeter,titleString);

	return camera;
}

bool SyDrive::toggleRenderMeter(bool toggle)
{
	if (m_showMeter == toggle)
		return false;

	m_showMeter = toggle;

	m_updateMeter->setText("3D SYNPOSIS");
	m_updateMeter->dirtyBound();
	return true;
}

void SyDrive::videoTick()
{
	if (!ParameterManager::getInstance().getVideoMode())
	{
		return;
	}
	if (!m_doTick || m_sc->getCaptureEnabled())
	{
		return;
	}

	if(GetRSSCount(m_folder + GConstant::C_VIDEO_PATH, m_imageNo)<=0 && m_semaphore == 0)
	{
		simulate();
		if(m_imageNo++%100 == 0)
			m_dataset->skyAnimation();
		if (m_imageNo < m_startNo)
		{
			return;
		}
	}

	if (m_semaphore == 1)
	{
		m_dataset->getCamera()->setPostDrawCallback( m_sc.get() );
		double fovy,ratio,znear,zfar;
		m_dataset->getCamera()->getProjectionMatrixAsPerspective(fovy,ratio,znear,zfar);
		m_sc->setNearFar(znear,zfar);
		m_sc->setCapture(true);
		m_sc->setUseFrameNumber(false);
		m_sc->setSnapNo(m_imageNo);
		m_sc->setNumFramesToCapture(1);
		m_semaphore = 0;
	}
	else
	{
		m_semaphore++;
	}

	//Sleep(SyConfig::getInstance().getVideoDeta());
}

void SyDrive::snapVideos()
{
	SYControl(CONTROL_RESET);
	SYControl(CONTROL_START);

	std::string rootName = m_folder + GConstant::C_VIDEO_PATH;
	std::string extName = ".png";
	m_sc = new SyScreenCapture(rootName, extName);
	m_imageNo = 0;
	m_startNo = 0;    //1570
	m_semaphore = 0;
}

int SyDrive::GetRSSCount(std::string folder, int count)
{

	QDir videodir(folder.c_str());
	if (!videodir.exists())
		return false;
	videodir.setFilter(QDir::Files);
	QFileInfoList list = videodir.entryInfoList();
	/*int i = 0;
	do{
		QFileInfo fileInfo = list.at(i);
		if(fileInfo.fileName()=="."|fileInfo.fileName()=="..")
			continue;
		if (fileInfo.isFile())
		{
			std::string extension =fileInfo.suffix().toStdString();
			if(extension == GConstant::C_IMAGE_EXTENSION)
		      i++;
		}
	 }
	while(i<list.size());*/
    if (list.size()>count)
		return -1;
	else if(list.size()<count)
		return 1;
	else 
		return 0;
}

void SyDrive::simulate()
{
	if (m_carPathIndex >= CARSINDEX)   //保证车先走
	{
		// move up TICK_SIM_MS milliseconds
		m_curTotalTime += TICK_SIM_MS * m_rate * m_rate_factor / 1000.0;
		m_segmentTime += TICK_SIM_MS * m_rate * m_rate_factor / 1000.0;

		double segmentDuration = m_smoothPath[m_pathIndex].duration();

		// move to next segment if we pass it in this tick
		while (m_pathIndex <= m_smoothPath.size() - 1 && m_segmentTime >= segmentDuration)
		{
			m_segmentTime -= segmentDuration;
			// adjust distances
			m_beforeSegmentDistance += m_smoothPath[m_pathIndex].distance();
			// update new position in path array
			m_pathIndex++;
			if (m_pathIndex > m_smoothPath.size() - 1) {
				m_doTick  = false;
				return;
			}
			segmentDuration = m_smoothPath[m_pathIndex].duration();
		}


		if (segmentDuration) {
			m_segmentDistance = m_smoothPath[m_pathIndex].distance() * std::min(1.0, m_segmentTime / segmentDuration);
			m_curSpeed = m_smoothPath[m_pathIndex].distance() / segmentDuration;
		} else {
			m_segmentDistance = 0.0;
			m_curSpeed = 0.0;
		}

		m_curTotalDistance = m_beforeSegmentDistance + m_segmentDistance;

		// update the current location
		m_curPos = interpolateLoc(m_smoothPath[m_pathIndex].spos(),m_smoothPath[m_pathIndex].tpos(),m_segmentTime/segmentDuration);

		//m_curPos = interpolateLoc(m_samplePath[m_pathIndex].spos(),m_samplePath[m_pathIndex].tpos(),m_segmentTime/segmentDuration);
	}
	//////////////////////////////处理汽车的////////////////////////////////////////////////////
	// update the car current location///
	osg::Vec3   curCarPos;
	SyView      curCarView;
	if (m_carPathIndex<=m_samplePath.size()-1)
	{
		m_carsegmentTime += TICK_SIM_MS * m_rate*m_rate_factor/ 1000.0;
		double carsegmentDuration = m_samplePath[m_carPathIndex].duration();
		// move to next segment if we pass it in this tick
		while (m_carPathIndex <= m_samplePath.size() - 1 && m_carsegmentTime >= carsegmentDuration)
		{
			m_carsegmentTime -= carsegmentDuration;
			// update new position in path array
			m_carPathIndex++;
			if (m_carPathIndex<m_samplePath.size())
			{
				carsegmentDuration = m_samplePath[m_carPathIndex].duration();
			}
		}
		if (m_carPathIndex<m_samplePath.size())
		{
			curCarPos = interpolateLoc(m_samplePath[m_carPathIndex].spos(),m_samplePath[m_carPathIndex].tpos(),m_carsegmentTime/m_samplePath[m_carPathIndex].duration());
			curCarView = interpolateView(m_views[m_carPathIndex].first,m_views[m_carPathIndex].second,m_carsegmentTime/m_samplePath[m_carPathIndex].duration());
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//相机视角
	SyView curView = interpolateView(m_views[m_pathIndex].first,m_views[m_pathIndex].second,m_segmentTime/m_smoothPath[m_pathIndex].duration());
	osg::Vec3 sdetaDis(m_camPreCordsV[m_pathIndex].x(),m_camPreCordsV[m_pathIndex].y(),0);
	osg::Vec3 tdetaDis(m_camPreCordsV[m_pathIndex+1].x(),m_camPreCordsV[m_pathIndex+1].y(),0);
	osg::Vec3 osgDetaDis = interpolateLoc(sdetaDis,tdetaDis,m_segmentTime/m_smoothPath[m_pathIndex].duration());
	drive(curCarPos,m_curPos, osgDetaDis, curView, curCarView);
}

void SyDrive::drive( osg::Vec3 tcarpoint, osg::Vec3 tpoint, osg::Vec3 detaDis, SyView& tview, SyView& tcarview)
{
	moveCar(tcarpoint,tcarview);      
	if (m_carPathIndex>=CARSINDEX)    //保证车先动
		moveCamera(tpoint,detaDis,tview); 
	//更新仪表显示
	if (m_showMeter)
	{
		updateSpeedIndicator();
	}
	//for car
	m_lastCarPos = tcarpoint;
	//for camera
	m_lastPos = tpoint;
	m_lastDetaDis = detaDis;
	m_lastView = tview;
}

void SyDrive::moveCar(osg::Vec3 tcarpoint, SyView& tview)
{
	if (m_carPathIndex==m_samplePath.size())
		return ;
	//translate
	osg::Matrix tmat = m_car->getMatrix();
	osg::Matrix transm;
	//tcarpoint.z() = int(tcarpoint.z()*1000+0.5)/1000.0;    //解决z轴抖动
	transm.makeTranslate(tcarpoint-m_lastCarPos);
	m_car->setMatrix(tmat*transm);

	//rotate
	osg::Matrix originmat = m_car->getMatrix();
	osg::Matrixd rotateMat,toriginMat,tocenMat;
	osg::Vec3 car_cen = m_car->getBound().center();    //车的中心
	toriginMat.makeTranslate(-(car_cen));
	double desiredHeading = tview.m_heading;
	if (m_lastcarheading<-90&&desiredHeading>90)
		desiredHeading = desiredHeading - 360;
	if (m_lastcarheading>90&&desiredHeading<-90)
		desiredHeading = desiredHeading + 360;
	double detaheading = getCarHeadingMove(m_lastcarheading, desiredHeading);
	rotateMat.makeRotate(osg::DegreesToRadians(detaheading), osg::Vec3f(0,0,1));
	tocenMat.makeTranslate(car_cen);
	m_car->setMatrix(originmat*toriginMat*rotateMat*tocenMat);
	m_lastcarheading= GLOBAL::fixAngle(m_lastcarheading + detaheading);
}

void SyDrive::moveCamera(osg::Vec3 tpoint, osg::Vec3 detaDis, SyView& tview )
{
	double desiredHeading = tview.m_heading;
	if (m_lastheading<-90&&desiredHeading>90)
		desiredHeading = desiredHeading - 360;
	if (m_lastheading>90&&desiredHeading<-90)
		desiredHeading = desiredHeading + 360;
	double nheading=m_lastheading + getHeadingMove(m_lastheading, desiredHeading);

	//std::cout<<" "<<m_lastheading<<" "<<desiredHeading<<" "<<nheading<<std::endl;

	double desiredTilt = tview.m_tilt;
	double ntilt=m_lasttilt + getTiltMove(m_lasttilt, desiredTilt);
	double desiredHeight = tview.m_height;

	double nheight = m_lastheight + (desiredHeight - m_lastheight) * 0.2;  //此处要调

	if (ParameterManager::getInstance().getTrackMode())
	{
		m_pSynopsisManipulator->setPosition(tpoint.x()- PRERATE*detaDis.x(),tpoint.y()- PRERATE*detaDis.y(),tpoint.z()+nheight*HEIGHTRATE); //延相机朝向往后退
		//m_pSynopsisManipulator->setPosition(tpoint.x(),tpoint.y(),tpoint.z()+nheight);
		//std::cout<<"detaX: "<<detaDis.x()<<"detaY: "<<detaDis.y()<<std::endl;
		m_pSynopsisManipulator->setTargetPosition(nheading,ntilt-TILTDETA);    
		moveDCCamera(tpoint, detaDis, desiredHeading, desiredTilt, nheight);
	}
	else
	{
		m_pSynopsisManipulator->setPosition(tpoint.x()- detaDis.x(),tpoint.y()- detaDis.y(),tpoint.z()+nheight); //延相机朝向往后退
		//m_pSynopsisManipulator->setPosition(tpoint.x(),tpoint.y(),tpoint.z()+nheight);
		//std::cout<<"detaX: "<<detaDis.x()<<"detaY: "<<detaDis.y()<<std::endl;
		m_pSynopsisManipulator->setTargetPosition(nheading,ntilt);   
	}

	m_lastheading = GLOBAL::fixAngle(nheading);
	m_lasttilt = ntilt;
	m_lastheight = nheight;
}

void SyDrive::moveDCCamera(osg::Vec3 tpoint, osg::Vec3 detaDis, double dcheading, double dctilt, double dcheight)
{
	//translate
	osg::Matrix tmat = m_DCCam->getMatrix();
	osg::Matrix transm;
	osg::Vec3 detaPos(tpoint.x()-m_lastPos.x() - CAMPRRATE*detaDis.x() + CAMPRRATE*m_lastDetaDis.x(), tpoint.y()-m_lastPos.y() - CAMPRRATE*detaDis.y() + CAMPRRATE*m_lastDetaDis.y(), tpoint.z()-m_lastPos.z() + dcheight - m_lastheight);
	transm.makeTranslate(detaPos);
	m_DCCam->setMatrix(tmat*transm);

	//rotate
	osg::Matrix originmat = m_DCCam->getMatrix();
	osg::Matrixd rotatehMat,rotatetMat,toriginMat,tocenMat;
	osg::Vec3 dccam_cen = m_DCCam->getBound().center();    
	toriginMat.makeTranslate(-(dccam_cen));

	double detaheading = getHeadingMove(m_lastheading, dcheading);
	rotatehMat.makeRotate(osg::DegreesToRadians(detaheading), osg::Vec3f(0,0,1));

	double detatilt = getTiltMove(m_lasttilt, dctilt);
	rotatetMat.makeRotate(osg::DegreesToRadians(detatilt), osg::Vec3f(0,1,0));   //可能会有问题

	tocenMat.makeTranslate(dccam_cen);

	m_DCCam->setMatrix(originmat*toriginMat*rotatehMat*rotatetMat*tocenMat);
}

osg::Vec3 SyDrive::interpolateLoc(osg::Vec3 p1, osg::Vec3 p2, double f)
{
	osg::Vec3 dis(f*(p2.x()-p1.x()),f*(p2.y()-p1.y()),f*(p2.z()-p1.z()));
	return p1+dis;
}

SyView SyDrive::interpolateView(SyView& v1, SyView& v2, double f)
{
	SyView nview;
	nview.m_heading = GLOBAL::fixAngle(v1.m_heading + GLOBAL::fixAngle(v2.m_heading - v1.m_heading)*f);
	nview.m_tilt = v1.m_tilt + (v2.m_tilt - v1.m_tilt)*f;
	nview.m_height = v1.m_height + (v2.m_height - v1.m_height)*f;
	return nview; 
}

double SyDrive::getHeadingMove( double heading1, double heading2 )
{
	double rate = 6 * TICK_SIM_MS / 50.0;
	//std::cout<<rate<<std::endl;
	if (std::abs(heading1 - heading2) < rate)
		return heading2 - heading1;
	return (GLOBAL::fixAngle(heading2 - heading1) < 0) ? -rate : rate;       //这个地方可以调整，保证heading有足够时间转弯  smooth 3  unsmooth 4
}

double SyDrive::getCarHeadingMove(double heading1, double heading2)
{
	if (std::abs(heading1 - heading2) < 8)
		return heading2 - heading1;
	return (GLOBAL::fixAngle(heading2 - heading1) < 0) ? -8 : 8;       //这个地方可以调整，保证heading有足够时间转弯
}

double SyDrive::getTiltMove( double tilt1, double tilt2 )
{
	if (std::abs(tilt1 - tilt2) < 1)
		return tilt2 - tilt1;
	return (GLOBAL::fixAngle(tilt2 - tilt1) < 0) ? -1 : 1;
}

double SyDrive::getHeightMove( double height1, double height2 )
{
	if (std::abs(height1 - height2) < 1)
		return height2 - height1;
	return (height2 - height1 < 0) ? -1 : 1;
}

SyView SyDrive::getViewMove( SyView view1, SyView view2 )
{
	SyView nview;
	nview.m_tilt = getTiltMove(view1.m_tilt, view2.m_tilt );
	nview.m_height = getHeightMove(view1.m_height, view2.m_height );
	nview.m_heading = getHeadingMove(view1.m_heading, view2.m_heading );
	return nview;
}

std::string SyDrive::formatTime(double seconds)
{
	int hour = int(int(seconds)/3600);
	int minute = int((int(seconds)%3600)/60);
	int second = int(seconds) - hour*3600 - minute*60;
	std::stringstream ss;
	ss<<hour<<":"<<minute<<":"<<second;
	std::string output=ss.str();
	return output;
}

void SyDrive::updateSpeedIndicator()
{
	std::string meterDisplay = "Time: "+formatTime(m_curTotalTime)+"   Distance: "+GLOBAL::double2string(m_curTotalDistance/1000,3)+" km   Speed: "+GLOBAL::double2string(m_curSpeed*3.6,3)+" km/h       Rate: "+GLOBAL::double2string(m_rate,3)+" x";
	m_updateMeter->setText(meterDisplay);
	m_updateMeter->dirtyBound();
}

bool SyDrive::readSpeedV()
{
	m_speedV.clear();
	std::string speedVfile;
	if (m_DrivingMode == 1)
		speedVfile = m_folder +  GConstant::C_GOOGLE_SPEED;
	else if(m_DrivingMode == 5)
		speedVfile = m_folder +  GConstant::C_JUMP_SPEED;
	else if(m_DrivingMode == 2||m_DrivingMode == 3)
		speedVfile = m_folder +  GConstant::C_OP_SPEED;   //此处要改


	return m_IOManager.readSpeed(speedVfile.c_str(),m_speedV);
}

bool SyDrive::readViewV()
{
	m_views.clear();
	std::string viewVfile;
	if (m_DrivingMode == 1)
		viewVfile  = m_folder + GConstant::C_CAMERA_GOOGLE_VIEW;
	if (m_DrivingMode == 5)
		viewVfile  = m_folder + GConstant::C_CAMERA_JUMP_VIEW;
	else if(m_DrivingMode== 2||m_DrivingMode == 3)
		viewVfile = m_folder + GConstant::C_CAMERA_OP_SVIEW; 

	return m_IOManager.readView(viewVfile.c_str(),m_views);
}

bool SyDrive::readCamDis()
{
	m_camPreCordsV.clear();
	std::string camPreVfile;
	if (m_DrivingMode == 1)
		camPreVfile  = m_folder + GConstant::C_CAMERA_GOOGLE_PRE;
	if (m_DrivingMode == 5)
		camPreVfile  = m_folder + GConstant::C_CAMERA_JUMP_PRE;
	else if(m_DrivingMode== 2||m_DrivingMode == 3)
		camPreVfile = m_folder + GConstant::C_CAMERA_PRE;  //此处要改

	return m_IOManager.readCamDis((camPreVfile).c_str(),m_camPreCordsV);
}

bool SyDrive::loadSmoothPath()
{
	m_smoothPath.clear();
	std::string smoothPathfile;
	if (m_DrivingMode == 1)
		smoothPathfile  = m_folder + GConstant::C_SCSAMPLE_PATH;
	if (m_DrivingMode == 5)
		smoothPathfile  = m_folder + GConstant::C_SJSAMPLE_PATH;
	else if(m_DrivingMode== 2||m_DrivingMode == 3)
		smoothPathfile  = m_folder + GConstant::C_SSAMPLE_PATH;
	return m_IOManager.loadSamplePath(smoothPathfile.c_str(),m_smoothPath); 
}



