#include "pca.h"

#include <Eigen/Eigenvalues>
#include <iostream>

PCA::PCA()
{

}

PCA::~PCA()
{

}

void PCA::calPCA( const EMatXd& V,EMatXd& resV )
{
	if( V.rows() < 3)
		return;

	resV = EMatXd(V.rows(),V.cols());
	
	EMatXd centered = V.rowwise() - V.colwise().mean();
	EMatXd cov = centered.adjoint() * centered;


	//then perform the eigen decomposition:
	Eigen::SelfAdjointEigenSolver<EMatXd> eig(cov);
	//std::cout << eig.eigenvalues().transpose() << std::endl;

	EV3 dir1, dir2,dir3;
	dir1 = eig.eigenvectors().col(2);
	dir2 = eig.eigenvectors().col(1);
	dir3 = eig.eigenvectors().col(0);
	//dir3 = eig.eigenvalues().col(0);
	PCAM = eig.eigenvectors();
	PCAINVERSEM = PCAM.inverse();

	//std::cout<<PCAM<<std::endl<<PCAINVERSEM<<std::endl<<PCAM*PCAINVERSEM<<std::endl;

	for( unsigned int i = 0 ;i!= V.rows(); ++i)
	{
		resV.row(i) = V.row(i)*PCAM;
		/*resV(i,0) = V.row(i)*dir1;
		resV(i,1) = V.row(i)*dir2;
		resV(i,2) = V.row(i)*dir3;*/
	}
}

EV3 PCA::getMainDir(const EMatXd& V)
{
	EV3 resDir(0,0,0);
	if( V.rows() < 3)
		return resDir;

	EMatXd centered = V.rowwise() - V.colwise().mean();
	EMatXd cov = centered.adjoint() * centered;


	//then perform the eigen decomposition:
	Eigen::SelfAdjointEigenSolver<EMatXd> eig(cov);
	//std::cout << eig.eigenvalues().transpose() << std::endl;

	return eig.eigenvectors().col(2);
}

EV2 PCA::get2DMainDir(const EMatXd& V)
{
	EMatXd centered = V.rowwise() - V.colwise().mean();
	EMatXd cov = centered.adjoint() * centered;


	//then perform the eigen decomposition:
	Eigen::SelfAdjointEigenSolver<EMatXd> eig(cov);
	//std::cout << eig.eigenvalues().transpose() << std::endl;

	return eig.eigenvectors().col(1);
}