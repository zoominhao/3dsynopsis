#include "mean_shift_manager.h"
#include "EuclideanGeometry.h"
#include "SE3Geometry.h"
#include "GrassmannGeometry.h"
#include "EssentialGeometry.h"
#include "DTIAffineInvariantGeometry.h"
#include "VectorPointSet.h"
#include "EssentialPointSet.h"


#include "MeanShift.h"


MeanShifeMgr::MeanShifeMgr()
	: _type( MeanShifeMgr::EUCLIDEAN)
	, _bandwidth(0.001)
	, _max_mode_percent(0.1)
{

}
void MeanShifeMgr::apply( const Eigen::MatrixXd& points, Eigen::MatrixXd& cen, Eigen::MatrixXd& cenmember)
{
	CMeanShift<double> ms;
	ms.setBandwidth( _bandwidth );

	int dim = points.cols();
	int num_p = points.rows();
	CEuclideanGeometry<double> geom(dim);

	// change format
	double *data = new double[dim * num_p];
	int rc = 0;
	for( int i = 0 ;i != num_p; ++i )
	{
		for( int j = 0; j!= dim; ++j )
		{
			data[rc] = points.coeff(i,j);
			++rc;
		}
	}
		
	CVectorPointSet<double> dataPoints(dim, num_p, data);
	delete data;

	CVectorPointSet<double> unprunedModes(dim, num_p);
	ms.doMeanShift(geom, dataPoints, unprunedModes);

	int num_max_m = num_p*_max_mode_percent;
	CVectorPointSet<double> prunedModes(dim, num_max_m);
	ms.pruneModes(geom, unprunedModes, prunedModes, _min_ClusterSize, _bandwidth);

	double* kernelDensities = new double[num_max_m];
	ms.getKernelDensities(geom, dataPoints, prunedModes, kernelDensities);


	//获取中心点对应成员
	cenmember = Eigen::MatrixXd( num_p, dim+1 );
	double *currentPosition	= new double[dim];
	int cenindex;                  //用于获取所属的cen索引
	for(int i = 0; i < num_p; i++){
		memcpy(currentPosition, dataPoints[i], sizeof(double) * dim);
		cenindex = ms.getKernelMembers(geom, prunedModes, currentPosition);
		if (cenindex != -1)
		{
			for( int j = 0 ;j!= dim; ++j )
			{
				cenmember(i,j) = dataPoints[i][j];
			}
			cenmember(i,dim) = cenindex;
		}
	}

	delete [] currentPosition;
	
	

	// change format
	//int num_m = prunedModes.size();
	cen = Eigen::MatrixXd( prunedModes.size(), dim+1 );
	
	int num_m = prunedModes.size();
	for( int i = 0; i < num_m; ++i )
	{
		const double* p = prunedModes[i];
		for( int j = 0 ;j!= dim; ++j )
		{
			cen(i,j) = p[j];
		}
		cen(i,dim) = kernelDensities[i];
	}
	delete kernelDensities;


}

