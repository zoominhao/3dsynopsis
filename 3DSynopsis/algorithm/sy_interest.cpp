#include <qmessagebox.h>
#include <QDir>
#include <windows.h>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include "sy_interest.h"
#include "parameter_manager.h"
#include "sy_config.h"
#include "global_func.h"
#include "osg_viewer_widget.h"

SyInterest::SyInterest( QObject * parent /*= 0 */ )
	:QObject(parent)
	,m_isSnap(false)
    ,m_camDone(false)
	,WIDTH(1280)
	,HEIGHT(720)
	,OFA_SIZE(1800)
{

}

SyInterest::~SyInterest()
{

}

void SyInterest::init( std::string folder )
{
	m_folder = folder;
	m_IOManager.loadSamplePath((folder+GConstant::C_SAMPLE_PATH).c_str(),m_samplePath);
	m_IOManager.loadSamplePath((folder+GConstant::C_SSAMPLE_PATH).c_str(),m_smoothPath);
	m_totalPointsNum = m_samplePath.size()+1;

	initialize();
}

void SyInterest::initialize()
{
	//sample height
	m_sample_height = 12;
	//sample tilt
	m_sample_tilt = 15;
	//heading   最直接的计算相邻点的heading
	EV2  xaxis(1,0);
	for (int i = 0; i < m_smoothPath.size(); ++i)
	{
		osg::Vec3 iniDir = m_smoothPath[i].tpos()-m_smoothPath[i].spos();
		EV2 EiniDir(iniDir.x(),iniDir.y());
		double nheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
		m_roadheadings.push_back(nheading);
	}
	m_roadheadings.push_back(m_roadheadings[m_roadheadings.size()-1]);  //加最后一个点

	//图片所在文件夹目录
	m_imgs_dir = m_folder + GConstant::C_SAMPLEVIEW_IMAGES;
	//录屏文件所在目录
	m_video_dir = m_folder + GConstant::C_VIDEO_PATH;
	//初始化图像的的参数
	m_midX = WIDTH/2;
	m_midY = HEIGHT/2;
	m_totalPixelNum = WIDTH * HEIGHT;
	m_radius = sqrt(m_midX*m_midX+m_midY*m_midY);
}

void SyInterest::uniformSample( double segLength )
{
	//均匀插值
	m_IOManager.loadSamplePath((m_folder + GConstant::C_SAMPLE_PATH).c_str(),m_samplePath); 
	//step1 将线段点取出
	std::vector<std::vector<double>> segPointVec;
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		std::vector<double> curPoint;
		curPoint.push_back(m_samplePath[i].spos().x());
		curPoint.push_back(m_samplePath[i].spos().y());
		curPoint.push_back(m_samplePath[i].spos().z());
		curPoint.push_back(m_samplePath[i].crossFlag());
		segPointVec.push_back(curPoint);
	}
	std::vector<double> curPoint;
	curPoint.push_back(m_samplePath[m_samplePath.size()-1].tpos().x());
	curPoint.push_back(m_samplePath[m_samplePath.size()-1].tpos().y());
	curPoint.push_back(m_samplePath[m_samplePath.size()-1].tpos().z());
	curPoint.push_back(m_samplePath[m_samplePath.size()-1].crossFlag());
	segPointVec.push_back(curPoint);

	//step2 计算采样的一些参数
	double intervalDis = segLength;  //采样间隔
	double curSampleDis = 0;
	std::vector<std::vector<double>> samplePathV;
	samplePathV.push_back(segPointVec[0]);
	for (int i = 0; i < segPointVec.size()-1; ++i)
	{
		//计算该段的采样数
		double curSegDis = std::sqrt((segPointVec[i][0]-segPointVec[i+1][0])*(segPointVec[i][0]-segPointVec[i+1][0])+(segPointVec[i][1]-segPointVec[i+1][1])*(segPointVec[i][1]-segPointVec[i+1][1])+(segPointVec[i][2]-segPointVec[i+1][2])*(segPointVec[i][2]-segPointVec[i+1][2]));
		int cursamplenum = (int)((curSegDis+curSampleDis)/intervalDis);

		for (int j = 1; j < cursamplenum+1; j++)
		{
			double samplePos = intervalDis*j - curSampleDis;
			double interx = (segPointVec[i+1][0]-segPointVec[i][0])*samplePos/curSegDis+segPointVec[i][0];
			double intery = (segPointVec[i+1][1]-segPointVec[i][1])*samplePos/curSegDis+segPointVec[i][1];
			double interz = (segPointVec[i+1][2]-segPointVec[i][2])*samplePos/curSegDis+segPointVec[i][2];

			std::vector<double> interPoint;
			interPoint.push_back(interx);
			interPoint.push_back(intery);
			interPoint.push_back(interz);
			interPoint.push_back(segPointVec[i][3]);
			samplePathV.push_back(interPoint);
		}
		curSampleDis = curSegDis - (cursamplenum*intervalDis - curSampleDis);
	}
	samplePathV.push_back(segPointVec[segPointVec.size()-1]);

	//step3  变换成sypath
	std::vector<SyPath> nPath;
	for (int i = 0; i < samplePathV.size()-1; ++i)
	{
		osg::Vec3 spos(samplePathV[i][0],samplePathV[i][1],samplePathV[i][2]);
		osg::Vec3 tpos(samplePathV[i+1][0],samplePathV[i+1][1],samplePathV[i+1][2]);
		bool crossFlag = samplePathV[i][3];
		double distance = intervalDis;
		if (i==samplePathV.size()-2)   //最后一段长度不一定等长
			distance = std::sqrt((samplePathV[i][0]-samplePathV[i+1][0])*(samplePathV[i][0]-samplePathV[i+1][0])+(samplePathV[i][1]-samplePathV[i+1][1])*(samplePathV[i][1]-samplePathV[i+1][1])+(samplePathV[i][2]-samplePathV[i+1][2])*(samplePathV[i][2]-samplePathV[i+1][2]));
		double duration = distance/5;
		SyPath nseg(spos,tpos,crossFlag,duration,distance);
		nPath.push_back(nseg);
	}

	//写出
	m_IOManager.saveSamplePath((m_folder+GConstant::C_SAMPLE_PATH).c_str(),nPath);
}

void SyInterest::calConstant()
{
	calSCPath();        //计算scpath.xml
	constantProduce();   //constant speed
}

void SyInterest::calSCPath()
{
	SyIoManager IOManager;

	std::vector<SyPath>         samplePath;
	if (m_IOManager.loadSamplePath((m_folder+GConstant::C_SCSAMPLE_PATH).c_str(), samplePath))
	{
		return;
	}
	std::vector<EV2> toSmooth,SmoothRes;
	std::vector<float> heightV;  //暂时不用这个
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		toSmooth.push_back(EV2(m_samplePath[i].spos().x(),m_samplePath[i].spos().y()));
		heightV.push_back(m_samplePath[i].spos().z());
	}
	toSmooth.push_back(EV2(m_samplePath[m_samplePath.size()-1].tpos().x(), m_samplePath[m_samplePath.size()-1].tpos().y()));
	heightV.push_back(m_samplePath[m_samplePath.size()-1].tpos().z());
	//写出外部文件
	IOManager.writeSPath((m_folder+"\\path").c_str(), toSmooth);
	Sleep(1000);  //1s
	while (!IOManager.readSPath((m_folder+"\\scpath").c_str(), SmoothRes))
	{ 
		Sleep(10000);  //10s
	}

	samplePath = m_samplePath;
	//转成path格式
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		samplePath[i].setSpos(osg::Vec3(SmoothRes[i].x(),SmoothRes[i].y(), m_samplePath[i].spos().z()));  //z不变
		samplePath[i].setTpos(osg::Vec3(SmoothRes[i+1].x(),SmoothRes[i+1].y(), m_samplePath[i].tpos().z()));  
	}
	IOManager.saveSamplePath((m_folder+"\\scpath.xml").c_str(), samplePath);
}

void SyInterest::constantProduce()
{
	//速度
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		m_speedV.push_back(DEFAULTSPEED);
	}
	m_IOManager.writSpeed((m_folder + GConstant::C_GOOGLE_SPEED).c_str(),m_speedV);
	calRoadheadings();
	std::vector<SyView> viewVec;

	for (int i = 0; i <= m_samplePath.size(); ++i)
	{
		SyView nview;
		nview.m_heading = m_nroadheadings[i];  //heading
		nview.m_tilt = 8;
		nview.m_height = SyConfig::getInstance().getInitHeight();

		//计算相机提前距离
		double predistance = nview.m_height/tan(osg::DegreesToRadians(18 - nview.m_tilt));
		double disx = predistance*cos(osg::DegreesToRadians(GLOBAL::fixAngle360(m_nroadheadings[i]+90)));
		double disy = predistance*sin(osg::DegreesToRadians(GLOBAL::fixAngle360(m_nroadheadings[i]+90)));
		m_camPreCordsV.push_back(EV2(disx,disy));
		viewVec.push_back(nview);
	}

	m_IOManager.writeCamDis((m_folder + GConstant::C_CAMERA_GOOGLE_PRE).c_str(),m_camPreCordsV);
	m_IOManager.writeView((m_folder + GConstant::C_CAMERA_GOOGLE_VIEW).c_str(),viewVec);
}

void SyInterest::calRoadheadings()
{
	std::vector<SyPath>  smoothSamplePath;
	m_IOManager.loadSamplePath((m_folder+"\\scpath.xml").c_str(),smoothSamplePath);  
	
	int pathsize = m_samplePath.size();
	//相机设的以x正半轴为0度角
	EV2 xaxis(1,0);
	//将seg转换成点集
	std::vector<osg::Vec3> pointSet,spointSet;
	for (int i = 0; i <= pathsize; ++i)
	{ 
		if (i < pathsize)
		{
			spointSet.push_back(smoothSamplePath[i].spos());
		}
		else
		{
			spointSet.push_back(smoothSamplePath[pathsize-1].tpos());
		}
	}
	m_nroadheadings.clear();
	for (int i = 0; i < pathsize; ++i)
	{ 
		//根据速度计算后退量
		double curspeed;
		//std::cout<<i<<std::endl;
		if (i==pathsize)
			curspeed = m_speedV[pathsize-1];
		else
			curspeed = m_speedV[i];
		int m_PN = curspeed*SyConfig::getInstance().getDetaTime()/SyConfig::getInstance().getSampleInterval();

		//只向前看，取平均
		int m_postPN = m_PN;
		while ((i+m_postPN-pathsize)>0)
		{
			m_postPN--;
		}
		//计算新的heading
		double headingSum = 0;
		//计算一个初始评估角,用于检查heading越界问题
		osg::Vec3 iniDir = spointSet[i+m_postPN]-spointSet[i];
		EV2 EiniDir(iniDir.x(),iniDir.y());
		double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
		for (int j = 1; j <= m_postPN; ++j)
		{
			osg::Vec3 nDir;
			nDir = spointSet[i+j]-spointSet[i];
			EV2 EnDir(nDir.x(),nDir.y());
			double nheading = GLOBAL::fixAngle(GLOBAL::calTheta(EnDir,xaxis)-90);//检查heading越界问题 -180~180
			if (iniheading < -90 && nheading > 90) 
			{
				nheading = nheading - 360;
			}
			if (iniheading > 90 && nheading < -90)
			{
				nheading = nheading + 360;
			}

			headingSum += nheading;
		}
		m_nroadheadings.push_back(headingSum/m_postPN);
	}
	m_nroadheadings.push_back(m_nroadheadings[m_nroadheadings.size()-1]);
}

void SyInterest::calJump()
{
	//smoothJumpPath(ParameterManager::getInstance().getDeta());
	//calJumpView();
	smoothJumpTrajectory();
}

void SyInterest::smoothJumpPath(double deta)
{
	//生成速度
	const int lneigh = 800;
	const int rneigh = 1200;
	const double minV = 10.0;
	const double maxV = 5000.0;
	std::vector<std::pair<int,int>> cityrange = SyConfig::getInstance().getCityRange();
	std::vector<std::pair<int,int>> ramprange = SyConfig::getInstance().getRampRange();
	int lSize = ramprange[0].second - ramprange[0].first + 1 + lneigh;
	int RSize = ramprange[1].second - ramprange[1].first + 1 + rneigh;
	double lDeta = 500.0 / lSize;
	double rDeta = 500.0 / RSize;
	m_speedV.clear();
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		if (i < cityrange[0].second || i >= cityrange[1].first)
		{
			m_speedV.push_back(DEFAULTSPEED);
		}
		else if (i < ramprange[0].second + lneigh)
		{
			m_speedV.push_back(min(maxV, DEFAULTSPEED * (1 + lDeta * (i - ramprange[0].first + 1))));
		}
		else if (i > ramprange[1].first - rneigh)
		{
			m_speedV.push_back(max(minV, DEFAULTSPEED * (500 - rDeta * (i - (ramprange[1].first - rneigh)))));
		}
		else
		{
			m_speedV.push_back(DEFAULTSPEED * 500);
		}
	}
	m_IOManager.writSpeed((m_folder + GConstant::C_JUMP_SPEED).c_str(),m_speedV);

	//均值smooth
	m_samplePath.clear();
	m_IOManager.loadSamplePath((m_folder+GConstant::C_SJSAMPLE_PATH).c_str(), m_samplePath);
	std::vector<EV3> toSmooth,SmoothRes;

	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		toSmooth.push_back(EV3(m_samplePath[i].spos().x(),m_samplePath[i].spos().y(), m_samplePath[i].spos().z()));
	}
	toSmooth.push_back(EV3(m_samplePath[m_samplePath.size()-1].tpos().x(), m_samplePath[m_samplePath.size()-1].tpos().y(),m_samplePath[m_samplePath.size()-1].tpos().z()));

	for (int i = 0; i < toSmooth.size() - 1; ++i)
	{
		int neighbourSize = std::max(1.0, m_speedV[i]/deta);
		int LSize = neighbourSize;
		int RSize = neighbourSize;


		if(int(i-LSize)<0||int(i+RSize-toSmooth.size()+1)>0)
		{
			SmoothRes.push_back(toSmooth[i]);
			continue;
		}

		EV3 posSum(0, 0, 0); 
		int count = RSize+LSize+1;
		for (int j = i-LSize; j <= i+RSize; ++j)
		{
			posSum += toSmooth[j];
		}
		SmoothRes.push_back(posSum/count);
	}
	SmoothRes.push_back(toSmooth[toSmooth.size() - 1]);

	//转成path格式
	for (int i = 0; i < m_samplePath.size(); ++i)
	{
		m_samplePath[i].setSpos(osg::Vec3(SmoothRes[i].x(),SmoothRes[i].y(),SmoothRes[i].z()));  
		m_samplePath[i].setTpos(osg::Vec3(SmoothRes[i+1].x(),SmoothRes[i+1].y(),SmoothRes[i + 1].z()));  
	}
	m_IOManager.saveSamplePath((m_folder+GConstant::C_SJSAMPLE_PATH).c_str(), m_samplePath);
}

void SyInterest::calJumpView()
{
	calRoadheadings2();
	std::vector<SyView> viewVec;

	for (int i = 0; i <= m_speedV.size(); ++i)
	{
		double curspeed;
		if (i == m_speedV.size())
		{
			curspeed = m_speedV[i - 1];
		}
		else
		{
			curspeed = m_speedV[i];
		}
		SyView nview;
		nview.m_heading = m_nroadheadings[i];  //heading
		nview.m_height = min(3000.0, curspeed);
		nview.m_tilt = max(-30.0, 8.5-curspeed/4);

		//计算相机提前距离
		//double predistance = nview.m_height/tan(osg::DegreesToRadians(30 - nview.m_tilt));
		double predistance = min(200.0, 40 * (curspeed / 10));
		double disx = predistance*cos(osg::DegreesToRadians(GLOBAL::fixAngle360(m_nroadheadings[i]+90)));
		double disy = predistance*sin(osg::DegreesToRadians(GLOBAL::fixAngle360(m_nroadheadings[i]+90)));
		m_camPreCordsV.push_back(EV2(disx,disy));
		viewVec.push_back(nview);
	}

	m_IOManager.writeCamDis((m_folder + GConstant::C_CAMERA_JUMP_PRE).c_str(),m_camPreCordsV);
	m_IOManager.writeView((m_folder + GConstant::C_CAMERA_JUMP_VIEW).c_str(),viewVec);
}

void SyInterest::calRoadheadings2()
{
	std::vector<SyPath>  smoothSamplePath;
	m_IOManager.loadSamplePath((m_folder+GConstant::C_SJSAMPLE_PATH).c_str(),smoothSamplePath);  
	m_IOManager.readSpeed((m_folder + GConstant::C_JUMP_SPEED).c_str(),m_speedV);
	int pathsize = m_samplePath.size();
	//相机设的以x正半轴为0度角
	EV2 xaxis(1,0);
	//将seg转换成点集
	std::vector<osg::Vec3> pointSet,spointSet;
	for (int i = 0; i <= pathsize; ++i)
	{ 
		if (i < pathsize)
		{
			spointSet.push_back(smoothSamplePath[i].spos());
		}
		else
		{
			spointSet.push_back(smoothSamplePath[pathsize-1].tpos());
		}
	}
	m_nroadheadings.clear();
	for (int i = 0; i < pathsize; ++i)
	{ 
		//根据速度计算后退量
		double curspeed;
		//std::cout<<i<<std::endl;
		if (i==pathsize)
			curspeed = m_speedV[pathsize-1];
		else
			curspeed = m_speedV[i];
		int m_PN = curspeed*SyConfig::getInstance().getDetaTime()/SyConfig::getInstance().getSampleInterval();

		//只向前看，取平均
		int m_postPN = m_PN;
		while ((i+m_postPN-pathsize)>0)
		{
			m_postPN--;
		}
		//计算新的heading
		double headingSum = 0;
		//计算一个初始评估角,用于检查heading越界问题
		osg::Vec3 iniDir = spointSet[i+m_postPN]-spointSet[i];
		EV2 EiniDir(iniDir.x(),iniDir.y());
		double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
		for (int j = 1; j <= m_postPN; ++j)
		{
			osg::Vec3 nDir;
			nDir = spointSet[i+j]-spointSet[i];
			EV2 EnDir(nDir.x(),nDir.y());
			double nheading = GLOBAL::fixAngle(GLOBAL::calTheta(EnDir,xaxis)-90);//检查heading越界问题 -180~180
			if (iniheading < -90 && nheading > 90) 
			{
				nheading = nheading - 360;
			}
			if (iniheading > 90 && nheading < -90)
			{
				nheading = nheading + 360;
			}

			headingSum += nheading;
		}
		m_nroadheadings.push_back(headingSum/m_postPN);
	}
	m_nroadheadings.push_back(m_nroadheadings[m_nroadheadings.size()-1]);
}

void SyInterest::smoothJumpTrajectory()
{
	//读取pos，并写出文件
	std::vector<SyPath> samplePath;
	std::vector<EV2> camPreCordsV;
	m_IOManager.loadSamplePath((m_folder+GConstant::C_SJSAMPLE_PATH).c_str(), samplePath);
	m_IOManager.readCamDis((m_folder + GConstant::C_CAMERA_JUMP_PRE).c_str(), camPreCordsV);

	std::vector<EV2> toSmooth, smoothRes;

	for (int i = 0; i < samplePath.size(); ++i)
	{
		toSmooth.push_back(EV2(samplePath[i].spos()[0] - camPreCordsV[i].x(), samplePath[i].spos()[1] - camPreCordsV[i].y()));
	}
	toSmooth.push_back(EV2(samplePath[samplePath.size() - 1].tpos()[0] - camPreCordsV[samplePath.size()].x(), samplePath[samplePath.size() - 1].tpos()[1] - camPreCordsV[samplePath.size()].y()));
	m_IOManager.writeSPath((m_folder+"\\data\\jumpcam").c_str(),toSmooth);
	//smooth
	Sleep(1000);  //1s
	while (!m_IOManager.readSPath((m_folder+"\\data\\sjumpcam").c_str(),smoothRes))
	{ 
		Sleep(10000);  //10s
	}
	//写出
	camPreCordsV.clear();
	for (int i = 0; i < smoothRes.size(); ++i)
	{
		if (i < smoothRes.size() - 1)
		{
			camPreCordsV.push_back(EV2(samplePath[i].spos()[0] - smoothRes[i].x(), samplePath[i].spos()[1] - smoothRes[i].y()));
		}
		else
		{
			camPreCordsV.push_back(EV2(samplePath[i - 1].tpos()[0] - smoothRes[i].x(), samplePath[i - 1].tpos()[1] - smoothRes[i].y()));
		}
		
	}

	m_IOManager.writeCamDis((m_folder + GConstant::C_CAMERA_JUMP_PRE).c_str(), camPreCordsV);
}

void SyInterest::snapSampleViews( osg::ref_ptr<osg::Camera> cam, osg::ref_ptr<SynopsisManipulator> pSynopsisManipulator, int iter_num )
{
	
	generateIniPos(iter_num);
	bool isContinue = false;
	
	std::string info = "Total Images:"+GLOBAL::int2string(m_totalPointsNum);
	switch(QMessageBox::question(NULL,"Question",("sample all views?"+info).c_str(),
		QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Ok))
	{
	case QMessageBox::Ok:
		isContinue = true;
		break;
	case QMessageBox::Cancel:
		isContinue = false;
		break;
	default:
		break;
	}
	if( !isContinue )
		return;

	m_pSynopsisManipulator = pSynopsisManipulator;
	if(!QDir(m_imgs_dir.c_str()).exists())
	{
		bool flag = QDir().mkdir( m_imgs_dir.c_str() );
		if(!flag) 
			return;
	}

	//初始化
	m_isSnap = true;
	std::string rootName = m_imgs_dir + "/view-";
	std::string extName = ".png";
	m_snap_sc = new SyScreenCapture(rootName, extName);
	m_snap_cam = cam;
	cam->setPostDrawCallback( m_snap_sc.get() );
	m_snap_ImageNo = 0;
}

void SyInterest::generateIniPos(int iter_num)
{
	//生成inipos
	SyIoManager IOManager;
	std::vector<SyPath> samplePath;
	std::vector<double> speedV;
	if (iter_num == 1)
	{
		IOManager.loadSamplePath((m_folder+GConstant::C_SCSAMPLE_PATH).c_str(), samplePath);
		IOManager.readSpeed((m_folder+GConstant::C_GOOGLE_SPEED).c_str(),speedV);
	}
	else
	{
		IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(), samplePath);
		IOManager.readSpeed((m_folder+GConstant::C_OP_SPEED).c_str(),speedV);
	}

	EV2 xaxis(1,0);
	double x, y, z, t, h;
	for (int i = 0; i <= samplePath.size(); ++i)
	{
		if (i == 0)   //初始的第一点
		{
			osg::Vec3 iniDir = samplePath[i].tpos() - samplePath[i].spos();
			EV2 EiniDir(iniDir.x(),iniDir.y());
			double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
			int dis;
			if (iter_num == 1)
			{
				dis = 10;
			}
			else
			{
				dis = 30;
			}
			double disx = dis*cos(osg::DegreesToRadians(GLOBAL::fixAngle360(iniheading+90)));
			double disy = dis*sin(osg::DegreesToRadians(GLOBAL::fixAngle360(iniheading+90)));
			x = samplePath[i].spos().x() - disx;
			y = samplePath[i].spos().y() - disy;
			if (speedV[i] < 5)
			{
				speedV[i] = 5;
			}

			int curheight = 15;
			z = samplePath[i].spos().z() + curheight;
			h = iniheading;
			t = -((speedV[i] - 5.0)/(100.0-5.0)*(40.0-10.0) + 10.0) + 20.0;
		}
		else
		{
			osg::Vec3 iniDir = samplePath[i-1].tpos() - samplePath[i-1].spos();
			EV2 EiniDir(iniDir.x(),iniDir.y());
			double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
			x = samplePath[i-1].spos().x();
			y = samplePath[i-1].spos().y();
			if (speedV[i-1] < 5)
			{
				speedV[i-1] = 5;
			}
			int curheight = 4 * speedV[i-1] * sin(osg::DegreesToRadians(180.0 - (20.0 - t) - 20.0))/sin(osg::DegreesToRadians(20.0))* sin(osg::DegreesToRadians(20.0 - t));
			z = samplePath[i-1].spos().z() + curheight;
			h = iniheading;
			t = -((speedV[i-1] - 5.0)/(100.0-5.0)*(40.0-10.0) + 10.0) + 20.0;
		}
		std::vector<double>  curP;
		curP.push_back(x);
		curP.push_back(y);
		curP.push_back(z);
		curP.push_back(t);
		curP.push_back(h);
		m_curIniPos.push_back(curP);
	}
}

void SyInterest::snapTick()
{
	if (!m_isSnap || m_snap_sc->getCaptureEnabled())
	{
		return;
	}

	if (!m_camDone)
	{
		if (m_snap_ImageNo > m_smoothPath.size())
		  return;
		camControl(m_snap_ImageNo);
		m_camDone = true;
	}
	else
	{
		double fovy,ratio,znear,zfar;
		m_snap_cam->getProjectionMatrixAsPerspective(fovy,ratio,znear,zfar);
		m_snap_sc->setNearFar(znear,zfar);
		m_snap_sc->setCapture(true);
		m_snap_sc->setUseFrameNumber(false);
		m_snap_sc->setSnapNo(++m_snap_ImageNo);
		m_snap_sc->setNumFramesToCapture(1);
		m_camDone = false;
	}
}

void SyInterest::camControl( int pointNo )
{
	if(pointNo > m_smoothPath.size())
		return;
	m_pSynopsisManipulator->setPosition(m_curIniPos[pointNo][0], m_curIniPos[pointNo][1], m_curIniPos[pointNo][2]);
	m_pSynopsisManipulator->setTargetPosition(m_curIniPos[pointNo][4], m_curIniPos[pointNo][3]);
}

void SyInterest::imageProcess()
{
	if (!m_IOManager.readImportance((m_folder+GConstant::C_IMPORTANCE).c_str(),m_importance))
	{
		m_importance.clear();
		m_importance.insert(m_importance.begin(), m_totalPointsNum, 0);

#ifdef TBB_USE
		tbb::parallel_for(tbb::blocked_range<int>(0, m_totalPointsNum), 
			[&](const tbb::blocked_range<int>& r)
		{
			for (int i = r.begin(); i < r.end(); ++i)
			{
				std::string depthimg_fn=m_imgs_dir+"view-"+GLOBAL::int2string(i+1)+"_depth.png";
				std::string colorimg_fn=m_imgs_dir+"view-"+GLOBAL::int2string(i+1)+"_rgba.png";

				IplImage*    depthimg = cvLoadImage(depthimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
				IplImage*    colorimg = cvLoadImage(colorimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);

				if (depthimg!=NULL&&colorimg!=NULL)
				{
					double viewImportance = 0;
					for(int m=0; m<colorimg->height; m++)
					{
						for(int n=0; n<colorimg->width; n++)
						{
							CvScalar depthPixel = cvGet2D(depthimg, m, n);
							CvScalar colorPixel = cvGet2D(colorimg, m, n);

							double dis = sqrt((m-m_midY)*(m-m_midY)+(n-m_midX)*(n-m_midX));
							double K = 3.1415/2;
							double exponent = 0.5;
							double val = dis/m_radius;
							double cenW = pow(cos(K*val),exponent);
							viewImportance += cenW*getImportanceForPixel(depthPixel,colorPixel);
						}
					}
					viewImportance /=m_totalPixelNum;
					if (viewImportance<0.001)
						viewImportance = 0.001;

					m_importance[i]+=viewImportance;
				}
				cvReleaseImage(&colorimg);
				cvReleaseImage(&depthimg);
			}
		});
#else
		for (int i = 0; i < m_totalPointsNum; ++i)
		{
			std::string depthimg_fn=m_imgs_dir+"\\view-"+GLOBAL::int2string(i+1)+"_depth.png";
			std::string colorimg_fn=m_imgs_dir+"\\view-"+GLOBAL::int2string(i+1)+"_rgba.png";

			IplImage*    depthimg = cvLoadImage(depthimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
			IplImage*    colorimg = cvLoadImage(colorimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);

			if (depthimg!=NULL&&colorimg!=NULL)
			{
				double viewImportance = 0;
				for(int m=0; m<colorimg->height; m++)
				{
					for(int n=0; n<colorimg->width; n++)
					{
						CvScalar depthPixel = cvGet2D(depthimg, m, n);
						CvScalar colorPixel = cvGet2D(colorimg, m, n);

						double dis = sqrt((m-m_midY)*(m-m_midY)+(n-m_midX)*(n-m_midX));
						double K = 3.1415/2;
						double exponent = 0.5;
						double val = dis/m_radius;
						double cenW = pow(cos(K*val),exponent);
						viewImportance += cenW*getImportanceForPixel(depthPixel,colorPixel);
					}
				}
				viewImportance /=m_totalPixelNum;
				if (viewImportance<0.001)
					viewImportance = 0.001;

				m_importance[i]+=viewImportance;

			}
			cvReleaseImage(&colorimg);
			cvReleaseImage(&depthimg);
		}
#endif
		m_IOManager.writeImportance((m_folder+GConstant::C_IMPORTANCE).c_str(),m_importance);
	}

	m_timer.end();
}

double SyInterest::getImportanceForPixel( CvScalar& depthpixel,CvScalar& colorpixel )
{
	if (colorpixel.val[0]==255&&colorpixel.val[1]==255&&colorpixel.val[2]==255)
	{
		return 0;
	}
	double depthThre = SyConfig::getInstance().getDepthThrehold()*255/300;
	double depthval = depthpixel.val[0];
	double colorval = color2scalar(colorpixel);
	double importanceVal = 1.0;
	if (depthval<depthThre)
		importanceVal = depthval/depthThre;        
	importanceVal = importanceVal*colorval;
	return importanceVal;
}

double SyInterest::color2scalar( CvScalar& npixel )
{
	double res;
	if(npixel.val[2]>0)
		res = (npixel.val[2]-npixel.val[1]+765)/1020;
	else
		res = (npixel.val[1]-npixel.val[0]+255)/1020;
	return res;
}

void SyInterest::solveSpeed()
{
	accumlateSpeed();
}

void SyInterest::accumlateSpeed()
{
	m_timer.start("accumlateSpeed");
	SyConfig::getInstance().loadConfig((m_folder+GConstant::C_CONFIG).c_str());

	if (m_importance.size()==0)
	{
		m_IOManager.readImportance((m_folder+GConstant::C_IMPORTANCE).c_str(),m_importance);
	}

	m_viewImp.clear();
	for (int i = 0; i < m_importance.size()-1; ++i)
	{
		m_viewImp.push_back((m_importance[i]+m_importance[i+1])/2);
	}
	//反求初始速度
	std::vector<double> inispeedVec;
	double totalInterest = 0;
	for (int i = 0; i < m_viewImp.size(); ++i)
	{
		if (m_viewImp[i]<0.01)   // 此处0.005 可调
			m_viewImp[i] = 0.01;
		totalInterest += m_viewImp[i];
	}
	double totalTime = ParameterManager::getInstance().getTotalTime();
	double dis = SyConfig::getInstance().getSampleInterval(); 
	for (int i = 0; i < m_viewImp.size(); ++i)
	{
		inispeedVec.push_back(dis/(m_viewImp[i]/totalInterest*totalTime)); 
	}
	//bezier curve fit speed  由外部调用matlab
	m_IOManager.writSpeed((m_folder + "//data//speed//tmp//inispeed").c_str(),inispeedVec);

	//计算speedSection
	std::vector<EV2> speedSection;
	int section_num = m_samplePath.size()/500;     //500一段,一段长度不能超过1000
	int section_size = 500 + (m_samplePath.size()-section_num*500)/section_num;
	int last_section_size = m_samplePath.size() - section_size * (section_num-1);
	for (int i = 0; i < section_num; ++i)
	{
		if (i < section_num-1)
			speedSection.push_back(EV2(i*section_size+1,(i+1)*section_size));
		else
			speedSection.push_back(EV2(i*section_size+1,i*section_size+last_section_size));
	}
	m_IOManager.writeSection((m_folder + "//data//speed//tmp//speedsection").c_str(),speedSection);

	//
	//外部光滑
	///

	m_timer.end();
}

void SyInterest::weightedSpeed()
{

}

void SyInterest::smoothPath()
{
     calCurvature();
	 doSmooth();
	 externBezier();
}

void SyInterest::calCurvature()
{
	SyIoManager IOManager;
	std::vector<SyPath> samplePath;
	std::vector<double> speedV;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(), samplePath);
	IOManager.readSpeed((m_folder+GConstant::C_OP_SPEED).c_str(), speedV);
	//转成直接处理的存储格式
	std::vector<EV2> pathV;
	for (int i = 0; i < samplePath.size(); ++i)
	{
		pathV.push_back(EV2(samplePath[i].spos().x(),samplePath[i].spos().y()));
	}
	pathV.push_back(EV2(samplePath[samplePath.size()-1].tpos().x(),samplePath[samplePath.size()-1].tpos().y()));
	//计算曲率，由速度决定邻域
	m_curvature.insert(m_curvature.begin(),pathV.size(), 0);
	for (int i = 0; i < pathV.size(); ++i)
	{
		if (i == 0 || i == pathV.size() - 1)
		{
			continue;
		}
		if (speedV[i] > 100)
		{
			speedV[i] = 100;
		}
		EV2 lDir(pathV[i] - pathV[i - 1]);
		EV2 rDir(pathV[i + 1] - pathV[i]);
		lDir.normalize();
		rDir.normalize();

		int angle = osg::RadiansToDegrees(acos(lDir.dot(rDir)));      //radian

		if (angle < 0)
		{
			if (i > 1)
				m_curvature[i] = m_curvature[i - 1];
			else
				m_curvature[i] = 1;
		}
		else
		{
			int curve = angle / 10 + 1;    //1~19
			m_curvature[i] = curve;
		}

		if (i == 1)
		{
			m_curvature[i - 1] = m_curvature[i];
		}
		if (i == pathV.size() - 2)
		{
			m_curvature[i + 1] = m_curvature[i];
		}
	}
	//smooth curvature
	std::vector<double> curvature = m_curvature;
	for (int i = 0; i < m_curvature.size(); ++i)
	{
		if (i < 10 || m_curvature.size() - 1 - i < 10)
		{
			m_curvature[i] *= speedV[i];
			continue;
		}
		double sum = 0;
		for (int j = -10; j <= 10; ++j)
		{
			sum += curvature[i + j];
		}
		
		m_curvature[i] = (sum / 21.0) * speedV[i];
	}

}

void SyInterest::calCurvature2()
{
	SyIoManager IOManager;
	std::vector<SyPath> samplePath;
	std::vector<double> speedV;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(), samplePath);
	IOManager.readSpeed((m_folder+GConstant::C_OP_SPEED).c_str(), speedV);

	//转成直接处理的存储格式
	std::vector<EV2> pathV;
	for (int i = 0; i < samplePath.size(); ++i)
	{
		pathV.push_back(EV2(samplePath[i].spos().x(),samplePath[i].spos().y()));
	}
	pathV.push_back(EV2(samplePath[samplePath.size()-1].tpos().x(),samplePath[samplePath.size()-1].tpos().y()));

	//计算曲率，由速度决定邻域
	m_curvature.insert(m_curvature.begin(),pathV.size(), 0);

	for (int i = 0; i < pathV.size(); ++i)
	{
		if (i == 0 || i == pathV.size() - 1)
		{
			continue;
		}
		if (speedV[i] > 100)
		{
			speedV[i] = 100;
		}

		int neighbourSize = std::max(1.0,speedV[i]/3);
		int LSize = neighbourSize;
		int RSize = neighbourSize;

		while ( int(i - LSize) < 1)
		{
			LSize--;
		}

		while ( int(i + RSize - pathV.size() + 1) > -1)
		{
			RSize--;
		}

		double sum = 0;
		int count = 0;
		for (int j = -LSize; j <= RSize; ++j)
		{
			EV2 lDir(pathV[i + j] - pathV[i + j - 1]);
			EV2 rDir(pathV[i + j + 1] - pathV[i + j]);
			double angle = curvature(lDir, rDir);
			if (angle < 0)
			{
				count++;
			}
			else
			{
				sum += angle;
			}
		}
		 
		double scale = (neighbourSize * 2 + 1.0) / (LSize + RSize - count + 1.0);
		sum *= scale;
		m_curvature[i] = sum;

		if (i == 1)
		{
			m_curvature[i - 1] = m_curvature[i];
		}
		if (i == pathV.size() - 2)
		{
			m_curvature[i + 1] = m_curvature[i];
		}
	}

	//smooth curvature
	std::vector<double> curvature = m_curvature;
	for (int i = 0; i < m_curvature.size(); ++i)
	{
		if (i < 10 || m_curvature.size() - 1 - i < 10)
		{
			continue;
		}
		double sum = 0;
		for (int j = -10; j <= 10; ++j)
		{
			sum += curvature[i + j];
		}

		m_curvature[i] = sum / 21.0;
	}

    //////////////////test///////////////////
	IOManager.writSpeed((m_folder + "\\data\\test").c_str(), m_curvature);
}

double SyInterest::curvature(EV2 lD, EV2 rD)
{
	lD.normalize();
	rD.normalize();
	int tmp = osg::RadiansToDegrees(acos(lD.dot(rD)));      //radian
	double angle = tmp + 1;
	return angle;
}

void SyInterest::doSmooth()
{
	SyIoManager IOManager;
	std::vector<SyPath> samplePath;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(), samplePath);

	//转成直接处理的存储格式
	std::vector<EV2> toSmooth,SmoothRes;
	std::vector<float> heightV;  //暂时不用这个
	for (int i = 0; i < samplePath.size(); ++i)
	{
		toSmooth.push_back(EV2(samplePath[i].spos().x(),samplePath[i].spos().y()));
		heightV.push_back(samplePath[i].spos().z());
	}
	toSmooth.push_back(EV2(samplePath[samplePath.size()-1].tpos().x(),samplePath[samplePath.size()-1].tpos().y()));
	heightV.push_back(samplePath[samplePath.size()-1].tpos().z());

	int neighbourSize = 5;   //  5
	for (int i = 0; i < toSmooth.size(); ++i)
	{
		/*if (m_curvature[i] > 100)
		{
			m_curvature[i] = 100;
		}*/
		int scale = std::max(1.0,m_curvature[i]/7);
		int LSize = neighbourSize*scale;
		int RSize = neighbourSize*scale;

		if((i-LSize)<0||int(i+RSize-toSmooth.size()+1)>0)
		{
			SmoothRes.push_back(toSmooth[i]);
			continue;
		}

		EV2 posSum(0,0); 
		int count = RSize+LSize+1;
		for (int j = i-LSize; j <= i+RSize; ++j)
		{
			posSum += toSmooth[j];
		}
		SmoothRes.push_back(posSum/count);
	}

	//转成path格式
	for (int i = 0; i < samplePath.size(); ++i)
	{
		samplePath[i].setSpos(osg::Vec3(SmoothRes[i].x(),SmoothRes[i].y(),samplePath[i].spos().z()));  //z不变
		samplePath[i].setTpos(osg::Vec3(SmoothRes[i+1].x(),SmoothRes[i+1].y(),samplePath[i].tpos().z()));  
	}
	IOManager.saveSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),samplePath);
}

void SyInterest::externBezier()
{
	SyIoManager IOManager;
	std::vector<SyPath> samplePath;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),samplePath);

	//转成直接处理的存储格式
	std::vector<EV2> toSmooth,SmoothRes;
	std::vector<float> heightV;  //暂时不用这个
	for (int i = 0; i < samplePath.size(); ++i)
	{
		toSmooth.push_back(EV2(samplePath[i].spos().x(),samplePath[i].spos().y()));
		heightV.push_back(samplePath[i].spos().z());
	}
	toSmooth.push_back(EV2(samplePath[samplePath.size()-1].tpos().x(),samplePath[samplePath.size()-1].tpos().y()));
	heightV.push_back(samplePath[samplePath.size()-1].tpos().z());
	//写出外部文件
	IOManager.writeSPath((m_folder+"\\path").c_str(),toSmooth);
	Sleep(1000);  //1s
	while (!IOManager.readSPath((m_folder+"\\spath").c_str(),SmoothRes))
	{ 
		Sleep(10000);  //10s
	}
	//转成path格式
	for (int i = 0; i < samplePath.size(); ++i)
	{
		samplePath[i].setSpos(osg::Vec3(SmoothRes[i].x(),SmoothRes[i].y(),samplePath[i].spos().z()));  //z不变
		samplePath[i].setTpos(osg::Vec3(SmoothRes[i+1].x(),SmoothRes[i+1].y(),samplePath[i].tpos().z()));  
	}
	IOManager.saveSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),samplePath);
}

void SyInterest::viewProduce(int step)
{
	if (step == 1 )
	{
		generateForMatlab();
	}
	else if (step == 2)
	{
		generateFromMatlab();
	}
	else if(step == 3)
	{
		smoothMatlabRes();
	}
}

void SyInterest::generateForMatlab()
{
	SyIoManager IOManager;
	// ini pos
	std::vector<SyPath> samplePath;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),samplePath);
	std::vector<double> speedV;
    IOManager.readSpeed((m_folder+GConstant::C_OP_SPEED).c_str(),speedV);
	
	//
	EV2 xaxis(1,0);
	std::ofstream fout(m_folder+"\\data\\inipos",std::ios::binary);
	double x, y, z, t, h;

	for (int i = 0; i <= samplePath.size(); ++i)
	{ 
		if (i == 0)
		{
			osg::Vec3 iniDir = samplePath[i].tpos() - samplePath[i].spos();
			EV2 EiniDir(iniDir.x(),iniDir.y());
			double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
			double disx = 30*cos(osg::DegreesToRadians(GLOBAL::fixAngle360(iniheading+90)));
			double disy = 30*sin(osg::DegreesToRadians(GLOBAL::fixAngle360(iniheading+90)));
			x = samplePath[i].spos().x() - disx;
			y = samplePath[i].spos().y() - disy;
			if (speedV[i] < 5)
			{
				speedV[i] = 5;
			}
			int curheight = 15;
			z = samplePath[i].spos().z() + curheight;
			h = iniheading;
			t = -((speedV[i] - 5.0)/(100.0-5.0)*(40.0-10.0) + 10.0) + 20.0;
			fout<<x<<" "<<y<<" "<<z<<" "<<t<<" "<<h<<std::endl;
		}
		else
		{
			osg::Vec3 iniDir = samplePath[i-1].tpos() - samplePath[i-1].spos();
			EV2 EiniDir(iniDir.x(),iniDir.y());
			double iniheading = GLOBAL::fixAngle(GLOBAL::calTheta(EiniDir,xaxis)-90);
			x = samplePath[i-1].spos().x();
			y = samplePath[i-1].spos().y();
			if (speedV[i-1] < 5)
			{
				speedV[i-1] = 5;
			}

			int curheight = 4 * speedV[i-1] * sin(osg::DegreesToRadians(180.0 - (20.0 - t) - 20.0))/sin(osg::DegreesToRadians(20.0))* sin(osg::DegreesToRadians(20.0 - t));
			z = samplePath[i-1].spos().z() + curheight;
			h = iniheading;
			t = -((speedV[i-1] - 5.0)/(100.0-5.0)*(40.0-10.0) + 10.0) + 20.0;

			fout<<x<<" "<<y<<" "<<z<<" "<<t<<" "<<h;
			if (i < samplePath.size())
				fout<<std::endl;
		}
	}
	fout.close();


	//car pos
	std::vector<SyPath> smoothPath;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),smoothPath);    //注意是spath
	std::vector<EV3> posvec;
	for (int i = 0; i < smoothPath.size(); ++i)
	{
		posvec.push_back(EV3(smoothPath[i].spos().x(),smoothPath[i].spos().y(),smoothPath[i].spos().z()));
	}
	posvec.push_back(EV3(smoothPath[smoothPath.size()-1].tpos().x(),smoothPath[smoothPath.size()-1].tpos().y(),smoothPath[smoothPath.size()-1].tpos().z()));

	IOManager.writeSPath((m_folder+"\\data\\carpos").c_str(),posvec);
}

void SyInterest::generateFromMatlab()
{
	SyIoManager IOManager;

	//ini pos
	std::vector<SyPath> samplePath;
	std::vector<EV2> camDisV;
	std::vector<SyView> viewVec;
	IOManager.loadSamplePath((m_folder+GConstant::C_SSAMPLE_PATH).c_str(),samplePath);


	std::ifstream fin((m_folder+"\\data\\res").c_str(),std::ios::binary);
	double x, y, z, t, h;
	double prex, prey;
	double height;
	for (int i = 0; i <= samplePath.size(); ++i)
	{
		fin>>x>>y>>z>>t>>h;
		if (i == samplePath.size())
		{
			prex = samplePath[i - 1].tpos().x() - x;
			prey = samplePath[i - 1].tpos().y() - y;
			height = z - samplePath[i - 1].tpos().z();
		}
		else
		{
			prex = samplePath[i].spos().x() - x;
			prey = samplePath[i].spos().y() - y;
			height = z - samplePath[i].spos().z();
		}
		SyView nview;
		nview.m_heading = h;
		nview.m_tilt = t;
		nview.m_height = height;
		viewVec.push_back(nview);

		camDisV.push_back(EV2(prex, prey));
	}
	fin.close();

	IOManager.writeCamDis((m_folder+GConstant::C_CAMERA_PRE).c_str(), camDisV);
	IOManager.writeView((m_folder+GConstant::C_CAMERA_OP_SVIEW).c_str(), viewVec);
}

void SyInterest::smoothMatlabRes()
{
	std::cout<<"smooth cam path!"<<std::endl;

	std::vector<EV2> toSmooth, SmoothRes;
	std::vector<double> heightV, sHeightV;
	std::vector<std::vector<double>> otherres;
	std::ifstream fin((m_folder+"\\data\\res").c_str(),std::ios::binary);

	if (!fin)
	{
		std::cout<<"res file doesn't exist!\n";
		return;
	}
	while(!fin.eof())
	{
		double x, y, z, t, h;
		std::vector<double> res;
		fin>>x>>y>>z>>t>>h;
		toSmooth.push_back(EV2(x,y));
		heightV.push_back(z);
		res.push_back(t);
		res.push_back(h);
		otherres.push_back(res);
	}
	fin.close();

	//写出外部文件
	SyIoManager IOManager;
	IOManager.writeSPath((m_folder+"\\data\\campath").c_str(),toSmooth);
	Sleep(1000);  //1s
	while (!IOManager.readSPath((m_folder+"\\data\\scampath").c_str(),SmoothRes))
	{ 
		Sleep(10000);  //10s
	}

	IOManager.writSpeed((m_folder+"\\data\\height").c_str(),heightV);
	Sleep(1000);  //1s
	while (!IOManager.readSpeed((m_folder+"\\data\\sheight").c_str(),sHeightV))
	{ 
		Sleep(10000);  //10s
	}

	std::ofstream fout((m_folder+"\\data\\nres").c_str(),std::ios::binary);

	for (int i = 0; i < SmoothRes.size()-1; ++i)
	{
		fout<<SmoothRes[i].x()<<" "<<SmoothRes[i].y()<<" "<<sHeightV[i]<<" "<<otherres[i][0]<<" "<<otherres[i][1]<<std::endl;  
	}
	fout<<SmoothRes[SmoothRes.size()-1].x()<<" "<<SmoothRes[SmoothRes.size()-1].y()<<" "<<sHeightV[SmoothRes.size()-1]<<" "<<otherres[SmoothRes.size()-1][0]<<" "<<otherres[SmoothRes.size()-1][1]; //防止多出一行空白
	fout.close();
}

void SyInterest::generateMap()
{
	getFileList();
	imgPro();
}

void SyInterest::getFileList()
{
	std::string rootDir = m_folder+GConstant::C_VIDEO_PATH;
	QDir videodir(rootDir.c_str());
	if (!videodir.exists())
		return;
	std::string rgba = "rgba", depth = "depth";
	videodir.setFilter(QDir::Files);
	QFileInfoList list = videodir.entryInfoList();
	std::vector<int> rgbaV;
	std::vector<int> depthV;
	int i = 0;
	do{
		QFileInfo fileInfo = list.at(i);
		if(fileInfo.fileName()=="."|fileInfo.fileName()=="..")
			continue;
		if (fileInfo.isFile())
		{
			std::string extension =fileInfo.suffix().toStdString();
			if(extension == GConstant::C_IMAGE_EXTENSION)
			{
				i++;
				std::string baseName = fileInfo.baseName().toStdString();
				std::vector<std::string> keyList;
				boost::split( keyList, baseName, boost::is_any_of( "_" ), boost::token_compress_on );
				if (keyList[1]=="rgba")
					rgbaV.push_back(std::atoi(keyList[0].c_str()));
				if (keyList[1]=="depth")
					depthV.push_back(std::atoi(keyList[0].c_str()));
			}
		}

	}
	while(i<list.size());
	//只取两个同时存在的情况
	for (int i = 0; i < rgbaV.size(); ++i)
	{
		for (int j = 0; j < depthV.size(); ++j)
		{
			if (rgbaV[i]==depthV[j])
			{
				m_VideoImgV.push_back(rgbaV[i]);
				break;
			}
		}

	}
	sort(m_VideoImgV.begin(),m_VideoImgV.end());
}

void SyInterest::imgPro()
{
	std::string rootDir = m_folder+GConstant::C_VIDEO_PATH;
	std::string resultfile = rootDir+"\\result";

	m_timer.start("map generate");
	m_importance.clear();
	for (int i = 0; i < m_totalPointsNum; ++i)
	{
		m_importance.push_back(0);
	}

	for (int i = 0; i < m_VideoImgV.size(); ++i)
	{
		std::string depthimg_fn=rootDir+"\\"+GLOBAL::int2string(m_VideoImgV[i])+"_depth.png";
		std::string colorimg_fn=rootDir+"\\"+GLOBAL::int2string(m_VideoImgV[i])+"_rgba.png";
		std::string impimg_fn=rootDir+"\\"+GLOBAL::int2string(m_VideoImgV[i])+".png";

		IplImage*    depthimg = cvLoadImage(depthimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
		IplImage*    colorimg = cvLoadImage(colorimg_fn.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
		IplImage*    impimg = cvCreateImage(cvSize((int)(colorimg->width),(int)(colorimg->height)), IPL_DEPTH_32F, 3); 
		if (depthimg!=NULL&&colorimg!=NULL)
		{
			double viewImportance = 0;
			for(int m=0; m<colorimg->height; m++)
			{
				for(int n=0; n<colorimg->width; n++)
				{
					CvScalar depthPixel = cvGet2D(depthimg, m, n);
					CvScalar colorPixel = cvGet2D(colorimg, m, n);

					double dis = sqrt((m-m_midY)*(m-m_midY)+(n-m_midX)*(n-m_midX));
					double K = 3.1415/2;
					double exponent = 0.5;
					double val = dis/m_radius;
					double cenW = pow(cos(K*val),exponent);
					double pixelImpVal = cenW*getImportanceForPixel(depthPixel,colorPixel);
					viewImportance += pixelImpVal;
					//获得重要性图片pixel
					EV4 ecolor = GLOBAL::scalar2color(pixelImpVal);
					CvScalar ncolorPixel;
					ncolorPixel.val[2] = ecolor[0]*255;
					ncolorPixel.val[1] = ecolor[1]*255;
					ncolorPixel.val[0] = ecolor[2]*255;
					ncolorPixel.val[3] = ecolor[3]*255;
					cvSet2D(impimg, m, n, ncolorPixel);
				}
			}
			viewImportance /=m_totalPixelNum;
			m_importance[i]=viewImportance;
		}

		cvReleaseImage(&colorimg);
		cvReleaseImage(&depthimg);
		cvSaveImage(impimg_fn.c_str(),impimg);
	}

	m_IOManager.writeImportance(resultfile.c_str(),m_importance);
	m_timer.end();
}

void SyInterest::msSpeed()
{
	std::vector<double> KeyPoints;   // 默认keypoints相隔距离都很大，至少大于80
	m_IOManager.readSpeed((m_folder+"\\data\\speed\\cross").c_str(),KeyPoints);
	std::vector<double> msSpeed;
	msSpeed.resize(m_totalPointsNum - 1);
	for (int i = 0; i < KeyPoints.size() - 1; ++i)
	{
		double Dis = KeyPoints[i + 1] - KeyPoints[i];
		double V = 200 * log(Dis / 500 + 1) + (2 * Dis) / 500 + 20;
		for (int j = KeyPoints[i]; j <= KeyPoints[i + 1]; ++j)
		{
			if (j < KeyPoints[i] + 40 || j > KeyPoints[i + 1] - 40)
			{
				msSpeed[j] = 10;
			}
			else
			{
				msSpeed[i] = V;
			}
		}
	}

	//smooth
	for (int i = 1; i < msSpeed.size(); ++i)
	{
		msSpeed[i] = 0.75 * msSpeed[i] + 0.25 * msSpeed[i - 1];
	}
	m_IOManager.readSpeed((m_folder+"\\data\\speed\\msspeed").c_str(),msSpeed);
}

void SyInterest::cmpImp()
{
	std::vector<double> lImpV, impV;
	SyIoManager IOManager;
	bool lflag = IOManager.readImportance((m_folder+GConstant::C_LIMPORTANCE).c_str(),lImpV);
	bool flag = IOManager.readImportance((m_folder+GConstant::C_IMPORTANCE).c_str(),impV);
	if (lflag && flag)
	{
		double sum = 0;
		for (int i = 0; i < lImpV.size(); ++i)
		{
			sum += std::abs(lImpV[i] - impV[i]);
		}
		sum /= lImpV.size();
		if (sum < 0.0012)
		{
			std::cout<<sum<<std::endl;
			QMessageBox::about(NULL,"NOTICE","convergence!");
		}
		else
		{
			std::cout<<sum<<std::endl;
			QMessageBox::about(NULL,"NOTICE","no convergence!");
		}
	}
	else
	{
		QMessageBox::about(NULL,"NOTICE","no data!");
	}
}

void SyInterest::calOptAcc()
{
	m_timer.start("Acceleration");
	if (!m_IOManager.readImportance((m_folder+GConstant::C_OPTICALFLOWACC).c_str(),m_OptAccV))
	{
		m_OptAccV.clear();
		m_OptAccV.insert(m_OptAccV.begin(), OFA_SIZE, 0);

        for (int k = 0; k < OFA_SIZE; k ++)
		{
			if(k == 0 || k == OFA_SIZE - 1)
			{
				continue;
			}

			QString dat_fn_p = (m_video_dir+"\\"+GLOBAL::int2string(2 * (k - 1) + 1)+".dat").c_str();
			QString dat_fn = (m_video_dir+"\\"+GLOBAL::int2string(2 * k + 1)+".dat").c_str();
			QString dat_fn_n = (m_video_dir+"\\"+GLOBAL::int2string(2 * (k + 1) + 1)+".dat").c_str();

			osg::ref_ptr<osg::Vec3dArray> worldPoints = new osg::Vec3dArray;
			osg::ref_ptr<osg::Vec3dArray> screenPoints1 = new osg::Vec3dArray;
			osg::ref_ptr<osg::Vec3dArray> screenPoints2 = new osg::Vec3dArray;

			getWorldPoints(dat_fn, worldPoints);
			getScreenPoints(dat_fn_p, worldPoints, screenPoints1);
			getScreenPoints(dat_fn_n, worldPoints, screenPoints2);

			for (int i = 0; i < HEIGHT; i++)
			{
				for (int j = 0; j < WIDTH; j++)
				{
					osg::Vec3d screen1 = screenPoints1->at(i * WIDTH + j);
					osg::Vec3d screen2 = screenPoints2->at(i * WIDTH + j);
					if (screen1.x() < 0 || screen1.x() > WIDTH || screen1.y() < 0 || screen1.y() > HEIGHT
						|| screen2.x() < 0 || screen2.x() > WIDTH || screen2.y() < 0 || screen2.y() > HEIGHT)
					{
						m_OptAccV[k] += 50;
						continue;
					}

					double deltaY1 = i - screen1.y();
					double deltaX1 = j - screen1.x();
					double deltaY2 = screen2.y() - i;
					double deltaX2 = screen2.x() - j;
					osg::Vec2d delta1(deltaX1, deltaY1);
					osg::Vec2d delta2(deltaX2, deltaY2);
					osg::Vec2d acceleration = delta2 - delta1;
					m_OptAccV[k] += acceleration.length();
				}
			}

			m_OptAccV[k] /= m_totalPixelNum;

			if (k == 1)
			{
				m_OptAccV[k - 1] = m_OptAccV[k];
			}
			if(k == OFA_SIZE - 2)
			{
				m_OptAccV[k + 1] = m_OptAccV[k];
			}
		}


		m_IOManager.writeImportance((m_folder+GConstant::C_OPTICALFLOWACC).c_str(),m_OptAccV);
		///////////////////////////////////////////////////////////////////////////
		////外部光滑
		/////
	}
	
	m_timer.end();
}

void SyInterest::getWorldPoints( const QString &datFile, osg::ref_ptr<osg::Vec3dArray> &worldPoints )
{
	osg::Matrix VPW;
	QVector<double> depths;
	QMatrix4x4 matrix;
	QFile file(datFile);
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);  
	in.setVersion(QDataStream::Qt_4_8);
	while(!in.atEnd()){
		in >> matrix;
		in >> depths;

	}
	fromQtMatrix2OsgMatrix(matrix, VPW);
	osg::Matrixf invetVPW;
	invetVPW.invert(VPW);
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			double depth = depths.at(i * WIDTH + j );
			osg::Vec3d screen(j, i, depth);
			osg::Vec3d world = screen * invetVPW;
			worldPoints->push_back(world);//计算每个点的世界坐标
		}
	}
}

void SyInterest::getScreenPoints( const QString &datFile, osg::ref_ptr<osg::Vec3dArray> &worldPoints, osg::ref_ptr<osg::Vec3dArray> &screenPoints )
{
	osg::Matrix VPW;
	QVector<double> depths;
	QMatrix4x4 matrix;
	QFile file(datFile);
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);  
	in.setVersion(QDataStream::Qt_4_8);
	while(!in.atEnd()){
		in >> matrix;
		in >> depths;
	}
	fromQtMatrix2OsgMatrix(matrix, VPW);
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			screenPoints->push_back(worldPoints->at(i * WIDTH + j) * VPW);
		}
	}
}

void SyInterest::fromQtMatrix2OsgMatrix( const QMatrix4x4 &srcMatrix, osg::Matrixd &destMatrix )
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j  = 0; j < 4; ++j)
		{
			destMatrix(i, j) = srcMatrix(i, j);
		}
	}
}

void SyInterest::calOptFlow()
{
	m_timer.start("optical flow");
	if (!m_IOManager.readImportance((m_folder+GConstant::C_OPTICALFLOW).c_str(),m_OptFlowV))
	{
		m_OptFlowV.clear();
		m_OptFlowV.insert(m_OptFlowV.begin(), OFA_SIZE, 0);

		for (int k = 0; k < OFA_SIZE; k ++)
		{
			if(k == 0)
			{
				continue;
			}

			QString dat_fn_p = (m_video_dir+"\\"+GLOBAL::int2string(2 * (k - 1) + 1)+".dat").c_str();
			QString dat_fn = (m_video_dir+"\\"+GLOBAL::int2string(2 * k + 1)+".dat").c_str();

			osg::ref_ptr<osg::Vec3dArray> worldPoints = new osg::Vec3dArray;
			osg::ref_ptr<osg::Vec3dArray> screenPoints = new osg::Vec3dArray;


			getWorldPoints(dat_fn_p, worldPoints);
			getScreenPoints(dat_fn, worldPoints, screenPoints);

			for (int i = 0; i < HEIGHT; i++)
			{
				for (int j = 0; j < WIDTH; j++)
				{
					osg::Vec3d screen = screenPoints->at(i * WIDTH + j);

					if (screen.x() < 0 || screen.x() > WIDTH || screen.y() < 0 || screen.y() > HEIGHT)
					{
						//tmpOpticalFlow[k] += 150;
						m_OptFlowV[k] += 50;
						continue;
					}
					double deltaY = i - screen.y();
					double deltaX = j - screen.x();
					osg::Vec2d delta(deltaX, deltaY);
					m_OptFlowV[k] += delta.length();
				}
			}

			m_OptFlowV[k] /= m_totalPixelNum;

			if (k == 1)
			{
				m_OptFlowV[k - 1] = m_OptFlowV[k];
			}
		}

		m_IOManager.writeImportance((m_folder+GConstant::C_OPTICALFLOW).c_str(),m_OptFlowV);
		///////////////////////////////////////////////////////////////////////////
		////外部光滑
		/////
	}
	m_timer.end();
}

void SyInterest::generateOptVideo(int mode)
{
	if (mode == 1)
	{
		generateOptArrow();
	}
	else if (mode == 2)
	{
		generateAccColor();
	}
	else if (mode == 3)
	{
		generateOptColor();
	}
}

void SyInterest::generateOptArrow()
{
	m_timer.start("generate Optical Flow Arrow");
	for (int k = 0; k < OFA_SIZE * 2; k ++)
	{
		if(k == 0)
		{
			continue;
		}

		QString dat_fn_p = (m_video_dir+"\\"+GLOBAL::int2string(k)+".dat").c_str();
		QString dat_fn = (m_video_dir+"\\"+GLOBAL::int2string(k + 1)+".dat").c_str();

		osg::ref_ptr<osg::Vec3dArray> worldPoints = new osg::Vec3dArray;
		osg::ref_ptr<osg::Vec3dArray> screenPoints = new osg::Vec3dArray;

		getWorldPoints(dat_fn_p, worldPoints);
		getScreenPoints(dat_fn, worldPoints, screenPoints);

		int screenX, screenY;
		int pixImageCount = 0;

        QString png_fn = (m_video_dir+"\\"+GLOBAL::int2string(k + 1)+"_rgba.png").c_str();
		QImage srcImg(png_fn);
		QPainter painter;
		painter.begin(&srcImg);
		for (int i = 0; i < HEIGHT;)
		{
			for (int j = 0; j < WIDTH;)
			{
				QRgb pixel = srcImg.pixel(j, HEIGHT -1 - i);
				osg::Vec3d screen = screenPoints->at(pixImageCount + j);
				screenX = screen.x(); 
				screenY = HEIGHT - screen.y();
				if ((screenX < WIDTH && screenX >= 0) && (screenY < HEIGHT && screenY >= 0))
				{
					painter.setPen(QPen(Qt::black, 2));
					painter.drawLine(QPointF(j, HEIGHT -1 - i), QPointF(screenX, screenY));
					painter.setPen(QPen(Qt::red, 5));
					painter.drawPoint(screenX, screenY);
				}
				j += 100;    //采样间隔
			}
			i += 100;
			pixImageCount+=100*WIDTH;
		}
		std::string resultImagePath = m_video_dir+"\\res\\"+ QString::number(k).toStdString() + ".png";
		srcImg.save(resultImagePath.c_str());
	}
	m_timer.end();
}

void SyInterest::generateOptColor()
{
	m_timer.start("generate Optical Flow Color");

	std::string dstfile = m_video_dir+"\\optflow_color.avi";
	int frameRate = 30;
	cv::VideoWriter outputVideo;
	outputVideo.open(dstfile, CV_FOURCC('M','J','P','G'), frameRate, cv::Size(WIDTH, HEIGHT), true);

	cv::Mat imgOptFlow(HEIGHT, WIDTH, CV_32FC2, cv::Scalar::all(0));

	for (int k = 0; k < OFA_SIZE * 2; k ++)
	{
		if(k == 0)
		{
			continue;
		}

		QString dat_fn_p = (m_video_dir+"\\"+GLOBAL::int2string(k)+".dat").c_str();
		QString dat_fn = (m_video_dir+"\\"+GLOBAL::int2string(k + 1)+".dat").c_str();

		osg::ref_ptr<osg::Vec3dArray> worldPoints = new osg::Vec3dArray;
		osg::ref_ptr<osg::Vec3dArray> screenPoints = new osg::Vec3dArray;

		getWorldPoints(dat_fn_p, worldPoints);
		getScreenPoints(dat_fn, worldPoints, screenPoints);

		int screenX, screenY;
		for (int i = 0; i < HEIGHT; i++)
		{
			for (int j = 0; j < WIDTH; j++)
			{
				osg::Vec3d screen = screenPoints->at(i * WIDTH + j);
				screenX = (int)screen.x() + 0.5; 
				screenY = (int)screen.y() + 0.5;
				int deltaY = screenY - i;
				int deltaX = screenX - j;
				imgOptFlow.at<cv::Vec2f>(HEIGHT-1-i, j)[1] = -deltaY;
				imgOptFlow.at<cv::Vec2f>(HEIGHT-1-i, j)[0] = deltaX;
			}
		}
		cv::Mat imgOptFlowColor;
		motionToColor(imgOptFlow, imgOptFlowColor);
		//imwrite("optical_flow_png//" + QString::number(k).toStdString() + ".png", imgOptFlowColor);
		outputVideo << imgOptFlowColor;
	}
	m_timer.end();
}

void SyInterest::generateAccColor()
{
	m_timer.start("generate Acceleration Color");
	std::string dstfile = m_video_dir+"\\optacc_color.avi";
	int frameRate = 30;
	cv::VideoWriter outputVideo;
	outputVideo.open(dstfile, CV_FOURCC('M','J','P','G'), frameRate, cv::Size(WIDTH, HEIGHT), true);

	cv::Mat imgOptFlow(HEIGHT, WIDTH, CV_32FC2, cv::Scalar::all(0));

	for (int k = 0; k < OFA_SIZE * 2; k ++)
	{
		if(k == 0 || k == OFA_SIZE*2 - 1)
		{
			continue;
		}

		QString dat_fn_p = (m_video_dir+"\\"+GLOBAL::int2string(k)+".dat").c_str();
		QString dat_fn = (m_video_dir+"\\"+GLOBAL::int2string(k + 1)+".dat").c_str();
		QString dat_fn_n = (m_video_dir+"\\"+GLOBAL::int2string(k + 2)+".dat").c_str();

		osg::ref_ptr<osg::Vec3dArray> worldPoints = new osg::Vec3dArray;
		osg::ref_ptr<osg::Vec3dArray> screenPoints1 = new osg::Vec3dArray;
		osg::ref_ptr<osg::Vec3dArray> screenPoints2 = new osg::Vec3dArray;

		getWorldPoints(dat_fn, worldPoints);
		getScreenPoints(dat_fn_p, worldPoints, screenPoints1);
		getScreenPoints(dat_fn_n, worldPoints, screenPoints2);

		QVector<double> acclateVec;
		for (int i = 0; i < HEIGHT; i++)
		{
			for (int j = 0; j < WIDTH; j++)
			{
				osg::Vec3d screen1 = screenPoints1->at(i * WIDTH + j);
				osg::Vec3d screen2 = screenPoints2->at(i * WIDTH + j);
				/*if (((screen1.x() > 0 && screen1.x() < WIDTH) && (screen1.y() > 0 && screen1.y() < HEIGHT)) 
					&& ((screen2.x() > 0 && screen2.x() < WIDTH) && (screen2.y() > 0 && screen2.y() < HEIGHT)))
				{*/
					double deltaY1 = i - screen1.y();
					double deltaX1 = j - screen1.x();
					double deltaY2 = screen2.y() - i;
					double deltaX2 = screen2.x() - j;
					osg::Vec2d delta1(deltaX1, deltaY1);
					osg::Vec2d delta2(deltaX2, deltaY2);
					osg::Vec2d acceleration = delta2 - delta1;
					imgOptFlow.at<cv::Vec2f>(HEIGHT-1-i, j)[1] = -acceleration.y();
					imgOptFlow.at<cv::Vec2f>(HEIGHT-1-i, j)[0] = acceleration.x();
				/*}*/

			}
		}
		cv::Mat imgOptFlowColor;
		motionToColor(imgOptFlow, imgOptFlowColor);
		outputVideo << imgOptFlowColor;
	}
	m_timer.end();
}

void SyInterest::makeColorWheel( std::vector<cv::Scalar> &colorwheel )
{
	int RY = 15;
	int YG = 6;
	int GC = 4;
	int CB = 11;
	int BM = 13;
	int MR = 6;
	for (int i = 0; i < RY; i++) colorwheel.push_back(cv::Scalar(255,			255*i/RY,	   0));
	for (int i = 0; i < YG; i++) colorwheel.push_back(cv::Scalar(255-255*i/YG,  255,		   0));
	for (int i = 0; i < GC; i++) colorwheel.push_back(cv::Scalar(0,				255,		   255*i/GC));
	for (int i = 0; i < CB; i++) colorwheel.push_back(cv::Scalar(0,				255-255*i/CB,  255));
	for (int i = 0; i < BM; i++) colorwheel.push_back(cv::Scalar(255*i/BM,		0,			   255));
	for (int i = 0; i < MR; i++) colorwheel.push_back(cv::Scalar(255,			0,			   255-255*i/MR));
}

void SyInterest::motionToColor( cv::Mat flow, cv::Mat &color )
{
	if (color.empty())
		color.create(flow.rows, flow.cols, CV_8UC3);

	static std::vector<cv::Scalar> colorwheel; //Scalar r,g,b
	if (colorwheel.empty())
		makeColorWheel(colorwheel);

	// determine motion range:
	float maxrad = -1;

	// Find max flow to normalize fx and fy
	for (int i= 0; i < flow.rows; ++i) 
	{
		for (int j = 0; j < flow.cols; ++j) 
		{
			cv::Vec2f flow_at_point = flow.at<cv::Vec2f>(i, j);
			float fx = flow_at_point[0];
			float fy = flow_at_point[1];
			if ((fabs(fx) >  UNKNOWN_FLOW_THRESH) || (fabs(fy) >  UNKNOWN_FLOW_THRESH))
				continue;
			float rad = sqrt(fx * fx + fy * fy);
			maxrad = maxrad > rad ? maxrad : rad;
		}
	}

	for (int i= 0; i < flow.rows; ++i) 
	{
		for (int j = 0; j < flow.cols; ++j) 
		{
			uchar *data = color.data + color.step[0] * i + color.step[1] * j;
			cv::Vec2f flow_at_point = flow.at<cv::Vec2f>(i, j);

			float fx = flow_at_point[0] / maxrad;
			float fy = flow_at_point[1] / maxrad;
			if ((fabs(fx) >  UNKNOWN_FLOW_THRESH) || (fabs(fy) >  UNKNOWN_FLOW_THRESH))
			{
				data[0] = data[1] = data[2] = 0;
				continue;
			}
			float rad = sqrt(fx * fx + fy * fy);

			float angle = atan2(-fy, -fx) / CV_PI;
			float fk = (angle + 1.0) / 2.0 * (colorwheel.size()-1);
			int k0 = (int)fk;
			int k1 = (k0 + 1) % colorwheel.size();
			float f = fk - k0;
			//f = 0; // uncomment to see original color wheel

			for (int b = 0; b < 3; b++) 
			{
				float col0 = colorwheel[k0][b] / 255.0;
				float col1 = colorwheel[k1][b] / 255.0;
				float col = (1 - f) * col0 + f * col1;
				if (rad <= 1)
					col = 1 - rad * (1 - col); // increase saturation with radius
				else
					col *= .75; // out of range
				data[2 - b] = (int)(255.0 * col);
			}
		}
	}
}

void SyInterest::computeOpticalFlowColor( std::string srcfile, std::string dstfile )
{
	/* read video parameters */
	cv::VideoCapture capture(srcfile);
	if( !capture.isOpened() )
		throw "Could not open the input video";

	int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));		// default codec format
	cv::Size s = cv::Size( (int)capture.get(CV_CAP_PROP_FRAME_WIDTH),
		(int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));

	/* write video parameters */	
	cv::VideoWriter outputVideo;
	outputVideo.open(dstfile, CV_FOURCC('M','J','P','G'), capture.get(CV_CAP_PROP_FPS), s, true);

	if( !outputVideo.isOpened() )
		throw "Could not open the output video for writing";

	cv::Mat prevgray, gray, flow, cflow, frame, motion2color;
	cv::namedWindow("Color Encoded Optical Flow", 1);

	for(;;)
	{
		double t = (double)cvGetTickCount();

		capture >> frame;
		cvtColor(frame, gray, CV_BGR2GRAY);		

		if ( frame.size().width <= 0 || frame.size().height <= 0 )
			break;

		imshow("Original", frame);

		if( prevgray.data )
		{
			calcOpticalFlowFarneback(prevgray, gray, flow, 0.5, 3, 15, 3, 5, 1.2, 0);
			motionToColor(flow, motion2color);

			if( motion2color.size().width <= 0 && motion2color.size().height <= 0 )
				break;

			imshow("Color Encoded Optical Flow", motion2color);
			outputVideo << motion2color;
		}

		if(cv::waitKey(10)>=0)
			break;
		std::swap(prevgray, gray);

		t = (double)cvGetTickCount() - t;
		std::cout << "cost time: " << t / ((double)cvGetTickFrequency()*1000.) << std::endl;
	}

	capture.release();
	std::cout << " Optical Flow Computation Finish " << std::endl;
}

void SyInterest::computeOpticalFlowColor( const cv::Mat& imgOptFlowDispMat, cv::Mat& imgOptFlowColor )
{
	motionToColor(imgOptFlowDispMat, imgOptFlowColor);
}

void SyInterest::viewCompare()
{
	m_timer.start("View Compare");
	double synopsisiImp = synopsisView();
	double entropyImp = entropyView();
	std::string folder = m_folder + "\\snapshot\\viewcmp";
	std::string filename = folder + "\\imp.txt";
	QFile txt_file(filename.c_str());
	txt_file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream txt_file_stream(&txt_file);
	txt_file_stream <<"Importance: " <<synopsisiImp << "\n";
	txt_file_stream <<"Entropy: " <<entropyImp;
	m_timer.end();
}

double SyInterest::synopsisView()
{

	std::string folder = m_folder + "\\snapshot\\viewcmp";
	std::string rgbimgfile = folder + "\\rgba.png";
	IplImage*  rgbimg = cvLoadImage(rgbimgfile.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
	std::string depthimgfile = folder + "\\depth.png";
	IplImage*  depthimg = cvLoadImage(depthimgfile.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
	double tImportance = 0;
	if (depthimg!=NULL && rgbimg!=NULL)
	{
		std::string centerwfile = folder + "\\cw.png";
		std::string ndepthfile = folder + "\\ndepth.png"; 
		std::string dresfile = folder + "\\depth_res.png";
		std::string cresfile = folder + "\\cen_res.png";
		std::string impfile = folder + "\\importance.png";

		IplImage*  centerwimg = cvCreateImage(cvSize((int)(rgbimg->width),(int)(rgbimg->height)),IPL_DEPTH_8U, 1); 
		IplImage*  ndepthimg = cvCreateImage(cvSize((int)(rgbimg->width),(int)(rgbimg->height)),IPL_DEPTH_8U, 1); 
		IplImage*  dresimg = cvCreateImage(cvSize((int)(rgbimg->width),(int)(rgbimg->height)),IPL_DEPTH_32F, 3); 
		IplImage*  cresimg = cvCreateImage(cvSize((int)(rgbimg->width),(int)(rgbimg->height)),IPL_DEPTH_32F, 3); 
		IplImage*  impimg = cvCreateImage(cvSize((int)(rgbimg->width),(int)(rgbimg->height)), IPL_DEPTH_32F, 3); 

		double m_midX = rgbimg->width/2;
		double m_midY = rgbimg->height/2;
		int m_totalPixelNum = (rgbimg->width)*(rgbimg->height);
		double m_radius = sqrt(m_midX*m_midX+m_midY*m_midY);

		std::vector<double> impV;
		std::vector<double> dresimpV;
		std::vector<double> cresimpV;

		double viewImportance = 0;
		double dresImportance = 0;
		double cresImportance = 0;
		
		double K = 3.1415/2;
		double exponent = 0.5;

		for(int m = 0; m < rgbimg->height; m++)
		{
			for(int n = 0; n < rgbimg->width; n++)
			{
				CvScalar depthPixel = cvGet2D(depthimg, m, n);
				CvScalar colorPixel = cvGet2D(rgbimg, m, n);
				CvScalar cwpixel;
				double dis = sqrt((m-m_midY)*(m-m_midY)+(n-m_midX)*(n-m_midX));
				double val = dis/m_radius;
				double cenW = pow(cos(K*val),exponent);
				cwpixel.val[0] = int(cenW*255.0);
				cwpixel.val[1] = 0; 
				cwpixel.val[2] = 0; 
				cwpixel.val[3] = 0; 
				cvSet2D(centerwimg,m, n, cwpixel);

				//view importance
				viewImportance = cenW * getImportanceForPixel(depthPixel, colorPixel, true);
				dresImportance = getImportanceForPixel(depthPixel, colorPixel, true);
				cresImportance = cenW * getImportanceForPixel(depthPixel, colorPixel, false);
				tImportance +=viewImportance;
				impV.push_back(viewImportance);
				dresimpV.push_back(dresImportance);
				cresimpV.push_back(cresImportance);
				//写新的灰度图，只用150做加权
				CvScalar dpixel;
				double depthThre = SyConfig::getInstance().getDepthThrehold() * 255.0 / 300.0;

				if (depthPixel.val[0] < depthThre)
					dpixel.val[0] = int(depthPixel.val[0]*255.0 / depthThre);
				else
					dpixel.val[0] = 255;
				dpixel.val[1] = 0; 
				dpixel.val[2] = 0; 
				dpixel.val[3] = 0; 
				cvSet2D(ndepthimg, m, n, dpixel);
			}
		}
		std::cout<<tImportance/m_totalPixelNum<<std::endl;
	
	    //GLOBAL::normalize(impV);   //归一化需要整体做！！！！
		int count = 0;
		for(int m = 0; m < rgbimg->height; m++)
		{
			for(int n = 0; n < rgbimg->width; n++)
			{
				EV4 ecolor = GLOBAL::scalar2color(impV[count]);
				CvScalar ncolorPixel;
				ncolorPixel.val[2] = ecolor[0]*255;
				ncolorPixel.val[1] = ecolor[1]*255;
				ncolorPixel.val[0] = ecolor[2]*255;
				ncolorPixel.val[3] = ecolor[3]*255;
				cvSet2D(impimg, m, n, ncolorPixel);

				EV4 ccolor = GLOBAL::scalar2color(cresimpV[count]);
				CvScalar ccolorPixel;
				ccolorPixel.val[2] = ccolor[0]*255;
				ccolorPixel.val[1] = ccolor[1]*255;
				ccolorPixel.val[0] = ccolor[2]*255;
				ccolorPixel.val[3] = ccolor[3]*255;
				cvSet2D(cresimg, m, n, ccolorPixel);

				EV4 dcolor = GLOBAL::scalar2color(dresimpV[count]);
				CvScalar dcolorPixel;
				dcolorPixel.val[2] = dcolor[0]*255;
				dcolorPixel.val[1] = dcolor[1]*255;
				dcolorPixel.val[0] = dcolor[2]*255;
				dcolorPixel.val[3] = dcolor[3]*255;
				cvSet2D(dresimg, m, n, dcolorPixel);

				count++;
			}
		}
		cvSaveImage(centerwfile.c_str(), centerwimg);
		cvSaveImage(ndepthfile.c_str(), ndepthimg);
		cvSaveImage(impfile.c_str(), impimg);
		cvSaveImage(dresfile.c_str(), dresimg);
		cvSaveImage(cresfile.c_str(), cresimg);

		cvReleaseImage(&rgbimg);
		cvReleaseImage(&depthimg);
	}
	
	return tImportance;
}

double SyInterest::getImportanceForPixel( CvScalar& depthpixel,CvScalar& colorpixel, bool mode )
{
	if (colorpixel.val[0]==255&&colorpixel.val[1]==255&&colorpixel.val[2]==255)
	{
		return 0;
	}
	double depthThre = SyConfig::getInstance().getDepthThrehold() * 255.0 / 300.0;
	double depthval = depthpixel.val[0];
	double colorval = color2scalar(colorpixel);
	double importanceVal = 1.0;
	if (depthval < depthThre)
		importanceVal = depthval/depthThre; 
	if (mode)
		importanceVal = importanceVal*colorval;
	else
		importanceVal = colorval;
	return importanceVal;
}

double SyInterest::entropyView()
{
	//refer: Viewpoint Selection using Viewpoint Entropy
	std::string folder = m_folder + "\\snapshot\\viewcmp";
	std::string rgbimgfile = folder + "\\rgba.png";
	IplImage*  rgbimg = cvLoadImage(rgbimgfile.data(), CV_LOAD_IMAGE_ANYCOLOR|CV_LOAD_IMAGE_ANYDEPTH);
	double entropyVal = 0;

	if (rgbimg!=NULL)
	{
		std::map<int, int> areaV;
		
		for(int m = 0; m < rgbimg->height; m++)
		{
			for(int n = 0; n < rgbimg->width; n++)
			{
				CvScalar colorPixel = cvGet2D(rgbimg, m, n);
				int index = colorPixel.val[0]*256*256 + colorPixel.val[1]*256 + colorPixel.val[2];
				if (areaV.find(index) != areaV.end())
				{
					areaV[index]++;
				}
				else
				{
					areaV.insert(std::pair<int, int>(index, 1));
				}
			}
		}
		int totalArea = ( rgbimg->width ) * ( rgbimg->height );

		for (std::map<int, int>::iterator iter = areaV.begin(); iter != areaV.end(); iter++)
		{
			int curArea = iter->second;
			double ratio = (double)curArea / (double)totalArea;
			entropyVal -= ratio * log(ratio)/log(2.0); 
		}
		cvReleaseImage(&rgbimg);
	}
	return entropyVal;
}
