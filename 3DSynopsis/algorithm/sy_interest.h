#pragma once
#ifndef SY_NDINTEREST_H
#define SY_NDINTEREST_H

#include <string>
#include <vector>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <tbb/parallel_for.h>
#include <tbb/concurrent_vector.h>
#include <tbb/queuing_mutex.h>
#include <QObject>

#include "sy_timer.h"
#include "eigen_types.h"
#include "global_constant.h"
#include "sy_manipulator.h"
#include "global_func.h"
#include "sy_ioManager.h"
#include "sy_screenCapture.h"


#define DEFAULTSPEED 10
#define UNKNOWN_FLOW_THRESH 1e9

#define TBB_USE

class SyInterest: public QObject
{

	Q_OBJECT 

		private slots:
			void snapTick();

public:
	SyInterest(QObject * parent = 0 );
	~SyInterest();

	void init(std::string folder);

	//均匀采样
	void uniformSample(double segLength);

	//计算匀速的速度，角度等
	void calConstant();

	//计算高速路fly过去的速度
	void calJump();

	//采importance值
	void snapSampleViews(osg::ref_ptr<osg::Camera> cam, osg::ref_ptr<SynopsisManipulator>  pSynopsisManipulator, int iter_num);

	//图片处理
	void imageProcess();

	//计算速度
	void solveSpeed();

	//光滑路径
	void smoothPath();

	//计算相机视角
	void viewProduce(int step);

	//比较两次重要性值差异
	void cmpImp();

	//计算能量图，后期视频
	void generateMap();

	//其他对比速度的计算
	void msSpeed();

	//计算optical flow acceleration
	void calOptAcc();

	//计算optical flow
	void calOptFlow();

	//生成光流视频
	void generateOptVideo(int mode);
    
	//view compare
	void viewCompare();

private:
	///初始化方法
	void initialize();

	///default:google
	void calSCPath();
	void constantProduce();
	void calRoadheadings();   
	
	//default:jump
	void smoothJumpPath(double deta);
	void calJumpView();
	void calRoadheadings2(); 
	void smoothJumpTrajectory();
	//截图操作
	void generateIniPos(int iter_num);
	void camControl(int pointNo);
	
	//图片处理
	double getImportanceForPixel(CvScalar& depthpixel,CvScalar& colorpixel);
	double color2scalar( CvScalar& npixel );

	//计算速度
	void accumlateSpeed();
	void weightedSpeed();

	//smooth path
	void calCurvature();
	void calCurvature2();
	double curvature(EV2 lD, EV2 rD);
	void doSmooth();
	void externBezier();

	//calculate view
	void generateForMatlab();
	void generateFromMatlab();
	void smoothMatlabRes();

	//后期录屏图片处理
	void getFileList();
	void imgPro();

	//calculate optical flow acceleration
	void getWorldPoints(const QString &datFile, osg::ref_ptr<osg::Vec3dArray> &worldPoints);
	void getScreenPoints(const QString &datFile, osg::ref_ptr<osg::Vec3dArray> &worldPoints, osg::ref_ptr<osg::Vec3dArray> &screenPoints);
	void fromQtMatrix2OsgMatrix( const QMatrix4x4 &srcMatrix, osg::Matrixd &destMatrix );

	//生成光流视频
	void generateOptArrow();
	void generateOptColor();
	void generateAccColor();
	
	void makeColorWheel(std::vector<cv::Scalar> &colorwheel);
	void motionToColor(cv::Mat flow, cv::Mat &color);
	void computeOpticalFlowColor(std::string srcfile, std::string dstfile);
	void computeOpticalFlowColor(const cv::Mat& imgOptFlowDispMat, cv::Mat& imgOptFlowColor);

	//view compare
	double synopsisView();
	double getImportanceForPixel( CvScalar& depthpixel,CvScalar& colorpixel, bool mode );
	double entropyView();
private:
	std::string                         m_folder;
	SyTimer							    m_timer;
	//读写文件控制
	SyIoManager                         m_IOManager;

	//采样路径
	std::string							m_imgs_dir;
	std::vector<SyPath>                 m_samplePath;  
	std::vector<SyPath>                 m_smoothPath;
	int									m_totalPointsNum;
	//采样视角参数
	double                              m_sample_height;
	double                              m_sample_tilt;
	//采样图片参数
	const  int                          WIDTH;
	const  int                          HEIGHT;
	int									m_midX;
	int									m_midY;
	double								m_radius;
	int                                 m_totalPixelNum;

	//道路heading
	std::vector<double>                 m_roadheadings;
	std::vector<double>                 m_nroadheadings;

	//速度
	std::vector<double>                 m_speedV;
	//提前坐标
	std::vector<EV2>                    m_camPreCordsV;

	//截图采样
	bool                                 m_isSnap;
	bool                                 m_camDone;
	std::vector<std::vector<double>>     m_curIniPos;
	osg::ref_ptr<SynopsisManipulator>    m_pSynopsisManipulator;
	osg::ref_ptr<SyScreenCapture>        m_snap_sc;
	osg::ref_ptr<osg::Camera>            m_snap_cam;
	int                                  m_snap_ImageNo;

	//视角importance值
	std::vector<double>					m_importance;      //importance值矩阵
	std::vector<double>                 m_viewImp;

	//smooth path
	std::vector<double>                 m_curvature;

	//录屏文件，理论上是有序列
	std::vector<int>                    m_VideoImgV;
	std::string							m_video_dir;

	//光流
	std::vector<double>                 m_OptAccV;
	std::vector<double>                 m_OptFlowV;
	const  int                          OFA_SIZE;


};
#endif