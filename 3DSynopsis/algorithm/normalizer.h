#pragma once

#include "eigen_types.h"

class InsNormalizer
{
public:
	InsNormalizer();
	~InsNormalizer();

	//************************************
	// Method:    computeMat
	// Returns:   void
	// Function:  计算归一化矩阵
	// Time:      2014/01/19 
	// Author:    qian
	//************************************
	void computeMat( EMatXd& vMat, double radius = 1 );

	//************************************
	// Method:    restore
	// Returns:   EV3
	// Function:  从归一化后的点恢复原始坐标
	// Time:      2014/01/19 
	// Author:    qian
	//************************************
	EV3 restore( const EV3& p);
	EV2 restore( const EV2& p);


	//************************************
	// Method:    normalize
	// Returns:   EV3
	// Function:  对原始坐标进行归一化
	// Time:      2014/01/19 
	// Author:    qian
	//************************************
	EV3 normalize( const EV3& p );
	EV2 normalize( const EV2& p);

	// Return true if a mesh has constant value of 0 in z coordinate
	// Inputs:
	//
	bool is_planar(const Eigen::MatrixXd & V);



private:
	ETform3   m_normlizeMat;
	ETform3   m_matInversed;
	double    m_radius;
};