#pragma  once
#include <string>
#include <Eigen/Core>
#include <Vector>


class MeanShifeMgr
{
public:
	enum MeanShiftType
	{
		EUCLIDEAN = 0,
		GRASSMANN = 1,
		ESSENTIAL = 2,
		SE3 = 3,
	};

public:
	MeanShifeMgr();
	~MeanShifeMgr() {}

	static std::string title() { return "[MeanShift]: "; }

	void setBandwidth(double band )  { _bandwidth = band; }
	void setType( MeanShiftType type )   { _type = type; }
	void setMaxModePercent( double percent)  {_max_mode_percent = percent; }
	void setMinClusterSize( int size)  {_min_ClusterSize = size; }

	void apply( const Eigen::MatrixXd& points, Eigen::MatrixXd& cen, Eigen::MatrixXd& cenmember);


public:
	double          _bandwidth;
	MeanShiftType   _type;
	double          _max_mode_percent;
	int             _min_ClusterSize;

	Eigen::MatrixXd    point_;
	Eigen::MatrixXd    cen_;

};