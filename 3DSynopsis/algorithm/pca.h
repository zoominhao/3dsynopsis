#pragma once
#ifndef PCA_H
#define PCA_H

#include "eigen_types.h"

class PCA
{
public:
	PCA();
	~PCA();

	void calPCA(const EMatXd& V,EMatXd& resV);
	EV3 getMainDir(const EMatXd& V);
	EV2 get2DMainDir(const EMatXd& V);
	EMatXd& getInverse() {return PCAINVERSEM;}
private:
	EMatXd PCAM;
	EMatXd PCAINVERSEM;
};
#endif  //PCA_H