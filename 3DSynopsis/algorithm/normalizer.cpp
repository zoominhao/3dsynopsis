#include "normalizer.h"



InsNormalizer::InsNormalizer()
{
	m_normlizeMat = ETform3::Identity();
	m_matInversed = ETform3::Identity();
	m_radius = 1;
}

InsNormalizer::~InsNormalizer()
{

}


void InsNormalizer::computeMat( EMatXd& vMat, double radius /*= 1 */ )
{
	m_radius = radius;
	double normalizedRadius = m_radius;


	bool flagPlanar = is_planar( vMat );
	double xMin = vMat.col(0).minCoeff();
	double yMin = vMat.col(1).minCoeff();
	double zMin = vMat.col(2).minCoeff();
	double xMax = vMat.col(0).maxCoeff();
	double yMax = vMat.col(1).maxCoeff();
	double zMax = vMat.col(2).maxCoeff();

	EV3 vMin( xMin, yMin, zMin );
	EV3 vMax( xMax, yMax, zMax );
	EV3 origCenter = (vMin+vMax)/2;
	double origRadius = (vMax-vMin).norm()/2;
	double ratio = ( normalizedRadius/origRadius );
	EV3 scale(1,1,1);

	if( flagPlanar )
	{
		scale << ratio, ratio, 0;
	}
	else
	{
		scale << ratio, ratio,ratio;
	}

	m_normlizeMat = ETform3::Identity();
	m_normlizeMat.scale( scale ).translate(-origCenter);
	m_matInversed = m_normlizeMat.inverse();
}

EV3 InsNormalizer::restore( const EV3& p )
{
	return m_matInversed*p;
}

EV2 InsNormalizer::restore( const EV2& p )
{
	EV3 p3( p.x(),p.y(),0 );
	p3 =  m_matInversed*p3;

	return EV2( p3.x(), p3.y() );
}

EV3 InsNormalizer::normalize( const EV3& p )
{
	return m_normlizeMat*p;
}

EV2 InsNormalizer::normalize( const EV2& p )
{
	EV3 p3( p.x(), p.y(),0 );
	p3 =  m_normlizeMat*p3;

	return EV2( p3.x(), p3.y() );
}

bool InsNormalizer::is_planar(const Eigen::MatrixXd & V)
{
	if(V.size() == 0) return false;
	if(V.cols() == 2) return true;
	for(int i = 0;i<V.rows();i++)
	{
		if(V(i,2) != 0) return false;
	}
	return true;
}
