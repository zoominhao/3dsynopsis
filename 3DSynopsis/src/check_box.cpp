#include <QStandardItem>

#include "check_box.h"

CheckBox::CheckBox(const QString & text, QWidget * parent)
  :QCheckBox(text, parent)
{
  setTristate(true);
  setCheckState(Qt::Checked);
}

CheckBox::~CheckBox()
{
}

void CheckBox::mouseReleaseEvent(QMouseEvent * event)
{
  if (checkState() == Qt::Unchecked)
    setCheckState(Qt::Checked);
  else
    setCheckState(Qt::Unchecked);

  return;
}