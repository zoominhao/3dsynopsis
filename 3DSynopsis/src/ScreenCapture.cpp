/*************** <auto-copyright.pl BEGIN do not edit this line> **************
 *
 * osgWorks is (C) Copyright 2009-2012 by Kenneth Mark Bryden
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *************** <auto-copyright.pl END do not edit this line> ***************/

#include <osg/Camera>
#include <osg/Image>
#include <osg/FrameStamp>
#include <osgDB/WriteFile>
#include <OpenThreads/Thread>
#include <OpenThreads/Mutex>
#include <OpenThreads/ScopedLock>
#include <string>
#include <sstream>
#include <osgDB/ReadFile>
#include <osgGA/TrackballManipulator>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "ScreenCapture.h"
#include "global_func.h"



namespace osgwTools
{
	ScreenCapture::ScreenCapture( const std::string& rootName, const std::string& ext, double znear,double zfar, bool useFrameNum )
		: rootName_( rootName ),
		ext_( ext ),
		znear_(znear),
		zfar_(zfar),
		useFrameNum_( useFrameNum ),
		captureOn_( false ),
		numFrames_( 0 ),
		snapNo_(0),
		wit_( NULL )
	{
	}

	ScreenCapture::~ScreenCapture()
	{
		if( wit_ != NULL )
		{
			if( wit_->isRunning() )
			{
				wit_->cancel();
				wit_->join();
			}
			if( wit_->isRunning() )
				osg::notify( osg::ALWAYS ) << "Thread is running after join() call." << std::endl;
			delete wit_;
			wit_ = NULL;
		}
	}

	void
		ScreenCapture::operator()( osg::RenderInfo& ri ) const
	{
		if( captureOn_ )
		{
			bool newThread( false );
			if( wit_ == NULL )
			{
				wit_ = new WriteImageThread();
				newThread = true;
			}
			osg::Image* image = new osg::Image;
			image->setFileName( 
				getFileName( useFrameNum_ ? ri.getState()->getFrameStamp() : NULL )+"_rgba"+ext_ );

			osg::notify( osg::INFO ) << "ScreenCapture: Reading image for file " << image->getFileName() << " ... " << std::endl;
			const osg::Viewport* vp( ( vp_ == NULL ) ?
				ri.getState()->getCurrentViewport() : vp_.get() );



		    ///////////////////////////////////获取深度图///////////////////////////////////////////////
			#ifdef DEPTH
				int width = vp->width();
				int height = vp->height();
				int size = width * height;
				//读取深度信息
				osg::Image* pTest = new osg::Image;
				pTest->readPixels( vp->x(), vp->y(), width, height, GL_DEPTH_COMPONENT, GL_FLOAT ); 
				float *res = (float*)pTest->data();


				IplImage* toSave = cvCreateImage(cvSize((int)width,(int)height), IPL_DEPTH_8U, 1); 

				toSave->origin = 1;
				CvScalar pixel;
				for (int i =0; i < height; ++i)
				{
					for (int j =0; j < width; ++j)
					{
						float depthval = calRealDepth(res[i*width+j])/300;
						if(depthval>1)
							pixel.val[0] = 255;
						else
							pixel.val[0] = int(depthval*255);
						pixel.val[1] = 0; 
						pixel.val[2] = 0; 
						pixel.val[3] = 0; 
						cvSet2D(toSave,i, j, pixel);
					}
				}

				cvSaveImage((getFileName( useFrameNum_ ? ri.getState()->getFrameStamp() : NULL )+"_depth"+ext_).c_str(), toSave);
		    #endif
			///////////////////////////////////////////////////////////////////////////////////////////
			image->readPixels( vp->x(), vp->y(), vp->width(), vp->height(),GL_RGBA, GL_UNSIGNED_BYTE); 
			{
				OpenThreads::ScopedLock< OpenThreads::Mutex > lock( wit_->lock_ );
				wit_->imageList_.push_back( image );
			}

			if( numFrames_ > 0 )
			{
				if( --numFrames_ == 0 )
					captureOn_ = false;
			}
            #ifdef DEPTH
			cvReleaseImage(&toSave);
			#endif
			if( newThread )
				wit_->start();
		}
		else
		{
			if( ( wit_ != NULL ) )
			{
				osg::notify( osg::INFO ) << "ScreenCapture: Thread cleanup" << std::endl;
				if( wit_->isRunning() )
				{
					wit_->cancel();
					wit_->join();
				}
				if( wit_->isRunning() )
					osg::notify( osg::ALWAYS ) << "Thread is running after join() call." << std::endl;
				delete wit_;
				wit_ = NULL;
			}
		}
	}

	void
		ScreenCapture::setCapture( bool enable )
	{
		captureOn_ = enable;
	}
	bool
		ScreenCapture::getCaptureEnabled() const
	{
		return( captureOn_ );
	}
	void
		ScreenCapture::setNumFramesToCapture( unsigned int numFrames )
	{
		numFrames_ = numFrames;
	}


	void
		ScreenCapture::setRootName( const std::string& name )
	{
		rootName_ = name;
	}
	void
		ScreenCapture::setExtension( const std::string& extension )
	{
		ext_ = extension;
	}
	void
		ScreenCapture::setUseFrameNumber( bool useFrameNum )
	{
		useFrameNum_ = useFrameNum;
	}
	void
		ScreenCapture::setViewport( osg::Viewport* vp )
	{
		vp_ = vp;
	}


	std::string
		ScreenCapture::getFileName( osg::FrameStamp* fs ) const
	{
		std::string fileName;
		if (fs==NULL)
		{
			fileName = rootName_+GLOBAL::int2string(snapNo_);
		}
		else
		{
			if( !rootName_.empty() )
				fileName = rootName_;
			else
				fileName = "screencapture";

			if( fs != NULL )
			{
				std::ostringstream ostr;
				ostr << fs->getFrameNumber();
				fileName += ostr.str();
			}
		}
		return( fileName );
	}

	float ScreenCapture::calRealDepth( float val ) const
	{
		float realdepth = 2*zfar_*znear_/((zfar_+znear_)-(2*val-1)*(zfar_-znear_));
		return realdepth;
	}

	bool ScreenCapture::isFinished( void )
	{
		if (wit_==NULL)
			return true;
		else
			return false;
	}




	ScreenCapture::WriteImageThread::WriteImageThread()
	{
	}

	void
		ScreenCapture::WriteImageThread::run()
	{
		osg::ref_ptr< osg::Image > image( NULL );
		{
			OpenThreads::ScopedLock< OpenThreads::Mutex > lock( lock_ );
			if( !imageList_.empty() )
			{
				image = imageList_.front();
				imageList_.pop_front();
			}
		}
		bool finished( image == NULL );
		while( !finished )
		{
			if( image != NULL )
			{
				osg::notify( osg::INFO ) << "ScreenCapture: Writing \"" << image->getFileName() << "\"" << std::endl;
				osgDB::writeImageFile( *image, image->getFileName() );
		         
				image = NULL;

			}
			else
			{
				YieldCurrentThread();
				microSleep( 500 );
			}

			{
				OpenThreads::ScopedLock< OpenThreads::Mutex > lock( lock_ );
				if( !imageList_.empty() )
				{
					image = imageList_.front();
					imageList_.pop_front();
				}
			}
			finished = ( (image == NULL) && testCancel() );
		}
	}

}