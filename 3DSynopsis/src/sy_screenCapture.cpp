#include "sy_screenCapture.h"
#include "global_func.h"

#include <osg/Camera>
#include <osg/Image>
#include <osg/FrameStamp>
#include <osgDB/WriteFile>
#include <opencv/cv.h>
#include <opencv/highgui.h>

SyScreenCapture::SyScreenCapture( const std::string& rootName/*=std::string( "" )*/, const std::string& ext/*=std::string( ".png" )*/, double znear/*=1.0*/, double zfar/*=1400.0*/, bool useFrameNum/*=true */ )
	: rootName_( rootName ),
	ext_( ext ),
	znear_(znear),
	zfar_(zfar),
	useFrameNum_( useFrameNum ),
	captureOn_( false ),
	numFrames_( 0 ),
	snapNo_(0)
{

}

SyScreenCapture::~SyScreenCapture()
{

}

void SyScreenCapture::operator()( osg::RenderInfo& ri ) const
{
	if( captureOn_ )
	{
		osg::ref_ptr<osg::Image> image = new osg::Image;
		image->setFileName( 
			getFileName( useFrameNum_ ? ri.getState()->getFrameStamp() : NULL )+"_rgba"+ext_ );

		osg::notify( osg::INFO ) << "ScreenCapture: Reading image for file " << image->getFileName() << " ... " << std::endl;
		const osg::Viewport* vp( ( vp_ == NULL ) ?
			ri.getState()->getCurrentViewport() : vp_.get() );
		image->readPixels( vp->x(), vp->y(), vp->width(), vp->height(),GL_RGBA, GL_UNSIGNED_BYTE); 
		osg::notify( osg::INFO ) << "ScreenCapture: Writing \"" << image->getFileName() << "\"" << std::endl;
 		osgDB::writeImageFile( *image, image->getFileName() );


		///////////////////////////////////////
#ifdef OPTICAL_FLOW
		QFile matrixFile((getFileName( useFrameNum_ ? ri.getState()->getFrameStamp() : NULL )+".dat").c_str());
		if (!matrixFile.open(QIODevice::WriteOnly)) {  
			std::cerr << "Cannot open file for writing: " 
				<< qPrintable(matrixFile.errorString()) << std::endl;   
		}
		QDataStream datOut(&matrixFile);
		datOut.setDevice(&matrixFile);
		datOut.setVersion(QDataStream::Qt_4_8);

		osg::ref_ptr<osg::Camera> camera = ri.getCurrentCamera();
		osg::Matrixd VPW = camera->getViewMatrix() * (camera->getProjectionMatrix()) * (camera->getViewport()->computeWindowMatrix());
		QMatrix4x4 localMatrix;
		fromOsgMatrix2QtMatrix(VPW, localMatrix);
		QVector<float> depths;
		datOut<< localMatrix;
#endif

		///////////////////////////////////获取深度图///////////////////////////////////////////////
#ifdef DEPTH
		int width = vp->width();
		int height = vp->height();
		int size = width * height;
		//读取深度信息
		osg::ref_ptr<osg::Image> pTest = new osg::Image;
		pTest->readPixels( vp->x(), vp->y(), width, height, GL_DEPTH_COMPONENT, GL_FLOAT ); 
		float *res = (float*)pTest->data();

		IplImage* toSave = cvCreateImage(cvSize((int)width,(int)height), IPL_DEPTH_8U, 1); 

		toSave->origin = 1;
		CvScalar pixel;
		for (int i = 0; i < height; ++i)
		{
			for (int j = 0; j < width; ++j)
			{
				float depthval = calRealDepth(res[i*width+j])/300;
				if(depthval>1)
					pixel.val[0] = 255;
				else
					pixel.val[0] = int(depthval*255);
				pixel.val[1] = 0; 
				pixel.val[2] = 0; 
				pixel.val[3] = 0; 
				cvSet2D(toSave,i, j, pixel);
				//add by Hisong
				#ifdef OPTICAL_FLOW
				depths.push_back(res[i*width+j]);
				#endif
			}
		}
		#ifdef OPTICAL_FLOW
		datOut<< depths;
		matrixFile.close();
		depths.clear();
		#endif

		cvSaveImage((getFileName( useFrameNum_ ? ri.getState()->getFrameStamp() : NULL )+"_depth"+ext_).c_str(), toSave);
		cvReleaseImage(&toSave);
#endif


		if( numFrames_ > 0 )
		{
			if( --numFrames_ == 0 )
				captureOn_ = false;
		}
   }
}

std::string SyScreenCapture::getFileName( osg::FrameStamp* fs ) const
{
	std::string fileName;
	if (fs==NULL)
	{
		fileName = rootName_+GLOBAL::int2string(snapNo_);
	}
	else
	{
		if( !rootName_.empty() )
			fileName = rootName_;
		else
			fileName = "screencapture";

		if( fs != NULL )
		{
			std::ostringstream ostr;
			ostr << fs->getFrameNumber();
			fileName += ostr.str();
		}
	}
	return( fileName );
}


float SyScreenCapture::calRealDepth( float val ) const
{
	float realdepth = 2*zfar_*znear_/((zfar_+znear_)-(2*val-1)*(zfar_-znear_));
	return realdepth;
}

bool SyScreenCapture::getCaptureEnabled() const
{
	return( captureOn_ );
}


void SyScreenCapture::fromOsgMatrix2QtMatrix( const osg::Matrix &srcMatrix, QMatrix4x4 &destMatrix ) const
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j  = 0; j < 4; ++j)
		{
			destMatrix(i, j) = srcMatrix(i, j);
		}
	}
}
