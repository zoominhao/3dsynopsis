#include "sy_animationEventHandle.h"

#include <osg/AnimationPath>


AnimationEventHandle::AnimationEventHandle(osg::ref_ptr<osg::Node> sceneNode)
	:m_sceneNode(sceneNode)
{

}

bool AnimationEventHandle::handle( const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& aa )
{
	//创建动画更新回调对象
	osg::ref_ptr<osg::AnimationPathCallback> animationPathCallback=new osg::AnimationPathCallback();
	osg::ref_ptr<osg::Group> group=dynamic_cast<osg::Group*>(m_sceneNode);

	//取得节点的动画属性
	animationPathCallback=dynamic_cast<osg::AnimationPathCallback*>(group->getChild(0)->getUpdateCallback());

	switch (ea.getEventType())
	{
	case osgGA::GUIEventAdapter::KEYDOWN:
		{
			//暂停
			if (ea.getKey()=='p')
			{
				animationPathCallback->setPause(true);
				return true;
			}
			//开始
			if (ea.getKey()=='s')
			{
				animationPathCallback->setPause(false);
				return true;
			}
			//重新开始
			if (ea.getKey()=='r')
			{
				animationPathCallback->reset();
				return true;
			}
			break;
		}
	default:
		break;
	}
	return false;
}
