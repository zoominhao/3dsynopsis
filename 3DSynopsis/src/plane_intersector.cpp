/* -*-c++-*- OpenSceneGraph Cookbook
 * Chapter 10 Recipe 7
 * Author: Wang Rui <wangray84 at gmail dot com>
*/

#include <osg/Geometry>
#include <osg/TemplatePrimitiveFunctor>
#include "plane_intersector.h"
#include <Cgal/intersections.h>
#include "cgal_types.h"

PlaneIntersector::PlaneIntersector()
:   osgUtil::LineSegmentIntersector(MODEL, 0.0, 0.0),
    _pickBias(1.5f)
{
}

PlaneIntersector::PlaneIntersector( const osg::Vec3& start, const osg::Vec3& end )
:   osgUtil::LineSegmentIntersector(start, end),
    _pickBias(1.5f)
{
}

PlaneIntersector::PlaneIntersector( CoordinateFrame cf, double x, double y )
:   osgUtil::LineSegmentIntersector(cf, x, y),
    _pickBias(1.5f)
{
}

osgUtil::Intersector* PlaneIntersector::clone( osgUtil::IntersectionVisitor& iv )
{
    if ( _coordinateFrame==MODEL && iv.getModelMatrix()==0 )
    {
        osg::ref_ptr<PlaneIntersector> cloned = new PlaneIntersector( _start, _end );
        cloned->_parent = this;
        cloned->_pickBias = _pickBias;
        return cloned.release();
    }
    
    osg::Matrix matrix;
    switch ( _coordinateFrame )
    {
        case WINDOW:
            if (iv.getWindowMatrix()) matrix.preMult( *iv.getWindowMatrix() );
            if (iv.getProjectionMatrix()) matrix.preMult( *iv.getProjectionMatrix() );
            if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
            if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
            break;
        case PROJECTION:
            if (iv.getProjectionMatrix()) matrix.preMult( *iv.getProjectionMatrix() );
            if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
            if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
            break;
        case VIEW:
            if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
            if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
            break;
        case MODEL:
            if (iv.getModelMatrix()) matrix = *iv.getModelMatrix();
            break;
    }
    
    osg::Matrix inverse = osg::Matrix::inverse(matrix);
    osg::ref_ptr<PlaneIntersector> cloned = new PlaneIntersector( _start*inverse, _end*inverse );
    cloned->_parent = this;
    cloned->_pickBias = _pickBias;
    return cloned.release();
}

void PlaneIntersector::intersect( osgUtil::IntersectionVisitor& iv, osg::Drawable* drawable )
{

	osg::BoundingBox bb = drawable->getBound();
	bb.xMin() -= _pickBias; bb.xMax() += _pickBias;
	bb.yMin() -= _pickBias; bb.yMax() += _pickBias;
	bb.zMin() -= _pickBias; bb.zMax() += _pickBias;

	osg::Vec3d s(_start), e(_end);
	if ( !intersectAndClip(s, e, bb) ) return;
	if ( iv.getDoDummyTraversal() ) return;

	osg::Geometry* geometry = drawable->asGeometry();
	if ( geometry )
	{
		osg::Vec3Array* vertices = dynamic_cast<osg::Vec3Array*>( geometry->getVertexArray() );
		if ( !vertices ) return;

		osg::Vec3d dir = e - s;
		double invLength = 1.0 / dir.length();
		for ( unsigned int i=0; i<vertices->size(); ++i )
		{
			double distance =  fabs( (((*vertices)[i] - s)^dir).length() );
			distance *= invLength;
			if ( _pickBias<distance ) continue;

			Intersection hit;
			hit.ratio = distance;
			hit.nodePath = iv.getNodePath();
			hit.drawable = drawable;
			hit.matrix = iv.getModelMatrix();
			hit.localIntersectionPoint = (*vertices)[i];
			hit.primitiveIndex = i;
			insertIntersection( hit );
		}
	}
    
}



//////////////////////////////////////////////////////////////////////////


#include <osg/Geometry>
#include <osg/TemplatePrimitiveFunctor>
#include "plane_intersector.h"

PlaneIntersector2::PlaneIntersector2()
	:   osgUtil::LineSegmentIntersector(MODEL, 0.0, 0.0),
	_pickBias(1.5f)
{
}

PlaneIntersector2::PlaneIntersector2( const osg::Vec3& start, const osg::Vec3& end )
	:   osgUtil::LineSegmentIntersector(start, end),
	_pickBias(1.5f)
{
}

PlaneIntersector2::PlaneIntersector2( CoordinateFrame cf, double x, double y )
	:   osgUtil::LineSegmentIntersector(cf, x, y),
	_pickBias(1.5f)
{
}

osgUtil::Intersector* PlaneIntersector2::clone( osgUtil::IntersectionVisitor& iv )
{
	if ( _coordinateFrame==MODEL && iv.getModelMatrix()==0 )
	{
		osg::ref_ptr<PlaneIntersector2> cloned = new PlaneIntersector2( _start, _end );
		cloned->_parent = this;
		cloned->_pickBias = _pickBias;
		return cloned.release();
	}

	osg::Matrix matrix;
	switch ( _coordinateFrame )
	{
	case WINDOW:
		if (iv.getWindowMatrix()) matrix.preMult( *iv.getWindowMatrix() );
		if (iv.getProjectionMatrix()) matrix.preMult( *iv.getProjectionMatrix() );
		if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
		if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
		break;
	case PROJECTION:
		if (iv.getProjectionMatrix()) matrix.preMult( *iv.getProjectionMatrix() );
		if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
		if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
		break;
	case VIEW:
		if (iv.getViewMatrix()) matrix.preMult( *iv.getViewMatrix() );
		if (iv.getModelMatrix()) matrix.preMult( *iv.getModelMatrix() );
		break;
	case MODEL:
		if (iv.getModelMatrix()) matrix = *iv.getModelMatrix();
		break;
	}

	osg::Matrix inverse = osg::Matrix::inverse(matrix);
	osg::ref_ptr<PlaneIntersector2> cloned = new PlaneIntersector2( _start*inverse, _end*inverse );
	cloned->_parent = this;
	cloned->_pickBias = _pickBias;
	return cloned.release();
}

void PlaneIntersector2::intersect( osgUtil::IntersectionVisitor& iv, osg::Drawable* drawable )
{

	osg::BoundingBox bb = drawable->getBound();
	bb.xMin() -= _pickBias; bb.xMax() += _pickBias;
	bb.yMin() -= _pickBias; bb.yMax() += _pickBias;
	bb.zMin() -= _pickBias; bb.zMax() += _pickBias;

	osg::Vec3d s(_start), e(_end);
	if ( !intersectAndClip(s, e, bb) ) return;
	if ( iv.getDoDummyTraversal() ) return;

	osg::Geometry* geometry = drawable->asGeometry();
	if ( geometry )
	{
		CgalSegment seg( CgalPoint(s.x(),s.y(),s.z()), CgalPoint(e.x(), e.y(),e.z()));
		CgalPlane line( CgalPoint(0,0,0), CgalVector(0,0,1));
		CGAL::Object result = CGAL::intersection(seg, line);
		if (const CgalPoint *ipoint = CGAL::object_cast<CgalPoint >(&result)) {
			// handle the point intersection case with *ipoint.
			Intersection hit;
			hit.ratio = 0;
			hit.nodePath = iv.getNodePath();
			hit.drawable = drawable;
			hit.matrix = iv.getModelMatrix();
			hit.localIntersectionPoint = osg::Vec3(ipoint->x(), ipoint->y(),ipoint->z());
			insertIntersection( hit );
		} 
	}

}


