#include "sample_point_set.h"
#include "sp1.h"

boost::shared_ptr<PointSet<TransformationPoint3>> 
	SamplePointSet2::computePairTransforamtion(SamplePointSet2Ptr target_set)
{
	typedef boost::shared_ptr<PointSet<TransformationPoint3>> TPSTpr;
	TPSTpr tps(new PointSet<TransformationPoint3>());

	size_t num_s = getPointNum();
	size_t num_t = target_set->getPointNum();

	
	for( size_t i = 0; i != num_s; ++i)
	{
		SamplePoint2& sp1 = [i];
		for( size_t j = 0; j != num_t; ++j )
		{
			SamplePoint2& target_s = target_set->points_[j];
			TransformationPoint3 mat_p = sp1.computeTransformation( target_s );
			tps->addAPoint(mat_p);
		}
	}

	return tps;
}