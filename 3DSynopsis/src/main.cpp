#include <QApplication>
#include "main_window.h"
#include <time.h>

//#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )
int main(int argc, char *argv[])
{
 // Make Xlib and GLX thread safe under X11
 QApplication::setAttribute(Qt::AA_X11InitThreads);
 QApplication application(argc, argv);

 srand (time(NULL));

 MainWindow main_window;
 main_window.showMaximized();

 return application.exec();

}

