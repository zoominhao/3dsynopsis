#include "axis_camera_callback.h"
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osg/Camera>
#include <osg/View>

void AxisCameraUpdateCallback::operator()( osg::Node* node, osg::NodeVisitor* nv )
{
	if(nv->getVisitorType() == osg::NodeVisitor::UPDATE_VISITOR)
	{
		osg::Camera* camera = dynamic_cast<osg::Camera*>(node);
		if (camera)
		{
			osg::View* view = camera->getView();
			if (view && view->getNumSlaves() > 0)
			{
				osg::View::Slave* slave = &view->getSlave(0);
				if(slave->_camera.get() == camera)
				{
					osg::Camera* masterCam = view->getCamera();
					osg::Vec3 eye, center, up;
					masterCam->getViewMatrixAsLookAt(eye, center, up, 30);
					osg::Matrixd matrix;
					matrix.makeLookAt(eye-center, osg::Vec3(0, 0, 0), up); // always look at (0, 0, 0)
					camera->setViewMatrix(matrix);
				}
			}
		}
	}
	traverse(node,nv);
}
