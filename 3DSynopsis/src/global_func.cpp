#include "global_func.h"

#include <iostream>
#include <sstream>
#include <Eigen/Eigenvalues>
#include <boost/random.hpp>

namespace GLOBAL
{
	
	
	void calPCA( const std::vector<osg::Vec3>& points_3d, std::vector<osg::Vec2>& points_2d )
	{

		// convert to mat
		if( points_3d.size() < 3)
			return;

		Eigen::MatrixXd mat( points_3d.size(), 3);
		for( unsigned int i = 0; i != points_3d.size(); ++i)
		{
			mat(i,0) = points_3d[i].x();
			mat(i,1) = points_3d[i].y();
			mat(i,2) = points_3d[i].z();
		}
		Eigen::MatrixXd centered = mat.rowwise() - mat.colwise().mean();
		Eigen::MatrixXd cov = centered.adjoint() * centered;


		//then perform the eigen decomposition:
		Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(cov);
		//std::cout << eig.eigenvalues().transpose() << std::endl;

		Eigen::Vector3d dir1, dir2,dir3;
		dir1 = eig.eigenvectors().col(2);
		dir2 = eig.eigenvectors().col(1);
		dir3 = eig.eigenvalues().col(0);

		points_2d.clear();
		for( unsigned int i = 0 ;i!= mat.rows(); ++i)
		{
			double v_dir1 = mat.row(i)*dir1;
			double v_dir2 = mat.row(i)*dir2;
			points_2d.push_back( osg::Vec2(v_dir1, v_dir2));
		}
	}


/////////////////////////////////////////////////////////////////////////////////////////////
/*@EurlDisPL  计算点线之间欧氏距离
  @sPoint，tline给定的点线
  @ author:zoomin
  @time:2013-08-27
*/
	double EurlDisPL(CgalPoint sPoint, CgalSegment tSeg)
	{
		double eurlDistance = 10000;

		CgalPoint point = tSeg.supporting_line().projection(sPoint);
		if (tSeg.collinear_has_on(point))
		{
			eurlDistance = (point.x()-sPoint.x())*(point.x()-sPoint.x())+(point.y()-sPoint.y())*(point.y()-sPoint.y())+(point.z()-sPoint.z())*(point.z()-sPoint.z());
			eurlDistance = std::sqrt(eurlDistance);
		}

		return eurlDistance;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////
/*@EurlDisPL  计算点线之间欧氏距离
  @sPoint，tline给定的点线
  @ author:zoomin
  @time:2013-08-27
*/
	double EurlDisPL(CgalPoint2 sPoint, CgalSegment2 tSeg)
	{
		double eurlDistance = 10000;

		CgalPoint2 point = tSeg.supporting_line().projection(sPoint);
		//if (tSeg.collinear_has_on(point))
		//{
			eurlDistance = (point.x()-sPoint.x())*(point.x()-sPoint.x())+(point.y()-sPoint.y())*(point.y()-sPoint.y());
			eurlDistance = std::sqrt(eurlDistance);
		//}

		return eurlDistance;
	}


	bool delVecDupElem(std::vector<int>& vec)
	{
		 std::sort( vec.begin(), vec.end() ); // 1,2,3,3,4,4,6,7,8,9 
		//然后使用unique算法返回值是重复元素的开始位置。//1,2,3,4,6,7,8,9,3,4 
		//最后删除后面的那段重复部分
		vec.erase( unique( vec.begin(), vec.end() ), vec.end());
		return  true;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
/*@PtInPolygon 判断点是否在多边形内
  @pt 给定点，poly给点的二维多边形
  @ author:zoomin
  @time:2013-09-16
*/
	bool PtInPolygon (CgalPoint2 pt, CgalPolygon2& poly) { 
		int nCross = 0;
		int nCount = poly.size() ;

		for (int i = 0; i < nCount; i++) 
		{ 
			CgalPoint2 p1 = poly[i]; 
			CgalPoint2 p2 = poly[(i + 1) % nCount];
			// 求解 y=p.y 与 p1p2 的交点
			if ( p1.y() == p2.y()) // p1p2 与 y=p0.y平行 
				continue;
			if ( pt.y()< min(p1.y(), p2.y()) ) // 交点在p1p2延长线上 
				continue; 
			if ( pt.y()>= max(p1.y(), p2.y()) ) // 交点在p1p2延长线上 
				continue;
			// 求交点的 X 坐标 -------------------------------------------------------------- 
			double x = (double)(pt.y()- p1.y()) * (double)(p2.x() - p1.x()) / (double)(p2.y() - p1.y()) + p1.x();
			if ( x > pt.x() ) 
				nCross++; // 只统计单边交点 
		}
		// 单边交点为偶数，点在多边形之外 --- 

		return !(nCross % 2 == 0); 
	}

		/////////////////////////////////////////////////////////////////////////////////////////////
/*@calTheta 计算两点之间的夹角
  @
  @ author:zoomin
  @time:2013-09-20
*/
	double calTheta(CgalPoint2 pt1,CgalPoint2 pt2)
	{
		double x1,y1,x2,y2,eurlDis1,eurlDis2,dotProduct,theta;
		x1 = pt1.x();
		y1 = pt1.y();
		x2 = pt2.x();
		y2 = pt2.y();
		eurlDis1 = sqrt(x1*x1+y1*y1);
		eurlDis2 = sqrt(x2*x2+y2*y2);
		dotProduct = x1*x2+y1*y2;
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
        if (y1<0)
			theta = 360 - theta;
		return theta;
	}

		/////////////////////////////////////////////////////////////////////////////////////////////
/*@calTheta 计算两向量之间的夹角
  @
  @ author:zoomin
  @ time:2013-10-21
*/
	double calTheta(CgalVector2 dir1,CgalVector2 dir2)
	{
		double eurlDis1,eurlDis2,dotProduct,theta;

		eurlDis1 = sqrt(dir1.x()*dir1.x()+dir1.y()*dir1.y());
		eurlDis2 = sqrt(dir2.x()*dir2.x()+dir2.y()*dir2.y());
		dotProduct = dir1*dir2;
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
		
		return theta;
	}

	double calTheta(EV2 &dir1,EV2 &dir2)
	{
		double eurlDis1,eurlDis2,dotProduct,theta;
		eurlDis1 = dir1.norm();
		eurlDis2 = dir2.norm();
		dotProduct = dir1.dot(dir2);
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
		if (dir1.y()<0)
			theta = 360 - theta;
		return theta;
	}

	double calReTheta(EV2 &dir1,EV2 &dir2)
	{
		double eurlDis1,eurlDis2,dotProduct,theta;

		eurlDis1 = dir1.norm();
		eurlDis2 = dir2.norm();
		dotProduct = dir1.dot(dir2);
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
		EV3 dir31 = EV3(dir1.x(),dir1.y(),0);
		EV3 dir32 = EV3(dir2.x(),dir2.y(),0);
		EV3 res = dir31.cross(dir32);
		double zval = res.z();
		if (zval > 0)
			theta = 360 - theta;
		return theta;
	}

	double calAngle(EV2 &dir1,EV2 &dir2)
	{
		double eurlDis1,eurlDis2,dotProduct,theta;
		eurlDis1 = dir1.norm();
		eurlDis2 = dir2.norm();
		dotProduct = dir1.dot(dir2);
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
		return theta;
	}

	double calAngle( EV3 &dir1,EV3 &dir2 )
	{
		double eurlDis1,eurlDis2,dotProduct,theta;
		eurlDis1 = dir1.norm();
		eurlDis2 = dir2.norm();
		dotProduct = dir1.dot(dir2);
		theta = std::acos(dotProduct/(eurlDis1*eurlDis2));
		theta = theta/M_PI * 180;
		return theta;
	}

	double gaussmf( double x, double sigma, double c )
	{
		double y = std::exp( -std::pow((x - c),2)/(2* std::pow(sigma,2)));
		return y;
	}

	EV4 scalar2color( double scalar )
	{
		double dis = (std::min)( (std::max)(scalar, 0.0 ), 1.0 ) ;

		EMatXd baseColor(9,4);
		baseColor<<
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.7, 1.0, 0.0,
			0.0, 1.0, 1.0, 0.0,
			0.0, 1.0, 0.7, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.7, 1.0, 0.0, 0.0,
			1.0, 1.0, 0.0, 0.0,
			1.0, 0.7, 0.0, 0.0,
			1.0, 0.0, 0.0, 0.0;


		double step = 1.0 / 8.0 ;

		int baseID = dis/step;
		if( baseID == 8 )
			baseID = 7 ;

		EV4 mixedColor =  baseColor.row(baseID) * (baseID*step+step- dis)  + baseColor.row(baseID+1) * (dis-baseID*step) ;
		mixedColor = (mixedColor/step);
		return mixedColor ;
	}

	void normalize(std::vector<double>& tonorm)
	{
		if (tonorm.size()==0)
		  return;
		std::vector<double> tmpV = tonorm;
		/*for (int i = 0; i < tonorm.size(); ++i)
		{
			tmpV.push_back(tonorm[i]); 
		}*/

		std::sort(tmpV.begin(),tmpV.end());
		double maxV = tmpV.at(tmpV.size()-1);
		double minV = tmpV.at(0);
		if (maxV==minV)
		{
			for (int i = 0; i < tonorm.size(); ++i)
			{
				tonorm[i] = 0.5;
			}
		}
		else
		{
			for (int i = 0; i < tonorm.size(); ++i)
			{
				tonorm[i] = (tonorm[i]-minV)/(maxV-minV);
			}
		}
		
	}

	std::string double2string(double input,int precision)
	{
		std::stringstream ss;
		ss<<std::setprecision(precision)<<input;
		std::string output=ss.str();
		return output;
	}

	std::string int2string(int input)
	{
		std::stringstream ss;
		ss<<input;
		std::string output=ss.str();
		return output;
	}

	bool  strreplace(std::string& input, std::string from, std::string to)
	{
		assert( from != to );

		std::string::size_type pos = 0;
		while ( (pos = input.find(from, pos)) != std::string::npos ) {
			input.replace( pos, from.size(), to );
			pos++;
		}
		
		return true;
	}

	void laplaceSmooth( std::vector<double> input, std::vector<double>& output, int iterationnum )
	{
		while (iterationnum>0)
		{
			output.clear();
			int vexSize = input.size();
			for(size_t i = 0; i< vexSize -1; ++i)
			{
				double p = input[i];
				double np;
				output.push_back(p);
				if (i == 0)
				{
					double p2 = input[i+1];
					double p3 = input[i+2];
					np = (p*3 + p2*6 - p3) / 8;
				}
				else if (i == vexSize - 2)
				{
					double p1 = input[i-1];
					double p2 = input[i+1];
					np = (p1*(-1) + p*6 + p2*3) / 8;
				}
				else
				{
					double p1 = input[i-1];
					double p2 = input[i+1];
					double p3 = input[i+2];
					np = (p1*(-1) + p*9 + p2*9 - p3) / 16;
				}
				output.push_back(np);
			}
			output.push_back(input[vexSize -1]);
			//拉普拉斯平滑
			for (size_t i = 1; i < output.size()-1; ++i)
			{
				output[i] = output[i]/2 + output[i-1]/4 + output[i+1]/4;
			}
			input = output;
			--iterationnum;
		}
	}

	void laplaceSmooth(std::vector<EV3>& input, std::vector<EV3>& output, int iterationnum)
	{
		while (iterationnum>0)
		{
			output.clear();
			int vexSize = input.size();
			for(size_t i = 0; i< vexSize -1; ++i)
			{
				EV3 p = input[i];
				EV3 np;
				output.push_back(p);
				if (i == 0)
				{
					EV3 p2 = input[i+1];
					EV3 p3 = input[i+2];
					np = (p*3 + p2*6 - p3) / 8;
				}
				else if (i == vexSize - 2)
				{
					EV3 p1 = input[i-1];
					EV3 p2 = input[i+1];
					np = (p1*(-1) + p*6 + p2*3) / 8;
				}
				else
				{
					EV3 p1 = input[i-1];
					EV3 p2 = input[i+1];
					EV3 p3 = input[i+2];
					np = (p1*(-1) + p*9 + p2*9 - p3) / 16;
				}
				output.push_back(np);
			}
			output.push_back(input[vexSize -1]);
			//拉普拉斯平滑
			for (size_t i = 1; i < output.size()-1; ++i)
			{
				output[i] = output[i]/2 + output[i-1]/4 + output[i+1]/4;
			}
		    /*for (size_t i = 2; i < output.size()-2; ++i)
			{
				output[i] = output[i]/4 + output[i-1]/4 + output[i+1]/4 + output[i-2]/8 + output[i+2]/8;
			}*/
			input = output;
			--iterationnum;
		}
	}

	void laplaceSmooth( std::vector<EV4>& input, std::vector<EV4>& output, int iterationnum )
	{
		while (iterationnum>0)
		{
			output.clear();
			int vexSize = input.size();
			for(size_t i = 0; i< vexSize -1; ++i)
			{
				EV4 p = input[i];
				EV4 np;
				output.push_back(p);
				if (i == 0)
				{
					EV4 p2 = input[i+1];
					EV4 p3 = input[i+2];
					np = (p*3 + p2*6 - p3) / 8;
				}
				else if (i == vexSize - 2)
				{
					EV4 p1 = input[i-1];
					EV4 p2 = input[i+1];
					np = (p1*(-1) + p*6 + p2*3) / 8;
				}
				else
				{
					EV4 p1 = input[i-1];
					EV4 p2 = input[i+1];
					EV4 p3 = input[i+2];
					np = (p1*(-1) + p*9 + p2*9 - p3) / 16;
				}
				output.push_back(np);
			}
			output.push_back(input[vexSize -1]);
			//拉普拉斯平滑
			for (size_t i = 1; i < output.size()-1; ++i)
			{
				output[i] = output[i]/2 + output[i-1]/4 + output[i+1]/4;
			}
			/*for (size_t i = 2; i < output.size()-2; ++i)
			{
				output[i] = output[i]/4 + output[i-1]/4 + output[i+1]/4 + output[i-2]/8 + output[i+2]/8;
			}*/
			input = output;
			--iterationnum;
		}
	}

	double fixAngle( double deg )
	{
		while (deg < -180)
			deg += 360;

		while (deg > 180)
			deg -= 360;

		return deg;
	}

	double fixAngle360( double deg )
	{
		while (deg < 0)
			deg += 360;

		while (deg > 360)
			deg -= 360;

		return deg;
	}

};