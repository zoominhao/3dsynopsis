#include "resize_handler.h"

ResizeHandler::ResizeHandler(void)
{
}

ResizeHandler::~ResizeHandler(void)
{
}

bool ResizeHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
  osgGA::GUIEventAdapter::EventType ev = ea.getEventType();
  if(ev != osgGA::GUIEventAdapter::RESIZE) return false;

  return true;
}