#include "renderable.h"
#include "update_callback.h"

//////////////////////////////////////////////////////////////////////////
RenderableUpdateCallback::RenderableUpdateCallback(Renderable* renderable)
  :renderable_(renderable)
{
}


RenderableUpdateCallback::~RenderableUpdateCallback(void)
{
}


void RenderableUpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
  renderable_->update();

  return;
}

