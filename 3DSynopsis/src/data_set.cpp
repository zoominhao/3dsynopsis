#include "data_set.h"
#include "main_window.h"
#include "pickup_handler.h"
#include "snapshot_handler.h"
#include "axis_camera_callback.h"
#include "sy_config.h"
#include "parameter_manager.h"

#include <osgDB/ReadFile>

#define RANGE 60

DataSet::DataSet( QWidget * parent /*= 0*/ )
	:OSGViewerWidget(parent)
	,m_scene( new SyScene())
	,m_car( new SyCar())
	,m_DCcam( new SyCam())
	,m_sceneCamera( new SyCamera())
	,m_water(new SyWater())
	,m_cars(new SyCars())
{
	createAxiaCamera();
	addSlave( m_axisCamera, false );
	para_root_->addChild( m_axisCamera );

	m_scene->setName("3DSCENE");
	addChild( m_scene );
	m_car->setName("CAR");
	addChild( m_car );
	m_DCcam->setName("DCCAM");
	addChild( m_DCcam );
	m_sceneCamera->setName("SCENECAMERA");
	addChild(m_sceneCamera);
	m_water->setName("WATER");
	addChild(m_water);
	m_cars->setName("CARS");
	addChild(m_cars);

	//addEventHandler(new SelectScenePosHandler(pickup_root_.get(),new osg::Vec3(0.0,0.0,0.0)));
	//addEventHandler(new SelectGeometryHandler());
	addEventHandler(new osgViewer::ScreenCaptureHandler(new WriteToFile(m_folder)));	
	addEventHandler(new SyKeyHandler(pickup_root_.get(), m_pSynopsisManipulator));
	addEventHandler(new SelectPointHandler( pickup_root_.get(), m_scene ));
}

DataSet::~DataSet( void )
{

}

void DataSet::createAxiaCamera()
{
	m_axisCamera = new osg::Camera;
	m_axisCamera->setProjectionMatrix(osg::Matrix::ortho2D(-1.5, 17.75, -1.5, 13.86));
	m_axisCamera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	m_axisCamera->setClearMask(GL_DEPTH_BUFFER_BIT);
	m_axisCamera->setRenderOrder(osg::Camera::POST_RENDER);
	m_axisCamera->setUpdateCallback(new AxisCameraUpdateCallback());

	osg::Node* axes = osgDB::readNodeFile("axes.osgt");
	m_axisCamera->addChild(axes);
}

void DataSet::mouseDoubleClickEvent( QMouseEvent *event )
{
	emit zoomin(this);
	std::string camconfig  = m_folder.toStdString() + GConstant::C_SCENE_CAMERA;
	if (camconfig!="")
		OSGViewerWidget::readCameraParameters( camconfig.c_str() );
	else
		centerScene();
	OSGViewerWidget::mouseDoubleClickEvent(event);

	return;
}

void DataSet::wheelEvent( QWheelEvent *event )
{
	emit zoomin(this);

	OSGViewerWidget::wheelEvent(event);
	return;
}

static QAction* addCheckableActionToMenu(const QString& text, bool checked, QMenu* menu)
{
	QAction* action = new QAction(text, menu);
	action->setCheckable(true);
	action->setChecked(checked);
	menu->addAction(action);

	return action;
}

void DataSet::contextMenuEvent( QContextMenuEvent *event )
{
	QMenu menu(this);

	{
		// 		QMenu* rendering_menu = menu.addMenu("Rendering");
		// 		connect(addCheckableActionToMenu("Render Source", !source_shape_->isHidden(), rendering_menu),
		// 			SIGNAL(toggled(bool)), this, SLOT(toggleRenderSource(bool)));
		// 		connect(addCheckableActionToMenu("Render Target", !target_shape_->isHidden(), rendering_menu),
		// 			SIGNAL(toggled(bool)), this, SLOT(toggleRenderTarget(bool)));
		// 		connect(addCheckableActionToMenu("Render Transformation", !transforms_->isHidden(), rendering_menu),
		// 			SIGNAL(toggled(bool)), this, SLOT(toggleRenderTransformation(bool)));
	}

	menu.exec(event->globalPos());

	return;
}

void DataSet::reinit()
{
	removeChild( m_scene.get(), true );
	removeChild( m_car.get(), true );
	removeChild( m_DCcam.get(), true);
	removeChild( m_sceneCamera.get(), true);
	removeChild( m_meterCamera, true);
	removeChild( m_water.get(),true);

	para_root_->removeChildren(0, para_root_->getNumChildren() );
	pickup_root_->removeChildren(0, pickup_root_->getNumChildren() );

	para_root_->addChild( m_axisCamera );

	m_scene = osg::ref_ptr<SyScene>( new SyScene());
	m_scene->setName("3DSCENE");
	addChild( m_scene );

	m_car = osg::ref_ptr<SyCar>( new SyCar());
	m_car->setName("CAR");
	addChild( m_car );

	m_DCcam = osg::ref_ptr<SyCam>( new SyCam());
	m_DCcam->setName("DCCAM");
	addChild( m_DCcam );

	m_sceneCamera = osg::ref_ptr<SyCamera>(new SyCamera());
	m_sceneCamera->setName("SCENECAMERA");
	addChild(m_sceneCamera);

	m_water = osg::ref_ptr<SyWater>(new SyWater());
	m_water->setName("WATER");
	addChild(m_water);

	m_cars = osg::ref_ptr<SyCars>(new SyCars());
	m_cars->setName("CARS");
	addChild(m_cars);

	addEventHandler(new osgViewer::ScreenCaptureHandler(new WriteToFile(m_folder)));
	//addEventHandler(new SelectScenePosHandler(pickup_root_.get(),new osg::Vec3(0.0,0.0,0.0)));
	addEventHandler(new SyKeyHandler(pickup_root_.get(), m_pSynopsisManipulator));
	addEventHandler(new SelectPointHandler( pickup_root_.get(), m_scene ));
}

bool DataSet::load( const QString& folder )
{
	m_folder = folder;
	std::string sceneFolder = m_folder.toStdString();

	SyConfig::getInstance().loadConfig((sceneFolder+GConstant::C_CONFIG).c_str());

	bool load_scene = false;
	load_scene = m_scene->setFromFile(sceneFolder);
	//cars
	bool load_cars = false;
	load_cars = m_cars->setFromFile(sceneFolder);

	bool load_car = false;
	load_car = m_car->setFromFile(sceneFolder);

	bool load_DCcam = false;
	load_DCcam = m_DCcam->setFromFile(sceneFolder);

	bool load_camera = false;
	load_camera = m_sceneCamera->setFromFile(sceneFolder);
	bool load_water = false;
	load_water = m_water->setFromFile(sceneFolder);

	//getCamera()->setComputeNearFarMode(osg::CullSettings::COMPUTE_NEAR_USING_PRIMITIVES );  //方便截取深度图
	//getCamera()->setComputeNearFarMode(osg::CullSettings::COMPUTE_NEAR_FAR_USING_PRIMITIVES);
	getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);


	double inifovy,iniratio,iniznear,inizfar;   //znear = 43
	getCamera()->getProjectionMatrixAsPerspective(inifovy,iniratio,iniznear,inizfar);
	inifovy = RANGE;
	getCamera()->setProjectionMatrixAsPerspective(inifovy,iniratio,1,100000);
	//getCamera()->setProjectionMatrixAsPerspective(inifovy,iniratio,iniznear,inizfar);

	if(load_scene&&load_car&&load_camera&&load_water&&load_DCcam)
	{
		m_interest.init(sceneFolder);
		addChild(m_meterCamera);
		std::string camconfig  = sceneFolder + GConstant::C_SCENE_CAMERA;
		if (camconfig!="")
			OSGViewerWidget::readCameraParameters( camconfig.c_str() );
		else
			centerScene();
	}
	else
	{
		std::cout<<"scene file doesn't exist!"<<std::endl;
	}
	//计算出场景的中心位置
	m_car->setCenter(m_car->getBound().center());
	m_DCcam->setCenter(m_DCcam->getBound().center());
	m_sceneCamera->setCenter(m_sceneCamera->getBound().center());

	return load_scene;
}

void DataSet::toggleRender( const std::vector<bool>& state )
{
	if( state.size() != 18)
		return;

	m_scene->toggleRenderScene(state[0]);      //scene
	m_scene->toggleRenderColor(state[1]);      //color scene
	m_scene->toggleRenderPath(state[2]);      //path
	m_scene->toggleRenderCoolPath(state[3]);      //path
	m_scene->toggleRenderSmoothPath(state[4]);      //path
	m_scene->toggleRenderCorLines(state[5]);    //cor lines
	m_car->toggleRenderPorsche(state[6]);      //porsche
	m_DCcam->toggleRenderDCCam(state[7]);      //DCCam
	m_sceneCamera->toggleRenderVisualPorsche(state[8]);      //visul porsches
	m_sceneCamera->toggleRenderCamera(state[9]);  //scene backup camera
	m_sceneCamera->toggleRenderCameraPath(state[10]);  //scene backup camera path
	m_sceneCamera->toggleRenderCorLines(state[11]);  //scene cor lines
	m_scene->toggleRenderSky(state[12]);      //sky
	m_scene->toggleRenderColorSky(state[13]);      //color sky
	m_water->toggleRenderWater(state[14]);   //water
	m_cars->toggleRenderCars(state[15]);   //Vehicle
	para_root_->setNodeMask( state[16] ? 1:0X0);
	m_axisCamera->setNodeMask( state[17]? 1:0X0); 
}

void DataSet::ZoominPoints()
{
	increasePointSize();
	increaseLineWidth();
}

void DataSet::ZoomoutPoints()
{
	decreasePointSize();
	decreaseLineWidth();
}

void DataSet::saveCameraParameters( void )
{
	MainWindow* main_window = MainWindow::getInstance();
	QString filename = QFileDialog::getSaveFileName(main_window, "Save Camera Parameters", main_window->getWorkspace(), "Camera Paramters(*.txt)");
	if (filename.isEmpty())
		return;

	OSGViewerWidget::writeCameraParameters( filename );

	return;
}

void DataSet::loadCameraParameters( void )
{
	MainWindow* main_window = MainWindow::getInstance();
	QString filename = QFileDialog::getOpenFileName(main_window, "Load Camera Parameters", main_window->getWorkspace(), "Camera Parameters (*.txt)");
	if (filename.isEmpty())
		return;
	OSGViewerWidget::readCameraParameters( filename );
}

void DataSet::skyAnimation( void )
{
	m_scene->SkyAnimation();
}

void DataSet::snap( void )
{
	bool isContinue = false;
	switch(QMessageBox::question(this,"Question",tr("save the snapshot?"),
		QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Ok))
	{
	case QMessageBox::Ok:
		isContinue = true;
		break;
	case QMessageBox::Cancel:
		isContinue = false;
		break;
	default:
		break;
	}
	if( !isContinue )
		return;

	std::string dir = m_folder.toStdString() + "/snapshot/";

	if( !QDir(dir.c_str()).exists())
	{
		bool flag = QDir().mkdir( dir.c_str() );
		if( flag == false) 
		{
			return;
		}
	}
	std::string rootName = dir + "snap-";

	int static imageNo = 0;  // to set
	if (ParameterManager::getInstance().getVideoMode())
	{
		rootName = dir;
		imageNo ++;
	}


	std::string extName = ".png";
	double fovy,ratio,znear,zfar;   
	getCamera()->getProjectionMatrixAsPerspective(fovy,ratio,znear,zfar);
	osg::ref_ptr< SyScreenCapture > sc = new SyScreenCapture(rootName, extName,znear,zfar);

	getCamera()->setPostDrawCallback( sc.get() );
	sc->setNearFar(znear,zfar);
	sc->setCapture(true);
	if (ParameterManager::getInstance().getVideoMode())
	{
		sc->setUseFrameNumber(false);
		sc->setSnapNo(imageNo);
	}
	else
	{
		sc->setUseFrameNumber(true);
	}
	sc->setNumFramesToCapture(1);
}

void DataSet::SYSample( void )
{
	std::cout<<"sample"<<std::endl;
	m_interest.uniformSample(10);
}

void DataSet::SYConstant( void )
{
	std::cout<<"cal constant speed"<<std::endl;
	if (ParameterManager::getInstance().getDrivingMode()==1)
		m_interest.calConstant();
	if (ParameterManager::getInstance().getDrivingMode()==5)
		m_interest.calJump();
}

void DataSet::SYSnaps( void )
{
	std::cout<<"snaps"<<std::endl;
	connect(this,SIGNAL(frameend()),&m_interest,SLOT(snapTick()));
	m_interest.snapSampleViews(getCamera(),m_pSynopsisManipulator, ParameterManager::getInstance().getIterationNum());
}

void DataSet::SYImgPro( void )
{
	std::cout<<"image process"<<std::endl;
	if (ParameterManager::getInstance().getDrivingMode()==2)
		m_interest.imageProcess();
}

void DataSet::SYSpeed( void )
{
	std::cout<<"solve speed"<<std::endl;
	m_interest.solveSpeed();
}

void DataSet::SYSmooth( void )
{
	std::cout<<"smooth"<<std::endl;
	m_interest.smoothPath();
}

void DataSet::SYView( void )
{
	std::cout<<"solve view"<<std::endl;
	m_interest.viewProduce(ParameterManager::getInstance().getDrivingMode());
}

void DataSet::SYCmp( void )
{
	std::cout<<"compare importance"<<std::endl;
	m_interest.cmpImp();
}

void DataSet::SYOptFlow( void )
{
	std::cout<<"optical flow"<<std::endl;
	m_interest.calOptFlow();
}

void DataSet::SYOptAcc( void )
{
	std::cout<<"optical flow acceleration"<<std::endl;
	m_interest.calOptAcc();
}

void DataSet::SYGenOptVideo( void )
{
	std::cout<<"optical flow video"<<std::endl;
	m_interest.generateOptVideo(ParameterManager::getInstance().getDrivingMode());
}

void DataSet::SYViewCmp( void )
{
	std::cout<<"View Compare & Entropy"<<std::endl;
	m_interest.viewCompare();
}

void DataSet::SYGenMap( void )
{
	std::cout<<"generate map"<<std::endl;
	m_interest.generateMap();
}



void DataSet::SYTest( void )
{
	osg::Vec3d dir(3,4,0);
	dir.normalize();
	std::cout<<dir.x()<<" "<<dir.y()<<std::endl;
}
