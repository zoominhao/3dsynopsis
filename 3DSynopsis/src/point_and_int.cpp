#include "point_and_int.h"


namespace MyKdTree
{
	Point_and_int_property_map::reference
		get(Point_and_int_property_map, Point_and_int_property_map::key_type p)
	{
		return boost::get<0>(p);
	}
}
