#include <QFileDialog>
#include <QTextStream>
#include <QResizeEvent>
#include <QFile>

#include <osg/Depth>
#include <osg/Point>
#include <osg/LineWidth>
#include <osgDB/WriteFile>
#include <osg/AnimationPath>
#include <osgGA/TrackballManipulator>
#include <osgGA/DriveManipulator>
#include <QtCore/QDebug>
#include <osg/StateAttribute>
#include <osg/LightModel>
#include <osg/StateSet>
#include <osgUtil/Optimizer>


#include "light_source.h"
#include "update_visitor.h"
#include "toggle_handler.h"
#include "stateset_manipulator.h"
#include "osg_viewer_widget.h"
#include "MotionBlurOperation.h"



OSGViewerWidget::OSGViewerWidget(QWidget * parent, const QGLWidget * shareWidget, Qt::WindowFlags f)
  :AdapterWidget(parent, shareWidget, f, true),
  scene_root_(new osg::Group),
  other_root_(new osg::Group),
  gl_thread_(this),
  threaded_painter_(this),
  pickup_root_( new osg::Group() ),
  para_root_(new osg::Group()),
  dragger_root_(new osg::Group())
{
  m_root = new osg::Group;
  m_root->addChild(scene_root_);
  m_root->addChild(other_root_);
  setSceneData(m_root);

  pickup_root_->setName("pickup_root");
  addChild( pickup_root_, false);

  para_root_->setName("para_root");
  addChild( para_root_, false);

  dragger_root_->setName("dragger_root");
  addChild( dragger_root_,false );

  scene_root_->getOrCreateStateSet()->setAttribute(new osg::Point(4.0f), osg::StateAttribute::ON);
  scene_root_->getOrCreateStateSet()->setAttribute(new osg::LineWidth(2.0f), osg::StateAttribute::ON);

  m_pSynopsisManipulator = new SynopsisManipulator;
  setCameraManipulator(m_pSynopsisManipulator);

  addEventHandler(new osgViewer::HelpHandler);
  addEventHandler(new osgViewer::StatsHandler);
  addEventHandler(new osgViewer::LODScaleHandler);
  addEventHandler(new osgViewer::ThreadingHandler);


  //修改帧率
  start_frame_time = 0;
  end_frame_time = 0;

  //控制帧率使用的睡眠时间
  sleep_time = 0.0;
  last_sleep_time = 0.0;
  //每帧的实际使用时间
  current_time = 0.0;
  //计算帧率
  counts = 0;
  ///////////////////////////////////////////
  for (int i = 0; i < 6; ++ i)
  {
    osg::ref_ptr<LightSource> light_source = new LightSource(i, scene_bounding_sphere_);
    light_sources_.push_back(light_source);
    char index = '0'+i;
    addEventHandler(new ToggleHandler(light_source, index, std::string("Toggle Light Source ")+index));
    addChild(light_source, false);
  }

  double w = width();
  double h = height();
  getCamera()->setViewport(new osg::Viewport(0, 0, w, h));
  getCamera()->setProjectionMatrixAsPerspective(60.0f, w/h, 1.0f, 10000.0f);
  getCamera()->setGraphicsContext(getOrganGraphicsWindow());
  getCamera()->setClearColor(osg::Vec4(1, 1, 1, 1.0));

  setThreadingModel(osgViewer::Viewer::SingleThreaded);

  //add by hisong
  /*osg::DisplaySettings::instance()->setMinimumNumAccumBits(8, 8, 8, 8);
  osgViewer::Viewer::Windows windows;
  getWindows(windows);
  for (osgViewer::Viewer::Windows::iterator itr = windows.begin(); itr != windows.end(); ++itr)
  {
	  (*itr)->add(new MotionBlurOperation(0.05));//persistence
  }*/
  ///
  this->doneCurrent();
}

OSGViewerWidget::~OSGViewerWidget()
{
  osgViewer::View::EventHandlers handlers = getEventHandlers();
  for (osgViewer::View::EventHandlers::iterator it = handlers.begin(); it != handlers.end(); ++ it)
    this->removeEventHandler(*it);

  stopRendering();
}

void OSGViewerWidget::increaseLineWidth(void)
{
  osg::StateSet* state_Set = scene_root_->getOrCreateStateSet();
  osg::LineWidth* line_width = dynamic_cast<osg::LineWidth*>(state_Set->getAttribute(osg::StateAttribute::LINEWIDTH));
  if (line_width == NULL)
    return;

  if (line_width->getWidth() >= 16.0)
  {
    line_width->setWidth(16.0);
    return;
  }

  line_width->setWidth(line_width->getWidth()+1.0);
  return;
}

void OSGViewerWidget::decreaseLineWidth(void)
{
  osg::StateSet* state_Set = scene_root_->getOrCreateStateSet();
  osg::LineWidth* line_width = dynamic_cast<osg::LineWidth*>(state_Set->getAttribute(osg::StateAttribute::LINEWIDTH));
  if (line_width == NULL)
    return;

  if (line_width->getWidth() <= 1.0)
  {
    line_width->setWidth(1.0);
    return;
  }

  line_width->setWidth(line_width->getWidth()-1.0);
  return;
}

void OSGViewerWidget::increasePointSize(void)
{
	osg::StateSet* state_Set = scene_root_->getOrCreateStateSet();
	osg::Point* point = dynamic_cast<osg::Point*>(state_Set->getAttribute(osg::StateAttribute::POINT));


	if (point == NULL)
		return;

	if (point->getSize() >= 16.0)
	{
		point->setSize(16.0);
		return;
	}

	point->setSize(point->getSize()+1.0);

  return;
}

void OSGViewerWidget::decreasePointSize(void)
{
  osg::StateSet* state_Set = scene_root_->getOrCreateStateSet();
  osg::Point* point = dynamic_cast<osg::Point*>(state_Set->getAttribute(osg::StateAttribute::POINT));
  if (point == NULL)
    return;

  if (point->getSize() <= 1.0)
  {
    point->setSize(1.0);
    return;
  }

  point->setSize(point->getSize()-1.0);
  return;
}

void OSGViewerWidget::startRendering()
{
  addEventHandler(new StateSetManipulator(getSceneData()->getOrCreateStateSet()));

  threaded_painter_.moveToThread(&gl_thread_);
  connect(&gl_thread_, SIGNAL(started()), &threaded_painter_, SLOT(start()));
  gl_thread_.start();
}

void OSGViewerWidget::stopRendering()
{
  threaded_painter_.stop();
  gl_thread_.wait();
}

void OSGViewerWidget::paintGL(void)
{
  QMutexLocker locker(&mutex_);
  frame();

  emit frameend();
}

void OSGViewerWidget::resizeEvent(QResizeEvent *event)
{
  threaded_painter_.resizeEvent(event);
}

void OSGViewerWidget::paintEvent(QPaintEvent * /*event*/)
{
  // Handled by the GLThread.
}

void OSGViewerWidget::closeEvent(QCloseEvent *event)
{
  stopRendering();
  QGLWidget::closeEvent(event);
}

void OSGViewerWidget::centerScene(void)
{
  QMutexLocker locker(&mutex_);
  scene_root_->accept(UpdateVisitor());

  //osgGA::CameraManipulator* camera_manipulator = getCameraManipulator();

  scene_bounding_sphere_ = scene_root_->computeBound();
  double radius = scene_bounding_sphere_.radius();
  const osg::Vec3d& center = scene_bounding_sphere_.center();
  osg::Vec3d eye_offset(0.0, 2*radius, 2*radius);
  //camera_manipulator->setHomePosition(center + eye_offset, center, osg::Vec3d(0.0f,-1.0f,0.0f));
  //camera_manipulator->home(0);
   m_pSynopsisManipulator->setHomePosition(center + eye_offset, center, osg::Vec3d(0.0f,-1.0f,0.0f));
   m_pSynopsisManipulator->home(0);
 

  double offset = radius/1.5;
  //double offset = radius/3;
  std::vector<osg::Vec3> offsets;
  offsets.push_back(osg::Vec3(0, 0, -offset));
  offsets.push_back(osg::Vec3(0, 0, offset));
  offsets.push_back(osg::Vec3(-offset, 0, 0));
  offsets.push_back(osg::Vec3(offset, 0, 0));
  offsets.push_back(osg::Vec3(0, -offset, 0));
  offsets.push_back(osg::Vec3(0, offset, 0));
  for (int i = 0; i < 6; ++ i)
    light_sources_[i]->init(center + offsets[i]);
 // light_sources_[i]->init(center + offsets[i],osg::Vec4(0.0,0.0,0.0,0.0),osg::Vec4(0.0,0.0,0.0,0.0),osg::Vec4(1.0,1.0,1.0,1.0));

  return;
}

void OSGViewerWidget::addChild(osg::Node *child, bool in_scene)
{
  QMutexLocker locker(&mutex_);
  if (in_scene)
  {
    scene_root_->addChild(child);
    scene_bounding_sphere_ = scene_root_->computeBound();
  }
  else
    other_root_->addChild(child);

  return;
}

void OSGViewerWidget::removeChild(osg::Node *child, bool in_scene)
{
  QMutexLocker locker(&mutex_);
  if (in_scene)
  {
    scene_root_->removeChild(child);
    scene_bounding_sphere_ = scene_root_->computeBound();
  }
  else
    other_root_->removeChild(child);

  return;
}

void OSGViewerWidget::removeChildren(bool in_scene)
{
  QMutexLocker locker(&mutex_);
  if (in_scene)
  {
    scene_root_->removeChildren(0, scene_root_->getNumChildren());
    scene_bounding_sphere_ = scene_root_->computeBound();
  }
  else
    other_root_->removeChildren(0, other_root_->getNumChildren());

  return;
}

void OSGViewerWidget::writeCameraParameters( const QString& filename )
{
	osg::Vec3d eye, center, up;
	double distance;
	m_pSynopsisManipulator->getTransformation(eye, center, up);
	distance = m_pSynopsisManipulator->getDistance();
	QFile txt_file(filename);
	txt_file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream txt_file_stream(&txt_file);
	txt_file_stream << eye.x() << " " << eye.y() << " " << eye.z() << "\n";
	txt_file_stream << center.x() << " " << center.y() << " " << center.z() << "\n";
	txt_file_stream << up.x() << " " << up.y() << " " << up.z() << "\n";
	txt_file_stream << distance;
	


	return;
}

void OSGViewerWidget::readCameraParameters( const QString& filename )
{
	QMutexLocker locker(&mutex_);
	scene_root_->accept(UpdateVisitor());


	QFile txt_file(filename);
	//qDebug()<<filename.toStdString().c_str();
	txt_file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream txt_file_stream(&txt_file);

	osg::Vec3d eye, center, up;
	double distance;
	osg::Vec3d eyeM, centerM, upM;
	txt_file_stream >> eye.x() >> eye.y() >> eye.z();
	txt_file_stream >> center.x() >> center.y() >> center.z();
	txt_file_stream >> up.x() >> up.y() >> up.z();
	txt_file_stream >> distance;
	//txt_file_stream >> eyeM.x() >> eyeM.y() >> eyeM.z();
	//txt_file_stream >> centerM.x() >> centerM.y() >> centerM.z();
	//txt_file_stream >> upM.x() >> upM.y() >> upM.z();
	m_pSynopsisManipulator->setTransformation(eye, center, up);
	m_pSynopsisManipulator->setDistance(distance);
	m_pSynopsisManipulator->setHomePosition(eye,center,up);
	m_pSynopsisManipulator->home(0);
	//osgGA::CameraManipulator* camera_manipulator = getCameraManipulator();
	

	//camera_manipulator->setHomePosition(eyeM, centerM, upM);
	//camera_manipulator->setAutoComputeHomePosition(false);
	//camera_manipulator->home(0);

	//getCamera()->setViewMatrixAsLookAt(eye, center, up);

	//camera_manipulator->setHomePosition(e, c, u);
	//camera_manipulator->home(0);


	//add by zoomin
	scene_bounding_sphere_ = scene_root_->computeBound();
    osg::Vec3d boundingCenter = scene_bounding_sphere_.center();
	double radius = scene_bounding_sphere_.radius();

	//double offset = radius/1.5;
    double offset = radius/12;
	std::vector<osg::Vec3> offsets;
	offsets.push_back(osg::Vec3(0, 0, -offset));
	offsets.push_back(osg::Vec3(0, 0, offset));
	offsets.push_back(osg::Vec3(-offset, 0, 0));
	offsets.push_back(osg::Vec3(offset, 0, 0));
	offsets.push_back(osg::Vec3(0, -offset, 0));
	offsets.push_back(osg::Vec3(0, offset, 0));
	for (int i = 0; i < 6; ++ i)
	{
		//light_sources_[i]->init(center + offsets[i]);
		//light_sources_[i]->addCullCallback();
		light_sources_[i]->init(center + offsets[i],osg::Vec4(0.5, 0.5f, 0.5f, 1.0f),osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f),osg::Vec4(1.0,1.0,1.0,1.0));
	}
    /*osg::Light* light = new osg::Light;
	light->setLightNum(0);
	light->setAmbient(osg::Vec4(0.5, 0.5f, 0.5f, 1.0f));
	light->setDiffuse(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
	light->setPosition(osg::Vec4(boundingCenter.x(), boundingCenter.y(), boundingCenter.z(), 1.0));
	osg::LightSource* lightSource = new osg::LightSource();
	lightSource->setLight(light);
	m_root->addChild(lightSource);*/

	return;
}

//void OSGViewerWidget::keyPressEvent( QKeyEvent* event )
//{
//	switch (event->key())
//	{
//	case Qt::Key_Left:
//		m_pSynopsisManipulator->yaw(osg::DegreesToRadians(5.0));
//		break;
//	case Qt::Key_Right:
//		m_pSynopsisManipulator->yaw(osg::DegreesToRadians(-5.0));
//		break;
//	case Qt::Key_Up:
//		m_pSynopsisManipulator->pitch(osg::DegreesToRadians(5.0));
//		break;
//	case Qt::Key_Down:
//		m_pSynopsisManipulator->pitch(osg::DegreesToRadians(-5.0));
//		break;
//	case Qt::Key_Q:
//		m_pSynopsisManipulator->roll(osg::DegreesToRadians(5.0));
//		break;
//	case Qt::Key_E:
//		m_pSynopsisManipulator->roll(osg::DegreesToRadians(-5.0));
//		break;
//	default:
//		break;
//	}
//}
