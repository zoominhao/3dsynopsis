#include <QDir>
#include <QFileInfo>

#include "file_system.h"

namespace FileSystem
{
    bool exists(const std::string& filename){
        return QFileInfo(filename.c_str()).exists();
    }

    bool isRelative(const std::string& filename) {
        return QFileInfo(filename.c_str()).isRelative();
    }

    std::string base(const std::string& filename)
    {
        return QFileInfo(filename.c_str()).completeBaseName().toStdString();
    }

    std::string extension(const std::string& filename)
    {
        return QFileInfo(filename.c_str()).suffix().toStdString();
    }

    std::string path(const std::string& filename)
    {
        return QFileInfo(filename.c_str()).path().toStdString();
    }

    std::string absolute(const std::string& filename)
    {
        return QFileInfo(filename.c_str()).absoluteFilePath().toStdString();
    }

    std::string relativePath(const std::string& filename, const std::string& path)
    {
        return QDir(path.c_str()).relativeFilePath(filename.c_str()).toStdString();
    }

	std::string dir( const std::string& filename )
	{
		return QFileInfo(filename.c_str()).dir().path().toStdString();
	} 

	std::string absoluteDir( const std::string& filename )
	{
		return QFileInfo(filename.c_str()).absoluteDir().path().toStdString();
	}

	std::string parentDir( const std::string& filename )
	{
		return filename.substr(0,absoluteDir(filename).find_last_of("\\"));  
	}

	std::string curDir( const std::string& filename )
	{
		std::string fulldirname = dir(filename);
		return fulldirname.substr(fulldirname.find_last_of("/")+1,fulldirname.size()-1);  
	}

};