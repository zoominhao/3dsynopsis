#include "pickup_handler.h"
#include "osg_utility.h"
#include "plane_intersector.h"
#include "point_intersector.h"

#include <osg/LineWidth>
#include <osg/Material>
#include <osg/Point>

///////////////////////////////BasicSelectModel///////////////////////////////////////////
void BasicSelectModel::setPointAttributes( osg::Geometry* selector )
{
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(1);
	(*colors)[0] = selectedColor_;

	selector->setDataVariance( osg::Object::DYNAMIC );
	selector->setUseDisplayList( false );
	selector->setUseVertexBufferObjects( true );
	selector->setVertexArray( new osg::Vec3Array(1) );
	selector->setColorArray( colors.get() );
	selector->setColorBinding( osg::Geometry::BIND_OVERALL );
	selector->addPrimitiveSet( new osg::DrawArrays(GL_POINTS, 0, 1) );
}

void BasicSelectModel::setLineAttributes( osg::Geometry* selector )
{
	//const osg::Vec4 selectedColor(0.0f, 0.0f, 0.0f, 1.0f);
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(1);
	(*colors)[0] = selectedColor_;

	selector->setDataVariance( osg::Object::DYNAMIC );
	selector->setUseDisplayList( false );
	selector->setUseVertexBufferObjects( true );
	selector->setVertexArray( new osg::Vec3Array(2) );
	selector->setColorArray( colors.get() );
	selector->setColorBinding( osg::Geometry::BIND_OVERALL );
	selector->addPrimitiveSet( new osg::DrawArrays(GL_LINES, 0, 2) );
}

bool BasicSelectModel::setPointPosition( osg::Geometry* selector, osg::Vec3 pos )
{
	osg::Vec3Array* selVertices = 0;
	selVertices = dynamic_cast<osg::Vec3Array*>(selector->getVertexArray() );
	if( selVertices == 0 || selVertices->size() ==0 )
		return false;
	selVertices->front() = pos;
	selVertices->dirty();
	selector->dirtyBound();
	return true;
}

bool BasicSelectModel::setLinePosition( osg::Geometry* selector, osg::Vec3 start, osg::Vec3 end )
{
	osg::Vec3Array* selVertices = 0;
	selVertices = dynamic_cast<osg::Vec3Array*>(selector->getVertexArray() );
	if( selVertices == 0 || selVertices->size() <=0 )
		return false;
	selVertices->front() = start;
	selVertices->at(1) = end;
	selVertices->dirty();
	selector->dirtyBound();
	return true;
}

void BasicSelectModel::setLineAttributes2( osg::Geometry* selector )
{
	const osg::Vec4 selectedColor(1.0f, 0.0f, 0.0f, 1.0f);
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array(1);
	(*colors)[0] = selectedColor;
	osg::LineWidth* lw = new osg::LineWidth( 4.0 );
	selector->getOrCreateStateSet()->setAttributeAndModes( lw,
		osg::StateAttribute::ON );
	selector->setDataVariance( osg::Object::DYNAMIC );
	selector->setUseDisplayList( false );
	selector->setUseVertexBufferObjects( true );
	selector->setVertexArray( new osg::Vec3Array() );
	selector->setColorArray( colors.get() );
	selector->setColorBinding( osg::Geometry::BIND_OVERALL );
	selector->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, 1));
}

void BasicSelectModel::addANode( osg::Node* node )
{
	osg::StateSet* state = node->getOrCreateStateSet();
	osg::Material* mtrl = dynamic_cast<osg::Material*>( state->getAttribute( osg::StateAttribute::MATERIAL));
	if( !mtrl )
	{
		mtrl = new osg::Material;
	}
	osg::Vec4 red(1,0,0,0.8);
	mtrl->setDiffuse( osg::Material::FRONT_AND_BACK, red);
	mtrl->setAmbient( osg::Material::FRONT_AND_BACK, red);
	//	mtrl->setTransparency( osg::Material::FRONT_AND_BACK, 0.2);
	state->setAttributeAndModes( mtrl, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);

	pickup_root_->addChild( node );
}
///////////////////////////////BasicSelectModel///////////////////////////////////////////

///////////////////////////////SelectScenePosHandler///////////////////////////////////////////
SelectScenePosHandler::SelectScenePosHandler( osg::Group* root, osg::Vec3* cur_origin )
	:BasicSelectModel(root)
	,_origin(cur_origin)
{

}

bool SelectScenePosHandler::handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa )
{
	switch (ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::PUSH):
		{
			if( (ea.getModKeyMask() & osgGA::GUIEventAdapter::MODKEY_SHIFT ) )
			{
				osgViewer::View* viewer = dynamic_cast<osgViewer::View*>(&aa);
				if ( viewer )
				{
					osg::ref_ptr<PlaneIntersector2> intersector =
						new PlaneIntersector2(osgUtil::Intersector::WINDOW, ea.getX(), ea.getY());
					osgUtil::IntersectionVisitor iv( intersector.get() );
					viewer->getCamera()->accept( iv );

					if ( intersector->containsIntersections() )
					{
						PlaneIntersector2::Intersections::iterator hitr = intersector->getIntersections().begin();
						*_origin = hitr->getWorldIntersectPoint();
						pickup_root_->removeChild( 0, pickup_root_->getNumChildren() );
						pickup_root_->addChild( OSGUtility::drawSphere( *_origin, 2, osg::Vec4(0.8,0.1,0,1)));
						std::cout<<"Point: ("<<_origin->x()<<","<<_origin->y()<<","<<_origin->z()<<")"<<std::endl;
					}
				}
			}     
		}
		break;
	}

	return false;
}

void SelectScenePosHandler::createSelector()
{
	pickup_root_->addChild( OSGUtility::drawSphere( *_origin, 0.1, osg::Vec4(1,0,0,1)));
}
///////////////////////////////SelectScenePosHandler///////////////////////////////////////////

///////////////////////////////SelectPointHandler///////////////////////////////////////////
SelectPointHandler::SelectPointHandler( osg::Group* root, osg::ref_ptr<SyScene> scene )
	: BasicSelectModel( root )
	, _scene(scene)
	,_visible(true)
	,_point_id(0)
{
	createSelector();
}

void SelectPointHandler::createSelector()
{
	_point_selector = new osg::Geometry;
	setSelectedColor(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f));
	setPointAttributes( _point_selector.get() );
	osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	geode->addDrawable( _point_selector.get() );
	geode->getOrCreateStateSet()->setAttributeAndModes( new osg::Point(20.0f) );
	geode->getOrCreateStateSet()->setMode( GL_LIGHTING, osg::StateAttribute::OFF );

	pickup_root_->addChild( geode );
}


bool SelectPointHandler::handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa )
{
	switch (ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::PUSH):
		{
			if(ea.getModKeyMask()&osgGA::GUIEventAdapter::MODKEY_CTRL)
			{
				osgViewer::View* viewer = dynamic_cast<osgViewer::View*>(&aa);
				if( viewer == 0)
					return false;

				osg::ref_ptr<PointIntersector> intersector =
					new PointIntersector(osgUtil::Intersector::WINDOW, ea.getX(), ea.getY());
				osgUtil::IntersectionVisitor iv( intersector.get() );
				viewer->getCamera()->accept( iv );
				if( intersector->containsIntersections() == false)
				{
					return false;
				}

				PointIntersector::Intersections::iterator hitr = intersector->getIntersections().begin();
				osg::Vec3 pos = hitr->getWorldIntersectPoint();
				if ( hitr->nodePath.size() >=2 )
				{
					const osg::NodePath& np = hitr->nodePath ;
					SyScene* tps = dynamic_cast<SyScene*>(np[np.size()-2]);
					if( tps != NULL )
					{
						osg::Vec3Array* selVertices = 0;
						if( tps->getName() == "3DSCENE")
						{
							setPointPosition( _point_selector.get(), pos );
							_point_id = hitr->primitiveIndex;
							std::cout<<"POINT ID: "<<_point_id<<std::endl;
							std::cout<<"POSITION: "<<pos.x()<<","<<pos.y()<<","<<pos.z()<<std::endl;
							pickup_root_->addChild( OSGUtility::drawSphere( pos, 1, osg::Vec4(0.8,0.8,0,1)));
						}
					}
				}

			}
			break;
	case osgGA::GUIEventAdapter::KEYDOWN:
		{
			if(ea.getKey()==osgGA::GUIEventAdapter::KEY_Space)
			{
				_visible = !_visible;
				pickup_root_->setNodeMask(_visible ? 0xffffffff : 0x0);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Alt_L)
			{
				pickup_root_->removeChild( 0, pickup_root_->getNumChildren() );
			}
		}

		break;
		}
		return false;
	}
	return false;
}
///////////////////////////////SelectPointHandler///////////////////////////////////////////


///////////////////////////////SelectGeometryHandler///////////////////////////////////////////
bool SelectGeometryHandler::handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa )
{
	switch(ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::PUSH):
		{
			osgViewer::View* view = dynamic_cast<osgViewer::View*>(&aa);
			if (view)
			{
				osgUtil::LineSegmentIntersector::Intersections intersections;

				std::string gdlist="";
				float x = ea.getX();
				float y = ea.getY();
#if 0
				osg::ref_ptr< osgUtil::LineSegmentIntersector > picker = new osgUtil::LineSegmentIntersector(osgUtil::Intersector::WINDOW, x, y);
				osgUtil::IntersectionVisitor iv(picker.get());
				view->getCamera()->accept(iv);
				if (picker->containsIntersections())
				{
					intersections = picker->getIntersections();
#else
				if (view->computeIntersections(x,y,intersections))
				{
#endif
					/*for(osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
						hitr != intersections.end();
						++hitr)
					{
						if (!hitr->nodePath.empty() && !(hitr->nodePath.back()->getName().empty()))
						{
							// the geodes are identified by name.
							std::cout <<"Object \""<<hitr->nodePath.back()->getName()<<"\""<<std::endl;
						}
					}*/
					osgUtil::LineSegmentIntersector::Intersection hitr = *(intersections.begin());
					if (!hitr.nodePath.empty() && !(hitr.nodePath.back()->getName().empty()))
					{
						std::cout <<"Node ID: "<<hitr.nodePath.back()->getName()<<std::endl;
					}
				}
			}
		}    
	}
	return false;
}
///////////////////////////////SelectGeometryHandler///////////////////////////////////////////


///////////////////////////////SyKeyHandler///////////////////////////////////////////
SyKeyHandler::SyKeyHandler( osg::Group* root, SynopsisManipulator* pSynopsisManipulator )
	:m_pSynopsisManipulator(pSynopsisManipulator)
{

}

bool SyKeyHandler::handle( const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa )
{
	osgViewer::Viewer* viewer = dynamic_cast<osgViewer::Viewer*>(&aa);
	if (!viewer) return false;
	osg::Vec3d currEye, currCenter, currUp;
	m_pSynopsisManipulator->getTransformation(currEye, currCenter, currUp );
	osg::Vec3d dir(currCenter - currEye);
	dir.normalize();
	osg::Vec3d xDir(dir.x(), dir.y(), 0);
	osg::Vec3d yDir(-dir.y(), dir.x(), 0);
	osg::Vec3d zDir(0, 0, 1);
	int deta = 5;
	switch(ea.getEventType())
	{
	case(osgGA::GUIEventAdapter::KEYDOWN):
		{
			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Up)
			{
				m_pSynopsisManipulator->setTransformation(currEye +  xDir * deta, currCenter  +  xDir * deta, currUp);
			}                
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Down)
			{
				m_pSynopsisManipulator->setTransformation(currEye -  xDir * deta, currCenter  -  xDir * deta, currUp);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Left)
			{
				m_pSynopsisManipulator->setTransformation(currEye +  yDir * deta, currCenter  +  yDir * deta, currUp);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Right)
			{
				m_pSynopsisManipulator->setTransformation(currEye -  yDir * deta, currCenter  -  yDir * deta, currUp);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Page_Up)
			{
				m_pSynopsisManipulator->setTransformation(currEye +  zDir * deta, currCenter  +  zDir * deta, currUp);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_Page_Down)
			{
				m_pSynopsisManipulator->setTransformation(currEye -  zDir * deta, currCenter  -  zDir * deta, currUp);
			}

			if (ea.getKey()==osgGA::GUIEventAdapter::KEY_6)
			{
				m_pSynopsisManipulator->setTargetDeta(1, 0);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_8)
			{
				m_pSynopsisManipulator->setTargetDeta(0, 1);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_7)
			{
				m_pSynopsisManipulator->setTargetDeta(-1, 0);
			}
			else if (ea.getKey()==osgGA::GUIEventAdapter::KEY_9)
			{
				m_pSynopsisManipulator->setTargetDeta(0, -1);
			}

			return false;
		}


	default:
		return false;
	}
}
///////////////////////////////SyKeyHandler///////////////////////////////////////////


