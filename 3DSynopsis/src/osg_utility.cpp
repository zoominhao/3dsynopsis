#include <osg/Geode>
#include <osg/Point>
#include <osg/Depth>
#include <osg/Geometry>
#include <osg/LineWidth>
#include <osg/Texture2D>
#include <osg/ShapeDrawable>
#include <osg/BlendFunc>
#include <osg/BlendColor>
#include <osg/LineSegment>
#include <osg/Material>
#include <osg/ShadeModel>

#include "osg_utility.h"
#include "color_interpolation.h"
#include "global_constant.h"


namespace OSGUtility
{
  osg::Geode* drawCylinder(const osg::LineSegment& axis, double thickness, const osg::Vec4& color)
  {
    return drawCylinder(axis.start(), axis.end(), thickness, color);
  }

  osg::Geode* drawCylinder(const osg::Vec3& top, const osg::Vec3& base, double thickness, const osg::Vec4& color)
  {
    osg::Geode* geode = new osg::Geode();

	//高光

	osg::StateSet* state = geode->getOrCreateStateSet();
	osg::ref_ptr<osg::Material> mat = new osg::Material;
	mat->setDiffuse( osg::Material::FRONT,
		osg::Vec4( .2f, .9f, .9f, 1.f ) );
	mat->setSpecular( osg::Material::FRONT,
		osg::Vec4( 1.f, 1.f, 1.f, 1.f ) );
	mat->setShininess( osg::Material::FRONT, 96.f ); 

	mat->setColorMode(osg::Material::ColorMode::AMBIENT_AND_DIFFUSE);
	state->setAttribute(mat.get());


    osg::Vec3 center = (top+base)/2;
    osg::Vec3 offset = base-top;
    osg::Vec3 zAxis(0.0, 0.0, 1.0);
    osg::Vec3 rotation = zAxis^offset;
    float angle = acos((zAxis*offset)/offset.length());

    osg::Cylinder* cylinder = new osg::Cylinder(center, thickness, offset.length());
    cylinder->setRotation(osg::Quat(angle, rotation));

    osg::ShapeDrawable* drawable = new osg::ShapeDrawable(cylinder);
    drawable->setColor(color);
    geode->addDrawable(drawable);

    return geode;
  }

  osg::Geode* drawCone(const osg::LineSegment& axis, double thickness, const osg::Vec4& color)
  {
    return drawCone(axis.start(), axis.end(), thickness, color);
  }

  osg::Geode* drawCone(const osg::Vec3& top, const osg::Vec3& base, double radius, const osg::Vec4& color)
  {
    osg::Vec3 offset = base-top;
    osg::Vec3 zAxis(0.0, 0.0, 1.0);
    osg::Vec3 rotation = zAxis^offset;
    float angle = acos((zAxis*offset)/offset.length());
    osg::Cone* cone = new osg::Cone(top, radius, offset.length());
    cone->setRotation(osg::Quat(angle, rotation));

    osg::Geode* geode = new osg::Geode();
    osg::ShapeDrawable* drawable = new osg::ShapeDrawable(cone);
    drawable->setColor(color);
    geode->addDrawable(drawable);

	osg::StateSet* stateset=geode->getOrCreateStateSet();

	stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	stateset->setMode(GL_BLEND,osg::StateAttribute::ON);

	stateset->setRenderingHint( osg::StateSet::TRANSPARENT_BIN );
	osg::BlendColor *bc=new osg::BlendColor(osg::Vec4(1.0,0,0,0));
	osg::BlendFunc *bf=new osg::BlendFunc();
	stateset->setAttributeAndModes(bf,osg::StateAttribute::ON);
	stateset->setAttributeAndModes(bc,osg::StateAttribute::ON);
	bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
	bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
	bc->setConstantColor(osg::Vec4(1, 1, 1,0.8f));

    return geode;
  }

  osg::Geode* drawTriangle(const osg::Vec3& src, const osg::Vec3& dest, double radius, const osg::Vec4& color)
  {
	  osg::Vec3 offset = dest - src;
	  osg::Vec3 ndest(dest.x(), dest.y(), dest.z()+1);
	  osg::Vec3 left(-offset.x()/2 + src.x(), offset.y()/2 + src.y(), src.z()+1);
	  osg::Vec3 right(offset.x()/2 + src.x(), -offset.y()/2 + src.y(), src.z()+1);
	  osg::Vec3Array* triangle = new osg::Vec3Array;
	  triangle->push_back(left);
	  triangle->push_back(ndest);
	  triangle->push_back(right);

	  osg::Vec4Array* colors = new osg::Vec4Array;
	  osg::Vec4 transparentColor = color;
	  transparentColor.a() = 0.5f;
	  colors->push_back(transparentColor);

	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexArray(triangle);
	  geometry->setColorArray(colors);
	  geometry->setColorBinding(osg::Geometry::BIND_OVERALL);
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, triangle->size()));

	  osg::Geode* geode = new osg::Geode();
	  geode->addDrawable(geometry);

	  osg::StateSet* stateset=geode->getOrCreateStateSet();

	  stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	  stateset->setMode(GL_BLEND,osg::StateAttribute::ON);

	  stateset->setRenderingHint( osg::StateSet::TRANSPARENT_BIN );
	  osg::BlendColor *bc=new osg::BlendColor(osg::Vec4(1.0,0,0,0));
	  osg::BlendFunc *bf=new osg::BlendFunc();
	  stateset->setAttributeAndModes(bf,osg::StateAttribute::ON);
	  stateset->setAttributeAndModes(bc,osg::StateAttribute::ON);
	  bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
	  bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
	  bc->setConstantColor(osg::Vec4(1, 1, 1,0.8f));

	  return geode;
  }

  osg::Geode* drawSphere(const osg::Vec3& center, double radius, const osg::Vec4& color)
  {
    osg::Geode* geode = new osg::Geode();

    osg::Sphere* sphere = new osg::Sphere(center, radius);
    osg::ShapeDrawable* drawable = new osg::ShapeDrawable(sphere);
    drawable->setColor(color);
    geode->addDrawable(drawable);


    return geode;
  }

  osg::Geode* drawBox(const osg::Vec3& center, double width, const osg::Vec4& color)
  {
    osg::Geode* geode = new osg::Geode();

    osg::Box* box = new osg::Box(center, width);
    osg::ShapeDrawable* drawable = new osg::ShapeDrawable(box);
    drawable->setColor(color);
    geode->addDrawable(drawable);

    return geode;
  }

  osg::Geode* drawTetrahedron(const osg::Vec3& center, double radius, const osg::Vec4& color)
  {
    double offset = 2*radius/std::sqrt(3.0);
    osg::Vec3 corner_1 = center + osg::Vec3(-offset, -offset, -offset);
    osg::Vec3 corner_2 = center + osg::Vec3(+offset, +offset, -offset);
    osg::Vec3 corner_3 = center + osg::Vec3(+offset, -offset, +offset);
    osg::Vec3 corner_4 = center + osg::Vec3(-offset, +offset, +offset);

    osg::ref_ptr<osg::Vec3Array>  vertices = new osg::Vec3Array;
    osg::ref_ptr<osg::Vec4Array>  colors = new osg::Vec4Array;
    colors->push_back(color);

    vertices->push_back(corner_1); vertices->push_back(corner_2), vertices->push_back(corner_3);
    vertices->push_back(corner_1); vertices->push_back(corner_3), vertices->push_back(corner_4);
    vertices->push_back(corner_1); vertices->push_back(corner_2), vertices->push_back(corner_4);
    vertices->push_back(corner_2); vertices->push_back(corner_3), vertices->push_back(corner_4);

    osg::Geode* geode = new osg::Geode;
    osg::Geometry* geometry = new osg::Geometry;
    geometry->setUseDisplayList(true);
    geometry->setUseVertexBufferObjects(true);
    geometry->setVertexData(osg::Geometry::ArrayData(vertices, osg::Geometry::BIND_PER_PRIMITIVE));
    geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_OVERALL));
    geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, vertices->size()));
    geode->addDrawable(geometry);

    return geode;
  }

  osg::Geode* drawPolygon(osg::Vec3Array* polygon, const osg::Vec4& color)
  {
    osg::Vec4Array* colors = new osg::Vec4Array;
    osg::Vec4 transparentColor = color;
    transparentColor.a() = 0.3f;
    colors->push_back(transparentColor);

    osg::Geometry* geometry = new osg::Geometry;
    geometry->setUseDisplayList(true);
    geometry->setUseVertexBufferObjects(true);
    geometry->setVertexArray(polygon);
    geometry->setColorArray(colors);
    geometry->setColorBinding(osg::Geometry::BIND_OVERALL);
    geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POLYGON, 0, polygon->size()));

    osg::Geode* geode = new osg::Geode();
    geode->addDrawable(geometry);

    geode->setName("polygon");

    osg::StateSet* stateset=geode->getOrCreateStateSet();

	stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	//stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED );
    stateset->setMode(GL_BLEND,osg::StateAttribute::ON);
    //stateset->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);
    stateset->setRenderingHint( osg::StateSet::TRANSPARENT_BIN );

    osg::BlendColor *bc=new osg::BlendColor(osg::Vec4(1.0,0,0,0));
    osg::BlendFunc *bf=new osg::BlendFunc();
    stateset->setAttributeAndModes(bf,osg::StateAttribute::ON);
    stateset->setAttributeAndModes(bc,osg::StateAttribute::ON);
    bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
    bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
    bc->setConstantColor(osg::Vec4(1, 1, 1,0.4f)); 

    return geode;
  }

  osg::Geode* drawBBox(osg::BoundingBox* bbox, const osg::Vec4& color)
  {
    osg::CompositeShape* composite_shape = new osg::CompositeShape();
    std::vector<osg::ref_ptr<osg::LineSegment> > edges;
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMin()), osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMin())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMin()), osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMin())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMin()), osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMax())));

    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMax()), osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMax())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMax()), osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMax())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMax()), osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMin())));

    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMin()), osg::Vec3(bbox->xMin(), bbox->yMax(), bbox->zMin())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMin()), osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMin())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMin()), osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMax())));

    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMax()), osg::Vec3(bbox->xMin(), bbox->yMin(), bbox->zMax())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMax()), osg::Vec3(bbox->xMax(), bbox->yMax(), bbox->zMax())));
    edges.push_back(new osg::LineSegment(osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMax()), osg::Vec3(bbox->xMax(), bbox->yMin(), bbox->zMin())));

    double radius = 0.1;
    for (size_t i = 0, i_end = edges.size(); i < i_end; ++ i) {
      osg::Vec3 center = (edges[i]->start()+edges[i]->end())/2;
      osg::Vec3 offset = edges[i]->end()-edges[i]->start();
      osg::Vec3 zAxis(0.0, 0.0, 1.0);
      osg::Vec3 rotation = zAxis^offset;
      float angle = acos((zAxis*offset)/offset.length());
      osg::Cylinder* cylinder = new osg::Cylinder(center, radius, offset.length());
      cylinder->setRotation(osg::Quat(angle, rotation));
      composite_shape->addChild(cylinder);
    }

    osg::Geode* geode = new osg::Geode();
    osg::ShapeDrawable* drawable = new osg::ShapeDrawable(composite_shape);
    drawable->setColor(color);
    geode->addDrawable(drawable);

    return geode;
  }

  void computeNodePathToRoot(osg::Node& node, osg::NodePath& np) {
    np.clear();
    osg::NodePathList nodePaths = node.getParentalNodePaths();
    if (!nodePaths.empty()) {
      np = nodePaths.front();
    }

    return;
  }

  osg::UIntArray* generateIndices(size_t partition_size, size_t partition_index, size_t item_num, size_t item_size)
  {
    size_t start = partition_size*partition_index;
    size_t end = partition_size*(partition_index+1);
    end = (end > item_num)?item_num:end;

    osg::UIntArray* indices = new osg::UIntArray();
    indices->reserve((end-start)*item_size);
    for (size_t i = start*item_size, i_end = end*item_size; i < i_end; ++ i)
      indices->push_back(i);

    return indices;
  }

  osg::Quat computeQuat( const osg::Vec3& source_dir, const osg::Vec3 target_dir )
  {
	  osg::Vec3 rotation = source_dir^target_dir;
	  float angle = acos(( source_dir*target_dir)/source_dir.length()/target_dir.length());

	  osg::Quat quat( angle, rotation );
	  return quat;
  }

  osg::Geode* drawDisk( const osg::Vec2& center, double raduis, const osg::Vec4& color )
  {
	  osg::ref_ptr<osg::Vec3Array>  vertices = new osg::Vec3Array;
	  osg::ref_ptr<osg::Vec4Array>  colors = new osg::Vec4Array;
	  colors->push_back(color); 

	  double C_M_PI =  3.14159265358979323846f;

	  double num_sample = 128;
	  double angle_gap = 2*C_M_PI/num_sample;
	  for( int i = 0; i!= num_sample; ++i)
	  {
		  double angle = i*angle_gap;
		  osg::Vec2 v( raduis*std::cos(angle), raduis*std::sin(angle));
		  v += center;
		  vertices->push_back( osg::Vec3( v.x(),v.y(),0));
	  }

	  osg::Geode* node = new osg::Geode();
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_OVERALL));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POLYGON, 0, vertices->size()));
	  node->addDrawable(geometry);

	  return  node;

  }


  osg::Geode* drawLines(const EMatXd& V, const osg::Vec4& color , double thick)
  {
	 
	  osg::Geode* geode = new osg::Geode;
	  osg::LineWidth* osg_line = new osg::LineWidth(thick);
	  //geode->getOrCreateStateSet()->setAttributeAndModes( osg_line,
		//  osg::StateAttribute::ON );
	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  colors->push_back( color);
	  osg::Vec3Array* rendering_vertices = convertToOSGArray( V );
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_OVERALL));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, rendering_vertices->size()));
	  geode->addDrawable(geometry);



	  osg::StateSet* stateset=geode->getOrCreateStateSet();
	  stateset->setAttributeAndModes( osg_line, osg::StateAttribute::ON );
	  stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	  stateset->setMode(GL_BLEND,osg::StateAttribute::ON);
	 // stateset->setMode(GL_LINE_SMOOTH, osg::StateAttribute::ON);
	  //stateset->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);
	  stateset->setRenderingHint( osg::StateSet::TRANSPARENT_BIN );
	  osg::BlendColor *bc=new osg::BlendColor(osg::Vec4(1.0,0,0,0));
	  osg::BlendFunc *bf=new osg::BlendFunc();
	  stateset->setAttributeAndModes(bf,osg::StateAttribute::ON);
	  stateset->setAttributeAndModes(bc,osg::StateAttribute::ON);

	 

	  bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
	  bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
	  bc->setConstantColor(osg::Vec4(1, 1, 1,color.a()));

	  return geode;

  }

   osg::Geode* drawLines(const Eigen::MatrixXd& V, const osg::ref_ptr<osg::Vec4Array> colors , double thick)
   {
	   osg::Geode* geode = new osg::Geode;
	   osg::LineWidth* osg_line = new osg::LineWidth(thick);
	   geode->getOrCreateStateSet()->setAttributeAndModes( osg_line,
		   osg::StateAttribute::ON );
	  
	   osg::Vec3Array* rendering_vertices = convertToOSGArray( V );
	   osg::Geometry* geometry = new osg::Geometry;
	   geometry->setUseDisplayList(true);
	   geometry->setUseVertexBufferObjects(true);
	   geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	   geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_PER_VERTEX));
	   geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, rendering_vertices->size()));
	   geode->addDrawable(geometry);
	   return geode;
   }




  osg::Geode* drawPoints( const EMatXd& V)
  {
	  if( V.rows() == 0 )
		  return NULL;
	  osg::Vec3Array* rendering_vertices = convertToOSGArray( V );

	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  ColorInterpolation lci( 0, V.rows() );
	  for (size_t i = 0 ;i!= V.rows(); ++i)
	  {
		  osg::Vec4 color = lci.getColor( i );
		  colors->push_back( color);
	  }
	 
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_PER_VERTEX));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, rendering_vertices->size()));

	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
/*
	  osg::Point* osg_point = new osg::Point(4);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );*/


	  return geode; 
  }
 

  osg::Geode* drawPoints( const EMatXd& V, const osg::Vec4& color )
  {

	  if( V.cols() == 0 )
		  return NULL;
	  osg::Vec3Array* rendering_vertices = convertToOSGArray( V );

	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  colors->push_back( color );
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_OVERALL));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, rendering_vertices->size()));

	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
	 /* osg::Point* osg_point = new osg::Point(4);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );*/


	  return geode; 
  }

  osg::Geode* drawPoints( const EMatXd& V, const osg::ref_ptr<osg::Vec4Array>& colors)
  {
	  if( V.cols() == 0 )
		  return NULL;
	  osg::Vec3Array* rendering_vertices = convertToOSGArray( V );

	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_PER_VERTEX));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, rendering_vertices->size()));

	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
	 /* osg::Point* osg_point = new osg::Point(4);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );*/


	  return geode; 
  }
  osg::Geode* drawPoints( const EMatXd& V, const EMatXd& color)
  {
	  if( V.cols() == 0 )
		  return NULL;
	  osg::Vec3Array* rendering_vertices = convertToOSGArray( V );

	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  for( int i = 0 ;i!= color.rows(); ++i)
	  {
		osg::Vec4 color(color(i,0), color(i,1), color(i,2),color(i,3));
		colors->push_back( color );
	  }

	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_PER_VERTEX));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POINTS, 0, rendering_vertices->size()));

	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
	  /*osg::Point* osg_point = new osg::Point(4);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );*/
	  return geode; 
  }

   osg::Geode* drawFaces( const EMatXd& V, const EMatXi& F, double alpha , const EMatXd& color)
   {
	   if( V.size() == 0 || F.size() == 0)
		   return NULL;
       osg::Geometry* geometry = new osg::Geometry;
	   //开启透明
	   osg::StateSet* stateset = geometry->getOrCreateStateSet();
	   stateset->setMode( GL_BLEND,osg::StateAttribute::ON );  //Alpha混合开启
	   //stateset->setMode( GL_DEPTH_TEST, osg::StateAttribute::OFF  );  //取消深度测试
	   stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	   //stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED );


	   osg::Vec3Array* rendering_vertices = new osg::Vec3Array;
	   osg::Vec4Array* rendering_colors = new osg::Vec4Array;

	   for( int i = 0 ;i < F.rows(); ++i)
	   {
		   EV3 p1 = V.row(F(i,0));
		   rendering_vertices->push_back(osg::Vec3(p1(0), p1(1), p1(2)));
		   EV3 p2 = V.row(F(i,1));
		   rendering_vertices->push_back(osg::Vec3(p2(0), p2(1), p2(2)));
		   EV3 p3 = V.row(F(i,2));
		   rendering_vertices->push_back(osg::Vec3(p3(0), p3(1), p3(2)));   
		   if (color.rows()==F.rows())
		   {
			   rendering_colors->push_back(osg::Vec4(color(i,0),color(i,1),color(i,2),color(i,3)));
			   rendering_colors->push_back(osg::Vec4(color(i,0),color(i,1),color(i,2),color(i,3)));
			   rendering_colors->push_back(osg::Vec4(color(i,0),color(i,1),color(i,2),color(i,3)));
		   }
		   else if (color.rows()==V.rows())
		   {
			   rendering_colors->push_back(osg::Vec4(color(F(i,0),0),color(F(i,0),1),color(F(i,0),2),color(F(i,0),3)));
			   rendering_colors->push_back(osg::Vec4(color(F(i,1),0),color(F(i,1),1),color(F(i,1),2),color(F(i,1),3)));
			   rendering_colors->push_back(osg::Vec4(color(F(i,2),0),color(F(i,2),1),color(F(i,2),2),color(F(i,2),3)));
		   }

	   }

	   /*geometry->setUseDisplayList(true);
	   geometry->setUseVertexBufferObjects(true);
	   geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	   geometry->setColorData(osg::Geometry::ArrayData(rendering_colors, osg::Geometry::BIND_OVERALL));
	   geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, rendering_vertices->size()));*/


	   geometry->setColorArray(rendering_colors);
	   geometry->setVertexArray(rendering_vertices);
	   geometry->setUseDisplayList(true);
	   geometry->setUseVertexBufferObjects(true);
	   geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	   geometry->setNormalBinding( osg::Geometry::BIND_PER_PRIMITIVE);
	   geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, rendering_vertices->size()));


	   osg::Geode* geode = new osg::Geode;
	   osg::StateSet* geodeset = geode->getOrCreateStateSet();
	   geodeset->setAttribute( new osg::ShadeModel(osg::ShadeModel::SMOOTH) );
	   geode->addDrawable(geometry);

	   return geode;
   }


   osg::Geode* drawFaces( const EMatXd& V, const EMatXi& F, double alpha , const osg::Vec4 color)
   {
	   if( V.size() == 0 || F.size() == 0)
		   return NULL;
       osg::Geometry* geometry = new osg::Geometry;
	   //开启透明
	   osg::StateSet* stateset = geometry->getOrCreateStateSet();
	   stateset->setMode( GL_BLEND,osg::StateAttribute::ON );  //Alpha混合开启
	  // stateset->setMode( GL_DEPTH_TEST, osg::StateAttribute::OFF  );  //取消深度测试
	   stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF);
	   //stateset->setMode( GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED );


	   osg::Vec3Array* rendering_vertices = new osg::Vec3Array;
	   osg::Vec4Array* rendering_colors = new osg::Vec4Array;
	 
	   for( int i = 0 ;i < F.rows(); ++i)
	   {
		   EV3 p1 = V.row(F(i,0));
		   rendering_vertices->push_back(osg::Vec3(p1(0), p1(1), p1(2)));
		   rendering_colors->push_back(color);
		   EV3 p2 = V.row(F(i,1));
		   rendering_vertices->push_back(osg::Vec3(p2(0), p2(1), p2(2)));
		   rendering_colors->push_back(color);
		   EV3 p3 = V.row(F(i,2));
		   rendering_vertices->push_back(osg::Vec3(p3(0), p3(1), p3(2)));   
		   rendering_colors->push_back(color);
	   }

	   /*geometry->setUseDisplayList(true);
	   geometry->setUseVertexBufferObjects(true);
	   geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	   geometry->setColorData(osg::Geometry::ArrayData(rendering_colors, osg::Geometry::BIND_OVERALL));
	   geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, rendering_vertices->size()));*/


	   geometry->setColorArray(rendering_colors);
	   geometry->setVertexArray(rendering_vertices);
	   geometry->setUseDisplayList(true);
	   geometry->setUseVertexBufferObjects(true);
	   geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	   geometry->setNormalBinding( osg::Geometry::BIND_PER_PRIMITIVE);
	   geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, rendering_vertices->size()));


	   osg::Geode* geode = new osg::Geode;
	   osg::StateSet* geodeset = geode->getOrCreateStateSet();
	   geodeset->setAttribute( new osg::ShadeModel(osg::ShadeModel::SMOOTH) );
	   geode->addDrawable(geometry);

	   return geode;
   }


   osg::Geode* drawBBox(const EVecXd v_min, const EVecXd v_max, const osg::Vec4& color)
   {
	   osg::CompositeShape* composite_shape = new osg::CompositeShape();
	   std::vector<osg::ref_ptr<osg::LineSegment> > edges;
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_min[1], v_min[2]), osg::Vec3(v_max[0], v_min[1], v_min[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_min[1], v_min[2]), osg::Vec3(v_min[0], v_max[1], v_min[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_min[1], v_min[2]), osg::Vec3(v_min[0], v_min[1], v_max[2])));

	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_max[1], v_max[2]), osg::Vec3(v_max[0], v_max[1], v_max[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_max[1], v_max[2]), osg::Vec3(v_min[0], v_min[1], v_max[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_min[0], v_max[1], v_max[2]), osg::Vec3(v_min[0], v_max[1], v_min[2])));

	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_max[1], v_min[2]), osg::Vec3(v_min[0], v_max[1], v_min[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_max[1], v_min[2]), osg::Vec3(v_max[0], v_min[1], v_min[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_max[1], v_min[2]), osg::Vec3(v_max[0], v_max[1], v_max[2])));

	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_min[1], v_max[2]), osg::Vec3(v_min[0], v_min[1], v_max[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_min[1], v_max[2]), osg::Vec3(v_max[0], v_max[1], v_max[2])));
	   edges.push_back(new osg::LineSegment(osg::Vec3(v_max[0], v_min[1], v_max[2]), osg::Vec3(v_max[0], v_min[1], v_min[2])));

	   double radius = 0.1;
	   for (size_t i = 0, i_end = edges.size(); i < i_end; ++ i) {
		   osg::Vec3 center = (edges[i]->start()+edges[i]->end())/2;
		   osg::Vec3 offset = edges[i]->end()-edges[i]->start();
		   osg::Vec3 zAxis(0.0, 0.0, 1.0);
		   osg::Vec3 rotation = zAxis^offset;
		   float angle = acos((zAxis*offset)/offset.length());
		   osg::Cylinder* cylinder = new osg::Cylinder(center, radius, offset.length());
		   cylinder->setRotation(osg::Quat(angle, rotation));
		   composite_shape->addChild(cylinder);
	   }

	   osg::Geode* geode = new osg::Geode();
	   osg::ShapeDrawable* drawable = new osg::ShapeDrawable(composite_shape);
	   drawable->setColor(color);
	   geode->addDrawable(drawable);

	   return geode;
   }

  osg::Geode* drawNormals( const EMatXd& V, const EMatXd& N, double len, const osg::Vec4& color )
  {
	  osg::Vec3Array* rendering_vertices = new osg::Vec3Array;
	  for( int i = 0 ;i!= V.rows(); ++i)
	  {
		  osg::Vec3 p1;
		  osg::Vec3 p2;
		  switch (V.cols())
		  {
		  case 2:
			  p1 = osg::Vec3( V(i,0), V(i,1), 0);
			  p2 = p1 + osg::Vec3( N(i,0), N(i,1), 0)*len;
			  break;
		  case 1:
			  p1 = osg::Vec3( V(i,0), 0, 0);
			  p2 = p1 + osg::Vec3( N(i,0), 0, 0)*len;
			  break;
		  default:
			  p1 = osg::Vec3( V(i,0), V(i,1), V(i,2));
			  p2 = p1 + osg::Vec3( N(i,0), N(i,1), N(i,2))*len;
			  break;
		  }
		  rendering_vertices->push_back(p1);
		  rendering_vertices->push_back(p2);
	  }

	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  colors->push_back( color );
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_PER_VERTEX));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, rendering_vertices->size()));
	  
	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
	  osg::LineWidth* osg_point = new osg::LineWidth(2);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );

	  return geode;
  }

  osg::Geode* drawGraph( const EMatXd& V, const EMatXi& E, const osg::Vec4& color )
  {
	  osg::Vec3Array* rendering_vertices = new osg::Vec3Array;
	  for( int i = 0 ;i!= E.rows(); ++i)
	  {
		  osg::Vec3 p1;
		  osg::Vec3 p2;
		  int idx1 = E(i,0);
		  int idx2 = E(i,1);
		  switch (V.cols())
		  {
		  case 2:
			  p1 = osg::Vec3( V(idx1, 0), V(idx1, 1), 0);
			  p2 = osg::Vec3( V(idx2, 0), V(idx2, 1), 0);
			  break;
		  case 1:
			  p1 = osg::Vec3( V(idx1, 0), 0, 0);
			  p2 = osg::Vec3( V(idx2, 0), 0, 0);
			  break;
		  default:
			  p1 = osg::Vec3( V(idx1,0), V(idx1,1), V(idx1,2));
			  p2 = osg::Vec3( V(idx2,0), V(idx2,1), V(idx2,2));
			  break;
		  }
		  rendering_vertices->push_back(p1);
		  rendering_vertices->push_back(p2);
	  }
	   
	  osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
	  colors->push_back( color );
	  osg::Geometry* geometry = new osg::Geometry;
	  geometry->setUseDisplayList(true);
	  geometry->setUseVertexBufferObjects(true);
	  geometry->setVertexData(osg::Geometry::ArrayData(rendering_vertices, osg::Geometry::BIND_PER_VERTEX));
	  geometry->setColorData(osg::Geometry::ArrayData(colors, osg::Geometry::BIND_OVERALL));
	  geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, rendering_vertices->size()));

	  osg::Geode* geode = new osg::Geode;
	  geode->addDrawable(geometry);
	  osg::LineWidth* osg_point = new osg::LineWidth(2);
	  geode->getOrCreateStateSet()->setAttributeAndModes( osg_point,
		  osg::StateAttribute::ON );

	  return geode;
  }

  osg::Vec3Array* convertToOSGArray( const EMatXd& V )
  {
	 osg::Vec3Array* rendering_vertices = new osg::Vec3Array;
	 for( int i = 0 ;i!= V.rows(); ++i)
	 {
		 osg::Vec3 p;
		 switch (V.cols())
		 {
		 case 2:
			 p = osg::Vec3( V(i,0), V(i,1), 0);
			 break;
		 case 1:
			 p = osg::Vec3( V(i,0), 0, 0);
			 break;
		 default:
			 p = osg::Vec3( V(i,0), V(i,1), V(i,2));
			 break;
		 }
		 rendering_vertices->push_back(p);
	 }
	 return rendering_vertices;
  }

  void setupProperties( osg::ref_ptr<osg::Camera> camera, osg::ref_ptr<osgText::Text> textObject,osgText::Font* font,float size,const osg::Vec3& pos,const osg::Vec4& color )
  {
	  textObject->setFont(font);//
	  textObject->setCharacterSize(size);//字体大小
	  textObject->setColor(color);
	  textObject->setDataVariance(osg::Object::DYNAMIC);
	  textObject->setAlignment( osgText::Text::LEFT_BOTTOM ); //文字显示方向
	  textObject->setPosition(pos);
	  //textObject.setCharacterSizeMode(osgText::Text::SCREEN_COORDS);//跟随视角不断变化，离物体越远，文字越大
	  //textObject.setAutoRotateToScreen(false);//跟随视角不断变化，但离物体越远，文字越小，和现实当中像类似
	  //textObject.setBackdropType(osgText::Text::OUTLINE);//对文字进行描边
	  //textObject.setBackdropColor(osg::Vec4(1.0,0.0,0.0,1.0));//描边颜色
	  //textObject.setDrawMode(osgText::Text::TEXT | osgText::Text::BOUNDINGBOX);//添加文字边框
	  //textObject.setAxisAlignment(osgText::Text::SCREEN);//获取文字对称轴方式正对屏幕方向

	  osg::ref_ptr<osg::Geode> geode = new osg::Geode;
	  geode->addDrawable( textObject.get() );
	  camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	  camera->setProjectionMatrixAsOrtho2D(0,1280,0,1024);
	  camera->setViewMatrix(osg::Matrix::identity());
	  camera->setClearMask(GL_DEPTH_BUFFER_BIT);
	  camera->getOrCreateStateSet()->setMode(GL_LIGHTING,osg::StateAttribute::OFF);
	  camera->addChild( geode );
  }

  void createContent( osg::ref_ptr<osgText::Text> textObject,const char* string )
  {
	  int requiredSize=mbstowcs(NULL,string,0);//如果mbstowcs第一参数为NULL那么返回字符串的数目
	  wchar_t* wText=new wchar_t[requiredSize+1];
	  mbstowcs(wText,string,requiredSize+1);//由char转换成wchar类型
	  textObject->setText(wText);
	  delete wText;
  }

};