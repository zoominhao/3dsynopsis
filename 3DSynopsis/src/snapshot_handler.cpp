#include <sstream>
#include <osg/io_utils>
#include <osgDB/WriteFile>
#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>

#include "snapshot_handler.h"

WriteToFile::WriteToFile(QString& workspace, osgViewer::ScreenCaptureHandler::WriteToFile::SavePolicy savePolicy )
  :workspace_(workspace),
  osgViewer::ScreenCaptureHandler::WriteToFile("screen_shot", "png", savePolicy)
{
}

WriteToFile::~WriteToFile(void)
{

}

void WriteToFile::operator() (const osg::Image& image, const unsigned int context_id)
{
  if (_savePolicy == SEQUENTIAL_NUMBER)
  {
    if (_contextSaveCounter.size() <= context_id)
    {
      unsigned int oldSize = _contextSaveCounter.size();
      _contextSaveCounter.resize(context_id + 1);
      // Initialize all new values to 0 since context ids may not be consecutive.
      for (unsigned int i = oldSize; i <= context_id; i++)
        _contextSaveCounter[i] = 0;
    }
  }

  std::stringstream filename;
  filename << workspace_.toStdString() << "/" << _filename << "_" << context_id;

  if (_savePolicy == SEQUENTIAL_NUMBER)
    filename << "_" << _contextSaveCounter[context_id];

  filename << "." << _extension;

  osgDB::writeImageFile(image, filename.str());

  OSG_INFO<<"ScreenCaptureHandler: Taking a screen shot, saved as '"<<filename.str()<<"'"<<std::endl;

  if (_savePolicy == SEQUENTIAL_NUMBER)
  {
    _contextSaveCounter[context_id]++;
  }
}


RecordCameraPathHandler::RecordCameraPathHandler(QString& workspace)
  :workspace_(workspace),
  osgViewer::RecordCameraPathHandler()
{
}

RecordCameraPathHandler::~RecordCameraPathHandler(void)
{
}

bool RecordCameraPathHandler::handle(const osgGA::GUIEventAdapter &ea, osgGA::GUIActionAdapter &aa)
{
  _filename = workspace_.toStdString()+"/camera.path";
  return osgViewer::RecordCameraPathHandler::handle(ea, aa);
}