#include "combo_box.h"


ComboBox::ComboBox( const std::vector<QString>& labels, QWidget * parent /*= 0*/ )
	:QComboBox( parent)
{
	for( size_t i = 0 ;i!= labels.size(); ++i)
	{
		addItem( QString(labels[i]));
	}
	if( labels.size() != 0 )
	{
		setCurrentIndex(0);
	}
	
}

ComboBox::~ComboBox()
{

}

