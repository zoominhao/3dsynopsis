#include <iostream>
#include <QDomElement>
#include <QDomDocument>
#include <QTextStream>
#include <QFile>

#include "parameter.h"
#include "main_window.h"
#include "parameter_widget.h"
#include "parameter_manager.h"

ParameterManager::ParameterManager(void)
  :m_trackMode( new BoolParameter("TrackMode", "TrackMode", false) )
  ,m_videoMode( new BoolParameter("VideoMode", "VideoMode",  false) )
  ,m_render_density( new IntParameter("Render_Density", "Render_Density",  3, 1, 30, 1) )
  ,m_iteration_num(new IntParameter("Iteration_Num", "Iteration_Num", 1, 1, 20, 1))
  ,m_driving_mode(new IntParameter("Mode", "Mode", 2, 1, 5, 1))
  ,m_totaltime( new DoubleParameter("TotalTime", "TotalTime",  3600, 0, 6200, 1) )
  ,m_deta( new DoubleParameter("MDeta", "MDeta",  10, 0, 30, 1) )
{
	generateAllParaWidget();
	generateRenderWidget();
	generateDrivingWidget();
}

ParameterManager::~ParameterManager(void)
{
  delete m_trackMode;
  delete m_videoMode;
  delete m_render_density;

  delete m_iteration_num;
  delete m_driving_mode;
  delete m_totaltime;
  delete m_deta;
}


void ParameterManager::generateAllParaWidget()
{
	if( all_parameters_widget_ != NULL )
	{
		all_parameters_widget_->close();
		delete all_parameters_widget_;
	}

	all_parameters_widget_ = new ParameterDockWidget("All Parameters", MainWindow::getInstance());
	//video
	all_parameters_widget_->addParameter(m_trackMode);
	all_parameters_widget_->addParameter(m_videoMode);
	all_parameters_widget_->addParameter(m_render_density);
	//driving
	all_parameters_widget_->addParameter( m_iteration_num );
	all_parameters_widget_->addParameter(m_driving_mode);
	all_parameters_widget_->addParameter(m_totaltime);
	all_parameters_widget_->addParameter(m_deta);

	all_parameters_widget_->generateModel();
	all_parameters_widget_->hide();
}


void ParameterManager::generateRenderWidget()
{
	if( render_parameters_widget_ != NULL )
	{
		render_parameters_widget_->close();
		delete render_parameters_widget_;
	}

	render_parameters_widget_ = new ParameterDockWidget("Video Parameters", MainWindow::getInstance());

	render_parameters_widget_->addParameter( m_trackMode );
	render_parameters_widget_->addParameter( m_videoMode );
	render_parameters_widget_->addParameter( m_render_density );
	render_parameters_widget_->generateModel();
	render_parameters_widget_->hide();
}

void ParameterManager::generateDrivingWidget()
{
	if( driving_parameters_widget_ != NULL )
	{
		driving_parameters_widget_->close();
		delete driving_parameters_widget_;
	}

	driving_parameters_widget_ = new ParameterDockWidget("Driving Parameters", MainWindow::getInstance());

	driving_parameters_widget_->addParameter( m_iteration_num );
	driving_parameters_widget_->addParameter( m_driving_mode );
	driving_parameters_widget_->addParameter( m_totaltime );
	driving_parameters_widget_->addParameter( m_deta );

	driving_parameters_widget_->generateModel();
	driving_parameters_widget_->hide();
}

QDockWidget* ParameterManager::getParameterWidget( int mode)
{
	QDockWidget* cur_widget = NULL;
	switch (mode)
	{
	case 0:
		cur_widget = all_parameters_widget_;
		break;
	case 1:
		cur_widget = render_parameters_widget_;
		break;
	case 2:
		cur_widget = driving_parameters_widget_;
		break;
	default:
		break;
	}
	if( cur_widget->isHidden() )
	{
		cur_widget->show();
	}
	return cur_widget;
}

bool ParameterManager::getTrackMode(void) const
{
	return *m_trackMode;
}

bool ParameterManager::getVideoMode(void) const
{
	return *m_videoMode;
}

int ParameterManager::getRenderDensity(void) const
{
	return *m_render_density;
}

//driving
int ParameterManager::getIterationNum(void) const
{
	return *m_iteration_num;
}

int ParameterManager::getDrivingMode(void) const
{
	return *m_driving_mode;
}

double ParameterManager::getTotalTime(void) const
{
	return *m_totaltime;
}

double ParameterManager::getDeta(void) const
{
	return *m_deta;
}

void ParameterManager::loadParameters(const QString& filename)
{
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return;
  }

  QDomDocument doc("parameters");
  if (!doc.setContent(&file)) {
    file.close();
    return;
  }
  QDomElement root = doc.documentElement();

  double double_value = 0.0;
  int int_value = 0;


  
  readAParameter( m_trackMode, root );
  readAParameter( m_videoMode, root );
  readAParameter( m_render_density, root );

  readAParameter( m_iteration_num, root );
  readAParameter( m_driving_mode, root );
  readAParameter( m_totaltime, root );
  readAParameter( m_deta, root );
  
  generateAllParaWidget();
  generateRenderWidget();
  generateDrivingWidget();

  return;
}

void ParameterManager::saveParameters(const QString& filename)
{
  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly)) {
    return;
  }

  QDomDocument doc("parameters");
  QDomProcessingInstruction xml_declaration = doc.createProcessingInstruction("xml", "version=\"1.0\"");
  doc.appendChild(xml_declaration);

  QDomElement root = doc.createElement(QString("parameters"));
  doc.appendChild(root);

 
  writeAParameter( m_trackMode, root, doc );
  writeAParameter( m_videoMode, root, doc);
  writeAParameter( m_render_density, root, doc);

  writeAParameter( m_iteration_num, root, doc  );
  writeAParameter( m_driving_mode, root, doc  );
  writeAParameter( m_totaltime, root, doc  );
  writeAParameter( m_deta, root, doc );


  QTextStream text_stream(&file);
  text_stream << doc.toString();
  file.close();

  return;
}


void ParameterManager::readAParameter( Parameter* paramter, QDomElement& root )
{

	QDomElement thresh = root.firstChildElement(QString(paramter->getName().c_str()));
	
	if( dynamic_cast<IntParameter*>(paramter) )
	{
		int value = thresh.attribute("value", "").toInt();
		if( value != 0 )
			paramter->setValue( value );
	}
	else if( dynamic_cast<DoubleParameter*>(paramter))
	{
		double value = thresh.attribute("value","").toDouble();
		if( value != 0.0 )
			paramter->setValue(value);
	}
	else if( dynamic_cast<BoolParameter*>(paramter))
	{
		bool value = thresh.attribute("value","").toInt();
		paramter->setValue(value);
	}
}



void ParameterManager::writeAParameter( Parameter* paramter, QDomElement& root, QDomDocument& doc )
{
	QDomElement thresh = doc.createElement(QString(paramter->getName().c_str()));
	if( dynamic_cast<IntParameter*>(paramter) )
	{
		thresh.setAttribute("value", (int)(*dynamic_cast<IntParameter*>(paramter) ));
	}
	else if( dynamic_cast<DoubleParameter*>(paramter))
	{
		thresh.setAttribute("value", (double)(*dynamic_cast<DoubleParameter*>(paramter) ));
	}
	else if (dynamic_cast<BoolParameter*>(paramter))
	{
		thresh.setAttribute("value", (bool)(*dynamic_cast<BoolParameter*>(paramter) ));
	}
	root.appendChild(thresh);
}

