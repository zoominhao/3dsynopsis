﻿#include <QDir>
#include <QLabel>
#include <QSettings>
#include <QDateTime>
#include <QFileDialog>
#include <QApplication>
#include <QPushButton>
#include <QGroupBox>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QUrl>


#include "data_set.h"
#include "check_box.h"
#include "combo_box.h"
#include "snapshot_handler.h"
#include "main_window.h"
#include "parameter_manager.h"
#include "parameter_visual.h"
#include "sy_config.h"


MainWindow::MainWindow(void)
  :workspace_(".")
  ,current_parameter_(NULL)
{
  ui_.setupUi(this);

  setCheckBox();
  setParameterBoxColor();

  MainWindowInstancer::getInstance().main_window_ = this;
  ParameterManager::getInstance();
  
  init();
  toggleParameterMaster( parameter_combo_box_->currentIndex() );

  setAcceptDrops(true);
}

MainWindow::~MainWindow()
{
  saveSettings();
  data_set_->deleteLater();
  //停止运行定时器
 if ( sysTimer_->isActive() )
	sysTimer_->stop();

  return;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  QMainWindow::closeEvent(event);

  return;
}


void MainWindow::init()
{
	initDataSet();
	setCentralWidget( data_set_ );

	//定时器
	//创建定时器
	sysTimer_ = new QTimer(this);
	//将定时器超时信号与槽(功能函数)联系起来
	
	//connect( sysTimer_, SIGNAL(timeout()), data_set_, SLOT(skyAnimation()));
	//开始运行定时器，定时时间间隔为1000ms
	sysTimer_->start(100);

	// file
	connect(ui_.actionSetWorkspace, SIGNAL(triggered()), this, SLOT(setWorkspace()));
	connect(ui_.actionLoad_Parameters, SIGNAL(triggered()), this, SLOT(loadParameters()));
	connect(ui_.actionSave_Parameters, SIGNAL(triggered()), this, SLOT(saveParameters()));

	

	connect(parameter_combo_box_, SIGNAL( activated (int) ),this, SLOT( toggleParameterMaster(int)) );

	createActionRecentWorkspaces();
	loadSettings();
	///////////////////////////////////////////////////////////////////////////////////////
	//初始驾驶状态和路径
	m_driver.init(data_set_);

	//simulator
	connect(ui_.actionReset,SIGNAL(triggered()),this,SLOT(SYReset()));
	connect(ui_.actionStart,SIGNAL(triggered()),this,SLOT(SYStart()));
	connect(ui_.actionPause,SIGNAL(triggered()),this,SLOT(SYPause()));
	connect(ui_.actionSpeedUp,SIGNAL(triggered()),this,SLOT(SYSpeedUp()));
	connect(ui_.actionSlowDown,SIGNAL(triggered()),this,SLOT(SYSlowDown()));
	//////////////////////////////////////////////////////////////////////////////////////

	//截屏录视频
	connect(ui_.actionVideo,SIGNAL(triggered()),this,SLOT(SYVideo()));

	return;
}

MainWindow* MainWindow::getInstance()
{
	if (MainWindowInstancer::getInstance().main_window_ == NULL)
		std::cout << "shit happens!" << std::endl;
	return MainWindowInstancer::getInstance().main_window_;
}


void MainWindow::openRecentWorkspace()
{
  QAction *action = qobject_cast<QAction *>(sender());
  if (action) {
    setWorkspace(action->data().toString());
  }

  return;
}

void MainWindow::createActionRecentWorkspaces()
{
  for (int i = 0; i < MaxRecentWorkspaces; ++i) {
    action_recent_workspaces_[i] = new QAction(this);
    action_recent_workspaces_[i]->setVisible(false);
    connect(action_recent_workspaces_[i], SIGNAL(triggered()), this, SLOT(openRecentWorkspace()));
  }

  for (size_t i = 0; i < MaxRecentWorkspaces; ++i) {
    ui_.menuRecentWorkspaces->addAction(action_recent_workspaces_[i]);
  }

  return;
}

void MainWindow::updateCurrentFile(const QString& filename)
{
  QFileInfo fileInfo(filename);
  setWindowTitle(tr("%1[*] - %2").arg(fileInfo.fileName()).arg(tr("RMS")));

  return;
}

void MainWindow::updateRecentWorkspaces()
{
  QMutableStringListIterator it(recent_workspaces_);
  while (it.hasNext()) {
    if (!QFile::exists(it.next()))
      it.remove();
  }

  recent_workspaces_.removeDuplicates();
  int num = (std::min)(MaxRecentWorkspaces, recent_workspaces_.size());
  for (int i = 0; i < num; ++i) {
    QString text = tr("&%1 %2").arg(i + 1).arg(recent_workspaces_[i]);
    action_recent_workspaces_[i]->setText(text);
    action_recent_workspaces_[i]->setData(recent_workspaces_[i]);
    action_recent_workspaces_[i]->setVisible(true);
  }

  for (int i = num; i < MaxRecentWorkspaces; ++ i) {
    action_recent_workspaces_[i]->setVisible(false);
  }

  while (recent_workspaces_.size() > num) {
    recent_workspaces_.removeAt(num);
  }

  return;
}

bool MainWindow::setWorkspace(void)
{
  QString directory = QFileDialog::getExistingDirectory(this,
    tr("Set Workspace"), workspace_,
    QFileDialog::ShowDirsOnly);

  if (directory.isEmpty())
    return false;

  data_set_->reinit();
  
   setWorkspace(directory);

   return true;
}



void MainWindow::setWorkspace(const QString& workspace)
{
  workspace_ = workspace;
  recent_workspaces_.removeAll(workspace);
  recent_workspaces_.prepend(workspace);

  saveSettings();
 
  data_set_->load(workspace);

  sendCheckBoxRenderState();

  return;
}

void MainWindow::loadSettings()
{
  QSettings settings("3DSynposis", "3DSynposis");

  recent_workspaces_ = settings.value("recentWorkspaces").toStringList();

  updateRecentWorkspaces();

  workspace_ = settings.value("workspace").toString();

  setWorkspace(workspace_);

  return;
}

void MainWindow::saveSettings()
{
  QSettings settings("3DSynposis", "3DSynposis");
  settings.setValue("recentWorkspaces", recent_workspaces_);
  QString workspace(workspace_);
  settings.setValue("workspace", workspace);

  return;
}


void MainWindow::initDataSet()
{
  data_set_ = new DataSet(this);
  data_set_->startRendering();
  data_set_->addEventHandler(new osgViewer::ScreenCaptureHandler(new WriteToFile(workspace_)));
  data_set_->addEventHandler(new RecordCameraPathHandler(workspace_));
  //获取速度仪表
  data_set_->setMeterCamera(m_driver.createMeter());  
  ////////////////////////////////

  // signal
  connect(this, SIGNAL(checkBoxRenderStateChanged(const std::vector<bool>&)), data_set_, SLOT(toggleRender(const std::vector<bool>&)));
  connect(data_set_, SIGNAL(renderStateChanged(const std::vector<bool>&)), this, SLOT(updateCheckBoxRenderState(const std::vector<bool>&)));

  // camera
  connect(ui_.actionLoad_Camera, SIGNAL(triggered()), data_set_, SLOT(loadCameraParameters()));
  connect(ui_.actionSave_Camera, SIGNAL(triggered()), data_set_, SLOT(saveCameraParameters()));
  connect(ui_.actionSnap, SIGNAL(triggered()), data_set_, SLOT(snap()));
  //video
  connect(ui_.actionOptFlow,SIGNAL(triggered()),data_set_,SLOT(SYOptFlow()));
  connect(ui_.actionOptAcc,SIGNAL(triggered()),data_set_,SLOT(SYOptAcc()));
  connect(ui_.actionOptVideo,SIGNAL(triggered()),data_set_,SLOT(SYGenOptVideo()));
  connect(ui_.actionGenMap,SIGNAL(triggered()), data_set_,SLOT(SYGenMap()));
  connect(ui_.actionViewCmp,SIGNAL(triggered()), data_set_,SLOT(SYViewCmp()));
  //zoom
  connect(ui_.actionZoomIn,SIGNAL(triggered()), data_set_, SLOT(ZoominPoints()));
  connect(ui_.actionZoomOut,SIGNAL(triggered()), data_set_, SLOT(ZoomoutPoints()));
  //testtool
  connect(ui_.actionTesttool,SIGNAL(triggered()),data_set_,SLOT(SYTest()));
  //precompute
  connect(ui_.actionSample,SIGNAL(triggered()),data_set_,SLOT(SYSample()));
  connect(ui_.actionConstant,SIGNAL(triggered()),data_set_,SLOT(SYConstant()));
  connect(ui_.actionSnaps,SIGNAL(triggered()),data_set_,SLOT(SYSnaps()));
  connect(ui_.actionImagePro,SIGNAL(triggered()),data_set_,SLOT(SYImgPro()));
  connect(ui_.actionSpeed,SIGNAL(triggered()),data_set_,SLOT(SYSpeed()));
  connect(ui_.actionSmooth,SIGNAL(triggered()),data_set_,SLOT(SYSmooth()));
  connect(ui_.actionView,SIGNAL(triggered()),data_set_,SLOT(SYView()));
  connect(ui_.actionCmpImp,SIGNAL(triggered()),data_set_,SLOT(SYCmp()));

  return;
}



void MainWindow::sendCheckBoxRenderState(void)
{
	int nState = render_states_.size();
	bool isShow = render_states_[nState-1]->isChecked();


	std::vector<bool> state;
	if( !isShow )
	{
		for( size_t i = 0 ; i != nState-2; ++i)
		{
			state.push_back( render_states_[i]->isChecked() );
		}
	}
	else
	{
		for( size_t i = 0; i != nState-2; ++i )
		{
			render_states_[i]->setChecked(false);
			state.push_back( render_states_[i]->isChecked() );
		}
	}
	m_driver.toggleRenderMeter(render_states_[nState-2]->isChecked());
	emit checkBoxRenderStateChanged( state );
	return;
}

void MainWindow::updateCheckBoxRenderState( const std::vector<bool>& state )
{
	for( size_t i = 0 ; i!= state.size(); ++i)
	{
		render_states_[i]->setChecked( state[i]);
	}
	sendCheckBoxRenderState();
}

void MainWindow::setCheckBox()
{
	//CheckBox
	std::vector<std::pair<QString,bool>> render_names;
	//sceneModel
	render_names.push_back( std::make_pair("Scene", true) );  //0
	render_names.push_back( std::make_pair("Color Scene", false) );  //1
	render_names.push_back( std::make_pair("Path", true) );  //2
	render_names.push_back( std::make_pair("CoolPath", false) );  //3
	render_names.push_back( std::make_pair("SmoothPath", false) );  //4
	render_names.push_back( std::make_pair("PathCor", false) );  //5
	//car model: porsche  camera model
	render_names.push_back( std::make_pair("Porsche", true) );  //6
	render_names.push_back( std::make_pair("DCCam", true) );  //7
	render_names.push_back( std::make_pair("VisulPorsche", false) );  //8
	render_names.push_back( std::make_pair("Camera", false) );  //9
	render_names.push_back( std::make_pair("CameraPath", false) );  //10
	render_names.push_back( std::make_pair("CameraCor", false) );  //11
	//sky
	render_names.push_back( std::make_pair("Sky", true) );  //12
	render_names.push_back( std::make_pair("ColorSky", false));  //13
	render_names.push_back( std::make_pair("Water", true));  //14
	render_names.push_back( std::make_pair("Vehicle", true));  //15
	//other
	render_names.push_back( std::make_pair("Para",true) ); //16
	render_names.push_back( std::make_pair("Camera",false) ); //17
	render_names.push_back( std::make_pair("Meter",false) ); //18
	render_names.push_back( std::make_pair("Non",false)); //19
	

	for( int i = 0 ;i!= render_names.size(); ++i)
	{
		CheckBox* render(new CheckBox(render_names[i].first, this));
		render->setChecked( render_names[i].second );
		render_states_.push_back( render );
		connect(render,SIGNAL(toggled(bool)), this, SLOT(sendCheckBoxRenderState(void)));
	}

	QGroupBox* d3_group= new QGroupBox(tr(""));
	QVBoxLayout *d3_vbox = new QVBoxLayout;
	for(int i = 0; i < 6; i++ ){
		d3_vbox->addWidget(render_states_[i]);
	}
	d3_group->setLayout(d3_vbox);

	QGroupBox* car_group= new QGroupBox(tr(""));
	QVBoxLayout *car_vbox = new QVBoxLayout;
	for(int i = 6; i < 12; i++ ){
		car_vbox->addWidget(render_states_[i]);
	}
	car_group->setLayout(car_vbox);

	QGroupBox* sky_group= new QGroupBox(tr(""));
	QVBoxLayout *sky_vbox = new QVBoxLayout;
	for(int i = 12; i < 16; i++ ){
		sky_vbox->addWidget(render_states_[i]);
	}
	sky_group->setLayout(sky_vbox);

	QGroupBox* other_group= new QGroupBox(tr(""));
	QVBoxLayout *other_vbox = new QVBoxLayout;
	for(int i = 16; i < 20; i++ ){
		other_vbox->addWidget(render_states_[i]);
	}
	other_group->setLayout(other_vbox);

	ui_.renderToolBar->addWidget(d3_group);
	ui_.renderToolBar->addWidget(car_group);
	ui_.renderToolBar->addWidget(sky_group);
	ui_.renderToolBar->addWidget(other_group);
	
}



void MainWindow::setParameterBoxColor()
{
	// Parameter Combo box
	std::vector<QString> parameter_names;
	parameter_names.push_back( "All Parameter");
	parameter_names.push_back( "Render");
	parameter_names.push_back( "Driving");

	parameter_combo_box_ = new ComboBox( parameter_names, this );
	ui_.mainToolBar->addWidget( parameter_combo_box_ );
	ui_.mainToolBar->addSeparator();

}



void MainWindow::toggleParameterMaster( int mode)
{
	if( current_parameter_ != NULL )
	{
		removeDockWidget( current_parameter_ );
	}
	current_parameter_ = ParameterManager::getInstance().getParameterWidget( mode );
	if( current_parameter_ != NULL )
	{
		addDockWidget(Qt::RightDockWidgetArea, current_parameter_);
	}
	connect(current_parameter_, SIGNAL(ModelDataChanged()), this, SLOT(changeParaState()));
}


void MainWindow::loadParameters()
{
	MainWindow* main_window = MainWindow::getInstance();
	QString filename = QFileDialog::getOpenFileName(main_window, "Load Parameters", main_window->getWorkspace(), "Parameters (*.xml)");
	if (filename.isEmpty())
		return;

	ParameterManager::getInstance().loadParameters(filename);

	current_parameter_ = ParameterManager::getInstance().getParameterWidget( parameter_combo_box_->currentIndex() );
	if( current_parameter_ != NULL )
	{
		addDockWidget(Qt::RightDockWidgetArea, current_parameter_);
	}
	connect(current_parameter_, SIGNAL(ModelDataChanged()), this, SLOT(changeParaState()));

}


void MainWindow::saveParameters(void)
{
	MainWindow* main_window = MainWindow::getInstance();
	QString filename = QFileDialog::getSaveFileName(main_window, "Save Parameters", main_window->getWorkspace(), "Parameters (*.xml)");
	if (filename.isEmpty())
		return;

	ParameterManager::getInstance().saveParameters(filename);

	return;
}

bool MainWindow::changeParaState()
{

	bool addVisualSphere = false;

	ParameterManager& paraMgr = ParameterManager::getInstance();

	//double raduis = paraMgr.getMeanshiftRadius();
	/*int shapeSampleNum = ParameterManager::getInstance().getShapeSampleNum();
	int sklSampleNum = ParameterManager::getInstance().getSKlSampleNum();
	int sklDownsampleNum = ParameterManager::getInstance().getSklDownSampleNum();
	double errorThreshold = ParameterManager::getInstance().getErrorThreshold();*/

	return addVisualSphere;
}

// 拖动进入事件
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    // 数据中是否包含URL，如果是则接收动作，否则忽略该事件
    if(event->mimeData()->hasUrls())
        event->acceptProposedAction();
    else event->ignore();
}

// 放下事件
void MainWindow::dropEvent(QDropEvent *event)
{
    // 获取MIME数据
    const QMimeData *mimeData = event->mimeData();
    // 如果数据中包含URL
    if(mimeData->hasUrls()){

        // 获取URL列表
        QList<QUrl> urlList = mimeData->urls();
        // 将其中第一个URL表示为本地文件路径
        QString fileName = urlList.at(0).toLocalFile();
    }
}

void MainWindow::SYReset( void )
{
	std::cout<<"reset"<<std::endl;
	m_driver.SYControl(CONTROL_RESET);
	//data_set_->resetDrive();
}

void MainWindow::SYStart( void )
{
	std::cout<<"start"<<std::endl;
	m_driver.SYControl(CONTROL_START);
	//data_set_->startDrive();
}

void MainWindow::SYPause( void )
{
	std::cout<<"pause"<<std::endl;
	m_driver.SYControl(CONTROL_PAUSE);
	//data_set_->pauseDrive();
}

void MainWindow::SYSpeedUp( void )
{
	std::cout<<"speed up"<<std::endl;
	m_driver.SYControl(CONTROL_SPEEDUP);
}

void MainWindow::SYSlowDown( void )
{
	std::cout<<"speed up"<<std::endl;
	m_driver.SYControl(CONTROL_SLOWDOWN);
}

void MainWindow::SYVideo( void )
{
	std::cout<<"video"<<std::endl;
	m_driver.snapVideos();
}



