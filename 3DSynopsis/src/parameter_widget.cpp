#include <cassert>
#include <fstream>

#include <QGridLayout>
#include <QTableView>
#include <QHeaderView>
#include <QPushButton>
#include <QAbstractItemView>

#include "parameter_widget.h"
#include "parameter.h"



//////////////////////////////////////////////////////////////////////////////////////////////
Parameter* ParameterDelegate::getCurrentParameter(const QModelIndex &index) const
{
	std::map<std::string, Parameter*>::iterator currentParameter = parameter_map_.begin();

	size_t currentRow = 0;
	while(currentRow < index.row() && currentParameter != parameter_map_.end()) {
		++ currentParameter;
		++ currentRow;
	}

	assert(currentParameter != parameter_map_.end());

	return currentParameter->second;
}

//////////////////////////////////////////////////////////////////////////////////////////////
ParameterDelegate::ParameterDelegate(std::map<std::string, Parameter*>& parameterMap, QObject *parent):
	QStyledItemDelegate(parent),
	parameter_map_(parameterMap)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////
QWidget *
	ParameterDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
	return getCurrentParameter(index)->createEditor(parent);
}

//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	getCurrentParameter(index)->setEditorData(editor);
}

//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	getCurrentParameter(index)->setModelData(editor, model, index);
}

//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}

//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
	option->displayAlignment |= Qt::AlignHCenter;
	QStyledItemDelegate::initStyleOption(option, index);
}






//////////////////////////////////////////////////////////////////////////////////////////////
ParameterDockWidget::ParameterDockWidget(const std::string& title, QWidget* parent) 
	: QDockWidget(parent)
	, parameter_model_(NULL)
{
	setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	setWindowTitle(QString(title.c_str()));
	
}
//////////////////////////////////////////////////////////////////////////////////////////////

void ParameterDockWidget::updateModelData(void)
{
	emit ModelDataChanged();
}
//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDockWidget::addParameter(Parameter* parameter)
{
	if (name_parameter_map_.find(parameter->getName()) == name_parameter_map_.end())
	{
		name_parameter_map_.insert(std::make_pair(parameter->getName(), parameter));
	}
	else
	{
		assert(false);
	}
	return;
}

//////////////////////////////////////////////////////////////////////////////////////////////
void
	ParameterDockWidget::removeParameter(Parameter* parameter)
{
	std::map<std::string, Parameter*>::iterator name_parameter_map_it = name_parameter_map_.find(parameter->getName());
	if (name_parameter_map_it == name_parameter_map_.end())
	{
		assert(false);
	}
	else
	{
		name_parameter_map_.erase(name_parameter_map_it);
	}
	return;
}


//////////////////////////////////////////////////////////////////////////////////////////////
void ParameterDockWidget::generateModel()
{
  parameter_model_ = new ParameterModel( int( name_parameter_map_.size()), 2, this);

  QStringList headerLabels;
  headerLabels.push_back("Variable Name");
  headerLabels.push_back("Variable Value");
  parameter_model_->setHorizontalHeaderLabels(headerLabels);

  QTableView* tableView = new QTableView;
  tableView->setModel(parameter_model_);

  size_t currentRow = 0;
  for(std::map<std::string, Parameter*>::iterator it = name_parameter_map_.begin();
    it != name_parameter_map_.end();
    ++ it)
  {
    QModelIndex name = parameter_model_->index(int (currentRow), 0, QModelIndex());
    parameter_model_->setData(name, QVariant(it->first.c_str()));

    QModelIndex value = parameter_model_->index(int (currentRow), 1, QModelIndex());
    std::pair<QVariant, int> model_data = it->second->toModelData();
    parameter_model_->setData(value, model_data.first, model_data.second);
    currentRow ++;
  }

  ParameterDelegate* parameterDelegate = new ParameterDelegate(name_parameter_map_);
  tableView->setItemDelegate(parameterDelegate);

  tableView->horizontalHeader()->setStretchLastSection(true);
  tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
  tableView->setShowGrid(true);
  tableView->verticalHeader()->hide();
  tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  tableView->resizeColumnsToContents();

  int totlen = tableView->columnWidth(0) + tableView->columnWidth(1);
  setMinimumWidth(totlen);

//   QHBoxLayout* gridLayout = new QHBoxLayout();
//   gridLayout->addWidget(tableView);
//   QWidget* widget = new QWidget();
//   widget->setLayout( gridLayout );
//   setWidget( widget);

  setWidget( tableView );
  connect(parameter_model_,SIGNAL(itemChanged(QStandardItem*)), this, SLOT(updateModelData(void)));


}