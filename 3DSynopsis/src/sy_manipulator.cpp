#include "sy_manipulator.h"

#include <osg/Quat>
#include <iostream>
#include <qmath.h>

using namespace osg;

/// Constructor.
SynopsisManipulator::SynopsisManipulator()
{
	/*double nheading,npitch,nroll;
	QuatToHPR(_rotation, nheading,npitch,nroll);*/
	m_tilt = 0.0;
	m_heading = 0.0;
	setTargetPosition( 0.0, 0.0 );
}

void SynopsisManipulator::setTargetPosition( const osg::Vec3d target )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	setTransformation(currEye, currEye+target, currUp);
}

void SynopsisManipulator::setCenterPosition( const osg::Vec3d target )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	setTransformation(currEye, target, currUp);
}

//************************************
// Method:    setTargetDeta
// FullName:  SynopsisManipulator::setTargetDeta
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: const float detaHeading   0~360   初始为指向y轴正向，逆时针旋转 
// Parameter: const float detaTilt    -90~90   初始化为平视   -90 俯视   0 平视    90仰视
//************************************
void SynopsisManipulator::setTargetDeta( const float detaHeading, const float detaTilt )
{
	//理论上是得到当前角度的基础上累加，此处用成员变量作为累加器,有跃变的情况
	//double nheading,npitch,nroll;
	//QuatToHPR(_rotation, nheading,npitch,nroll);
	float headingR, tiltR;  //以弧度表示

	if (((m_tilt + detaTilt) >= 0.0f && (m_tilt + detaTilt) <= 180.0f)) {
		m_heading += detaHeading;
		m_tilt += detaTilt;
		if (m_heading >= 360.0f) {
			m_heading -= 360.f;
		}
		if (m_heading <= 0.0f) {
			m_heading += 360.f;
		}
		headingR = osg::DegreesToRadians(m_heading);
		tiltR = osg::DegreesToRadians(m_tilt);
		float x, y, z;
		x = 2.0 * qSin(tiltR) * qCos(headingR);
		y = 2.0 * qSin(tiltR) * qSin(headingR);
		z = 2.0 * qCos(tiltR);
		osg::Vec3d curEye, curCenter, curUp;
		getTransformation(curEye, curCenter, curUp );
		setTransformation(curEye, curEye+osg::Vec3d(x,y,z), osg::Vec3d(0, 0, 1));
		//getCamera()->setViewMatrixAsLookAt(osg::Vec3d(5, 5, 5), osg::Vec3d( 5 + x, 5 + y, 5 + z), osg::Vec3d(0, 0, 1));
	}
	double nheading,npitch,nroll;
	QuatToHPR(_rotation, nheading,npitch,nroll);
	//std::cout<<osg::RadiansToDegrees(nheading)<<" "<<osg::RadiansToDegrees(npitch)<<" "<<osg::RadiansToDegrees(nroll)<<std::endl;
}


//************************************
// Method:    setTargetPosition
// FullName:  SynopsisManipulator::setTargetPosition
// Access:    virtual public 
// Returns:   void
// Qualifier:
// Parameter: const float targetHeading   0~360   初始为指向y轴正向，逆时针旋转 
// Parameter: const float targetTilt      -90~90   初始化为平视   -90 俯视   0 平视    90仰视
//************************************
void SynopsisManipulator::setTargetPosition( const float targetHeading, const float targetTilt )
{
	float headingR, tiltR;  //以弧度表示
	float headingD, tiltD;  //以度数表示
	headingD = 90.0 + targetHeading;
	tiltD = 90.0 - targetTilt;
	if ((tiltD >= 0.0f && tiltD <= 180.0f)) {
		if (headingD >= 360.0f) {
			headingD -= 360.f;
		}
		if (headingD <= 0.0f) {
			headingD += 360.f;
		}
		m_heading = headingD;
		m_tilt = tiltD;
		headingR = osg::DegreesToRadians(headingD);
		tiltR = osg::DegreesToRadians(tiltD);
		float x, y, z;
		x = 2.0 * qSin(tiltR) * qCos(headingR);
		y = 2.0 * qSin(tiltR) * qSin(headingR);
		z = 2.0 * qCos(tiltR);
		osg::Vec3d curEye, curCenter, curUp;
		getTransformation(curEye, curCenter, curUp );
		setTransformation(curEye, curEye+osg::Vec3d(x,y,z), osg::Vec3d(0, 0, 1));
     }
}

void SynopsisManipulator::setAltitude( const double naltitude )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	setTransformation(osg::Vec3(currEye.x(),currEye.y(),naltitude), osg::Vec3(currCenter.x(),currCenter.y(),naltitude-currEye.z()+currCenter.z()), currUp);
}

double SynopsisManipulator::getAltitude( )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	return currEye.z();
}

void SynopsisManipulator::QuatToHPR(const osg::Quat& q, double& heading, double& pitch, double& roll)
{
	osg::Vec3d Y1 = q * osg::Vec3d(0.0,1.0,0.0);

	// 方向向量垂直，属于gambal lock，约定此时roll=0.0
	if(Y1.z()>0.9999)
	{
		heading = 2.0 * atan2(q.z(), q.w());
		pitch = osg::PI_2;
		roll = 0.0;
		return;
	}
	if(Y1.z()<-0.9999)
	{
		heading = 2.0 * atan2(q.z(), q.w());
		pitch = -osg::PI_2;
		roll = 0.0;
		return;
	}

	heading = atan2(-Y1.x(),Y1.y());                  // 和Y轴夹角，所以是atan2(-x,y)
	pitch = asin(Y1.z());

	osg::Vec3d X1 = q * osg::Vec3d(1.0,0.0,0.0);
	osg::Vec3d X2 = osg::Vec3d(Y1.y(),-Y1.x(),0.0);

	osg::Quat temp;
	temp.makeRotate(X2,X1);
	double sinhalfangle = sqrt( temp.x() * temp.x() + temp.y() * temp.y() + temp.z() * temp.z() );
	roll = 2.0 * atan2( sinhalfangle, temp.w() );
}

void SynopsisManipulator::setPosition( const float x, const float y, const float z/*=0*/ )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	setTransformation(osg::Vec3(x,y,z), osg::Vec3(currCenter.x()+(x-currEye.x()),currCenter.y()+(y-currEye.y()),currCenter.z()+(z-currEye.z())), currUp);
}

void SynopsisManipulator::setPosition( const osg::Vec3& npos )
{
	osg::Vec3d currEye, currCenter, currUp;
	getTransformation(currEye, currCenter, currUp );
	setTransformation(npos, osg::Vec3(currCenter.x()+(npos.x()-currEye.x()),currCenter.y()+(npos.y()-currEye.y()),currCenter.z()+(npos.z()-currEye.z())), currUp);
}

double SynopsisManipulator::getheading()
{
	double nheading,npitch,nroll;
	QuatToHPR(_rotation, nheading,npitch,nroll);
	return nheading;
}



