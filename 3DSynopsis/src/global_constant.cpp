#include "global_constant.h"


//model
std::string GConstant::C_SCENE_MODEL = "\\model\\scene";                      //场景模型    
std::string GConstant::C_COLOR_MODEL = "\\model\\color";                      //场景颜色模型
std::string GConstant::C_SCENE_MODEL_EXTENSION = "ive";                     //场景模型后缀
std::string GConstant::C_SCENE_SKY = "\\model\\sky\\sky.ive";
std::string GConstant::C_COLOR_SKY = "\\model\\sky\\colorsky.ive";
std::string GConstant::C_PORSCHE = "\\model\\car\\porsche.ive"; 
std::string GConstant::C_CAMERA = "\\model\\camera\\camera.ive";     
std::string GConstant::C_VIDEO_WALL = "\\model\\video";
///////////water///////////
std::string GConstant::C_SCENE_WATER = "water.obj";
std::string GConstant::C_SCENE_WATER_SZ = "hpcvwater.osg";
std::string GConstant::C_SCENE_WATER_VERT = "water.vert";
std::string GConstant::C_SCENE_WATER_FRAG = "water.frag";
std::string GConstant::C_SCENE_WATER_IMAGE_POS_X = "\\model\\water\\cubemap\\posx.bmp";
std::string GConstant::C_SCENE_WATER_IMAGE_NEG_X = "\\model\\water\\cubemap\\negx.bmp";
std::string GConstant::C_SCENE_WATER_IMAGE_POS_Y = "\\model\\water\\cubemap\\posy.bmp";
std::string GConstant::C_SCENE_WATER_IMAGE_NEG_Y = "\\model\\water\\cubemap\\negy.bmp";
std::string GConstant::C_SCENE_WATER_IMAGE_POS_Z = "\\model\\water\\cubemap\\posz.bmp";
std::string GConstant::C_SCENE_WATER_IMAGE_NEG_Z = "\\model\\water\\cubemap\\negz.bmp";

//data
std::string GConstant::C_CAMERA_OP_SVIEW = "\\data\\view\\view";
std::string GConstant::C_CAMERA_PRE = "\\data\\view\\preCord";
std::string GConstant::C_CAMERA_GOOGLE_VIEW = "\\data\\view\\gview";
std::string GConstant::C_CAMERA_GOOGLE_PRE = "\\data\\view\\gpreCord";
std::string GConstant::C_CAMERA_JUMP_VIEW = "\\data\\view\\jview";
std::string GConstant::C_CAMERA_JUMP_PRE = "\\data\\view\\jpreCord";
std::string GConstant::C_OP_SPEED = "\\data\\speed\\optimizedspeed";  
std::string GConstant::C_GOOGLE_SPEED = "\\data\\speed\\googlespeed"; 
std::string GConstant::C_JUMP_SPEED = "\\data\\speed\\jumpspeed";
std::string GConstant::C_IMPORTANCE = "\\data\\imp\\importance";      //视角重要性矩阵
std::string GConstant::C_LIMPORTANCE = "\\data\\imp\\limportance";      //视角重要性矩阵
std::string GConstant::C_OPTICALFLOW = "\\data\\imp\\optflow"; 
std::string GConstant::C_OPTICALFLOWACC = "\\data\\imp\\optacc"; 

//images
std::string GConstant::C_SAMPLEVIEW_IMAGES = "\\snapshot\\samples\\";    //采样点的采样图片
std::string GConstant::C_VIDEO_PATH = "\\snapshot\\snapVideo\\";
std::string GConstant::C_VIDEO_DIFF_PATH = "\\snapshot\\snapVideo\\diff";
std::string GConstant::C_IMAGE_EXTENSION = "png"; 

//root config
std::string GConstant::C_SCENE_CAMERA = "\\cam.txt";                   //场景相机视角
std::string GConstant::C_SAMPLE_PATH = "\\path.xml"; 
std::string GConstant::C_SSAMPLE_PATH = "\\spath.xml"; 
std::string GConstant::C_SCSAMPLE_PATH = "\\scpath.xml"; 
std::string GConstant::C_SJSAMPLE_PATH = "\\sjpath.xml";
std::string GConstant::C_CONFIG = "\\config.xml";


//font
osgText::Font*  GConstant::C_FONT_HEI = osgText::readFontFile("C:\\WINDOWS\\Fonts\\simhei.ttf");                            //黑体
osgText::Font*  GConstant::C_FONT_KAI = osgText::readFontFile("C:\\WINDOWS\\Fonts\\simkai.ttf");                            //楷体
osgText::Font*  GConstant::C_FONT_TIMES = osgText::readFontFile("times.ttf");                                               //times