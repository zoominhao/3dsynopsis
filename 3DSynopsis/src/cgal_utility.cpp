#include "cgal_utility.h"
#include <CGAL/Polygon_2_algorithms.h>

namespace CGALUtility
{
	bool check_inside(CgalPoint2 pt, CgalPoint2 *pgn_begin, CgalPoint2 *pgn_end, Kernel traits)
	{
		bool b;
		switch(CGAL::bounded_side_2(pgn_begin, pgn_end,pt, traits)) {
		case CGAL::ON_BOUNDED_SIDE :
			b = true;
			break;
		case CGAL::ON_BOUNDARY:
			b = true;
			break;
		case CGAL::ON_UNBOUNDED_SIDE:
			b = false;
			break;
		}

		return b;
	}

	bool check_inside( CgalPoint pt, CgalPoint* pgn_begin, CgalPoint* png_end, Kernel traits )
	{
		// find the plane
		CgalPlane plane( *pgn_begin, *(pgn_begin+1), *(pgn_begin+2));
		CgalVector n = plane.orthogonal_vector();
		std::vector<float> vecCoord;
		vecCoord.push_back( std::abs(n.x()));
		vecCoord.push_back( std::abs(n.y()));
		vecCoord.push_back( std::abs(n.z()));
		int iRomve = std::max_element( vecCoord.begin(), vecCoord.end() ) - vecCoord.begin();

		std::vector<CgalPoint2> points;
		for( CgalPoint* pgn = pgn_begin; pgn != png_end; ++pgn )
		{
			CgalPoint& v = *pgn;
			points.push_back( DimReducer<CgalPoint, CgalPoint2>( v, iRomve ) );
		}

		CgalPoint2 pt2 = DimReducer<CgalPoint, CgalPoint2>( pt, iRomve );		
		bool bFlag = check_inside( pt2, &(points[0]), &(points[0])+points.size(), traits );
		return bFlag;
	}

};


