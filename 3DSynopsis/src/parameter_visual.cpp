#include "parameter_visual.h"

ParameterVisual::ParameterVisual ()
	:center_(0,0,0)
	,radius_(0.001)
	,color_(0.9, 0.1, 0.1, 0.4)
{
	toggleHidden(false);
}

ParameterVisual::~ParameterVisual(void)
{
	
}

void ParameterVisual::updateImpl()
{
	renderSphere();	
}

bool ParameterVisual::setSphere(const osg::Vec3& center,  double radius )
{
	if (radius<0 || center.isNaN())
	{
		return false;
	}
	center_ = center;
	radius_ = radius;

	expire();
	return true;
}

bool ParameterVisual::renderSphere()
{
	osg::Geode* geode = OSGUtility::drawSphere(center_,radius_,color_);

	//先取得node的属性
	osg::StateSet* ss=geode->getOrCreateStateSet();
	//Alpha混合开启
	ss->setMode(GL_BLEND,osg::StateAttribute::ON);
	//取消深度测试
	ss->setMode(GL_DEPTH_TEST,osg::StateAttribute::OFF);

	addChild( geode );

	return true;
}