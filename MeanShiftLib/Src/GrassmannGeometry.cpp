#include "GrassmannGeometry.h"

/*
void CGrassmannGeometry::parallelTransport(double a, double *g, double *tg){

	int i, j, k, idx;

	for(i = 0; i < m_k; i++){
		m_s[i] = sin(a * m_dels[i]);
		m_c[i] = cos(a * m_dels[i]);
	}

	// m_dbArrWork0 = y * v * sn
	idx = 0;
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_n; j++){
			for(k = 0, m_ucd[idx] = 0; k < m_k; k++)
				m_ucd[idx] += m_x[k * m_n + j] * m_delv[k * m_k + i] * m_s[i];
			idx++;
		}
	}

	// m_vsd = u^t * g
	idx = 0;
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_k; j++){
			for(k = 0, m_vsd[idx] = 0; k < m_n; k++)
				m_vsd[idx] += m_u[j * m_n + k] * g[i * m_n + k];
			idx++;
		}
	}

	// compute th ang tg
	idx = 0;
	memcpy(tg, g, sizeof(double) * m_d);
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_n; j++){
			for(k = 0; k < m_k; k++){
				tg[idx] -= (m_ucd[k * m_n + j] + m_delu[k * m_n + j] * (1.0 - m_c[k])) * m_vsd[i * m_k + k];
			}
			idx++;
		}
	}

}


void CGrassmannGeometry::selfTransport(double a, double *th){

	int i, j, k, idx;

	for(i = 0; i < m_k; i++){
		m_s[i] = sin(a * m_dels[i]);
		m_c[i] = cos(a * m_dels[i]);
	}

	// m_dbArrWork0 = y * v * sn
	idx = 0;
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_n; j++){
			for(k = 0, m_ucd[idx] = 0; k < m_k; k++)
				m_ucd[idx] += m_x[k * m_n + j] * m_delv[k * m_k + i] * m_s[i];
			idx++;
		}
	}

	// compute th
	idx = 0;
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_n; j++){
			for(k = 0, th[idx] = 0; k < m_k; k++){
				th[idx] += (m_delu[k * m_n + j] * m_c[k] - m_ucd[k * m_n + j]) * m_dels[k] * m_delv[i * m_k + k];
			}
			idx++;
		}
	}

}
*/