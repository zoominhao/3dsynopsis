/////////////////////////////////////////////////////////////////////////////
// Name:        MeanShift.h
// Purpose:     Mean Shift class
// Author:      Raghav Subbarao
// Modified by:
// Created:     02/02/2004
// Copyright:   (c) Raghav Subbarao
// Version:     v0.1
/////////////////////////////////////////////////////////////////////////////

#ifndef _C_ESSENTIAL_POINT_SET_
#define _C_ESSENTIAL_POINT_SET_

#include "PointSet.h"
#include "f2c.h"

// Each essential matrix is decomposed into its SVD 
// and stored as the 18 elements of U and V^T, where
//			E = U * diag(1,1,0) * V^T
//

extern "C" int dgesvd_(char*, char*, int*, int*, double*, int*, double*,
					   double*, int*, double*, int*, double*, int*, int*);

template<typename T> class CEssentialPointSet : public CPointSet<T>{

public:

	// Empty Constructor
	CEssentialPointSet();

	// Standard Constructor
	// n = number of points to allocate memory for.
	CEssentialPointSet(int n);

	// Empty Constructor
	CEssentialPointSet(int n, T *data);

	// Copy Constructor
	CEssentialPointSet(CEssentialPointSet<T>& ps);

	// Destructor
	virtual ~CEssentialPointSet(){};

	// Performs SVD of 3X3 matrix in x.
	// x is assumed to be column-organized
	inline void loadPoint(T *x);

	// Converts U and V into essential matrix
	inline void returnPoint(T *x, int n);

protected:
	
	// m_ps_d = 12

	// memory used in pre and post computation
	double m_workuv[18];
	double m_works[3];
	double m_work[15];


};


template<typename T> CEssentialPointSet<T>::CEssentialPointSet():
CPointSet<T>(18, 9, 0)
{}

template<typename T> CEssentialPointSet<T>::CEssentialPointSet(int n):
CPointSet<T>(18, 9, n)
{}

template<typename T> CEssentialPointSet<T>::CEssentialPointSet(int n, T *data):
CPointSet<T>(18, 9, n)
{

	T *ptr = data;
	for(int i = 0; i < m_ps_nmax; i++, ptr += 9)
		loadPoint(ptr);

}

template<typename T> CEssentialPointSet<T>::CEssentialPointSet(CEssentialPointSet<T>& ps):
CPointSet(18, 9, ps.m_ps_nmax)
{
	m_ps_n			=	ps.m_ps_n;
	m_ps_loc		=	ps.m_ps_loc;
	memcpy(m_ps_points, ps.m_ps_points, sizeof(T) * m_ps_n * m_ps_dim);
}

template<typename T> inline void CEssentialPointSet<T>::loadPoint(T *x){

	if(m_ps_n == m_ps_nmax)
		return;

	int m = 3, lwork = 15, info;
	for(int i = 0; i < 9; i++)
		m_workuv[i] = x[i];
	
	dgesvd_("O", "S", &m, &m, m_workuv, &m, m_works, 0, &m, m_workuv + 9, &m, m_work, &lwork, &info);
	assert(info == 0);

	double dU = m_workuv[0] * (m_workuv[4] * m_workuv[8] - m_workuv[7] * m_workuv[5]) + 
				m_workuv[3] * (m_workuv[7] * m_workuv[2] - m_workuv[1] * m_workuv[8]) + 
				m_workuv[6] * (m_workuv[1] * m_workuv[5] - m_workuv[4] * m_workuv[2]);
	if(dU < 0){
		m_workuv[6] *= -1.0;
		m_workuv[7] *= -1.0;
		m_workuv[8] *= -1.0;
	}

	double dV = m_workuv[ 9] * (m_workuv[13] * m_workuv[17] - m_workuv[16] * m_workuv[14]) + 
				m_workuv[12] * (m_workuv[16] * m_workuv[11] - m_workuv[10] * m_workuv[17]) + 
				m_workuv[15] * (m_workuv[10] * m_workuv[14] - m_workuv[13] * m_workuv[11]);
	if(dV < 0){
		m_workuv[11] *= -1.0;
		m_workuv[14] *= -1.0;
		m_workuv[17] *= -1.0;
	}

	for(int i = 0; i < 18; i++)
		m_ps_points[m_ps_n * m_ps_dim + i] = m_workuv[i];

	m_ps_n++;

}

template<typename T> inline void CEssentialPointSet<T>::returnPoint(T *x, int n){

	if(n >= m_ps_n)
		return;

	T *ptr = m_ps_points + n * 18;

	x[0] = ptr[ 0] * ptr[ 9] + ptr[ 3] * ptr[10];
	x[1] = ptr[ 1] * ptr[ 9] + ptr[ 4] * ptr[10];
	x[2] = ptr[ 2] * ptr[ 9] + ptr[ 5] * ptr[10];

	x[3] = ptr[ 0] * ptr[12] + ptr[ 3] * ptr[13];
	x[4] = ptr[ 1] * ptr[12] + ptr[ 4] * ptr[13];
	x[5] = ptr[ 2] * ptr[12] + ptr[ 5] * ptr[13];

	x[6] = ptr[ 0] * ptr[15] + ptr[ 3] * ptr[16];
	x[7] = ptr[ 1] * ptr[15] + ptr[ 4] * ptr[16];
	x[8] = ptr[ 2] * ptr[15] + ptr[ 5] * ptr[16];

}

#endif