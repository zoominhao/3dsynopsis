/*******************************************************

                 Riemannian Geometry Library
	=============================================

	This set of files if a collection of routines for 
	simple operations over Riemannian manifolds.

Implemented by Raghav Subbarao
********************************************************/

#ifndef _C_AFFINE_INVERIANT_DTI_GEOMETRY_H_
#define _C_AFFINE_INVERIANT_DTI_GEOMETRY_H_

#include <math.h>
#include <assert.h>
#include <memory.h>

#include "Geometry.h"

#ifndef PI
#define PI 3.14159265358979
#endif

#ifndef MAX
#define MAX(a, b) ((a)>(b)?(a):(b))
#endif

template<typename T> class CAffineInvariantDTIGeometry : public CGeometry<T>{

public:

	CAffineInvariantDTIGeometry();
	virtual ~CAffineInvariantDTIGeometry(){};

	inline double g(const T *gamma, const T *delta);

	inline void expm(const T *x, const T *delta, T *y);

	inline void logm(const T *x, const T *y, T *delta);

private:

	// explicit matrix multiplication functions
	inline void mult_U_V(const T* u, const T* v, T* uv);

	inline void logsymmetric(const T* R, T* lR);
	inline void expsymmetric(const T* R, T* eR);

	inline void tql2(T *V, T *d, T *e);
	inline void tred2(T *V, T *d, T *e);

	// used to store results of matrix multiplications
	T m_V[9];
	T m_d[3];
	T m_e[3];

	T m_M1[6];
	T m_M2[6];

};

template<typename T> CAffineInvariantDTIGeometry<T>::CAffineInvariantDTIGeometry():
CGeometry<T>(6, 6)
{}

template<typename T> double CAffineInvariantDTIGeometry<T>::g(const T *gamma, const T *delta){

	double d = 0;
	for(int i = 0; i < m_m; i++)
		d += gamma[i] * delta[i];
	return d;

}



template<typename T> void CAffineInvariantDTIGeometry<T>::expm(const T* x, const T *del, T *y){

	// compute matrix exponential
	expsymmetric(del, m_M1);
	mult_U_V(m_M1, x, m_M2);
	mult_U_V(x, m_M2, y);

	m_ldU[0] = del[0];	m_ldU[1] = del[1];	m_ldU[2] = del[4];
	exprodrigues(m_ldU, m_dU);
	mult_U_V(x, m_dU, y);

	m_ldV[0] = del[2];	m_ldV[1] = del[3];	m_ldV[2] = del[4];
	exprodrigues(m_ldV, m_dV);
	mult_Ut_V(x + 9, m_dV, y + 9);

}

template<typename T> void CAffineInvariantDTIGeometry<T>::logm(const T* x, const T *y, T *del){

	

}


template<typename T> inline void CAffineInvariantDTIGeometry<T>::mult_U_V(const T *u, const T* v, T* uv){

	uv[0] = u[0] * v[0] + u[3] * v[3] + u[5] * v[5];
	uv[1] = u[3] * v[3] + u[1] * v[1] + u[4] * v[4];
	uv[2] = u[5] * v[5] + u[4] * v[4] + u[2] * v[2];

	uv[3] = u[0] * v[3] + u[3] * v[1] + u[5] * v[4];
	uv[4] = u[3] * v[5] + u[1] * v[4] + u[4] * v[2];
	uv[5] = u[0] * v[5] + u[3] * v[4] + u[5] * v[2];

}

template<typename T> void CAffineInvariantDTIGeometry<T>::logsymmetric(const T* X, T* lX){

	m_V[0] = X[0];	m_V[3] = X[3];	m_V[6] = X[5];
	m_V[1] = X[3];	m_V[4] = X[1];	m_V[7] = X[4];
	m_V[2] = X[5];	m_V[5] = X[4];	m_V[8] = X[2];
	
	tred2(m_V, m_d, m_e);
	tql2(m_V, m_d, m_e);

	m_d[0] = log(m_d[0]);	m_d[1] = log(m_d[1]);	m_d[2] = log(m_d[2]);

	lX[0] = m_d[0] * m_V[0] * m_V[0] + m_d[1] * m_V[3] * m_V[3] + m_d[2] * m_V[6] * m_V[6];
	lX[1] = m_d[0] * m_V[1] * m_V[1] + m_d[1] * m_V[4] * m_V[4] + m_d[2] * m_V[7] * m_V[7];
	lX[2] = m_d[0] * m_V[2] * m_V[2] + m_d[1] * m_V[5] * m_V[5] + m_d[2] * m_V[8] * m_V[8];

	lX[3] = m_d[0] * m_V[0] * m_V[1] + m_d[1] * m_V[3] * m_V[4] + m_d[2] * m_V[6] * m_V[7];
	lX[4] = m_d[0] * m_V[1] * m_V[2] + m_d[1] * m_V[4] * m_V[5] + m_d[2] * m_V[7] * m_V[8];
	lX[5] = m_d[0] * m_V[0] * m_V[2] + m_d[1] * m_V[3] * m_V[5] + m_d[2] * m_V[6] * m_V[8];

}


template<typename T> void CAffineInvariantDTIGeometry<T>::expsymmetric(const T *X, T *eX){

	m_V[0] = X[0];	m_V[3] = X[3];	m_V[6] = X[5];
	m_V[1] = X[3];	m_V[4] = X[1];	m_V[7] = X[4];
	m_V[2] = X[5];	m_V[5] = X[4];	m_V[8] = X[2];
	
	tred2(m_V, m_d, m_e);
	tql2(m_V, m_d, m_e);

	m_d[0] = exp(m_d[0]);	m_d[1] = exp(m_d[1]);	m_d[2] = exp(m_d[2]);

	lX[0] = m_d[0] * m_V[0] * m_V[0] + m_d[1] * m_V[3] * m_V[3] + m_d[2] * m_V[6] * m_V[6];
	lX[1] = m_d[0] * m_V[1] * m_V[1] + m_d[1] * m_V[4] * m_V[4] + m_d[2] * m_V[7] * m_V[7];
	lX[2] = m_d[0] * m_V[2] * m_V[2] + m_d[1] * m_V[5] * m_V[5] + m_d[2] * m_V[8] * m_V[8];

	lX[3] = m_d[0] * m_V[0] * m_V[1] + m_d[1] * m_V[3] * m_V[4] + m_d[2] * m_V[6] * m_V[7];
	lX[4] = m_d[0] * m_V[1] * m_V[2] + m_d[1] * m_V[4] * m_V[5] + m_d[2] * m_V[7] * m_V[8];
	lX[5] = m_d[0] * m_V[0] * m_V[2] + m_d[1] * m_V[3] * m_V[5] + m_d[2] * m_V[6] * m_V[8];

}


// Symmetric Householder reduction to tridiagonal form.

template<typename T> void CAffineInvariantDTIGeometry<T>::tred2(T *V, T* d, T* e){

	//  This is derived from the Algol procedures tred2 by
	//  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
	//  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
	//  Fortran subroutine in EISPACK.

	int i, j, k;
	T scale, h, f, g, hh;

	// set d to last row
	d[0] = V[2]; d[1] = V[5]; d[2] = V[8];

	// Householder reduction to tridiagonal form.
	for(i = 2; i > 0; i--){

		// Scale to avoid under/overflow.
		scale = h = 0.0;
		for (k = 0; k < i; k++)	scale = scale + fabs(d[k]);

		if (scale == 0.0) {
			e[i] = d[i - 1];
			for(j = 0; j < i; j++) {
				d[j] = V[3 * j + i - 1];
				V[3 * j + i] = V[3 * i + j] = 0.0;
			}
		} 
		else{

			// Generate Householder vector.
			for(k = 0; k < i; k++){
				d[k] /= scale;
				h += d[k] * d[k];
			}
			f = d[i - 1];
			g = sqrt(h);

			if (f > 0)
				g = -g;

			e[i] = scale * g;
			h = h - f * g;
			d[i - 1] = f - g;
			for (j = 0; j < i; j++)
				e[j] = 0.0;

			// Apply similarity transformation to remaining columns.
			for(j = 0; j < i; j++){
				f = d[j];
				V[3 * i + j] = f;
				g = e[j] + V[3 * j + j] * f;
				for(k = j + 1; k <= i - 1; k++){
					g += V[3 * j + k] * d[k];
					e[k] += V[3 * j + k] * f;
				}
				e[j] = g;
			}
			f = 0.0;
			for(j = 0; j < i; j++){
				e[j] /= h;
				f += e[j] * d[j];
			}

			hh = f / (h + h);
			for(j = 0; j < i; j++)
				e[j] -= hh * d[j];

			for(j = 0; j < i; j++){
				f = d[j];
				g = e[j];
				for(k = j; k <= i - 1; k++)
					V[3 * j + k] -= (f * e[k] + g * d[k]);
				d[j] = V[3 * j + i-1];
				V[3 * j + i] = 0.0;
			}
		} // end of else...

		d[i] = h;

	}

	// Accumulate transformations.
	for(i = 0; i < 2; i++){

		V[3 * i + 2] = V[3 * i + i];
		V[3 * i + i] = 1.0;
		h = d[i + 1];
		if (h != 0.0){

			for (k = 0; k <= i; k++)
				d[k] = V[3 * (i + 1) + k] / h;

			for(j = 0; j <= i; j++){
				g = 0.0;
				for (k = 0; k <= i; k++)
					g += V[3 * (i + 1) + k] * V[3 * j + k];
				for(k = 0; k <= i; k++)
					V[3 * j + k] -= g * d[k];
			}

		}
		for (k = 0; k <= i; k++)
			V[3 * (i + 1) + k] = 0.0;

	}

	for(j = 0; j < 3; j++){
		d[j] = V[3 * j + 2];
		V[3 * j + 2] = 0.0;
	}

	V[8] = 1.0;
	e[0] = 0.0;

} 


// Symmetric tridiagonal QL algorithm.

template<typename T> void CAffineInvariantDTIGeometry<T>::tql2(T *V, T *d, T *e) {

	//  This is derived from the Algol procedures tql2, by
	//  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
	//  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
	//  Fortran subroutine in EISPACK.

	e[0] = e[1]; e[2] = e[2], e[2] = 0.0;

	T f = 0.0, tst1 = 0.0, g, p, r, dl1, h, 
		c, c2, c3, el1, s, s2;
	double eps = pow(2.0,-52.0);
	int l, m, iter;

	for (l = 0; l < 3; l++) {

		// Find small subdiagonal element
		tst1 = MAX(tst1,fabs(d[l]) + fabs(e[l]));
		m = l;
		while (m < 3){
			if (fabs(e[m]) <= eps * tst1){
				break;
			}
			m++;
		}

		// If m == l, d[l] is an eigenvalue,
		// otherwise, iterate.
		if (m > l) {
			iter = 0;
			do{
				iter++; // (Could check iteration count here.)

				// Compute implicit shift

				g = d[l];
				p = (d[l+1] - g) / (2.0 * e[l]);
				r = sqrt(p * p + 1);
				if (p < 0){
					r = -r;
				}

				d[l] = e[l] / (p + r);
				d[l + 1] = e[l] * (p + r);
				dl1 = d[l+1];
				h = g - d[l];
				for (i = l + 2; i < 3; i++){
					d[i] -= h;
				}
				f = f + h;

				// Implicit QL transformation.
				p = d[m];
				c = 1;
				c2 = c;
				c3 = c;
				el1 = e[l + 1];
				s = s2 = 0;
				for (i = m - 1; i >= l; i--){
					c3 = c2;
					c2 = c;
					s2 = s;
					g = c * e[i];
					h = c * p;
					r = sqrt(p * p + e[i] * e[i]);
					e[i + 1] = s * r;
					s = e[i] / r;
					c = p / r;
					p = c * d[i] - s * g;
					d[i + 1] = h + s * (c * g + s * d[i]);

					// Accumulate transformation.
					for (k = 0; k < 3; k++) {
						h = V[3 * (i + 1) + k];
						V[3 * (i + 1) + k] = s * V[3 * i + k] + c * h;
						V[3 * i + k] = c * V[3 * i + k] - s * h;
					}
				}

				p = -s * s2 * c3 * el1 * e[l] / dl1;
				e[l] = s * p;
				d[l] = c * p;

				// Check for convergence.

			} while (fabs(e[l]) > eps * tst1);
		}

		d[l] = d[l] + f;
		e[l] = 0.0;

	}

}

#endif
