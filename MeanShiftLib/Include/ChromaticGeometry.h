/****************************************************************

                 Chromatic Geometry Library
	=============================================

Implementation of manifold operations for space of chromaticities.
This space is equivalent go the Grassmann manifold G_{3,1), the 
set of directions in R^3.

Implemented by Raghav Subbarao
****************************************************************/

#ifndef _C_CHROMATIC_GEOMETRY_H_
#define _C_CHROMATIC_GEOMETRY_H_

//include needed libraries
#include <math.h>
#include <assert.h>
#include <memory.h>

#include "Geometry.h"

template<typename T> class CChromaticGeometry : public CGeometry<T>{

public:

	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/
	/* Class Constructor and Destructor */
	/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/

	// Default Constructor
	CChromaticGeometry();

	// Virtual Destructor
	virtual ~CChromaticGeometry(){};

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	g									|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes the Riemannian inner product of tangents.|//
	//|   It always	returns a double value.	Computation is 	|//
	//|   equivalent to dot product in R^3.				 	|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* gamma>									|//
	//|   An array of type T containing the tangent.		|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An array of type T containing the tangent.		|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		g(delta, gamma)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline double g(const T *gamma, const T *delta);
	

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	expm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Compute manifold exponential. The memory for y	|//
	//|   is assumed to have been allocated already.		|//
	//|	  The function rotates x around the axis delta.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An array of type T containing the point x.		|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An array of type T containing the tangent vector.	|//
	//|														|//
	//|   <T* y>											|//
	//|   The result is returned in y.						|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		expm(delta, y)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void expm(const T *x, const T *delta, T *y);



	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	logm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Compute manifold logarithm at x. The memory for	|//
	//|   delta is assumed to have been allocated already.	|//
	//|   Estimates the axis of rotation between x and y.	|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An array of type T containing the point x.		|//
	//|														|//
	//|   <const T* y>										|//
	//|   An array of type T containing the point y.		|//
	//|														|//
	//|   <T* delta>										|//
	//|   The tangent is returned in delta.					|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		logm(y, delta)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void logm(const T *x, const T *y, T *delta);
	
};



/********************************************************/
//Class Constructor										//
//*******************************************************/
//Post:													//
//      The ChromaticGeometry class is initialized.		//
/********************************************************/
template<typename T> CChromaticGeometry<T>::CChromaticGeometry():
CGeometry<T>(3, 3)
{}




/********************************************************/
//Inner Product											//
//******************************************************//
//Computes the dot product of tangents.					//
/********************************************************/
//Pre:													//
//      - gamma is a tangent of size 3.					//
//      - delta is a tangent of size 3.					//
//Post:													//
//      - the product is returned as a double value.	//
//*******************************************************/
template<typename T> double CChromaticGeometry<T>::g(const T *gamma, const T *delta){

	double d = gamma[0] * delta[0] + gamma[1] * delta[1] + gamma[2] * delta[2];
	return d;

}



/********************************************************/
//Manifold Exponential									//
//******************************************************//
//Computes the exponential by rotating x baout delta.	//
/********************************************************/
//Pre:													//
//      - x is a vector of size 3 and norm 1.			//
//      - delta is of size 3 and perpendicular to x.	//
//Post:													//
//      - the returned vector is y						//
//*******************************************************/
template<typename T> void CChromaticGeometry<T>::expm(const T* x, const T *delta, T *y){

	// NOTE: delta is assumed to be perpendicular to x!!!
	double t = sqrt(delta[0] * delta[0] + delta[1] * delta[1] + delta[2] * delta[2]);

	if(t < 1.0e-4){
		y[0] = x[0];	y[1] = x[1];	y[2] = x[2];
		return;
	}

	double cs = cos(t);
	double sn = sin(t) / t;

	y[0] = cs * x[0] + sn * delta[0];
	y[0] = cs * x[1] + sn * delta[1];
	y[0] = cs * x[2] + sn * delta[2];
	
}



/********************************************************/
//Manifold Logarithm									//
//******************************************************//
//Computes the rotation from x to y.					//
/********************************************************/
//Pre:													//
//      - x is a vector of size 3 and norm 1.			//
//      - y is a vector of size 3 and norm 1.			//
//Post:													//
//      - the returned tangent is delta					//
//*******************************************************/
template<typename T> void CChromaticGeometry<T>::logm(const T* x, const T *y, T *delta){

	T dp = x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
	T cs = acos(dp);

	delta[0] = y[0] - dp * x[0];
	delta[1] = y[1] - dp * x[1];
	delta[2] = y[2] - dp * x[2];
	double n = sqrt(delta[0] * delta[0] + delta[1] * delta[1] + delta[2] * delta[2]);
	n = cs / n;

	delta[0] *= n;	delta[1] *= n;	delta[2] *= n;
	
}


#endif
