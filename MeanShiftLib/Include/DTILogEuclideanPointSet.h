/////////////////////////////////////////////////////////////////////////////
// Name:        MeanShift.h
// Purpose:     Mean Shift class
// Author:      Raghav Subbarao
// Modified by:
// Created:     02/02/2004
// Copyright:   (c) Raghav Subbarao
// Version:     v0.1
/////////////////////////////////////////////////////////////////////////////

#ifndef _C_DTI_LOG_EUCLIDEAN_POINT_SET_
#define _C_DTI_LOG_EUCLIDEAN_POINT_SET_

#include <math.h>

#include "PointSet.h"
#include "f2c.h"

// Each 3X3 symmetric positive definite matrix is stored as its matrix log
//			E = U * diag(1,1,0) * V^T
// the elements are stored in the order Dxx Dyy Dzz Dxy Dyz Dxz

#define mylog(x) (x > 0 ? log(x) : -1.0e10)

extern "C" int dsyev_(char*, char*, int*, double*, int*, double*, double*, int*, int*);

template<typename T> class CDTILogEuclideanPointSet : public CPointSet<T>{

public:

	// Empty Constructor
	CDTILogEuclideanPointSet();

	// Standard Constructor
	// n = number of points to allocate memory for.
	CDTILogEuclideanPointSet(int n);

	// Standard Constructor
	CDTILogEuclideanPointSet(int n, T *data);

	// Copy Constructor
	CDTILogEuclideanPointSet(CDTILogEuclideanPointSet<T>& ps);

	// Destructor
	virtual ~CDTILogEuclideanPointSet(){};

	// Performs SVD of 3X3 matrix in x.
	// x is assumed to be column-organized
	inline void loadPoint(T *x);

	// Converts U and V into essential matrix
	inline void returnPoint(T *x, int n);

protected:

	// memory used in pre and post computation
	double m_eigvec[9];
	double m_eigval[3];

	double m_work[15];


};


template<typename T> CDTILogEuclideanPointSet<T>::CDTILogEuclideanPointSet():
CPointSet<T>(6, 6, 0)
{}

template<typename T> CDTILogEuclideanPointSet<T>::CDTILogEuclideanPointSet(int n):
CPointSet<T>(6, 6, n)
{}

template<typename T> CDTILogEuclideanPointSet<T>::CDTILogEuclideanPointSet(int n, T *data):
CPointSet<T>(6, 6, n)
{

	T *ptr = data;
	for(int i = 0; i < m_ps_nmax; i++, ptr += 6)
		loadPoint(ptr);

}

template<typename T> CDTILogEuclideanPointSet<T>::CDTILogEuclideanPointSet(CDTILogEuclideanPointSet<T>& ps):
CPointSet<T>(6, 6, ps.m_ps_nmax)
{
	m_ps_n			=	ps.m_ps_n;
	m_ps_loc		=	ps.m_ps_loc;
	memcpy(m_ps_points, ps.m_ps_points, sizeof(T) * m_ps_n * m_ps_dim);
}


template<typename T> inline void CDTILogEuclideanPointSet<T>::loadPoint(T *x){

	if(m_ps_n == m_ps_nmax)
		return;

	m_eigvec[0] = x[0];	m_eigvec[3] = x[3];	m_eigvec[6] = x[5];
	m_eigvec[1] = x[3];	m_eigvec[4] = x[1];	m_eigvec[7] = x[4];
	m_eigvec[2] = x[5];	m_eigvec[5] = x[4];	m_eigvec[8] = x[2];
	
	int n = 3, lwork = 15, info = 0;
	dsyev_("V", "U", &n, m_eigvec, &n, m_eigval, m_work, &lwork, &info);

	if(m_eigval[0] < 0)
		m_eigval[0] = -m_eigval[0];
	else if(m_eigval[0] == 0)
		m_eigval[0] = (T)(1.0e-14);
				
	if(m_eigval[1] < 0)
		m_eigval[1] = -m_eigval[1];
	else if(m_eigval[1] == 0)
		m_eigval[1] = (T)(1.0e-14);
				
	if(m_eigval[2] < 0)
		m_eigval[2] = -m_eigval[2];
	else if(m_eigval[2] == 0)
		m_eigval[2] = (T)(1.0e-14);

	m_eigval[0] = mylog(m_eigval[0]); m_eigval[1] = mylog(m_eigval[1]); m_eigval[2] = mylog(m_eigval[2]);

	T *ptr = m_ps_points + m_ps_n * m_ps_dim;
	ptr[0] = m_eigval[0] * m_eigvec[0] * m_eigvec[0] + m_eigval[1] * m_eigvec[3] * m_eigvec[3] + m_eigval[2] * m_eigvec[6] * m_eigvec[6];
	ptr[1] = m_eigval[0] * m_eigvec[1] * m_eigvec[1] + m_eigval[1] * m_eigvec[4] * m_eigvec[4] + m_eigval[2] * m_eigvec[7] * m_eigvec[7];
	ptr[2] = m_eigval[0] * m_eigvec[2] * m_eigvec[2] + m_eigval[1] * m_eigvec[5] * m_eigvec[5] + m_eigval[2] * m_eigvec[8] * m_eigvec[8];

	ptr[3] = m_eigval[0] * m_eigvec[0] * m_eigvec[1] + m_eigval[1] * m_eigvec[3] * m_eigvec[4] + m_eigval[2] * m_eigvec[6] * m_eigvec[7];
	ptr[4] = m_eigval[0] * m_eigvec[1] * m_eigvec[2] + m_eigval[1] * m_eigvec[4] * m_eigvec[5] + m_eigval[2] * m_eigvec[7] * m_eigvec[8];
	ptr[5] = m_eigval[0] * m_eigvec[0] * m_eigvec[2] + m_eigval[1] * m_eigvec[3] * m_eigvec[5] + m_eigval[2] * m_eigvec[6] * m_eigvec[8];

	m_ps_n++;

}

template<typename T> inline void CDTILogEuclideanPointSet<T>::returnPoint(T *x, int n){

	if(n >= m_ps_nmax)
		return;

	T *ptr = m_ps_points + n * m_ps_dim;

	m_eigvec[0] = ptr[0];	m_eigvec[3] = ptr[3];	m_eigvec[6] = ptr[5];
	m_eigvec[1] = ptr[3];	m_eigvec[4] = ptr[1];	m_eigvec[7] = ptr[4];
	m_eigvec[2] = ptr[5];	m_eigvec[5] = ptr[4];	m_eigvec[8] = ptr[2];
	
	int m = 3, lwork = 15, info = 0;
	dsyev_("V", "U", &m, m_eigvec, &m, m_eigval, m_work, &lwork, &info);

	m_eigval[0] = exp(m_eigval[0]); m_eigval[1] = exp(m_eigval[1]); m_eigval[2] = exp(m_eigval[2]);

	x[0] = m_eigval[0] * m_eigvec[0] * m_eigvec[0] + m_eigval[1] * m_eigvec[3] * m_eigvec[3] + m_eigval[2] * m_eigvec[6] * m_eigvec[6];
	x[1] = m_eigval[0] * m_eigvec[1] * m_eigvec[1] + m_eigval[1] * m_eigvec[4] * m_eigvec[4] + m_eigval[2] * m_eigvec[7] * m_eigvec[7];
	x[2] = m_eigval[0] * m_eigvec[2] * m_eigvec[2] + m_eigval[1] * m_eigvec[5] * m_eigvec[5] + m_eigval[2] * m_eigvec[8] * m_eigvec[8];

	x[3] = m_eigval[0] * m_eigvec[0] * m_eigvec[1] + m_eigval[1] * m_eigvec[3] * m_eigvec[4] + m_eigval[2] * m_eigvec[6] * m_eigvec[7];
	x[4] = m_eigval[0] * m_eigvec[1] * m_eigvec[2] + m_eigval[1] * m_eigvec[4] * m_eigvec[5] + m_eigval[2] * m_eigvec[7] * m_eigvec[8];
	x[5] = m_eigval[0] * m_eigvec[0] * m_eigvec[2] + m_eigval[1] * m_eigvec[3] * m_eigvec[5] + m_eigval[2] * m_eigvec[6] * m_eigvec[8];

}

#endif