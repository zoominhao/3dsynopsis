/****************************************************************

                 SE(3) Geometry Library
	=============================================

Implementation of manifold operations for the Grassmann manifold G_{n,k}

Implemented by Raghav Subbarao
****************************************************************/

#ifndef _C_GRASSMANN_GEOMETRY_H_
#define _C_GRASSMANN_GEOMETRY_H_

//include needed libraries
#include <math.h>
#include <assert.h>
#include <memory.h>

#include "f2c.h"
#include "Geometry.h"

// LAPACK implementation of SVD
extern "C" int dgesvd_(char*, char*, int*, int*, double*, int*, double*,
					   double*, int*, double*, int*, double*, int*, int*);

// LAPACK implementation of generalized SVD
extern "C" int dggsvd_(char*, char*, char*, int*, int*, int*, int*, int*, double*,
					   int*, double*, int*, double*, double*, double*, int*,
					   double*, int*, double*, int*, double*, int*, int*);

#define myacos(a) (fabs(a) > 1.0 ? 0.0 : acos(a))
#define myasin(a) (fabs(a) > 1.0 ? 0.0 : asin(a))

template<typename T> class CGrassmannGeometry : public CGeometry<T>{

public:

	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/
	/* Class Constructor and Destructor */
	/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/

	// Default Constructor
	// (n,k) = Parameters of Grassmann manifold
	CGrassmannGeometry(int n, int k);

	// Virtual Destructor
	virtual ~CGrassmannGeometry();

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	setTangent							|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Sets a tangent to be stored in m_del.	The SVD of 	|//
	//|   the tangent is stored in m_delu,m_dels & m_delv.	|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* del>									|//
	//|   An nXk array of type T containing the tangent.	|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		setTangent(delta)								|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void setTangent(const T *del);


	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	dism								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes Riemannian distance between x and y by  	|//
	//|   perfomring an SVD of the kXk matrix x^Ty.			|//
	//|   Overwrites virtual function from CGeometry.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* x>										|//
	//|   An nXk array of type T containing the point x.	|//
	//|														|//
	//|   <const T* y>										|//
	//|   An nXk array of type T containing the point y.	|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		dism(x, y)										|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline double dism(const T *x, const T *y);

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	g									|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes the Riemannian inner product of tangents.|//
	//|   It always	returns a double value.					|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* gamma>									|//
	//|   An nXk array of type T containing the tangent.	|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An nXk array of type T containing the tangent.	|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		g(delta, gamma)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline double g(const T *gamma, const T *delta);


	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	expm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes manifold exponential. The memory for y	|//
	//|   is assumed to have been allocated already.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An nXk array of type T containing the point x.	|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An nXk array of type T containing the tangent.	|//
	//|														|//
	//|   <T* y>											|//
	//|   The result is returned in y.						|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		expm(delta, y)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void expm(const T *x, const T *delta, T *y);


	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	logm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes manifold logarithm at x. The memory for	|//
	//|   delta is assumed to have been allocated already.	|//
	//|   Estimates the axis of rotation between x and y.	|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An nXk array of type T containing the point x.	|//
	//|														|//
	//|   <const T* y>										|//
	//|   An nXk array of type T containing the point y.	|//
	//|														|//
	//|   <T* delta>										|//
	//|   The tangent is returned in delta.					|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		logm(y, delta)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void logm(const T *x, const T *y, T *delta);
	
//	void parallelTransport(T a, T *g, T *tg);
//	void selfTransport(T a, T *th);

private:

	int m_n;		// dimensions of Gramssmann manifold
	int m_k;

	// Workspaces and values used for LAPACK computation.
	int m_lwork;
	double *m_workspace;
	int *m_iwork;

	// Storage for matrix computations.
	// Allocated by constructor.
	double *m_ucd;	// m_n X m_k
	double *m_vsd;	// m_k X m_k
	double *m_u;	// m_n X m_n
	double *m_v;	// m_k X m_k
	double *m_s;	// m_k
	double *m_c;	// m_k

	// svd of tangent
	double *m_delu;	// m_n * m_k
	double *m_delv;	// m_k * m_k
	double *m_dels;	// m_k

};


/********************************************************/
//Class Constructor										//
//*******************************************************/
//Post:													//
//      The CGrassmannGeometry class is initialized.	//
/********************************************************/
template<typename T> CGrassmannGeometry<T>::CGrassmannGeometry(int n, int k):
CGeometry<T>(n * k, n * k), m_n(n), m_k(k)
{

	m_ucd = new double[m_n * m_k];
	m_vsd = new double[m_k * m_k];
	m_u   = new double[m_n * m_n];
	m_v   = new double[m_k * m_k];
	m_s   = new double[m_k];
	m_c   = new double[m_k];

	m_delu   = new double[m_n * m_n];
	m_delv   = new double[m_k * m_k];
	m_dels   = new double[m_k];

	m_iwork = new int[m_k];
	m_lwork = max(max(3 * m_k, m_n) + m_k, max(3 * m_k + m_n, 5 * m_k));
	m_workspace = new double[m_lwork];

}


/********************************************************/
//Virtual Destructor									//
//*******************************************************/
//Post:													//
//      The CGrassmannGeometry class is destroyed.		//
/********************************************************/
template<typename T> CGrassmannGeometry<T>::~CGrassmannGeometry(){

	if(m_n){
		m_n = m_k = 0;
		delete [] m_ucd;	m_ucd = 0;
		delete [] m_vsd;	m_vsd = 0;
		delete [] m_u;		m_u = 0;
		delete [] m_v;		m_v = 0;
		delete [] m_s;		m_s = 0;
		delete [] m_c;		m_c = 0;
		delete [] m_delu;	m_delu = 0;
		delete [] m_delv;	m_delv = 0;
		delete [] m_dels;	m_dels = 0;
	}

	if(m_lwork){
		m_lwork = 0;
		delete [] m_workspace;	m_workspace = 0;
	}
	
}



/********************************************************/
//setTangent											//
//******************************************************//
//Computes the SVD of a tangnet and stores it.			//
/********************************************************/
//Pre:													//
//      - delta is a tangent of size nXk.				//
//Post:													//
//														//
//*******************************************************/
template<typename T> inline void CGrassmannGeometry<T>::setTangent(const T *del){

	memcpy(m_del, del, sizeof(T) * m_d);

	int info;
	for(int i = 0; i < m_d; i++)
		m_ucd[i] = (double)del[i];
	dgesvd_("S", "S", &m_n, &m_k, m_ucd, &m_n, m_dels, m_delu, &m_n, m_delv, &m_k, m_workspace, &m_lwork, &info);
	assert(!info);

	// TODO: do we verify tangnet really lies in tangent space?
	
}


/********************************************************/
//Riemannian Distance									//
//******************************************************//
//Computes the Riemannian distance through an SVD.		//
/********************************************************/
//Pre:													//
//      - x is a point of size nXk.						//
//      - y is a point of size nXk.						//
//Post:													//
//      - the distance is returned as a double value.	//
//*******************************************************/
template<typename T> inline double CGrassmannGeometry<T>::dism(const T* x, const T* y){

	int i, j, k, idx;

	// compute m_vsd = x^Ty
	memset(m_vsd, 0, sizeof(double) * m_k * m_k);
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_k; j++){
			idx = j * m_k  + i;
			for(k = 0; k < m_n; k++)
				m_vsd[idx] += x[i * m_n + k] * y[j * m_n + k];
		}
	}

	// SVD of x^T y
	int info;
	dgesvd_("N", "N", &m_k, &m_k, m_vsd, &m_k, m_s, 0, &m_k, 0, &m_k, m_workspace, &m_lwork, &info);
	assert(!info);

	double d = 0;
	for(i = 0; i < m_k; i++)
		d += pow(myacos(m_s[i]), 2);

	return d;

}


/********************************************************/
//Inner Product											//
//******************************************************//
//Computes the dot product of tangents.					//
/********************************************************/
//Pre:													//
//      - gamma is a tangent of size nXk.				//
//      - delta is a tangent of size nXk.				//
//Post:													//
//      - the product is returned as a double value.	//
//*******************************************************/
template<typename T> inline double CGrassmannGeometry<T>::g(const T* gamma, const T* delta){

	double dis = 0.0;
	for(int i = 0; i < m_d; i++)
		dis += gamma[i] * delta[i];
	
	return dis;

}


/********************************************************/
//Manifold Exponential									//
//******************************************************//
//Computes the exponential by (2.65) of Edelman et.al.	//
/********************************************************/
//Pre:													//
//      - x is a vector of size nXk.					//
//      - delta is of size nXk and should be a tangent!	//
//Post:													//
//      - the returned point is y						//
//*******************************************************/
template<typename T> inline void CGrassmannGeometry<T>::expm(const T* x, const T* d, T* y){

	int i, j, k, l, info;

	// Compute SVD of tangent
	for(i = 0; i < m_d; i++)
		m_ucd[i] = (double)d[i];
	dgesvd_("S", "S", &m_n, &m_k, m_ucd, &m_n, m_s, m_u, &m_n, m_v, &m_k, m_workspace, &m_lwork, &info);
	assert(!info);
	
	// Move along geodesic
	// y = x * v * cos(sigma*t) * v^T + u * sin(sigma*t) * v^T
	memset(y, 0, sizeof(T) * m_d);
	for(i = 0; i < m_n; i++){
		for(j = 0; j < m_k; j++){
			for(k = 0; k < m_k; k++){
				for(l = 0; l < m_k; l++)
					y[j * m_n + i] += x[k * m_n + i] * m_v[k * m_k + l] * cos(m_s[l]) * m_v[j * m_k + l];
			
				y[j * m_n + i] += m_u[k * m_n + i] * sin(m_s[k]) * m_v[j * m_k + k];
			}
		}
	}
	
}

/********************************************************/
//Manifold Logarithm									//
//******************************************************//
//Computes the logarithm by inverting the exponential.	//
/********************************************************/
//Pre:													//
//      - x is a vector of size nXk.					//
//      - y is a vector of size nXk.					//
//Post:													//
//      - the returned vector is delta					//
//*******************************************************/
template<typename T> inline void CGrassmannGeometry<T>::logm(const T* x, const T* y, T* d){

	int i, j, k, idx;

	// compute m_vsd = x^Ty
	memset(m_vsd, 0, sizeof(double) * m_k * m_k);
	for(i = 0; i < m_k; i++){
		for(j = 0; j < m_k; j++){
			idx = j * m_k  + i;
			for(k = 0; k < m_n; k++)
				m_vsd[idx] += x[i * m_n + k] * y[j * m_n + k];
		}
	}

	// compute m_ucd = y - x * m_vsd = y - x * x^T * y
	for(i = 0; i < m_d; i++)
		m_ucd[i] = (double)y[i];
	for(i = 0; i < m_n; i++){
		for(j = 0; j < m_k; j++){
			idx = j * m_n  + i;
			for(k = 0; k < m_k; k++)
				m_ucd[idx] -= x[k * m_n + i] * m_vsd[j * m_k + k];
		}
	}

	int info;
	// perform a generalized svd
	dggsvd_("U", "V", "N", &m_n, &m_k, &m_k, &i, &j, m_ucd, &m_n, m_vsd, &m_k, m_s, m_c, m_u, &m_n, m_v, &m_k, 0, &m_k, m_workspace, m_iwork, &info);
	assert(!info);

	for(i = 0; i < m_k; i++)
		m_s[i] = myasin(m_s[i]);

	// d = m_u * m_s + m_v^T
	memset(d, 0, sizeof(T) * m_d);
	for(i = 0; i < m_n; i++){
		for(j = 0; j < m_k; j++){
			idx = j * m_n  + i;
			for(k = 0; k < m_k; k++)
				d[idx] += m_u[k * m_n + i] * m_v[k * m_k + j] * m_s[k];
		}
	}
	
}


#endif
