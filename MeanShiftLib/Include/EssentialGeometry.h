/////////////////////////////////////////////////////////////////////////////
// Name:        rsPbM.h
// Purpose:     pbM Function Definitions
// Author:      Raghav Subbarao
// Modified by:
// Created:     19/05/2004
// Copyright:   (c) Raghav Subbarao
// Version:     v0.1
/////////////////////////////////////////////////////////////////////////////

#ifndef _C_ESSENTIAL_GEOMETRY_H_
#define _C_ESSENTIAL_GEOMETRY_H_

#include <math.h>
#include <assert.h>
#include <memory.h>

#include "Geometry.h"

#ifndef PI
#define PI 3.14159265358979
#endif

// TODO: add possible scalings of translations to class

template<typename T> class CEssentialGeometry : public CGeometry<T>{

public:

	CEssentialGeometry();
	virtual ~CEssentialGeometry(){};

	inline double g(const T *gamma, const T *delta);

	inline void expm(const T *x, const T *delta, T *y);
	
	inline void logm(const T *x, const T *y, T *delta);
	
private:

	// explicit matrix multiplication functions
	inline void mult_U_V(const T* u, const T* v, T *uv);
	inline void mult_Ut_V(const T* u, const T* v, T *uv);
	inline void mult_U_Vt(const T* u, const T* v, T *uv);

	inline void logrodrigues(const T R[9], T lR[3]);
	inline void exprodrigues(const T R[3], T eR[9]);
	
	// used to store results of matrix multiplications
	T m_dU[9];
	T m_dV[9];

	T m_ldU[3];
	T m_ldV[3];

};

template<typename T> CEssentialGeometry<T>::CEssentialGeometry():
CGeometry<T>(18, 5)
{}

template<typename T> double CEssentialGeometry<T>::g(const T *gamma, const T *delta){

	double d = 0;
	for(int i = 0; i < m_m; i++)
		d += gamma[i] * delta[i];
	return d;

}



template<typename T> void CEssentialGeometry<T>::expm(const T* x, const T *del, T *y){

	// compute matrix exponential
	m_ldU[0] = del[0];	m_ldU[1] = del[1];	m_ldU[2] = del[4];
	exprodrigues(m_ldU, m_dU);
	mult_U_V(x, m_dU, y);

	m_ldV[0] = del[2];	m_ldV[1] = del[3];	m_ldV[2] = del[4];
	exprodrigues(m_ldV, m_dV);
	mult_Ut_V(x + 9, m_dV, y + 9);
	
}

template<typename T> void CEssentialGeometry<T>::logm(const T* x, const T *y, T *del){

	double su, sv, thetau, thetav, theta, dtheta, c, s, t;
	
	// parity check
	int parity = (x[6] * y[6] + x[7] * y[7] + x[8] * y[8] > 0 ? 1 : -1);

	// compute relative rotation for U
	mult_Ut_V(x,     y,     m_dU);
	mult_U_Vt(x + 9, y + 9, m_dV);

	if(parity == -1){
		m_dU[3] = -m_dU[3];	m_dU[4] = -m_dU[4];	m_dU[5] = -m_dU[5];
		m_dU[6] = -m_dU[6];	m_dU[7] = -m_dU[7];	m_dU[8] = -m_dU[8];
		m_dV[3] = -m_dV[3];	m_dV[4] = -m_dV[4];	m_dV[5] = -m_dV[5];
		m_dV[6] = -m_dV[6];	m_dV[7] = -m_dV[7];	m_dV[8] = -m_dV[8];
	}

	// Remove rotation by multiples of PI
	su = (m_dU[5] * m_dU[6] - m_dU[2] * m_dU[7]) / (m_dU[2] * m_dU[2] + m_dU[5] * m_dU[5]);
	thetau = asin(su);
	if(m_dU[2] * m_dU[6] + m_dU[5] * m_dU[7] > 1.0e-10){
		if(su < 0)
			thetau = - thetau - PI;
		else
			thetau = PI - thetau;
	}
	
	sv = (m_dV[5] * m_dV[6] - m_dV[2] * m_dV[7]) / (m_dV[2] * m_dV[2] + m_dV[5] * m_dV[5]);
	thetav = asin(sv);
	if(m_dV[2] * m_dV[6] + m_dV[5] * m_dV[7] > 1.0e-10){
		if(sv < 0)
			thetav = - thetav - PI;
		else
			thetav = PI - thetav;
	}

	theta = floor((thetau - thetav) / PI + 0.5) * PI;
	assert(fabs(thetau - thetav - theta) < PI);
	dtheta = thetau + thetav - theta;	
	
	thetau = theta + dtheta / 2.0;
	thetav = dtheta / 2.0;
	
	c = cos(thetau);	s = sin(thetau);
	t = m_dU[0]; m_dU[0] = c * t - s * m_dU[3]; m_dU[3] = c * m_dU[3] + s * t;
	t = m_dU[1]; m_dU[1] = c * t - s * m_dU[4]; m_dU[4] = c * m_dU[4] + s * t;
	t = m_dU[2]; m_dU[2] = c * t - s * m_dU[5]; m_dU[5] = c * m_dU[5] + s * t;
	
	c = cos(thetav);	s = sin(thetav);
	t = m_dV[0]; m_dV[0] = c * t - s * m_dV[3]; m_dV[3] = c * m_dV[3] + s * t;
	t = m_dV[1]; m_dV[1] = c * t - s * m_dV[4]; m_dV[4] = c * m_dV[4] + s * t;
	t = m_dV[2]; m_dV[2] = c * t - s * m_dV[5]; m_dV[5] = c * m_dV[5] + s * t;


	logrodrigues(m_dU, m_ldU);
	logrodrigues(m_dV, m_ldV);

	// continue removing common parts
	for(int it = 0; it < 10 && abs(m_ldU[2] + m_ldV[2]) > 1.0e-5; it++){
		
		theta = -(m_ldU[2] + m_ldV[2]) / 2.0;
		c = cos(theta);
		s = sin(theta);
		
		// multiply by common rotation
		t = m_dU[0]; m_dU[0] = c * t + s * m_dU[3]; m_dU[3] = c * m_dU[3] - s * t;
		t = m_dU[1]; m_dU[1] = c * t + s * m_dU[4]; m_dU[4] = c * m_dU[4] - s * t;
		t = m_dU[2]; m_dU[2] = c * t + s * m_dU[5]; m_dU[5] = c * m_dU[5] - s * t;
		
		t = m_dV[0]; m_dV[0] = c * t + s * m_dV[3]; m_dV[3] = c * m_dV[3] - s * t;
		t = m_dV[1]; m_dV[1] = c * t + s * m_dV[4]; m_dV[4] = c * m_dV[4] - s * t;
		t = m_dV[2]; m_dV[2] = c * t + s * m_dV[5]; m_dV[5] = c * m_dV[5] - s * t;
		
		logrodrigues(m_dU, m_ldU);
		logrodrigues(m_dV, m_ldV);
		
	}

	del[0] = m_ldU[0];	del[1] = m_ldU[1];
	del[2] = m_ldV[0];	del[3] = m_ldV[1];
	del[4] = (m_ldU[2] + m_ldV[2]) / 2.0;
	
}


template<typename T> inline void CEssentialGeometry<T>::mult_U_V(const T* u, const T* v, T *uv){

	uv[0] = u[0] * v[0] + u[3] * v[1] + u[6] * v[2];
	uv[1] = u[1] * v[0] + u[4] * v[1] + u[7] * v[2];
	uv[2] = u[2] * v[0] + u[5] * v[1] + u[8] * v[2];

	uv[3] = u[0] * v[3] + u[3] * v[4] + u[6] * v[5];
	uv[4] = u[1] * v[3] + u[4] * v[4] + u[7] * v[5];
	uv[5] = u[2] * v[3] + u[5] * v[4] + u[8] * v[5];

	uv[6] = u[0] * v[6] + u[3] * v[7] + u[6] * v[8];
	uv[7] = u[1] * v[6] + u[4] * v[7] + u[7] * v[8];
	uv[8] = u[2] * v[6] + u[5] * v[7] + u[8] * v[8];

}

template<typename T> inline void CEssentialGeometry<T>::mult_Ut_V(const T* u, const T* v, T *uv){

	uv[0] = u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
	uv[1] = u[3] * v[0] + u[4] * v[1] + u[5] * v[2];
	uv[2] = u[6] * v[0] + u[7] * v[1] + u[8] * v[2];

	uv[3] = u[0] * v[3] + u[1] * v[4] + u[2] * v[5];
	uv[4] = u[3] * v[3] + u[4] * v[4] + u[5] * v[5];
	uv[5] = u[6] * v[3] + u[7] * v[4] + u[8] * v[5];

	uv[6] = u[0] * v[6] + u[1] * v[7] + u[2] * v[8];
	uv[7] = u[3] * v[6] + u[4] * v[7] + u[5] * v[8];
	uv[8] = u[6] * v[6] + u[7] * v[7] + u[8] * v[8];
}

template<typename T> inline void CEssentialGeometry<T>::mult_U_Vt(const T* u, const T* v, T *uv){

	uv[0] = u[0] * v[0] + u[3] * v[3] + u[6] * v[6];
	uv[1] = u[1] * v[0] + u[4] * v[3] + u[7] * v[6];
	uv[2] = u[2] * v[0] + u[5] * v[3] + u[8] * v[6];

	uv[3] = u[0] * v[1] + u[3] * v[4] + u[6] * v[7];
	uv[4] = u[1] * v[1] + u[4] * v[4] + u[7] * v[7];
	uv[5] = u[2] * v[1] + u[5] * v[4] + u[8] * v[7];

	uv[6] = u[0] * v[2] + u[3] * v[5] + u[6] * v[8];
	uv[7] = u[1] * v[2] + u[4] * v[5] + u[7] * v[8];
	uv[8] = u[2] * v[2] + u[5] * v[5] + u[8] * v[8];
}


template<typename T> void CEssentialGeometry<T>::logrodrigues(const T X[9], T lX[3]){

	double trace = X[0] + X[4] + X[8];
	double cs = (trace - 1.0) / 2.0;
	double theta = acos(cs);
	double sn = sqrt(1 - cs * cs);

	if(sn < 1.0e-3){
		if(cs > 0)	// no rotation
			lX[0] = lX[1] = lX[2] = 0.0;
		else{
			lX[0] = sqrt(max(X[0] + 1.0, 0.0) / 2.0);
			lX[1] = sqrt(max(X[4] + 1.0, 0.0) / 2.0);
			lX[2] = sqrt(max(X[8] + 1.0, 0.0) / 2.0);
		}
		return;
	}

	sn *= 2.0;
	lX[0] = theta * (X[5] - X[7]) / sn;
	lX[1] = theta * (X[6] - X[2]) / sn;
	lX[2] = theta * (X[1] - X[3]) / sn;

}


template<typename T> void CEssentialGeometry<T>::exprodrigues(const T X[3], T eX[9]){

	double theta = sqrt(X[0] * X[0] + X[1] * X[1] + X[2] * X[2]);

	memset(eX, 0, sizeof(double) * 9);

	if(theta == 0){ // pure translation!!!
		eX[0] = eX[4] = eX[8] = 1.0;
		return;
	}

	double vx = X[0] / theta;
	double vy = X[1] / theta;
	double vz = X[2] / theta;

	double vx2 = vx * vx;
	double vy2 = vy * vy;
	double vz2 = vz * vz;

	double vyz = vy * vz;
	double vzx = vz * vx;
	double vxy = vx * vy;

	double s = sin(theta);
	double c = 1 - cos(theta);

	eX[0] = 1 - c * (vy2 + vz2);
	eX[1] = c * vxy + s * vz;
	eX[2] = c * vzx - s * vy;

	eX[3] = c * vxy - s * vz;
	eX[4] = 1.0 - c * (vz2 + vx2);
	eX[5] = c * vyz + s * vx;

	eX[6] = c * vzx + s * vy;
	eX[7] = c * vyz - s * vx;
	eX[8] = 1.0 - c * (vx2 + vy2);

}

#endif
