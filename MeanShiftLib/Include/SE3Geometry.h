/****************************************************************

                 SE(3) Geometry Library
	=============================================

Implementation of manifold operations for the Lie Group SE(3)

Implemented by Raghav Subbarao
****************************************************************/

#ifndef _C_SE3_GEOMETRY_H_
#define _C_SE3_GEOMETRY_H_

//include needed libraries
#include <math.h>
#include <assert.h>
#include <memory.h>

#include "Geometry.h"

#ifndef MAX
#define MAX(a,b) ((a) >= (b) ? (a) : (b))
#endif

template<typename T> class CSE3Geometry : public CGeometry<T>{

public:

	/*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/
	/* Class Constructor and Destructor */
	/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/*/


	// Default Constructor
	// scale = unit to be used for measuring distance. Standard value 
	//		   is set to 1.0, but using different values leads to 
	//		   different weights for the rotation and translation part 
	//		   of the trasnformations.
	CSE3Geometry(double scale = 1.0);

	// Virtual Destructor
	virtual ~CSE3Geometry(){};

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	g									|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes the Riemannian inner product of tangents.|//
	//|   It always	returns a double value.					|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* gamma>									|//
	//|   An array of type T containing the tangent.		|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An array of type T containing the tangent.		|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		g(delta, gamma)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline double g(const T *gamma, const T *delta);



	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	expm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Compute manifold exponential. The memory for y	|//
	//|   is assumed to have been allocated already. The	|//
	//|   Rodrigues formula is used to for computation.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An array of type T containing the point x.		|//
	//|														|//
	//|   <const T* delta>									|//
	//|   An array of type T containing the tangent vector.	|//
	//|														|//
	//|   <T* y>											|//
	//|   The result is returned in y.						|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		expm(delta, y)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void expm(const T *x, const T *delta, T *y);
	


	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	logm								|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Compute manifold logarithm at x. The memory for	|//
	//|   delta is assumed to have been allocated already.	|//
	//|   The Rodrigues formula is inverted.				|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <T* x>											|//
	//|   An array of type T containing the point x.		|//
	//|														|//
	//|   <const T* y>										|//
	//|   An array of type T containing the point.			|//
	//|														|//
	//|   <T* delta>										|//
	//|   The tangent is returned in delta.					|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		logm(y, delta)									|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void logm(const T *x, const T *y, T *delta);
	
private:

	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	mult_U_V							|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Perform matrix multiplication of two 4X4 3D rigid |//
	//|	  body transformations.	The matrices are assumed to	|//
	//|	  be column organized. The memory for the output	|//
	//|	  is assumed to have been allocated already.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* u>										|//
	//|   An size 16 array of type T containing u.			|//
	//|														|//
	//|   <const T* v>										|//
	//|   An size 16 array of type T containing v.			|//
	//|														|//
	//|   <T* uv>											|//
	//|   The product is returned in uv.					|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		mult_U_V(u, v, uv)								|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void mult_U_V(const T* u, const T* v, T *uv);


	/*\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\\--/
	//<---------------------------------------------------->|//
	//|														|//
	//|	Method Name:	mult_U_V							|//
	//|   ============										|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Description:										|//
	//|	============										|//
	//|														|//
	//|   Computes the relative transformation between two	|//
	//|	  3D rigid body motions. The matrices are assumed 	|//
	//|	  to be column organized. The memory for the output	|//
	//|	  is assumed to have been allocated already.		|//
	//|														|//
	//|   The following arguments must be provided:			|//
	//|														|//
	//|   <const T* u>										|//
	//|   An size 16 array of type T containing u.			|//
	//|														|//
	//|   <const T* v>										|//
	//|   An size 16 array of type T containing v.			|//
	//|														|//
	//|   <T* uv>											|//
	//|   The difference is returned in uv.					|//
	//|														|//
	//<---------------------------------------------------->|//
	//|														|//
	//|	Usage:												|//
	//|   ======											|//
	//|		mult_Ui_V(u, v, uv)								|//
	//|														|//
	//<---------------------------------------------------->|//
	//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||//--\\||\*/
	inline void mult_Ui_V(const T* u, const T* v, T *uv);
	
	T m_dR[16];			// used to store results of matrix multiplications
	double m_scale;		// the scale value

};


/********************************************************/
//Class Constructor										//
//*******************************************************/
//Post:													//
//      The SE3Geometry class is initialized.			//
/********************************************************/
template<typename T> CSE3Geometry<T>::CSE3Geometry(double scale):
CGeometry<T>(16, 6), m_scale(scale)
{}


/********************************************************/
//Inner Product											//
//******************************************************//
//Computes the Riemannian inner product of tangents.	//
/********************************************************/
//Pre:													//
//      - gamma is a tangent of size 6.					//
//      - delta is a tangent of size 6.					//
//Post:													//
//      - the product is returned as a double value.	//
//*******************************************************/
template<typename T> double CSE3Geometry<T>::g(const T *gamma, const T *delta){

	double d = 0;
	int i;
	for(i = 0; i < 3; i++)
		d += gamma[i] * delta[i];
	for(i = 3; i < 6; i++)
		d += (gamma[i] * delta[i]) / (m_scale * m_scale);
	return d;

}



/********************************************************/
//Manifold Exponential									//
//******************************************************//
//Computes the matrix exponential of a six vector to	//
//get a rigid body transformation in 3D using Rodrigues	//
//formula.												//
/********************************************************/
//Pre:													//
//      - x is a transformation.						//
//      - delta is a tangent of size 6.					//
//Post:													//
//      - the returned transformation y = x * exp(del)	//
//*******************************************************/
template<typename T> void CSE3Geometry<T>::expm(const T* x, const T *del, T *y){

	double theta = sqrt((double)(del[0] * del[0] + del[1] * del[1] + del[2] * del[2]));

	memset(m_dR, 0, sizeof(T) * 16);
	m_dR[15] = 1.0;

	if(theta == 0){ // pure translation!!!

		memcpy(y, x, 16 * sizeof(T));
		y[12] += del[3];
		y[13] += del[4];
		y[14] += del[5];

		return;
	}

	double vx = (double)del[0] / theta;
	double vy = (double)del[1] / theta;
	double vz = (double)del[2] / theta;

	double vx2 = vx * vx;
	double vy2 = vy * vy;
	double vz2 = vz * vz;

	double vyz = vy * vz;
	double vzx = vz * vx;
	double vxy = vx * vy;

	double s = sin(theta);
	double c = 1.0 - cos(theta);

	m_dR[ 0] = (T)(1.0 - c * (vy2 + vz2));
	m_dR[ 1] = (T)(c * vxy + s * vz);
	m_dR[ 2] = (T)(c * vzx - s * vy);

	m_dR[ 4] = (T)(c * vxy - s * vz);
	m_dR[ 5] = (T)(1.0 - c * (vz2 + vx2));
	m_dR[ 6] = (T)(c * vyz + s * vx);

	m_dR[ 8] = (T)(c * vzx + s * vy);
	m_dR[ 9] = (T)(c * vyz - s * vx);
	m_dR[10] = (T)(1.0 - c * (vx2 + vy2));

	s = theta - s;
	double ux = (double)del[3] / theta;
	double uy = (double)del[4] / theta;
	double uz = (double)del[5] / theta;
	double vxyz = vx * ux + vy * uy + vz * uz;

	m_dR[12] = (T)(theta * ux + c * (vy * uz - vz * uy) + s * (vx * vxyz - ux));
	m_dR[13] = (T)(theta * uy + c * (vz * ux - vx * uz) + s * (vy * vxyz - uy));
	m_dR[14] = (T)(theta * uz + c * (vx * uy - vy * ux) + s * (vz * vxyz - uz));

	mult_U_V(x, m_dR, y);
	
}

/********************************************************/
//Manifold Logarithm									//
//******************************************************//
//Computes the matrix logarithm of the relative motion	//
//between two rigid body trnasformations in 3D by		//
//inverting the Rodrigues formula.						//
/********************************************************/
//Pre:													//
//      - x is a transformation.						//
//      - y is a transformation.						//
//Post:													//
//      - the returned tangent is del = log(x^{-1}y)	//
//*******************************************************/
template<typename T> void CSE3Geometry<T>::logm(const T* x, const T *y, T *del){

	mult_Ui_V(x, y, m_dR);

	double trace = (double)(m_dR[0] + m_dR[5] + m_dR[10]);
	double cs = (trace - 1.0) / 2.0;
	double theta = (fabs(cs) > 1.0 ? (cs > 0 ? 0 : 3.14159265358979) : acos(cs));
	double sn = sqrt(MAX(0, 1 - cs * cs));

	if(theta == 0){
		del[0] = del[1] = del[2] = 0.0;
		del[3] = m_dR[12];
		del[4] = m_dR[13];
		del[5] = m_dR[14];
		return;
	}
	
	double vx, vy, vz;

	if(sn > 1.0e-14){
		vx = (x[6] - x[9]) / (2 * sn);
		vy = (x[8] - x[2]) / (2 * sn);
		vz = (x[1] - x[4]) / (2 * sn);
	}
	else{
		vx = sqrt(MAX((x[ 0] + 1) / 2.0, 0.0));
		vy = sqrt(MAX((x[ 5] + 1) / 2.0, 0.0));
		vz = sqrt(MAX((x[10] + 1) / 2.0, 0.0));
		if(x[1] < 0) vy = -vy;
		if(x[2] < 0) vz = -vz;
	}

	del[0] = (T)(theta * vx);
	del[1] = (T)(theta * vy);
	del[2] = (T)(theta * vz);

	double sx = del[1] * m_dR[14] - del[2] * m_dR[13];
	double sy = del[2] * m_dR[12] - del[0] * m_dR[14];
	double sz = del[0] * m_dR[13] - del[1] * m_dR[12];

	double nn = 1.0 / (2.0 * tan(theta / 2.0));
	double nx = (sy * vz - sz * vy) * nn;
	double ny = (sz * vx - sx * vz) * nn;
	double nz = (sx * vy - sy * vx) * nn;

	double ip = vx * m_dR[12] + vy * m_dR[13] + vz * m_dR[14];

	del[3] = (T)(nx - sx / 2.0 + ip * vx);
	del[4] = (T)(ny - sy / 2.0 + ip * vy);
	del[5] = (T)(nz - sz / 2.0 + ip * vz);
	
}


/********************************************************/
//Matrix multiplication									//
//******************************************************//
//Computes the product of two rigid body transformations//
//******************************************************//
//Pre:													//
//      - u is a transformation.						//
//      - v is a transformation.						//
//Post:													//
//      - the returned transformations is uv = u * v	//
//*******************************************************/
template<typename T> inline void CSE3Geometry<T>::mult_U_V(const T* u, const T* v, T *uv){

	uv[ 0] = u[ 0] * v[ 0] + u[ 4] * v[ 1] + u[ 8] * v[ 2];
	uv[ 1] = u[ 1] * v[ 0] + u[ 5] * v[ 1] + u[ 9] * v[ 2];
	uv[ 2] = u[ 2] * v[ 0] + u[ 6] * v[ 1] + u[10] * v[ 2];
	uv[ 3] = 0.0;

	uv[ 4] = u[ 0] * v[ 4] + u[ 4] * v[ 5] + u[ 8] * v[ 6];
	uv[ 5] = u[ 1] * v[ 4] + u[ 5] * v[ 5] + u[ 9] * v[ 6];
	uv[ 6] = u[ 2] * v[ 4] + u[ 6] * v[ 5] + u[10] * v[ 6];
	uv[ 7] = 0.0;

	uv[ 8] = u[ 0] * v[ 8] + u[ 4] * v[ 9] + u[ 8] * v[10];
	uv[ 9] = u[ 1] * v[ 8] + u[ 5] * v[ 9] + u[ 9] * v[10];
	uv[10] = u[ 2] * v[ 8] + u[ 6] * v[ 9] + u[10] * v[10];
	uv[11] = 0.0;

	uv[12] = u[ 0] * v[12] + u[ 4] * v[13] + u[ 8] * v[14] + u[12];
	uv[13] = u[ 1] * v[12] + u[ 5] * v[13] + u[ 9] * v[14] + u[13];
	uv[14] = u[ 2] * v[12] + u[ 6] * v[13] + u[10] * v[14] + u[14];
	uv[15] = 1.0;

}

/********************************************************/
//Matrix multiplication									//
//******************************************************//
//Computes the relative transformation between two		//
//rigid body transformations							//
//******************************************************//
//Pre:													//
//      - u is a transformation.						//
//      - v is a transformation.						//
//Post:													//
//      - the returned transformations is uv = u^{-1}*v	//
//*******************************************************/
template<typename T> inline void CSE3Geometry<T>::mult_Ui_V(const T* u, const T* v, T *uv){

	uv[ 0] = u[ 0] * v[ 0] + u[ 1] * v[ 1] + u[ 2] * v[ 2];
	uv[ 1] = u[ 4] * v[ 0] + u[ 5] * v[ 1] + u[ 6] * v[ 2];
	uv[ 2] = u[ 8] * v[ 0] + u[ 9] * v[ 1] + u[10] * v[ 2];
	uv[ 3] = 0.0;

	uv[ 4] = u[ 0] * v[ 4] + u[ 1] * v[ 5] + u[ 2] * v[ 6];
	uv[ 5] = u[ 4] * v[ 4] + u[ 5] * v[ 5] + u[ 6] * v[ 6];
	uv[ 6] = u[ 8] * v[ 4] + u[ 9] * v[ 5] + u[10] * v[ 6];
	uv[ 7] = 0.0;

	uv[ 8] = u[ 0] * v[ 8] + u[ 1] * v[ 9] + u[ 2] * v[10];
	uv[ 9] = u[ 4] * v[ 8] + u[ 5] * v[ 9] + u[ 6] * v[10];
	uv[10] = u[ 8] * v[ 8] + u[ 9] * v[ 9] + u[10] * v[10];
	uv[11] = 0.0;

	T tx = v[12] - u[12], ty = v[13] - u[13], tz = v[14] - u[14];
	uv[12] = u[ 0] * tx + u[ 1] * ty + u[ 2] * tz;
	uv[13] = u[ 4] * tx + u[ 5] * ty + u[ 6] * tz;
	uv[14] = u[ 8] * tx + u[ 9] * ty + u[10] * tz;
	uv[15] = 1.0;

}

#endif
